<!doctype html>
<html>
<head>
<?php require'./main_head.php'; ?>
</head>

<body>
<div id="wrapper">

<!-- Top Start -->
<?php require './main_top.php'; ?>
<!-- Top End -->

<!-- Menu Start -->
<?php require './main_menu.php'; ?>
<!-- Menu End --> 

<!-- Contents Start -->
<?php 
///201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
			case 'Viewer':
			case 'Serial':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_GET['menu'])){
	$menu = preg_replace("/<|\/|_|>/","", $_GET['menu']);
	}else{
		$menu = 'default';
	}
	switch($menu){
		case 'expiredApps':
			$target ='./issuer/issuer_expiredApps.php';
			break;
		case 'oldApps':
			$target ='./issuer/issuer_oldApps.php';
			break;
		case 'search':
			$target = './issuer/issuer_search.php';
			break;
		case 'searchDetail':
			$target = './issuer/issuer_searchDetail.php';
			break;
		case 'staffId':
			$target = './manager/manager_staffId.php';
			break;
		case 'accountSerial':
			$target = './issuer/issuer_accountSerial.php';
			break;
		case 'accountSerialM':
			if($user_type === 'Manager')
			{
				$target = './manager/manager_accountSerial.php';
			}
			else
			{
				$target = './issuer/issuer_search.php';
			}
			break;
		default :
			if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing ==='yes' && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
				$target = './issuer/issuer_issuingOne.php';
			}else{
				$target = './issuer/issuer_search.php';
			}
			break;
	}
	if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing ==='yes' && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		switch($menu){
			case 'issuingOne':
				$target = './issuer/issuer_issuingOne.php';
				break;
			case 'appFileUpload':
				$target ='./issuer/issuer_appFileUpload.php';
				break;
			case 'issued':
				$target = './issuer/issuer_issued.php';
				break;
			case 'appFromIo':
				$target = './issuer/issuer_appFromIo.php';
				break;
			case 'bulkList':
				$target = './issuer/issuer_bulkList.php';
				break;
			default:
				break;
		}
	}else{}
	if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports ==='yes' && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
		switch($menu){
			case 'daily':
				$target = './issuer/issuer_daily.php';
				break;
			case 'dailyAll':
				$target = './issuer/issuer_dailyAll.php';
				break;
			case 'monthly':
				$target = './issuer/issuer_monthly.php';
				break;	
			case 'saveReport':
				$target ='./issuer/issuer_saveReport.php';
				break;				
			case 'backup':
				$target = './manager/manager_backup.php';
				break;
			default:
				break;
		}
	}else{}
	if(isset($resultUserInfo->sMenuSerial) && $resultUserInfo->sMenuSerial ==='yes' && isset($resultOfficeInfo->menuSerial) && $resultOfficeInfo->menuSerial ==='yes' ){
		switch($menu){
			case 'cardSerial':
				$target = './manager/manager_cardSerial.php';
				break;
			case 'cardSerialNew':
				$target = './manager/manager_cardSerialNew.php';
				break;
			case 'cardSerialDetail':
				$target = './manager/manager_cardSerialDetail.php';
				break;
			case 'checkSerial':
				$target = './manager/manager_checkSerial.php';
				break;
			case 'checkSerialDetail':
				$target = './manager/manager_checkSerialDetail.php';
				break;
			default:
				break;
		}
	}else{}
	if(isset($resultUserInfo->sMenuIo) && $resultUserInfo->sMenuIo ==='yes' && isset($resultOfficeInfo->menuIo) && $resultOfficeInfo->menuIo ==='yes' ){
		switch($menu){
			case 'issuingOffice':
				$target = './manager/manager_issuingOffice.php';
				break;
			case 'issuingOfficeDetail':
				$target = './manager/manager_issuingOfficeDetail.php';
				break;
			case 'issuingOfficeNew':
				$target = './manager/manager_issuingOfficeNew.php';
				break;
			case 'staffNew':
				$target = './manager/manager_staffNew.php';
				break;
			case 'staffDetail':
				$target = './manager/manager_staffDetail.php';
				break;	
			case 'dwnLog':
				$target = './manager/manager_dwnLog.php';
				break;				
			default:
				break;
		}
	}else{}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication ==='yes' && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
		switch($menu){
			case 'printTest':
				$target = './issuer/issuer_printTest.php';
				break;
			case 'promoList':
				$target ='./issuer/issuer_promoList.php';
				break;
			case 'promoNew':
				$target ='./issuer/issuer_promoNew.php';
				break;
			case 'promoDetail':
				$target ='./issuer/issuer_promoDetail.php';
				break;
			case 'appUrlNew':
				$target = './issuer/issuer_appUrlNew.php';
				break;
			case 'appUrlList':
				$target = './issuer/issuer_appUrlList.php';
				break;
			case 'appUrlDetail':
				$target = './issuer/issuer_appUrlDetail.php';
				break;
			case 'promoList':
				$target = './issuer/issuer_promoList.php';
				break;
			case 'promoNew':
				$target = './issuer/issuer_promoNew.php';
				break;
			case 'promoDetail':
				$target = './issuer/issuer_promoDetail.php';
				break;		
			case 'designEditor1':
				$target = './manager/manager_designEditor1.php';
				break;
			case 'designEditor2':
				$target = './manager/manager_designEditor2.php';
				break;						
			case 'designEditorNew':
				$target = './manager/manager_designEditorNew.php';
				break;						
			case 'designList':
				$target = './manager/manager_designList.php';
				break;			
			case 'designList2':
				$target = './manager/manager_designList2.php';
				break;					
			default:
				break;
		}
	}else{}
	if (isset($_SESSION['warning1']) && $_SESSION['warning1'] ==='YES'){
		$target = './manager/manager_staffId.php';
	}else{}
	require $target;
?>
<!-- Contents End -->
  
<!-- Footer Start -->
<?php require './main_foot.php'; ?>
<!-- Footer End -->
    
</div>
</body>
</html>
