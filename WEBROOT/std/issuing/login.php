<!doctype html>
<html>
<head>
					  
																	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ISIC issuing management system</title>
<link href="./css/bt.css" rel="stylesheet" type="text/css">
<link href="./css/yoonCustom.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./login.js"></script>
<link href="../css/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
	<style type="text/css">
					 
										  
								
		body {background:#777 !important; padding:120px 10px; color:#666; font-size:13px; max-width: 1280px; margin: 0 auto;}
		#wrap {padding:0; margin: 0;}
		#contents {padding: 0; margin: 0;}
    
		h1 {font-size: 40px; text-align: center; margin: 0; padding: 90px 0 60px 0; color: #478f8c; letter-spacing: 0.25em;}
    h1 small {color: #999; font-size: 65%;}
    h2 {font-size: 25px; color: #fff; font-weight: bold; text-align: center; margin-bottom: 40px;}
    
    .dpTable {display: table;}
    .dpTableCell {display: table-cell; vertical-align: middle;}
    
    .textShadow {text-shadow: 3px 3px 10px #222;}
    .boxShadow {box-shadow: 3px 3px 10px #555;}
    .btn-login {background: #478f8c; border: 1px solid #478f8c; color: #fff !important;}
		  
    .inherit {color: inherit!important; text-decoration: none!important;}
    
    #footer {background: #222; color: #fff !important;}
	</style>
<?php 
///201114 check			
	session_start();
	$field_id = 'text';
	$field_pwd = 'password';
	$button_login = 'button';
	$button_logout ='hidden';
?>
</head>

<body>
<div id="wrap">
<div id="contents">
    
<form id="form01" name="form01" enctype="multipart/form-data" action="" method="post">
<?php
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
	$loginStatus = 'no';
	if (isset($_SESSION['valid_user']))
	{
		$user = $_SESSION['valid_user'];
		$user_type = $_SESSION['user_type'];
		/*
		$field_id = 'hidden';
		$field_pwd = "hidden";
		$button_login = 'hidden';
		$button_signup ='hidden';
		$button_logout ='button';
		*/
		echo "<span style=\"color:#ffffff;\">You are logged in as : $user $user_type </span>"; 
		switch($user_type)
		{
			case 'Manager':
			case 'Issuer':
				//echo "<a href=\"./main_content.php\"><span style=\"color:#ffffff;\">[Main Page]</span></a>";
				$loginStatus = 'yes';
				break;
			default:
				exit;
		}
	}
	  
  
	else
	{}
	$now = date("Y-M-d H:i:s");
	echo "<span style=\"color:#fffffff; display:none;\"> $now </span><br />"; 	
	?>
	<h1><strong>ISIC</strong> <?php echo $countryName[$countryId]?></h1>
	<div style="background: url('./images/loginBG.jpg') center bottom no-repeat; background-size: cover;">
		<div class="dpTable w100" style="min-height: 600px;">
			<div class="dpTableCell">
				<h2 class="textShadow"><img src="./images/loginCard.png" height="60" class="mr-15">ISSUING SYSTEM</h2>
				<table style="width: 40%; margin: 0 auto;">
					<tr>
						<?php 
						if($loginStatus === 'yes')
						{
						?>
						<td class="p-5">
							<a href="./main_content.php" class="btn btn-block btn-lg btn-kyp btn-login boxShadow">Enter</a>
						</td>
						<td class="p-5" width="30%">
							<input type="button" class="btn btn-block btn-lg btn-kyp btn-login boxShadow" id="button_logout" value="Log out" onclick="submit1(3)"/>
						</td>

						<?php
						}
						else
						{
						?>
						<td class="p-5">
						<input type="<?php echo $field_id; ?>" class="form-control input-lg boxShadow" id ="id" name="id" placeholder="ID" value = ""/>
						</td>
						<td class="p-5">
						<input type="<?php echo $field_pwd; ?>" class="form-control input-lg boxShadow" id ="passwd" name="passwd" placeholder="PASSWORD" value = "" />
						</td>
						<td class="p-5" width="30%">
						<input type="button" class="btn btn-block btn-lg btn-kyp btn-login boxShadow" id="button_login" value="Login"  onclick="submit1(1)"/>
						</td>
						<?php 
						}
						?>
						<input type="hidden" name="status" value="login">
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>
</div>
</div>
  

<div id="footer" class="p-20">
	KYPL platform © Arete Consulting GmbH
</div>
</body>
</html>
