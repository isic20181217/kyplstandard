<?php 
///200927 check		
?>
<?php 
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']) &&isset($_SESSION['id']) && isset($_SESSION['idNo'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
			case 'Viewer':
				try {
					$now1 = time();
					$today1 = date("d \/ m \/ Y",$now1); 
					$today2 = date("d \/ M \/ Y",$now1); 
					$goParent ='/..';
					$goParent2 ='/../..';
					$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
					require __DIR__.$goParent.'/req.php';
					require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
					require __DIR__.$goParent.$reqDir1.'/_require1/encDec.php';	
					require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
					$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
					$stmt = $db->prepare($query);
					$stmt->bindParam(':id', $_SESSION['id']);
					$stmt->bindParam(':no', $_SESSION['idNo']);
					$stmt->execute();
					if($stmt->rowCount() === 1 ){
						$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
					}
					else {
						
						if(isset($_SESSION)){
							session_destroy();
						}else{}
						//print_r($stmt->errorInfo());
						echo '<script>alert(\'Please Login 99\');</script>';
						echo '<script>location.replace("/std/issuing/login.php");</script>';
						exit;
					}
					$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
					$stmt = $db->prepare($query);
					$stmt->bindParam(':officeId', $_SESSION['officeId']);
					$stmt->execute();
					if($stmt->rowCount() === 1 ){
						$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
					}
					else {
						
						if(isset($_SESSION)){
							session_destroy();
						}else{}
						//print_r($stmt->errorInfo());
						echo '<script>alert(\'Please Login 99\');</script>';
						echo '<script>location.replace("/std/issuing/login.php");</script>';
						exit;
					}	
					$db= NULL;
				}
				catch (PDOExeception $e){
					//echo "Error: ".$e->getMessage();
					if(isset($_SESSION)){
						session_destroy();
					}else{}
					$db= NULL;
					echo '<script>alert(\'Please Login 999\');</script>';
					echo '<script>location.replace("/std/index.php");</script>';
					exit;
				}
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?>
<?php 
?>
<!-- top start -->
<div id="top">
	<table id="top1" class="w100" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="175"><img src="images/loginCard.png" id="topIcons" /></td>
			<td id="topTitle">
				<p class="m0"><strong>Issuing system</strong></p>
				<p id="country"><strong><?php echo $countryName[$countryId]?></strong></p>
			</td>
			<td class="text-right">
				<div id="logoutField" class="logout">
				<?php if ($_SESSION['user_type'] === 'Manager'){ ?>
					<i class="fa fa-user"></i><a href="./main_content.php?menu=staffId"> <?php echo $_SESSION['officeName'];?> - <?php echo '['.$_SESSION['user_type'].']'.' '.$_SESSION['valid_user']; ?></a> <br><?php echo $today2; ?> ｜ <a href="./work/logout.php" class="btn btn-xs btn-kyp2 logoutBt">Log out</a>
				<?php } else if ($_SESSION['user_type'] === 'Issuer'){ ?>
					<i class="fa fa-user"></i><a href="./main_content.php?menu=staffId"> <?php echo $_SESSION['officeName'];?> - <?php echo '['.$_SESSION['user_type'].']'.' '.$_SESSION['valid_user']; ?> <i class="fa fa-caret-right"></i></a><br><?php echo $today2; ?> ｜ <a href="./work/logout.php" class="btn btn-xs btn-kyp2 logoutBt">Log out</a>
				<?php } else{}?>
				</div>
			</td>
		</tr>
	</table>
</div>
<!-- top end -->