<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>addOffice</title>
<?php
// 200604 checked
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (isset($_POST['formName'])){
		$formName =preg_replace("/<|\/|_|>/","", $_POST['formName']);
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['officeDesc']) && !empty($_POST['officeDesc'])){
		$officeDesc= preg_replace("/<|\/|_|>/","", $_POST['officeDesc']); 
	}else{
		echo '<script>alert(\'Not Enough Values(Office Desc)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['name']) && !empty($_POST['name'])){
		$name= preg_replace("/<|\/|_|>/","", $_POST['name']); 
	}else{
		echo '<script>alert(\'Not Enough Values(Office Name)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['address']) && !empty($_POST['address'])){
		$address= preg_replace("/<|\/|_|>/","", $_POST['address']); 
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(Address)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['city']) && !empty($_POST['city'])){
		$city= preg_replace("/<|\/|_|>/","", $_POST['city']); 
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(City)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
		$zipcode= preg_replace("/<|\/|_|>/","", $_POST['zipcode']); 
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(Zipcode)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['tel']) && !empty($_POST['tel'])){
		$tel= preg_replace("/<|\/|_|>/","", $_POST['tel']); 
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(Tel)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['contact']) && !empty($_POST['contact'])){
		$contact= preg_replace("/<|\/|_|>/","", $_POST['contact']); 
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(contact)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['email']) && !empty($_POST['email'])){
		$email= preg_replace("/<|\/|_|>/","", $_POST['email']);
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(Email)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['workHour']) && !empty($_POST['workHour'])){
		$workHour= preg_replace("/<|\/|_|>/","", $_POST['workHour']);
	}else{
		/*
		echo '<script>alert(\'Not Enough Values(WorkHour)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	$role = 'Manager';
	$today = date("Y-m-d H:i:s");
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$db->beginTransaction();		
		$query = "INSERT INTO $tablename12 (modifiedDate, modifiedBy";
		$query .= ", activeOffice";
		$query .= ", name ";
		$query .= ", officeDesc ";
		$query .= ", canUrl ";
		$query .= ", canCsv ";
		$query .= ", canValidate ";
		$query .= ", canIssue ";
		$query .= ", canPrint ";
		$query .= ", canPrintLabel ";
		$query .= ", menuIssuing ";
		$query .= ", menuReports ";
		$query .= ", menuSerial ";
		$query .= ", menuIo ";
		$query .= ", menuApplication ";
		$query .= ", address ";
		$query .= ", city ";
		$query .= ", zipcode ";
		$query .= ", tel";
		$query .= ", contact ";
		$query .= ", email ";
		$query .= ", workHour ";
		$query .=")";
		$query .= " VALUES (";
		$query .= ":modifiedDate";
		$query .= ", :modifiedBy ";
		$query .= ", :activeOffice";
		$query .= ", :name ";
		$query .= ", :officeDesc ";
		$query .= ", :canUrl ";
		$query .= ", :canCsv ";
		$query .= ", :canValidate ";
		$query .= ", :canIssue ";
		$query .= ", :canPrint ";
		$query .= ", :canPrintLabel ";
		$query .= ", :menuIssuing ";
		$query .= ", :menuReports ";
		$query .= ", :menuSerial ";
		$query .= ", :menuIo ";
		$query .= ", :menuApplication ";
		$query .= ", :address ";
		$query .= ", :city ";
		$query .= ", :zipcode ";
		$query .= ", :tel";
		$query .= ", :contact ";
		$query .= ", :email ";
		$query .= ", :workHour ";
		$query .=")";
		echo $query;
		$activeOffice ='yes';
		$canUrl ='no';
		$canCsv ='no';
		$canValidate ='no';
		$canIssue ='no';
		$canPrint ='no';
		$canPrintLabel ='no';
		$menuIssuing ='yes';
		$menuReports ='yes';
		$menuSerial ='no';
		$menuIo ='no';
		$menuApplication = 'no';
		$stmt = $db->prepare($query);
		$stmt->bindParam(':modifiedDate', $today);
		$stmt->bindParam(':modifiedBy', $_SESSION['valid_user']);
		$stmt->bindParam(':activeOffice', $activeOffice);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':officeDesc', $officeDesc);
		$stmt->bindParam(':address', $address);
		$stmt->bindParam(':city', $city);
		$stmt->bindParam(':zipcode', $zipcode);
		$stmt->bindParam(':contact', $contact);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':workHour', $workHour);
		$stmt->bindParam(':tel', $tel);
		$stmt->bindParam(':canUrl', $canUrl);
		$stmt->bindParam(':canCsv', $canCsv);
		$stmt->bindParam(':canValidate', $canValidate);
		$stmt->bindParam(':canIssue', $canIssue);
		$stmt->bindParam(':canPrint', $canPrint);
		$stmt->bindParam(':canPrintLabel', $canPrintLabel);
		$stmt->bindParam(':menuIssuing', $menuIssuing);
		$stmt->bindParam(':menuReports', $menuReports);
		$stmt->bindParam(':menuSerial', $menuSerial);
		$stmt->bindParam(':menuIo', $menuIo);
		$stmt->bindParam(':menuApplication', $menuApplication);
		if($stmt->execute()){
			$officeId = $db->lastInsertId();
		}else {
			//print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'Error2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$target ='../main_content.php?menu=issuingOffice';
		$db->commit();
		$db= NULL;
		echo '<script>location.replace("'.$target.'");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	<input type="submit">
	</form> 
</body>
</html>