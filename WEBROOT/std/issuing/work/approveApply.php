<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>aprroveapply</title>
<?php
// 200611 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (empty($_GET['appNo']) || empty($_GET['cardType']) ){
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	else{}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	$appNo = preg_replace("/<|\/|_|>/","", $_GET['appNo']);
	$status ='approved';
	$cardType = preg_replace("/<|\/|_|>/","", $_GET['cardType']);
	$modifiedBy = 'approve'.$_SESSION['valid_user'];
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sCanValidate) && $resultUserInfo->sCanValidate === 'yes'  && isset($resultOfficeInfo->canValidate) && $resultOfficeInfo->canValidate ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied2\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}		
		$db->beginTransaction();
		$query0 = "UPDATE $tablename07 SET"; //approve 
		$query0 .= " status=:inputValue1, approvedDate=now(), modifiedDate=now(), modifiedBy=:inputValue8";
		$query0 .= " WHERE appNo=:inputValue100";
		$stmt2 = $db->prepare($query0);
		$stmt2->bindParam(':inputValue1', $status);;
		$stmt2->bindParam(':inputValue8', $modifiedBy);
		$stmt2->bindParam(':inputValue100', $appNo);
		if($stmt2->execute()){
			// changed 180919
			$db->commit();
			$db= NULL;
			echo "<script> window.history.go(-1); </script>";
			// changed 180919
		}
		else {
			//print_r($stmt2->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'modify ERROR1\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}	
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'DB CONNECT ERROR1\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
?>
<script>
	window.onload = function(){
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php //echo $target ?>" method="post"> 
	</form> 
</body>
</html>