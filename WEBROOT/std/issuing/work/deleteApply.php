<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>deleteapply</title>
<?php 
	/*
	echo "<pre>";
	print_R($_POST);
	print_R($_FILES);
	echo "</pre>";
	exit;
	*/
?>

<?php
// 200730 fix typo change db table name 07 to 11 (in serial delete)
// 200728 fix typo $_GET['appNo'] to $_GET["appNo'];
// 200611 check					   
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (empty($_POST['appNo'])){
		echo '<script>alert(\'forbidden1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	else{}
	if (isset($_POST['formName']) && $_POST['formName'] === 'searchDetail'){
		$formName =$_POST['formName'];
	}	
	else{
		echo '<script>alert(\'forbidden2\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if ($_SESSION['officeId'] === '1'){
	}	
	else{
		echo '<script>alert(\'You are not Main Office Issuer.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	$appNo = preg_replace("/<|\/|_|>/","", $_POST['appNo']);

	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$db->beginTransaction();
		$query = "SELECT * FROM $tablename07 WHERE appNo = :searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		$stmt->execute();
		if($stmt->rowCount() == 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else{
			$db->rollBack();
			$db= NULL;
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'DB ERROR1\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		if($result->photoName){
			$oldFileName = pathinfo($result->photoName);
			$path = ' ';
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$result->photoName;
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$result->photoName);
				echo ' delete photo ori';
			}else{}	
			$path = ' ';			
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$oldFileName['filename'].'.old';
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$oldFileName['filename'].'.old');
				echo ' delete photo old ';
			}else{}	
		}else{}
		if($result->proof1Name){
			$path = ' ';	
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$result->proof1Name;
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$result->proof1Name);
				echo ' delete proof1 ';
			}else{}	
		}else{}
		if($result->proof2Name){
			$path = ' ';	
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$result->proof2Name;
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$result->proof2Name);	
				echo ' delete proof2 ';
			}else{}	
		}else{}
		if($result->proof3Name){
			$path = ' ';	
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$result->proof3Name;
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$result->proof3Name);	
				echo ' delete proof3 ';
			}else{}	
		}else{}
		if($result->proof4Name){
			$path = ' ';	
			$path = __DIR__.$goParent2.$reqDir1.$storageDir.$result->proof4Name;
			$isFileExists = file_exists($path);
			if($isFileExists){
				unlink(__DIR__.$goParent2.$reqDir1.$storageDir.$result->proof4Name);	
				echo ' delete proof4 ';
			}else{}	
		}else{}
		$query = "DELETE FROM $tablename07 WHERE appNo= :searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if($stmt->execute()){
		}
		else {
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'DB ERROR2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$query = "DELETE FROM $tablename18 WHERE cusAppNo= :searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if($stmt->execute()){
		}
		else {
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'DB ERROR3\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}	
		$query = "DELETE FROM $tablename20 WHERE revaAppNo= :searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if($stmt->execute()){
		}
		else {
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'DB ERROR4\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		if($result->cardSerialNum === 'NoNeed'  || $result->cardSerialNum == NULL){
			$db->commit();
			$db= NULL;
			echo '<script>alert(\'Delete Complete\');</script>';
			echo "<script> window.history.go(-2); </script>";
		}else{
			$querySerial = "SELECT serId,serialNum FROM $tablename11 WHERE serialNum = :searchValue1";
			$stmtSerial = $db->prepare($querySerial);
			$stmtSerial->bindParam(':searchValue1', $result->cardSerialNum);
			$stmtSerial->execute();
			if($stmtSerial->rowCount() > 0){
				$query2 = "UPDATE $tablename11 SET"; //remove appNo from serialNumber
				$query2 .= " serAppNo = NULL, returned = 'yes'";
				$query2 .= " WHERE serAppNo = :searchValue1";
				$stmt2 = $db->prepare($query2);
				$stmt2->bindParam(':searchValue1', $appNo);
				$stmt2->execute();
				if($stmt2->rowCount() === 1){
					$db->commit();
					$db= NULL;
					echo '<script>alert(\'Delete Complete\');</script>';
					echo "<script> window.history.go(-2); </script>";
				}
				else {
					//print_r($stmt2->errorInfo());
					$db->rollBack();
					$db= NULL;
					echo '<script>alert(\'DB ERROR5\');</script>';
					echo "<script> window.history.go(-1); </script>";
					exit;
				}
			}else{
				$db->commit();
				$db= NULL;
				echo '<script>alert(\'Delete Complete\');</script>';
				echo "<script> window.history.go(-2); </script>";
			}
		}		
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'apply modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php //echo $target ?>" method="post"> 
	</form> 
</body>
</html>