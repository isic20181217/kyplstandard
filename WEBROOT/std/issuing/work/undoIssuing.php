<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Undo Issuing</title>
<link href="../css/bt.css" rel="stylesheet" type="text/css">
<link href="../css/yoonCustom.css" rel="stylesheet" type="text/css">
<link href="../css/yoonCustom_menu.css" rel="stylesheet" type="text/css">
<?php 
// 200615 check

	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['appNo'])){
		$appNo = preg_replace("/<|\/|_|>/","",$_GET['appNo']);
	}else{
		echo 'Access Denied1';
		exit;
	}
?>
	<script language="JavaScript">
		function submit01(index){
			if(index === 1){
				if(document.getElementById('appNo1').value === document.getElementById('appNo2').value){
					document.undoIssuing.action='./undoIssuingWork.php';
					document.undoIssuing.submit();
				}else{
					alert('Application Number error');
				}
			}else{}
		}
		function close01(index){
			self.close();
		}
	</script>
	</head>
	<body>
	<form name="undoIssuing" method="POST">
		<table>
			<tr>
				<th class="text-center thGrey">
					Application Number
				</th>
				<td class="w30p">
					<input type="text" readonly id="appNo1" name="appNo1" value="<?php echo $appNo?>">
				</td>
			</tr>
			<tr>
				<th class="text-center thGrey">
					Application Number Confirm
				</th>
				<td class="w30p">
					<input type="text" id="appNo2" name="appNo2" value="">
				</td>
			</tr>
		</table>

		<table>
			<tr>
				<td width="530" style="text-align:center;">
					<button type="button" class="btn btn-lg btn-kyp" onclick="submit01(1);">Undo Issuing</button>
					<button type="button" class="btn btn-lg btn-kyp" onclick="close01(1);">Close</button>
				</td>
			</tr>
		</table>
	</form>
	</body>
</html>

