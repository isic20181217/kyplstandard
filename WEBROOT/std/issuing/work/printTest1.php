<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ISIC/ITIC/IYTC Print Test</title>
<link href="../css/bt.css" rel="stylesheet" type="text/css">
<link href="../css/yoonCustom.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./printTest1.js"></script>
<?php 
// 200615 check
?>
<?php
	/*
	echo '<pre>';
	print_r($_GET);
	print_r($_POST);
	echo '</pre>';
	exit;
	*/
?>
<?php

$width1 = 540;
$height1 = 331;
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['currentDesign']) && isset($_GET['printerType']) && isset($_GET['showBackImg'])){
		$currentDesign= preg_replace("/<|\/|>/"," ",$_GET['currentDesign']);
		$printerType= preg_replace("/<|\/|>/"," ",$_GET['printerType']);
		$showBackImg= preg_replace("/<|\/|>/"," ",$_GET['showBackImg']);
	}else{
		echo 'Access Denied1';
		exit;
	}
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent2.'/req.php';
		require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		/*
		$query = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId AND activeApp = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingId', $result->appSettingIdApp);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);

			//$result = (object) array();
			$result->appSetting = $appSettingInfo;

			echo '<pre>';
			print_r($result);
			echo '</pre>';
		
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		*/
		$result = (object) array();
		$query = "SELECT * FROM $tablename30 WHERE designId = :designId AND activeDesign = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':designId', $currentDesign);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$designInfo = $stmt->fetch(PDO::FETCH_OBJ);

			//$result = (object) array();
			$result->cardDesign = $designInfo;
/*
			echo '<pre>';
			print_r($result);
			echo '</pre>';
*/			
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		$result->cardDesign->validityStart = $defValidityStart; //from setting.php
		$result->cardDesign->validityEnd = $defValidityEnd; //from setting.php
		if ($result->cardDesign->validityStart === 'validityStartFix'){
			$result->validityStart  = $validityStartFix;
		}else if ($result->cardDesign->validityStart === 'validityStart1'){
			$result->validityStart  = $validityStart1;
		}else{
			$result->validityStart  = $result->cardDesign->validityStart11;
		}
		if ($result->cardDesign->validityEnd === 'validityEndFix'){
			$result->validityEnd = $validityEndFix;
		}else if ($result->cardDesign->validityStart === 'validityStart1'){
			$result->validityEnd = $validityEnd1;
		}else{
			$result->validityEnd = $result->cardDesign->validityEnd11;
		}
		if (isset($result->photoName)){
				$photoName = pathinfo($result->photoName);
		}else{
			$photoName = 'nophoto';
		}
		$cardDesignDetail = '';
		$appDetail = '';
		for($element=0;$element<$result->cardDesign->total;$element++){
			$info = 'info'.(string)($element+1);
			$content = 'content'.(string)($element+1);
			$cardDesignDetail .= $result->cardDesign->$info;
			$cardDesignDetail .= '/iEnd/';
			$cardDesignDetail .= $result->cardDesign->$content;
			$cardDesignDetail .= '/cEnd/';
			if(substr($result->cardDesign->$content,0,6) ==='birth_'){
				$birthDateType = substr($result->cardDesign->$content,6);
				$birthDateType = explode('_',$birthDateType);
				for($birthCount=0;$birthCount<count($birthDateType);$birthCount++){
					if($birthCount != 0){
						$appDetail .= '/';
					}else{}
					switch($birthDateType[$birthCount]){
						case 'DD':
							$day = '01';
							$appDetail .= $day;
							break;
						case 'MM':
							$month = '10';
							if(strlen($month) < 2){
								$month = '0'.$month;
							}else{}
							$appDetail .= $month;
							break;
						case 'YY':
							$year = '19';
							$appDetail .= $year;
							break;
						case 'YYYY':
							$year = '2019';
							$appDetail .= $year;
							break;
						default:
							$appDetail .= 'ERROR';
							break;
					}
					
				}
			}else if(substr($result->cardDesign->$content,0,7) ==='engName'){
				if($nameStyle1 === 'FL'){
					$appDetail .= 'Firstname Lastname';
				}else if($nameStyle1 === 'LF'){
					$appDetail .= 'Lastname Firstname';
				}else{
					echo 'ERROR';
				}						
			}else if(substr($result->cardDesign->$content,0,9) ==='serialNum'){
				$cardSerialNum ='A 123 456 789 012 B';
				$appDetail .= $cardSerialNum;
			}else if(substr($result->cardDesign->$content,0,9) ==='validity_'){
				$validityType = substr($result->cardDesign->$content,9);
				switch($validityType){
					case 'END_MMYY':
						$appDetail .= substr($result->validityEnd,0,2).'/'.substr($result->validityEnd,5,2);
						break;
					case 'STARTEND_MMYYYY':
						$appDetail .= $result->validityStart.' ~ '.$result->validityEnd;
						break;
					default:
						$appDetail .= 'ERROR';
						break;
				}
			}else if(substr($result->cardDesign->$content,0,5) === 'logo_'){
				$appDetail .= $cardDesignDir.$designInfo->designId.'/'.substr($result->cardDesign->$content,5);
			}else if(substr($result->cardDesign->$content,0,5) === 'fill_'){
				$appDetail .= $result->cardDesign->$content;
			}else{
				switch($result->cardDesign->$content){
					case 'engFname':
						$contentOne = 'Firstname';
						break;
					case 'engLname':
						$contentOne = 'Lastname';
						break;
					case 'name1':
						$contentOne = ' ';
						break;
					case 'birthYear':
						$contentOne = '2019';
						break;
					case 'birthMonth':
						$contentOne = '01';
						break;
					case 'birthDay':
						$contentOne = '01';
						break;
					case 'school':
						//$contentOne = $result->cardDesign->$content;
						//$contentOne = decrypt1($result->$contentOne, $result->encId);	
						$contentOne = 'school Name';
						break;
					case 'photoName':
						//$contentOne = $result->cardDesign->$content;
						//$contentOne = $result->$contentOne;
						if(isset($_GET['img1']) && $_GET['img1'] === 'blueTest'){
							$file_path = '../images/sample_blueTest.jpg';
						}else{
							$file_path = '../images/sample_blue.jpg';
						}
						//$fp = fopen($file_path, 'r'); 
						//$arr = fread($fp, filesize($file_path)); 
						//fclose($fp); 
						$contentOne = $file_path;
						break;
					default:
						$contentOne = $result->cardDesign->$content;
						//$contentOne = $result->$contentOne;
				}
				$appDetail .= $contentOne;
			}
			$appDetail .= '/aEnd/';
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}
?> 
	<style type="text/css" media="print">
		@page {
			/*size:landscape;*/
			margin-top:0;
			margin-right:0;
			margin-bottom:0;
			margin-left:0;
		}
		table {
			color:black;
			font-family:arial;
			text-align:left;
			border: 0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		td {
			vertical-align:top;
			border:0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		body {
		}
		.hideinprint {
			display:none;
		}
	</style>
	<style type="text/css" media="screen">
		table {
			color:black;
			font-family:arial;
			text-align:left;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		td {
			vertical-align:top;
			border:0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		body {
			background-image: url("<?php echo $cardDesignDir.$designInfo->designId.'/'.$result->cardDesign->bgImgName?>.jpg");
			background-repeat:no-repeat;
			background-size: <?php echo $width1 ?>px <?php echo $height1 ?>px;
			background-position: <?php echo $result->cardDesign->leftMargin?>px <?php echo $result->cardDesign->topMargin?>px; 
			zoom: 100%;
		}
		.hideinprint {
		}
		.hideinscreen{
			background-color: #ffffff;
		}
	</style>
	<script language="JavaScript">
		//window.onload = function() {
		  //print01();
		//}
		function print01(){
			window.print();
		}
		function back01(){
			window.history.go(-1);
		}
		function close01(){
			self.close();
		}
	</script>
	</head>
	<body>
		<!-- spacing for label printing -->
		<?php if($printerType === 'label'){?> 
		<table>
			<tr height="<?php echo $totalHeight?>">
				<td></td>
			</tr>
		</table>
		<?php }else{} ?>
		<!-- now start -->
		<input type="hidden" id="maxnum" value="1">
		<input type="hidden" id="prevUrl1" value="<?php echo $cardDesignDir.$designInfo->designId.'/'.$result->cardDesign->bgImgName?>.jpg" size="100">
		<input type="hidden" id="prevUrlShow1" value="<?php echo $showBackImg?>">
		<input type="hidden" id="prevTotal1" value="<?php echo $result->cardDesign->total?>" size="100">
		<input type="hidden" id="prevDetail1" value="<?php echo $cardDesignDetail?>" size="100">
		<input type="hidden" id="prevAppDetail1" value="<?php echo $appDetail?>" size="100">
		<table class="cardTable">
			<tr height="<?php echo $result->cardDesign->topMargin?>">
				<td width="<?php echo $result->cardDesign->leftMargin?>">
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<canvas id="prev1" style="border:0px solid #ccc;" width="<?php echo $width1 ?>" height="<?php echo $height1 ?>">HTML5 Support Required</canvas>
				</td>
			</tr>
		</table>
		<?php if($showBackImg === 'yes'){?>
		<div class="hideinprint">
		<table>
			<tr>
				<td width="530" style="text-align:center;">
					<button type="button" class="btn btn-lg btn-kyp" onclick="close01();">Close</button>
				</td>
			</tr>
		</table>
		</div>	
		<?php }else{ ?>
		<div class="hideinprint">
		<table>
			<tr>
				<td width="530" style="text-align:center;">
					<button type="button" class="btn btn-lg btn-kyp" onclick="print01();">Print</button>
					<button type="button" class="btn btn-lg btn-kyp" onclick="close01();">Close</button>
				</td>
			</tr>
		</table>
		</div>		
		<?php } ?>
	</body>
</html>

