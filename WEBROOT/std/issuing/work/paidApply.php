<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>paidapply</title>
<?php
// 200614 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($user_type = $_SESSION['user_type']){
			case 'Issuer':
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (empty($_POST['appNo'])){
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	else{
		$appNo = preg_replace("/<|\/|_|>/","",$_POST['appNo']);	
	}
	if (isset($_POST['formName']) && isset($_POST['cardType'])){
		$formName = preg_replace("/<|\/|>/","",$_POST['formName']);	
		$cardType = preg_replace("/<|\/|>/","",$_POST['cardType']);	
	}	
	else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	$modifiedBy = 'officePay'.$_SESSION['valid_user'];
	$status = 'paid';
	$officePay = 'officePay';

	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$db->beginTransaction();
		$query0 = "UPDATE $tablename07 SET"; //paid
		$query0 .= " payStatus=:inputValue1, paidDate=now(), modifiedDate=now(), modifiedBy=:inputValue8, paymentType=:inputValue9";
		$query0 .= " WHERE appNo=:inputValue100";
		$stmt2 = $db->prepare($query0);
		$stmt2->bindParam(':inputValue1', $status);;
		$stmt2->bindParam(':inputValue8', $modifiedBy);
		$stmt2->bindParam(':inputValue9', $officePay);
		$stmt2->bindParam(':inputValue100', $appNo);
		if($stmt2->execute()){
			//changed 180919
			$db->commit();
			//$target ='../main_content.php?menu=documentApproval';
			$db= NULL;
			$target ='../main_content.php?menu='.$formName.'&no='.$_POST['appNo'];
			echo '<script>location.replace("'.$target.'");</script>';
			//echo "<script> window.history.go(-1); </script>";
			//changed 180919
		}
		else {
			//print_r($stmt2->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'modify ERROR1\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}	
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'modify error6\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	</form> 
</body>
</html>