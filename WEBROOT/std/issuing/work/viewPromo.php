<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>View Promotion Codes</title>
<link href="../css/bt.css" rel="stylesheet" type="text/css">
<link href="../css/yoonCustom.css" rel="stylesheet" type="text/css">
<?php
//200616 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	*/
	$promoCodeGroup = preg_replace("/<|\/|_|>/","", $_POST['promoGroupId']);
	//exit;
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	//require __DIR__.$goParent2.$reqDir1.'/_require/setting.php';
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$queryTotal = "SELECT * FROM $tablename26 WHERE promoCodeGroup = :searchValue1";
		$stmtTotal = $db->prepare($queryTotal);
		$stmtTotal->bindParam(':searchValue1', $promoCodeGroup);
		$stmtTotal->execute();
		if($stmtTotal->rowCount() > 0){
			$promoCodeCount =0;
			while($promoCode[$promoCodeCount]= $stmtTotal->fetch(PDO::FETCH_OBJ) ){
				$promoCodeCount++;
			}
		}else{
			echo '<script>alert(\'No code\');</script>';
			$db=NULL;
			exit;
		}
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		echo '<script>alert(\'DB error3\');</script>';
		$db= NULL;
		exit;
	}
	/*
	echo '<pre>';
	print_r($promoCode);
	echo '</pre>';
	*/
?>
	</head>
	<body>
	<table class="table table-bordered">
		<tr>
			<th class="text-center thGrey"></th>
			<th class="text-center thGrey">Code</th>
			<th class="text-center thGrey">Used</th>
		</tr>
	<?php 
		for($i=0;$i<$promoCodeCount;$i++){
	?>
		<tr>
			<td class="text-center"><?php echo $i+1;?></td>
			<td class="text-center"><?php echo $promoCode[$i]->promoCode;?></td>
			<td class="text-center"><?php echo $promoCode[$i]->promoUsed;?></td>
		</tr>
	<?php 
		} 
	?>
	</table>
	</body>
</html>
