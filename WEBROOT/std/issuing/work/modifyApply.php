<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>modifyapply</title>
<?php
// 210630 add when upload new photo or proof. remove old photo and proof process
// 201114 check
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	*/
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($user_type = $_SESSION['user_type']){
			case 'Issuer':
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	/*
	if (empty($_GET['appNo'])){
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/twn/index.php");</script>';
		exit;
	}
	else{}*/
	if(empty($_FILES) && empty($_POST) && isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post'){ //catch file overload error...
		$postMax = ini_get('post_max_size'); //grab the size limits...
		echo "<script>alert('Sum of files larger than {$postMax}');</script>";
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	else if (isset($_POST['formName'])){
		$formName =$_POST['formName'];
	}	
	else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/function.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	$applyDir = __DIR__.$goParent2.$reqDir1.$storageDir;
	$modifiedBy = 'modify'.$_SESSION['valid_user'];
	$cardType =$_POST['cardType'];
	$cardType = preg_replace("/<|>/","",$cardType);
	$appSettingIdApp = $_POST['appSettingIdApp'];
	$appSettingIdApp = preg_replace("/<|>/","",$appSettingIdApp);
	$receiveType =$_POST['receiveType'];
	$receiveType = preg_replace("/<|>/","",$receiveType);
	$appNo =$_POST['appNo'];
	$appNo = preg_replace("/<|>/","",$appNo);
	$officeId = $_POST['officeId'];
	$officeId = preg_replace("/<|>/","",$officeId);
	$gender =$_POST['gender'];
	$gender = preg_replace("/<|>/","",$gender);
	$emailNews =$_POST['emailNews'];
	$emailNews = preg_replace("/<|>/","",$emailNews);
	$nationality =$_POST['nationality'];
	$nationality = preg_replace("/<|>/","",$nationality);
	$validityStart =$_POST['validityStart'];
	$validityStart = preg_replace("/<|>/","",$validityStart);
	$validityEnd =$_POST['validityEnd'];
	$validityEnd = preg_replace("/<|>/","",$validityEnd);
	$comment =$_POST['comment'];
	$comment = preg_replace("/<|>/","",$comment);
	$photoName = 'photoName';
	$proof1Name = 'proof1Name';
	$proof2Name = 'proof2Name';
	$proof3Name = 'proof3Name';
	$proof4Name = 'proof4Name';
	$oriSerialNum = $_POST['oriSerialNum'];
	$oriSerialNum = preg_replace("/<|>/","",$oriSerialNum);
	$revaNum = $_POST['revaNum'];
	$revaNum = preg_replace("/<|>/","",$revaNum);
	$revaStart =  $_POST['revaStart'];
	$revaStart = preg_replace("/<|>/","",$revaStart);
	$revaEnd =  $_POST['revaEnd'];
	$revaEnd = preg_replace("/<|>/","",$revaEnd);	
	$encMode = 'YES';
	//enc data
	$engFname = $_POST['engFname']; 
	$engFname = preg_replace("/<|\/|_|>/","",$engFname);
	$engLname = $_POST['engLname'];
	$engLname = preg_replace("/<|\/|_|>/","",$engLname);
	$name1 = $_POST['name1']; 
	$name1= preg_replace("/<|\/|_|>/","",$name1);
	$birthYear = $_POST['birthYear'];
	$birthYear = preg_replace("/<|\/|_|>/","",$birthYear);
	$birthMonth = $_POST['birthMonth'];
	$birthMonth = preg_replace("/<|\/|_|>/","",$birthMonth);
	if(strlen($birthMonth) < 2)
	{
		$birthMonth = '0'.$birthMonth;
	}
	else
	{}
	$birthDay = $_POST['birthDay'];
	$birthDay= preg_replace("/<|\/|_|>/","",$birthDay);
	if(strlen($birthDay) < 2)
	{
		$birthDay = '0'.$birthDay;
	}
	else
	{}
	$school = $_POST['school']; 
	$school = preg_replace("/<|\/|_|>/"," ",$school);
	$email = $_POST['email'];
	$email = preg_replace("/<|\/|>/","",$email); //accept _
	/*
	$tel = $_POST['tel1'].$_POST['tel2'].$_POST['tel3'].$_POST['tel'];
	$tel = preg_replace("/<|\/|_|>/","",$tel);
	$mobile = $_POST['mobile1'].$_POST['mobile2'].$_POST['mobile3'].$_POST['mobile'];
	$mobile = preg_replace("/<|\/|_|>/","",$mobile);*/
	$phone  = preg_replace("/<|\/|_|>/","",$_POST['phone']);
	$city= $_POST['city'];
	$city = preg_replace("/<|\/|_|>/"," ",$city);
	$address = $_POST['address'];
	$address = preg_replace("/<|\/|_|>/"," ",$address);
	$address2  = preg_replace("/<|\/|_|>/","",$_POST['address2']);
	$country = preg_replace("/<|\/|_|>/","",$_POST['country']);
	$postal = $_POST['postal'];
	$postal = preg_replace("/<|\/|_|>/"," ",$postal);	
	if($encMode === 'YES'){
		$engFname = encrypt1($engFname);
		$engLname = encrypt1($engLname);
		$name1 = encrypt1($name1);
		$birthYear = encrypt1($birthYear);
		$birthMonth = encrypt1($birthMonth);
		$birthDay = encrypt1($birthDay);
		if($cardType != 'IYTC'){
			$school = encrypt1($school);
		}else{}
		$email = encrypt1($email);
		/*
		$tel = encrypt1($tel);
		$mobile = encrypt1($mobile);*/
		$phone = encrypt1($phone);
		if($receiveType!='visit'){
			$city = encrypt1($city);
			$address = encrypt1($address);
			$address2= encrypt1($address2);
			$postal = encrypt1($postal);
			$country = encrypt1($country);
		}else{}
		if(isset($_POST['associateName']) && !empty($_POST['associateName'])){
			$associateName = preg_replace("/<|\/|_|>/"," ",$_POST['associateName']); 
			$associateName = encrypt1($associateName);
		}else{}
		if(isset($_POST['associateRelation']) && !empty($_POST['associateRelation'])){
			$associateRelation = preg_replace("/<|\/|_|>/"," ",$_POST['associateRelation']); 
			$associateRelation = encrypt1($associateRelation);
		}else{}
		if(isset($_POST['associateNumber']) && !empty($_POST['associateNumber'])){
			
			
			$associateNumber = preg_replace("/<|\/|_|>/"," ",$_POST['associateNumber']); 
			$associateNumber = encrypt1($associateNumber);
			//echo $_POST['associateNumber'];
			//echo $associateNumber;
		}else{}
	}else{}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$db->beginTransaction();				  
		$query = "SELECT countryId,officeId,appNo,docuDir, photoName, proof1Name, proof2Name, proof3Name, proof4Name FROM $tablename07 WHERE appNo = :searchValue1;";
		if ($_SESSION['officeId'] === '1'){
			//$query .= " AND ((officeId = 1) OR (officeId = 990))";
		}else{
			$query .= " AND (officeId = :officeId)";
		}
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':officeId', $_SESSION['officeId']);
		}
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		if(!$result){			
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'Error2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		else {
			if(isset($result->docuDir) && !empty($result->docuDir))
			{
				$applyDir = $applyDir.$result->docuDir;
				if(!is_dir($applyDir))
				{
					mkdir($applyDir,0777,true);
				}
				else
				{
					//echo 'exist';
				}
			}
			else
			{}
			if(isset($result->photoName) && !empty($result->photoName))
			{
				$photoNameOld = pathinfo($result->photoName);		
				$photoNameOldExt = $photoNameOld['extension'];
				$photoNameOldExt = strtolower($photoNameOldExt);
			}
			else
			{}
			if(isset($result->proof1Name) && !empty($result->proof1Name))
			{
				$proof1NameOld = pathinfo($result->proof1Name);		
				$proof1NameOldExt = $proof1NameOld['extension'];
				$proof1NameOldExt = strtolower($proof1NameOldExt);
			}
			else
			{}
			if(isset($result->proof2Name) && !empty($result->proof2Name))
			{
				$proof2NameOld = pathinfo($result->proof2Name);
				$proof2NameOldExt = $proof2NameOld['extension'];
				$proof2NameOldExt = strtolower($proof2NameOldExt);
			}
			else
			{}
			if(isset($result->proof3Name) && !empty($result->proof3Name))
			{
				$proof3NameOld = pathinfo($result->proof3Name);
				$proof3NameOldExt = $proof3NameOld['extension'];
				$proof3NameOldExt = strtolower($proof3NameOldExt);
			}
			else
			{}
			if(isset($result->proof4Name) && !empty($result->proof4Name))
			{
				$proof4NameOld = pathinfo($result->proof4Name);
				$proof4NameOldExt = $proof4NameOld['extension'];
				$proof4NameOldExt = strtolower($proof4NameOldExt);
			}
			else
			{}
			$newfilename = $result->countryId.'_'.$result->officeId.'_'.$result->appNo;
			$orifilename = pathinfo($newfilename);
			$newfilename = $orifilename['filename'];
		}
		$query = "UPDATE $tablename07 SET ";
		$query .= "
		officeId = :officeId,
		name1=:inputValue1, 
		engFname=:inputValue2, 
		engLname=:inputValue3, 
		gender=:inputValue4, 
		birthDay=:inputValue5,
		birthMonth=:inputValue6, 
		birthYear=:inputValue7, 
		phone=:inputValue8, 
		email=:inputValue10, 
		emailNews=:inputValue11, 
		nationality=:inputValue12, 
		school=:inputValue13, 
		address=:inputValue14, 
		city=:inputValue15, 
		postal=:inputValue16, 
		validityStart=:inputValue17, 
		validityEnd=:inputValue18, 
		comment=:inputValue19, 
		modifiedDate=now(), 
		modifiedBy=:inputValue101, 
		encId=:inputValue32,
		address2=:inputValue33,
		country=:inputValue34
		";
		if(isset($_POST['vatNumber'])&& !empty($_POST['vatNumber'])){
			$vatNumber = preg_replace("/<|\/|_|>/"," ",$_POST['vatNumber']); 
			$query .= ", vatNumber =:vatNumber";
		}else{}
		if(isset($_POST['promo'])&& !empty($_POST['promo'])){
			$promo = preg_replace("/<|\/|_|>/"," ",$_POST['promo']); 
			$query .= ", promo =:promo";
		}else{}
		if(isset($_POST['associateNumber'])&& !empty($_POST['associateNumber'])){
			
			$query .= ", associateNumber =:associateNumber";
		}else{}
		if(isset($_POST['associateName'])&& !empty($_POST['associateName'])){
			
			$query .= ", associateName =:associateName";
		}else{}
		if(isset($_POST['associateRelation'])&& !empty($_POST['associateRelation'])){
			
			$query .= ", associateRelation =:associateRelation";
		}else{}
		
		if(isset($_FILES[$photoName]['name']) && !empty($_FILES[$photoName]['name'])){
			if($_FILES[$photoName]['error'] == 0){
				echo 'photo';
				uploadFile2($photoName,$newfilename.'.',$applyDir);
				$name = $_FILES[$photoName]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", photoName='{$newfilename}.{$ext}'";
				if(isset($photoNameOldExt) && $photoNameOldExt === $ext)
				{
					echo $photoNameOldExt.' '.$ext;
				}
				else
				{
					$path = '';	
					$path = $applyDir.$result->photoName;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($applyDir.$result->photoName);	
						echo ' delete photo ';
					}else{
						echo $path;
					}
				}
			}
			else if($_FILES[$photoName]['error'] == 1){
				$thisName = $_FILES[$photoName]['name'];
				$db->rollBack();
				$db= NULL;	   
				echo "<script>alert('{$thisName} is Too Big');</script>";
				echo "<script> window.history.go(-1); </script>";
			}else{}
		}else{}
		/*
		echo '<pre>';
		print_r($_FILES);
		echo '</pre>';*/
		if(isset($_FILES[$proof1Name]['name']) && !empty($_FILES[$proof1Name]['name'])){
			if($_FILES[$proof1Name]['error'] == 0){
				echo 'proof1';
				$name = $_FILES[$proof1Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof1Name='{$newfilename}_1.{$ext}'";
				uploadFile2($proof1Name,$newfilename.'_1.',$applyDir);
				if(isset($proof1NameOldExt) && $proof1NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $applyDir.$result->proof1Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($applyDir.$result->proof1Name);	
						echo ' delete old proof1 ';
					}else{}
				}
			}
			else if($_FILES[$proof1Name]['error'] == 1){
				$thisName = $_FILES[$proof1Name]['name'];
				$db->rollBack();
				$db= NULL; 
				echo "<script>alert('{$thisName} is Too Big');</script>";
				echo "<script> window.history.go(-1); </script>";
			}else{}
		}else{}
		if(isset($_FILES[$proof2Name]['name']) && !empty($_FILES[$proof2Name]['name'])){
			if($_FILES[$proof2Name]['error'] == 0){
				echo 'proof2';
				$name = $_FILES[$proof2Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof2Name='{$newfilename}_2.{$ext}'";
				uploadFile2($proof2Name,$newfilename.'_2.',$applyDir);
				if(isset($proof2NameOldExt) && $proof2NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $applyDir.$result->proof2Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($applyDir.$result->proof2Name);	
						echo ' delete old proof2 ';
					}else{}
				}
			}
			else if($_FILES[$proof2Name]['error'] == 1){
				$thisName = $_FILES[$proof2Name]['name'];
				$db->rollBack();
				$db= NULL; 
				echo "<script>alert('{$thisName} is Too Big');</script>";
				echo "<script> window.history.go(-1); </script>";
			}else{}
		}else{}
		if(isset($_FILES[$proof3Name]['name']) && !empty($_FILES[$proof3Name]['name'])){
			if($_FILES[$proof3Name]['error'] == 0){
				echo 'proof3';
				$name = $_FILES[$proof3Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof3Name='{$newfilename}_3.{$ext}'";
				uploadFile2($proof3Name,$newfilename.'_3.',$applyDir);
				if(isset($proof3NameOldExt) && $proof3NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $applyDir.$result->proof3Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($applyDir.$result->proof3Name);	
						echo ' delete old proof3 ';
					}else{}
				}
			}
			else if($_FILES[$proof3Name]['error'] == 1){
				$thisName = $_FILES[$proof3Name]['name'];
				echo "<script>alert('{$thisName} is Too Big');</script>";
				$db->rollBack();
				$db= NULL; 
				echo "<script> window.history.go(-1); </script>";
			}else{}
		}else{}
		if(isset($_FILES[$proof4Name]['name']) && !empty($_FILES[$proof4Name]['name'])){
			if($_FILES[$proof4Name]['error'] == 0){
				echo 'proof4';
				$name = $_FILES[$proof4Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof4Name='{$newfilename}_4.{$ext}'";
				uploadFile2($proof4Name,$newfilename.'_4.',$applyDir);
				if(isset($proof4NameOldExt) && $proof4NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $applyDir.$result->proof4Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($applyDir.$result->proof4Name);	
						echo ' delete old proof4 ';
					}else{}
				}
			}
			else if($_FILES[$proof4Name]['error'] == 1){
				$thisName = $_FILES[$proof4Name]['name'];
				echo "<script>alert('{$thisName} is Too Big');</script>";
				$db->rollBack();
				$db= NULL; 
				echo "<script> window.history.go(-1); </script>";
			}else{}
		}else{}
		if(isset($_POST['newPassword']) && !empty($_POST['newPassword'])){
			$password = $_POST['newPassword'];
			$password1= password_hash($password, PASSWORD_BCRYPT);
		}
		if(isset($password1)){
			$query .= ", password=:inputValue20";
		}else{}
		$query .= ", encDate = now() WHERE appNo=:appNo";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->bindParam(':inputValue1', $name1);
		$stmt->bindParam(':inputValue2', $engFname);
		$stmt->bindParam(':inputValue3', $engLname);
		$stmt->bindParam(':inputValue4', $gender);
		$stmt->bindParam(':inputValue5', $birthDay);
		$stmt->bindParam(':inputValue6', $birthMonth);
		$stmt->bindParam(':inputValue7', $birthYear);
		$stmt->bindParam(':inputValue8', $phone);
		$stmt->bindParam(':inputValue10', $email);
		$stmt->bindParam(':inputValue11', $emailNews);
		$stmt->bindParam(':inputValue12', $nationality);
		$stmt->bindParam(':inputValue13', $school);
		$stmt->bindParam(':inputValue14', $address);
		$stmt->bindParam(':inputValue15', $city);
		$stmt->bindParam(':inputValue16', $postal);
		$stmt->bindParam(':inputValue17', $validityStart);
		$stmt->bindParam(':inputValue18', $validityEnd);
		$stmt->bindParam(':inputValue19', $comment);
		$stmt->bindParam(':inputValue32', $currentKey);
		$stmt->bindParam(':inputValue33', $address2);
		$stmt->bindParam(':inputValue34', $country);
		$stmt->bindParam(':inputValue101', $modifiedBy);
		if(isset($_POST['vatNumber'])&& !empty($_POST['vatNumber'])){
			$stmt->bindParam(':vatNumber', $vatNumber);
		}else{}
		if(isset($_POST['promo'])&& !empty($_POST['promo'])){
			$stmt->bindParam(':promo', $promo);
		}else{}
		if(isset($_POST['associateNumber'])&& !empty($_POST['associateNumber'])){
			$stmt->bindParam(':associateNumber', $associateNumber);
			///echo $associateNumber;
		}else{}
		if(isset($_POST['associateName'])&& !empty($_POST['associateName'])){
			$stmt->bindParam(':associateName', $associateName);
		}else{}
		if(isset($_POST['associateRelation'])&& !empty($_POST['associateRelation'])){
			$stmt->bindParam(':associateRelation', $associateRelation);
		}else{}
		if(isset($password1)){
			$stmt->bindParam(':inputValue20', $password1);
		}else{}
		$stmt->bindParam(':appNo', $appNo);
		if($stmt->execute()){
			//echo "<script> window.history.go(-1); </script>";
		}
		else {
			//echo $query;
			print_r($stmt->errorInfo());
			echo '<script>alert(\'apply modify ERROR1\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			$db->rollBack();
			$db= NULL;
			exit;
		}
		//// modify extra info start ///
		$query = "SELECT extraFieldName FROM $tablename25 WHERE extraAppSettingId = :extraAppSettingId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':extraAppSettingId', $appSettingIdApp);
		$stmt->execute();
		//echo $query;
		$extraCount =0;
		if($stmt->rowCount() >0 ){
			$extraCount = $stmt->rowCount();
			$extraC = 1;
			while($result2 = $stmt->fetch(PDO::FETCH_OBJ)){
				$extraList[$extraC] = $result2;
				$extraC++;
			}
		}else{
			//echo 'noextra';
		}
		//echo '<br/>'.$extraCount.'<br/>';
		if($extraCount > 0){
			$query = "UPDATE $tablename24 SET extra1Name =:extra1Name, extra1Value =:extra1Value";
			for($extraC=1;$extraC<$extraCount;$extraC++){
				$nextExtra = $extraC+1;
				$query .= ", extra".$nextExtra."Name =:extra".$nextExtra."Name";
				$query .= ", extra".$nextExtra."Value =:extra".$nextExtra."Value";
			}
			$query .= " WHERE extraAppNo =:extraAppNo";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':extraAppNo', $appNo);
			//echo $query;
			for($extraC=1;$extraC<$extraCount+1;$extraC++){
				if(isset($_POST['extra'.(string)$extraC.'Value']) && !empty($_POST['extra'.(string)$extraC.'Value']) ){
				}else{
					$extraValue[$extraC] =' ';
				}
				$extraValue[$extraC] = preg_replace("/\'|\"|<|\/|>/"," ",$_POST['extra'.(string)$extraC.'Value']); 
				//echo '<br/>'.$extraValue[$extraC].'<br/>';
				$extraValue[$extraC] = encrypt1($extraValue[$extraC]);
				$stmt->bindParam(':extra'.(string)$extraC.'Name', $extraList[$extraC]->extraFieldName);
				$stmt->bindParam(':extra'.(string)$extraC.'Value', $extraValue[$extraC]);
			}
			if($stmt->execute()){
			//print_r($stmt->errorInfo());
			}else{
				print_r($stmt->errorInfo());
				$db->rollBack();
				$db= NULL;
				echo '<script>alert(\'Extra Info Error\');</script>';
				echo '<br/>';
				exit;
			}
		}else{}
		//// modify extra info end ///
		if(isset($oriSerialNum) && !empty($oriSerialNum)){
			$query = "UPDATE $tablename20 SET ";
			$query .= "
			oriSerialNum=:inputValue1, 
			revaNum=:inputValue2, 
			revaStart=:inputValue3, 
			revaEnd=:inputValue4, 
			revaBy=:inputValue5 
			WHERE 
			revaAppNo=:searchValue101";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $oriSerialNum);
			$stmt->bindParam(':inputValue2', $revaNum);
			$stmt->bindParam(':inputValue3', $revaStart);
			$stmt->bindParam(':inputValue4', $revaEnd);
			$stmt->bindParam(':inputValue5', $modifiedBy);
			$stmt->bindParam(':searchValue101', $appNo);
			if($stmt->execute()){
			}
			else {
				//echo $query;
				print_r($stmt->errorInfo());
				echo '<script>alert(\'apply modify ERROR3\');</script>';
				//echo "<script> window.history.go(-1); </script>";
				$db->rollBack();
				$db= NULL;
				exit;
			}
		}else{}
		$target ='../main_content.php?menu='.$formName.'&no='.$_POST['appNo'];
		$db->commit();
		$db= NULL;
		echo '<script>location.replace("'.$target.'");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();	
		$db= NULL;
		echo '<script>alert(\'apply modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="POST"> 
	<button type="submit">
	</form> 
</body>
</html>