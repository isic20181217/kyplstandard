<?php
// 200604 checked
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$promoCodeGroup = preg_replace("/<|>/","",$_POST['promoGroupId']);
	if(isset($_FILES['file1']['tmp_name']) && !empty($_FILES['file1']['tmp_name'])){
		if(is_uploaded_file($_FILES['file1']['tmp_name'])){
			$name = $_FILES['file1']['name'];	
			$orifilename = pathinfo($name);
			$ext = $orifilename['extension'];
			$ext = strtolower($ext);
			switch($ext){
				case 'csv':
					$handle = fopen($_FILES['file1']['tmp_name'], "r");				
					$i = 0;
					while(($data = fgetcsv($handle,",")) !== FALSE){
						//echo $i.' ';
						//echo $i.' 0 '.$data[0][0].' 1 '.$data[0][1].' 2 '.$data[0][2].' 3 '.$data[0][3].' 4 '.$data[0][4].' ';
						if($data[0][0].$data[0][1].$data[0][2]==chr(hexdec('EF')).chr(hexdec('BB')).chr(hexdec('BF'))){
						   $data[0] = substr($data[0],3);
						   //echo $data[0][0];
						}else{
							//echo ' ok ';
							//echo $data[0][0];
						}						
						$fileArray[$i]= preg_replace("/<|>/","",$data[0]);
						$i++;
						//print_r ($data[0]);
						//echo '<br/>';
					}
					//echo '<pre>';
					//print_r($fileArray);
					//echo '</pre>';
					//exit;

					//$fileArray = file($_FILES['file1']['tmp_name']);
					break;
				default:
					echo "<script> window.history.go(-1); </script>";
					echo "<script>alert('Can not upload {$ext}');</script>";
					exit;
			}
		}
		else{
			echo "<script> window.history.go(-1); </script>";
			echo '<script>alert(\'File Upload Error2\');</script>';
			exit;
		}
	}else{
		echo '<script>alert(\'Not Enough Values(File)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	$addedBy= $_SESSION['valid_user'];
	$arraySize = count($fileArray);	
	echo '<pre>';
	print_r($fileArray);
	echo '</pre>';
	//exit;
	$today = date("Y-m-d H:i:s");   
	$modifiedBy= $_SESSION['valid_user'];
	$promoUsed = 'no';
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';

	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}	
		$db->beginTransaction();
		for($i=0;$i<$arraySize;$i++){
			$query = "INSERT INTO $tablename26 (";
			$query .= "promoCodeGroup, promoCode, promoUsed)";
			$query .= " VALUES (:promoCodeGroup, :promoCode, :promoUsed)";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':promoCodeGroup', $promoCodeGroup);
			$stmt->bindParam(':promoCode', $fileArray[$i]);
			$stmt->bindParam(':promoUsed', $promoUsed);
			$stmt->execute();	
			if($stmt->rowCount() === 1){

			}else{
				echo '<script>alert(\'Code Add ERROR1\');</script>';
				echo $fileArray[$i].' ERROR <br/>';
				print_r($stmt->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;			
			}
		}
		$db->commit();
		$db= NULL;
		echo '<script>alert(\'Code Add Complete\');</script>';
		echo "<script>window.opener.location.reload();</script>";
		echo "<script>self.close();</script>";
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		echo '<script>alert(\'modify error3\');</script>';
		$db= NULL;
		exit;
	}
?>