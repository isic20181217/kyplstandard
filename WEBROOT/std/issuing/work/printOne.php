<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ISIC/ITIC/IYTC Print</title>
<link href="../css/bt.css" rel="stylesheet" type="text/css">
<link href="../css/yoonCustom.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./printTest1.js"></script>
<?php 
//201114 check
$width1 = 540;
$height1 = 331;
$totalHeight = 0;		
 	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['appId']) && isset($_GET['printerType'])){
		$appId = preg_replace("/<|\/|_|>/","",$_GET['appId']);
		$printerType = preg_replace("/<|\/|_|>/","",$_GET['printerType']);
		$showBackImg = 'no';
	}else{
		echo 'Access Denied1';
		exit;
	}
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent2.'/req.php';
		require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';

		$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename24 ON $tablename07.appNo = $tablename24.extraAppNo WHERE appNo =:searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			//$result->cardDesign = $designInfo;
		}else{
			echo 'Access Denied4';
			print_r($stmt->errorInfo());
			$db= NULL;
			exit;
		}
		$query = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId AND activeApp = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingId', $result->appSettingIdApp);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);

			//$result = (object) array();
			$result->appSetting = $appSettingInfo;
/*
			echo '<pre>';
			print_r($result);
			echo '</pre>';	
*/			
		}else{
			echo 'Access Denied2';
			print_r($stmt->errorInfo());
			$db= NULL;
			exit;
		}
		$query = "SELECT * FROM $tablename30 WHERE designId = :designId AND activeDesign = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':designId', $result->appSetting->designId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$designInfo = $stmt->fetch(PDO::FETCH_OBJ);

			//$result = (object) array();
			$result->cardDesign = $designInfo;
/*
			echo '<pre>';
			print_r($result);
			echo '</pre>';
*/			
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		//exit;
		$school = decrypt1($result->school, $result->encId);
		$birthDay = decrypt1($result->birthDay, $result->encId);
		$birthMonth = decrypt1($result->birthMonth, $result->encId);
		$birthYear = decrypt1($result->birthYear, $result->encId);
		$engFname = decrypt1($result->engFname, $result->encId);
		$engLname = decrypt1($result->engLname, $result->encId);
		if (isset($result->photoName)){
				$photoName = pathinfo($result->photoName);
		}else{
			$photoName = 'nophoto';
		}
		if(substr($result->cardType,0,4) === 'REVA'){
			$cardSerialNum = substr($result->revaNum, 0, 1).' '.substr($result->revaNum, 1, 3).' '.substr($result->revaNum, 4, 3).' '.substr($result->revalNum, 7, 3).' '.substr($result->revaNum, 10, 3).' '.substr($result->revaNum, 13, 1);
		}else{
			$cardSerialNum = substr($result->cardSerialNum, 0, 1).' '.substr($result->cardSerialNum, 1, 3).' '.substr($result->cardSerialNum, 4, 3).' '.substr($result->cardSerialNum, 7, 3).' '.substr($result->cardSerialNum, 10, 3).' '.substr($result->cardSerialNum, 13, 1);
		}
		$cardDesignDetail = '';
		$appDetail = '';
		for($element=0;$element<$result->cardDesign->total;$element++){
			$info = 'info'.(string)($element+1);
			$content = 'content'.(string)($element+1);
			$cardDesignDetail .= $result->cardDesign->$info;
			$cardDesignDetail .= '/iEnd/';
			$cardDesignDetail .= $result->cardDesign->$content;
			$cardDesignDetail .= '/cEnd/';
			if(substr($result->cardDesign->$content,0,6) ==='birth_'){
				$birthDateType = substr($result->cardDesign->$content,6);
				$birthDateType = explode('_',$birthDateType);
				for($birthCount=0;$birthCount<count($birthDateType);$birthCount++){
					if($birthCount != 0){
						$appDetail .= '/';
					}else{}
					switch($birthDateType[$birthCount]){
						case 'DD':
							$day = decrypt1($result->birthDay, $result->encId);
							$appDetail .= $day;
							break;
						case 'MM':
							$month = decrypt1($result->birthMonth, $result->encId);
							if(strlen($month) < 2){
								$month = '0'.$month;
							}else{}
							$appDetail .= $month;
							break;
						case 'YY':
							$year = substr(decrypt1($result->birthYear, $result->encId),2);
							$appDetail .= $year;
							break;
						case 'YYYY':
							$year = decrypt1($result->birthYear, $result->encId);
							$appDetail .= $year;
							break;
						default:
							$appDetail .= 'ERROR';
							break;
					}	
				}
			}else if(substr($result->cardDesign->$content,0,7) ==='engName'){
				if($nameStyle1 === 'FL'){
					$appDetail .= decrypt1($result->engFname, $result->encId).' '.decrypt1($result->engLname, $result->encId);			
				}else if($nameStyle1 === 'LF'){
					$appDetail .= decrypt1($result->engLname, $result->encId).' '.decrypt1($result->engFname, $result->encId);			
				}else{
					echo 'ERROR';
				}								
			}else if(substr($result->cardDesign->$content,0,9) ==='serialNum'){
				//$cardSerialNum =' ';
				$appDetail .= $cardSerialNum;
			}else if(substr($result->cardDesign->$content,0,9) ==='validity_'){
				$validityType = substr($result->cardDesign->$content,9);
				switch($validityType){
					case 'END_MMYY':
						$appDetail .= substr($result->validityEnd,0,2).'/'.substr($result->validityEnd,5,2);
						break;
					case 'STARTEND_MMYYYY':
						$appDetail .= $result->validityStart.' ~ '.$result->validityEnd;
						break;
					default:
						$appDetail .= 'ERROR';
						break;
				}
			}else if(substr($result->cardDesign->$content,0,5) === 'logo_'){
				$appDetail .= $cardDesignDir.$designInfo->designId.'/'.substr($result->cardDesign->$content,5);
			}else if(substr($result->cardDesign->$content,0,5) === 'fill_'){
				$appDetail .= $result->cardDesign->$content;
			}else{
				switch($result->cardDesign->$content){
				case 'engFname':
				case 'engLname':
				case 'name1':
				case 'birthYear':
				case 'birthMonth':
				case 'birthDay':
				case 'school':
				case 'extra1Value':
				case 'extra2Value':
				case 'extra3Value':
				case 'extra4Value':
				case 'extra5Value':
				case 'extra6Value':
				case 'extra7Value':
				case 'extra8Value':
				case 'extra9Value':
				case 'extra10Value':
					$contentOne = $result->cardDesign->$content;
					$contentOne = decrypt1($result->$contentOne, $result->encId);	
					break;
				case 'photoName':
					$contentOne = $result->cardDesign->$content;
					$contentOne = $result->$contentOne;
					$contentOne = read_img('1',$photoName['filename'],$photoName['extension']);
					break;
				default:
					$contentOne = $result->cardDesign->$content;
					$contentOne = $result->$contentOne;
				}
				$appDetail .= $contentOne;
			}
			$appDetail .= '/aEnd/';
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}
	function read_img($index,$name,$ext){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}
?> 
	<style type="text/css" media="print">
		@page {
			/*size:landscape;*/
			margin-top:0;
			margin-right:0;
			margin-bottom:0;
			margin-left:0;
		}
		table {
			color:black;
			font-family:arial;
			text-align:left;
			border: 0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		td {
			vertical-align:top;
			border:0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		body {
		}
		.hideinprint {
			display:none;
		}
	</style>
	<style type="text/css" media="screen">
		table {
			color:black;
			font-family:arial;
			text-align:left;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		td {
			vertical-align:top;
			border:0px solid black;
			padding: 0px;
			border-spacing: 0px 0px;
		}
		body {
			background-image: url("<?php echo $cardDesignDir.$designInfo->designId.'/'.$result->cardDesign->bgImgName?>.jpg");
			background-repeat:no-repeat;
			background-size: <?php echo $width1?>px <?php echo $height1?>px;
			background-position: <?php echo $result->cardDesign->leftMargin?>px <?php echo $result->cardDesign->topMargin?>px; 
			zoom: 100%;
		}
		.hideinprint {
		}
		.hideinscreen{
			background-color: #ffffff;
		}
	</style>
	<script language="JavaScript">
		//window.onload = function() {
		  //print01();
		//}
		function print01(){
			alert('please wait 5 seconds');
			setTimeout(function() {
				window.print();
			}, 3000);
		}
		function back01(){
			window.history.go(-1);
		}
		function close01(){
			self.close();
		}
	</script>
	</head>
	<body>

	
		<!-- spacing for label printing -->
		<?php if($printerType === 'label'){?> 
		<table>
			<tr height="<?php echo $totalHeight?>">
				<td></td>
			</tr>
		</table>
		<?php }else{} ?>
		<!-- now start -->
		<input type="hidden" id="maxnum" value="1">
		<input type="hidden" id="prevUrl1" value="<?php echo $cardDesignDir.$designInfo->designId.'/'.$result->cardDesign->bgImgName?>.jpg" size="100">
		<input type="hidden" id="prevUrlShow1" value="<?php echo $showBackImg?>">
		<input type="hidden" id="prevTotal1" value="<?php echo $result->cardDesign->total?>" size="100">
		<input type="hidden" id="prevDetail1" value="<?php echo $cardDesignDetail?>" size="100">
		<input type="hidden" id="prevAppDetail1" value="<?php echo $appDetail?>" size="100">
		<table class="cardTable">
			<tr height="<?php echo $result->cardDesign->topMargin?>">
				<td width="<?php echo $result->cardDesign->leftMargin?>">
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<canvas id="prev1" style="border:0px solid #ccc;" width="<?php echo $width1?>" height="<?php echo $height1?>">HTML5 Support Required</canvas>
				</td>
			</tr>
		</table>
		<?php if($showBackImg === 'yes'){?>
		<div class="hideinprint">
		<table>
			<tr>
				<td width="530" style="text-align:center;">
					<button type="button" class="btn btn-lg btn-kyp" onclick="close01();">Close</button>
				</td>
			</tr>
		</table>
		</div>	
		<?php }else{ ?>
		<div class="hideinprint">
		<table>
			<tr>
				<td width="530" style="text-align:center;">
					<button type="button" class="btn btn-lg btn-kyp" onclick="print01();">Print</button>
					<button type="button" class="btn btn-lg btn-kyp" onclick="close01();">Close</button>
				</td>
			</tr>
		</table>
		</div>		
		<?php } ?>
	</body>
</html>

