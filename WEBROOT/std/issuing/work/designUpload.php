<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>modifyDesign</title>
<?php
// 200612 check		
?>
<?php 
	/*
	echo "<pre>";
	print_R($_POST);
	print_R($_FILES);
	echo "</pre>";
	exit;
	*/
?>
<?php
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if(empty($_FILES) && empty($_POST) && isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post'){ //catch file overload error...
		$postMax = ini_get('post_max_size'); //grab the size limits...
		echo "<script>alert('Sum of files larger than {$postMax}');</script>";
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	else if (isset($_POST['formName'])){
		$formName= preg_replace("/<|\/|>/"," ",$_POST['formName']);
	}	
	else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['designId'])){
		$designId= preg_replace("/<|\/|>/"," ",$_POST['designId']);
	} else {
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['designNo']) && !empty($_POST['designNo']) ){
		$designNo= preg_replace("/<|\/|>/"," ",$_POST['designNo']);
	} else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['addedDate']) && !empty($_POST['addedDate']) ){
		$addedDate= preg_replace("/<|>/"," ",$_POST['addedDate']);
		$addedDateArray = explode('/',$addedDate);
		$addedDate = $addedDateArray[2].'-'.$addedDateArray[1].'-'.$addedDateArray[0];
	} else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['activeDesign'])){
		$activeDesign= preg_replace("/<|\/|>/"," ",$_POST['activeDesign']);
		if($activeDesign === 'yes'){
		} else {
			$activeDesign = 'no';
		}
	} else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['cardType'])){
		$cardType= preg_replace("/<|\/|>/"," ",$_POST['cardType']);
		switch($cardType){
			case 'ISIC':
			case 'ITIC':
			case 'IYTC':
				break;
			default:
				echo '<script>alert(\'forbidden\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	} else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	echo '<a href="../main_content.php?menu=designEditor1&designId='.$designId.'">Go Back</a>';

	$today = date("Y-m-d H:i:s");   
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	$uploadDir = __DIR__.$goParent.$goParent2.$cardDesignDir;	
	$modifiedBy= $_SESSION['valid_user'];	
	$bgImgName = 'cardPreview_'.$designId;
	
	$elementArray = array();
	$elementArrayCount = 0;
	$currentType = array('p','u','f','l');
	for($typeNum=0;$typeNum<count($currentType);$typeNum++){
		$currentCount = 'elementTotal_'.$currentType[$typeNum];
		for($i=1;$i<$_POST[$currentCount]+1;$i++){
			//echo ' i : '.$i;
			$isThisOk = 'no';
			$currentContent = 'elementContent_'.$currentType[$typeNum].'_'.$i;
			$currentContentFile = 'elementContentFile_'.$currentType[$typeNum].'_'.$i;
			$currentXpos = 'elementXpos_'.$currentType[$typeNum].'_'.$i;	
			$currentYpos = 'elementYpos_'.$currentType[$typeNum].'_'.$i;	
			$currentWidth = 'elementWidth_'.$currentType[$typeNum].'_'.$i;	
			$currentHeight = 'elementHeight_'.$currentType[$typeNum].'_'.$i;	
			$currentSize = 'elementSize_'.$currentType[$typeNum].'_'.$i;				
			if(isset($_POST[$currentContent]) && !empty($_POST[$currentContent])){
				$isThisOk = 'yes';

				$elementArrayCount++;
				//echo ' currentType : '.$currentType[$typeNum];
				//echo ' currentContent : '.$currentContent;
				//echo ' elementArrayCount : '.$elementArrayCount;
				echo '<br/>';
				$elementArray[$elementArrayCount] = new stdClass();
				$elementArray[$elementArrayCount]->type = $currentType[$typeNum];
				$elementArray[$elementArrayCount]->content = preg_replace("/<|>/"," ",$_POST[$currentContent]);//accept '_,/'
			}else if(isset($_FILES[$currentContentFile]['size']) && $_FILES[$currentContentFile]['size'] >0){
				$isThisOk = 'yes';
				$elementArrayCount++;
				//echo ' currentType : '.$currentType[$typeNum];
				//echo ' currentContent : '.$currentContent;
				//echo ' elementArrayCount : '.$elementArrayCount;
				echo '<br/>';
				$elementArray[$elementArrayCount] = new stdClass();
				$elementArray[$elementArrayCount]->type = $currentType[$typeNum];
				$elementArray[$elementArrayCount]->content = 'logo_logo'.$i.'.jpg';
			}else{	
				//echo '<br/>';
				break;
			}
			if(isset($_POST[$currentXpos]) && !empty($_POST[$currentXpos]) && preg_match( "/^[0-9]/i", $_POST[$currentXpos]) ){
				$isThisOk = 'yes';
				$elementArray[$elementArrayCount]->Xpos = $_POST[$currentXpos];
			}else{
				$isThisOk = 'Xpos';
			}
			if(isset($_POST[$currentYpos]) && !empty($_POST[$currentYpos]) && preg_match( "/^[0-9]/i", $_POST[$currentYpos])){
				$isThisOk = 'yes';
				$elementArray[$elementArrayCount]->Ypos = $_POST[$currentYpos];
			}else{
				$isThisOk = 'Ypos';
			}
			switch($currentType[$typeNum]){
				case 'p': //photo
					if(isset($_POST[$currentWidth]) && !empty($_POST[$currentWidth]) && preg_match( "/^[0-9]/i", $_POST[$currentWidth]) ){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Width = $_POST[$currentWidth];
					}else{
						$isThisOk = 'Width';
					}
					if(isset($_POST[$currentHeight]) && !empty($_POST[$currentHeight]) && preg_match( "/^[0-9]/i", $_POST[$currentHeight])){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Height = $_POST[$currentHeight];
					}else{
						$isThisOk = 'Height';
					}
					$elementArray[$elementArrayCount]->info = 'i' .'_'. $elementArray[$elementArrayCount]->Xpos .'_'. $elementArray[$elementArrayCount]->Ypos .'_'. $elementArray[$elementArrayCount]->Width .'_'. $elementArray[$elementArrayCount]->Height;
					break;
				case 'u': //user info
					if(isset($_POST[$currentSize]) && !empty($_POST[$currentSize]) && preg_match( "/^[0-9]/i", $_POST[$currentSize]) ){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Size = $_POST[$currentSize];
					}else{
						$isThisOk = 'Size';
					}
					$elementArray[$elementArrayCount]->info = 't' .'_'. $elementArray[$elementArrayCount]->Xpos .'_'. $elementArray[$elementArrayCount]->Ypos .'_'. $elementArray[$elementArrayCount]->Size;
					break;
				case 'f'; // filled text
					if(isset($_POST[$currentSize]) && !empty($_POST[$currentSize]) && preg_match( "/^[0-9]/i", $_POST[$currentSize]) ){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Size = $_POST[$currentSize];
					}else{
						$isThisOk = 'Size';
					}
					$elementArray[$elementArrayCount]->content = 'fill_'.$elementArray[$elementArrayCount]->content;
					$elementArray[$elementArrayCount]->info = 't' .'_'. $elementArray[$elementArrayCount]->Xpos .'_'. $elementArray[$elementArrayCount]->Ypos .'_'. $elementArray[$elementArrayCount]->Size;
					break;
				case 'l'; // logo image
					if(isset($_POST[$currentWidth]) && !empty($_POST[$currentWidth]) && preg_match( "/^[0-9]/i", $_POST[$currentWidth]) ){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Width = $_POST[$currentWidth];
					}else{
						$isThisOk = 'Width';
					}
					if(isset($_POST[$currentHeight]) && !empty($_POST[$currentHeight]) && preg_match( "/^[0-9]/i", $_POST[$currentHeight])){
						$isThisOk = 'yes';
						$elementArray[$elementArrayCount]->Height = $_POST[$currentHeight];
					}else{
						$isThisOk = 'Height';
					}
					$elementArray[$elementArrayCount]->info = 'i' .'_'. $elementArray[$elementArrayCount]->Xpos .'_'. $elementArray[$elementArrayCount]->Ypos .'_'. $elementArray[$elementArrayCount]->Width .'_'. $elementArray[$elementArrayCount]->Height;
					if(isset($_FILES[$currentContentFile]['size']) && $_FILES[$currentContentFile]['size'] >0){
						if($_FILES[$currentContentFile]['error'] == 0){
							//echo 'bgImg...Ok';
							$name = $_FILES[$currentContentFile]['name'];	
							$orifilename = pathinfo($name);
							$ext = $orifilename['extension'];
							$ext = strtolower($ext);
							switch($ext){
								case 'jpg':
									$saveName = 'logo'.$i.'.jpg';
									if (!is_dir($uploadDir.$designId) ) {
										mkdir($uploadDir.$designId);
									}else{}
									if(!move_uploaded_file($_FILES[$currentContentFile]['tmp_name'], $uploadDir.$designId.'/'.$saveName)){
										echo '<script>alert(\'File Upload Error1\');</script>';
										//echo "<script> window.history.go(-1); </script>";
										exit;
									}
									else{}				
									break;
								default:
									echo "<script>alert('Can not upload {$ext} only JPG');</script>";
									echo "<script> window.history.go(-1); </script>";
									exit;
							}				
						}
						else if($_FILES[$currentContentFile]['error'] == 1){
							$thisName = $_FILES[$currentContentFile]['name'];
							echo "<script>alert('{$thisName} is Too Big');</script>";
							echo "<script> window.history.go(-1); </script>";
							exit;
						}else{
							$isThisOk = 'Logo';
						}
					}else{}
					break;
				default:
					echo 'element type error';
					exit;
			}
			if($isThisOk === 'yes'){
			}else{
				echo $i.' '.$currentType[$typeNum].' '.$isThisOk.' Error ';
				exit;
			}
			//echo '<br/>';
		}
	}
	/*
	echo '<pre>';
	print_r($elementArray);
	echo '</pre>';
	*/
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$db->beginTransaction();
		$query = "SELECT designId FROM $tablename30 WHERE designNo = :designNo";
		$stmt = $db->prepare($query);
		$stmt->bindParam(":designNo", $designNo);
		$stmt->execute();
		$designRowCount = $stmt->rowCount();
		if($designRowCount > 1 ){
			echo '<script>alert(\'Duplicated DesignNo\');</script>';
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>location.replace("../main_content.php?menu=designEditorNew");</script>';
			exit;
		} else if ($designRowCount === 1) {
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			if($result->designId === $designId){
			}else {
				echo '<script>alert(\'Duplicated DesignNo\');</script>';
				print_r($stmt->errorInfo());
				$db->rollBack();
				$db= NULL;
				echo '<script>location.replace("../main_content.php?menu=designEditorNew");</script>';
				exit;
			}
		} else {}
		$query = "UPDATE $tablename30 SET ";
		$query .= " total=:total";
		for($i=1;$i<30+1;$i++){
			if($i<count($elementArray)+1){
				$query .= ", info".$i." = :info".$i.", content".$i." = :content".$i;
			}else{
				$query .= ", info".$i." = null, content".$i." = null"; // fill by null unused elements
			}
		}
		
		$query .= ", modifiedBy =:modifiedBy";
		$query .= ", modifiedDate=:modifiedDate";
		$query .= ", activeDesign =:activeDesign";
		$query .= ", bgImgName =:bgImgName";
		$query .= ", designNo =:designNo";
		$query .= ", addedDate =:addedDate";
		$query .= ", cardType =:cardType";
		$query .= " WHERE designId = :designId";
		$stmt = $db->prepare($query);
		for($i=1;$i<count($elementArray)+1;$i++){
			//echo 'info'.$i.' '.$currentInfo.' '.'content'.$i.' '.$currentContent.'<br/>';
			$stmt->bindParam(":info$i", $elementArray[$i]->info);
			$stmt->bindParam(":content$i", $elementArray[$i]->content);
		}
		$total = count($elementArray);
		$stmt->bindParam(":total", $total);
		$stmt->bindParam(":modifiedBy", $modifiedBy);
		$stmt->bindParam(":modifiedDate", $today);
		$stmt->bindParam(":activeDesign", $activeDesign);
		$stmt->bindParam(":bgImgName", $bgImgName);
		$stmt->bindParam(":designNo", $designNo);
		$stmt->bindParam(":addedDate", $addedDate);
		$stmt->bindParam(":cardType", $cardType);
		$stmt->bindParam(":designId", $designId);
		echo $query;
		//echo '<br/>';
		if($stmt->execute()){
		}else{
			echo '<script>alert(\'modify ERROR1\');</script>';
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}
		//print_r($stmt->errorInfo());
		//$db->rollBack();
		$db->commit();
		$uploadDir = __DIR__.$goParent.$goParent2.$cardDesignDir;
		if(isset($_FILES['bgImg']['tmp_name'])){
			if($_FILES['bgImg']['error'] == 0){
				echo 'bgImg...Ok';
				$name = $_FILES['bgImg']['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				switch($ext){
					case 'jpg':
						$saveName = $bgImgName.'.'.$ext;
						if (!is_dir($uploadDir.$designId) ) {
							mkdir($uploadDir.$designId);
						}else{}						
						if(!move_uploaded_file($_FILES['bgImg']['tmp_name'], $uploadDir.$designId.'/'.$saveName)){
							echo '<script>alert(\'File Upload Error1\');</script>';
							//echo "<script> window.history.go(-1); </script>";
							exit;
						}
						else{}				
						break;
					default:
						echo "<script>alert('Can not upload {$ext} only JPG');</script>";
						echo "<script> window.history.go(-1); </script>";
						exit;
				}				
			}
			else if($_FILES['bgImg']['error'] == 1){
				$thisName = $_FILES['bgImg']['name'];
				echo "<script>alert('{$thisName} is Too Big');</script>";
				echo "<script> window.history.go(-1); </script>";
				exit;
			}else{
				echo 'bgImg...No';
			}
		}		
		$db= NULL;
		echo '<script>alert(\'Modify Complete\');</script>';
		if($formName === 'designEditor1'){
			echo '<script>location.replace("../main_content.php?menu=designEditor1&designId='.$designId.'");</script>';
		} else {
			echo '<script>location.replace("../main_content.php?menu=designEditor2&designId='.$designId.'");</script>';
		}
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		echo '<script>alert(\'INSERT error3\');</script>';
		print_r($stmt->errorInfo());
		$db= NULL;
		//echo "<script> window.history.go(-1); </script>";
		exit;
	}	

?>
<script>
	window.onload = function(){
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php //echo $target ?>" method="post"> 
	<button type="submit">
	</form> 
</body>
</html>