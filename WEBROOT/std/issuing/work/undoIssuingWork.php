<?php
// 210629 add expiredDate null process
// 210614 fix typo db query SELECT idMonthly,mCount FROM $tablename31 WHERE mYear :mYear
// 210605 add remove issuedlist
// 201114 check

	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']) ){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['appNo1']) && isset($_POST['appNo2']) && !empty($_POST['appNo1']) && $_POST['appNo1'] === $_POST['appNo2'] ){
		$appNo = preg_replace("/<|\/|_|>/","",$_POST['appNo2']);
	} else {
		echo 'Access Denied1';
		exit;
	}
	/*
	echo '<pre>';
	print_r($_POST);
	echo $appNo;
	echo '</pre>';
	*/
	$now1 = time();
	$currentDate = date("Y-m-d H:i:s",$now1);
	$currentYear = date("Y",$now1);
	$currentMonth = date("m",$now1);
	$modifiedBy = 'undo'.$_SESSION['valid_user'];
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent2.'/req.php';
		//require __DIR__.$goParent2.$reqDir1.'/_require/setting.php';	
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied2\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		//require __DIR__.$goParent2.$reqDir1.'/_require/encDec.php';
		$db->beginTransaction();
		$query = "SELECT * FROM $tablename07 WHERE appNo =:searchValue1 AND cardSerialNum IS NOT NULL";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo 'Access Denied4';
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			exit;
		}
		$queryMonthCalc = "SELECT idMonthly,mCount FROM $tablename31 WHERE mYear =:mYear AND mMonth = :mMonth AND mYearMonth =:mYearMonth";
		$stmtMonthCalc = $db->prepare($queryMonthCalc);
		$currentYearMonth = $currentYear.$currentMonth;
		$stmtMonthCalc->bindParam(':mYear', $currentYear);
		$stmtMonthCalc->bindParam(':mMonth', $currentMonth);
		$stmtMonthCalc->bindParam(':mYearMonth', $currentYearMonth);
		$stmtMonthCalc->execute();
		if($stmtMonthCalc->rowCount() == 1)
		{
			$resultMonthCalc = $stmtMonthCalc->fetch(PDO::FETCH_OBJ);
			if((int)$resultMonthCalc->mCount > 0)
			{
				$newMcount = (int)$resultMonthCalc->mCount - 1;
			}
			else
			{
				$newMcount = 0;
			}
			$queryMonthCalc2 = "UPDATE $tablename31 SET";
			$queryMonthCalc2 .= " mCount =:mCount";
			$queryMonthCalc2 .= " ,modifiedDate =:modifiedDate";
			$queryMonthCalc2 .= " ,modifiedBy =:modifiedBy";
			$queryMonthCalc2 .= " WHERE idMonthly = :idMonthly";
			$stmtMonthCalc2 = $db->prepare($queryMonthCalc2);
			$stmtMonthCalc2->bindParam(':mCount', $newMcount, PDO::PARAM_INT);
			$stmtMonthCalc2->bindParam(':modifiedDate', $currentDate);
			$stmtMonthCalc2->bindParam(':modifiedBy', $modifiedBy);
			$stmtMonthCalc2->bindParam(':idMonthly', $resultMonthCalc->idMonthly);
			if($stmtMonthCalc2->execute())
			{}
			else 
			{	
				echo 'Access Denied50';
				print_r($stmtMonthCalc2->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;
			}
		}
		else 
		{
		}
		
		
		$query2 = "UPDATE $tablename07 SET"; //remove serial number from application
		$query2 .= " issuedDate = NULL"; 
		$query2 .= ", sendDate = NULL";
		$query2 .= ", expiredDate = NULL";
		$query2 .= ", cardSerialNum = NULL";
		$query2 .= ", validityStart = NULL";
		$query2 .= ", validityEnd = NULL";
		$query2 .= ", modifiedDate = now()";
		$query2 .= ", modifiedBy = :inputValue1";
		$query2 .= " WHERE appNo = :searchValue1";
		$stmt2 = $db->prepare($query2);
		$stmt2->bindParam(':inputValue1', $modifiedBy);
		$stmt2->bindParam(':searchValue1', $appNo);
		$stmt2->execute();
		if($stmt2->rowCount() === 1){
		}
		else {
			echo 'Access Denied5';
			print_r($stmt2->errorInfo());
			$db->rollBack();
			$db= NULL;
			exit;
		}

		if($result->cardSerialNum === 'NoNeed'){
			$db->commit();
			$db= NULL;
			echo '<script>alert(\'Undo Issuing Complete\');</script>';
			echo "<script>window.opener.location.reload();</script>";
			echo "<script>self.close();</script>";
		}else{
			$queryIssued = "DELETE FROM $tablename32 WHERE iss_AppNo=:iss_AppNo";
			$stmtIssued = $db->prepare($queryIssued);
			$stmtIssued->bindParam(':iss_AppNo', $appNo);
			$stmtIssued->execute();
			if($stmtIssued ->rowCount() == 1){
			}
			else {
				echo 'Access Denied61';
				print_r($stmtIssued->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;
			}
			$query2 = "UPDATE $tablename11 SET"; //remove appNo from serialNumber
			$query2 .= " serAppNo = NULL, returned = 'yes'";
			$query2 .= " WHERE serAppNo = :searchValue1";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':searchValue1', $appNo);
			$stmt2->execute();
			if($stmt2->rowCount() === 1){
				$db->commit();
				$db= NULL;
				echo '<script>alert(\'Undo Issuing Complete\');</script>';
				echo "<script>window.opener.location.reload();</script>";
				echo "<script>self.close();</script>";
			}
			else {
				echo 'Access Denied6';
				print_r($stmt2->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;
			}
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}
	/*
	echo '<pre>';
	print_r($result);
	echo '</pre>';
	*/
?>
