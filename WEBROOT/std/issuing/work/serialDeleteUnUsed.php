<script>self.close();</script>
<?php 
session_start();
// 200615 check

	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	*/
	if(isset($_POST['formName']) && !empty($_POST['formName'])){
		$formName  = preg_replace("/<|\/|_|>/","",$_POST['formName']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;		
	}
	if(isset($_POST['stock']) && !empty($_POST['stock'])){
		$stockId  = preg_replace("/<|\/|_|>/","",$_POST['stock']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;			
	}	
	if(isset($_POST['addedBy']) && !empty($_POST['addedBy'])){
		$addedBy  = preg_replace("/<|\/|_|>/","",$_POST['addedBy']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;	
	}
	$now0 =time();
	$now1  =date("Y-m-d H:i:s",$now0);
	$modifiedBy = $_SESSION['valid_user'].'_serialDeleteUnUsed';
	$modifiedDate = $now1;	
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename10 WHERE stockId =:stockId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':stockId', $stockId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$stockInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($stockInfo->serialType) && !empty($stockInfo->serialType) && isset($stockInfo->serialName) && !empty($stockInfo->serialName) && isset($stockInfo->serialDesc) && !empty($stockInfo->serialDesc) && isset($stockInfo->addedDate) && !empty($stockInfo->addedDate) && isset($stockInfo->addedBy) && !empty($stockInfo->addedBy)){
		}else{
			echo '<script>alert(\'serial stock Info Error\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$timeStamp = @strtotime($stockInfo->addedDate);
		if(isset($timeStamp) && !empty($timeStamp) ){
		}else{
			echo '<script>alert(\'AddedDate Error\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$stockInfo->addedDate = date("Ymd_His",$timeStamp);
		$stockInfo->serialDesc = preg_replace('/\\\\|\/|:|\*|\?|\"|<|>|\|/',"_",$stockInfo->serialDesc);
		$fileName = 'Delete'.$stockInfo->serialType.'_'.$stockInfo->serialName.'_'.$stockInfo->serialDesc.'_'.$stockInfo->addedDate.'.csv';
		//echo $fileName;
		$query2 = "SELECT * FROM $tablename11 WHERE addedBy =:addedBy AND serAppNo IS NULL";
		$stmt2 = $db->prepare($query2);
		$stmt2->bindParam(':addedBy', $stockInfo->addedBy);
		$stmt2->execute();
		$serialCount = 0;
		while($result2 = $stmt2->fetch(PDO::FETCH_OBJ)){
			$serialList[$serialCount] = array (
			'serialNum' => $result2->serialNum, 
			'serAppNo' => $result2->serAppNo, 		
			'usedDate' => $result2->usedDate, 
			'usedBy' => $result2->usedBy, 
			'returned' => $result2->returned);
			$serialCount = $serialCount + 1;
		}
		if($serialCount >0){
			$db->beginTransaction();
			$queryDelete ="DELETE FROM $tablename11 WHERE addedBy =:addedBy AND serAppNo IS NULL";
			$stmtDelete = $db->prepare($queryDelete);
			$stmtDelete->bindParam(':addedBy', $stockInfo->addedBy);
			$stmtDelete ->execute();
			if($stmtDelete->rowCount() === $serialCount){
				$query = "UPDATE $tablename10 SET";
				$query .=" modifiedBy =:modifiedBy, modifiedDate =:modifiedDate";
				$query .=" WHERE stockId =:stockId";
				$stmt = $db->prepare($query);
				$stmt->bindParam(':modifiedBy', $modifiedBy);
				$stmt->bindParam(':modifiedDate', $modifiedDate);
				$stmt->bindParam(':stockId', $stockId);
				$stmt->execute();
				if($stmt->rowCount() === 1){
				}else{
					$db->rollBack();
					echo '<script>alert(\'Can not Delete\');</script>';
					echo '<script>location.replace("../main_content.php?menu=cardSerialDetail&stock='.$stockId.'");</script>';
					exit;
				}				
			}else{
				//echo $stmtDelete->rowCount();
				$db->rollBack();
				//print_r($stmtDelete->errorInfo());
				$db = NULL; 
				echo '<script>alert(\'Can not Delete\');</script>';
				//echo '<script>location.replace("../main_content.php?menu=cardSerialDetail&stock='.$stockId.'");</script>';
				exit;
			}
		}else{
			echo '<script>alert(\'No UnUsedSerial\');</script>';
			//echo '<script>location.replace("../main_content.php?menu=cardSerialDetail&stock='.$stockId.'");</script>';
			exit;
		}

		if(isset($serialList[0]) && !empty($serialList[0])){
			ob_end_clean();
			$fp = fopen('php://output', 'w');
			header("Content-Type:application/csv"); 
			header('Content-Disposition: attachment; filename="'.$fileName.'";');
			echo "\xEF\xBB\xBF"; // utf-8
			foreach($serialList[0] as $key => $arr) {
				fwrite($fp,$key);
				fwrite($fp,',');
			}
			fwrite($fp,"\r\n");	
			for($i=0;$i<$serialCount;$i++){
				fputcsv($fp, $serialList[$i]);
			}
			//fwrite($fp,"\r\n");
			//fwrite($fp,"Delete $serialCount serials");		
			fclose($fp);
			$db->commit();
			$db= NULL;
		}else{
			$db->rollBack();
			echo '<script>alert(\'No UnUsedSerial\');</script>';
			//echo '<script>location.replace("../main_content.php?menu=cardSerialDetail&stock='.$stockId.'");</script>';
		}
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		exit;
	}

?>