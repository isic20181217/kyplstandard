<?php 
// 210518 add docuDir
// 200612 check
	if (isset($_POST['status'])){
		$status = $_POST['status'];
	}
	else{
		echo '<script>alert(\'forbidden1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['filename'])){
		$filename = $_POST['filename'];
	}
	else{
		echo '<script>alert(\'forbidden2\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['appNo'])){
		$appNo = $_POST['appNo'];
	}
	else{
		echo '<script>alert(\'forbidden3\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	try 
	{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT docuDir FROM $tablename07 WHERE appNo = :appNo;";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appNo', $appNo);
		$stmt->execute();
		if($result = $stmt->fetch(PDO::FETCH_OBJ))
		{
			$docuDir = $result->docuDir;
		}
		else
		{
			$docuDir ='';
		}
	}
	catch (PDOExeception $e)
	{
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	//require __DIR__.$goParent2.$reqDir1.'_require/encDec.php';		
	if ($status === 'apply_manager'){
		//$now = time();
		//require __DIR__.$goParent2.$reqDir1.'_require/db_co.php';
		$tempdir = __DIR__.$goParent2.$reqDir1.$storageDir.$docuDir;
		$uploaddir = __DIR__.$goParent2.$reqDir1.$storageDir.$docuDir;
		$scale = $_POST['scale'];
		//$orifilename = pathinfo($filename);
		$realname = $_POST['filenameName'];
		$ext = $_POST['filenameExt'];
		$ext = strtolower($ext);
		$img = createimage($ext,$tempdir.$realname);
		//Check Image.
		switch($img){
			case 1:
				//unlink($tempdir.$filename);
				echo '<script>alert(\'Wrong Extension.\');</script>';
				//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
				exit;
			case 2:
				//unlink($tempdir.$filename);
				echo '<script>alert(\'JPG/PNG Only\');</script>';
				//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
				exit;
			case 3:
				//unlink($tempdir.$filename);
				echo '<script>alert(\'image create error.\');</script>';
				//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
				exit;
			default:
				/////////////////////////////////////////Crop/////////////////////////////////////////
				if($_POST['y2']>0){
					$croped_img = crop1($img, $_POST['x1'], $_POST['y1'], $_POST['x2'], $_POST['y2'], $scale);
					if($croped_img){
						$img = $croped_img; 
					}
					else{
						//unlink($tempdir.$filename);
						echo '<script>alert(\'Can not crop this image.\');</script>';
						//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
						exit;
					}
				}
				else{}
				/////////////////////////////////////////Crop/////////////////////////////////////////
				/////////////////////////////////////////Rotate/////////////////////////////////////////
				if($_POST['rotate'] === '0'){
				}
				else{
					$rotated_img = rotate1($img, $_POST['rotate']);
					if($rotated_img){
						$img = $rotated_img; 
					}
					else{
						//unlink($tempdir.$filename);
						echo '<script>alert(\'Cannot Rotate this Image.\');</script>';
						//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
						exit;
					}
				}
				/////////////////////////////////////////Rotate/////////////////////////////////////////
				////////////////////////////////////////Upload/////////////////////////////////////////
				/*if(copy($tempdir.$filename,$tempdir.$filename.'.old')){
				}
				else{
					//echo $uploaddir.$realname.'.jpg';
					echo '<script>alert(\'backup error\');</script>';
					//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
					exit;
				}*/
				if(imagejpeg($img,$uploaddir.$realname.'.jpg',100)){
					//unlink($tempdir.$filename); // delete tempfile
				}
				else{
					//echo $uploaddir.$realname.'.jpg';
					echo '<script>alert(\'upload error\');</script>';
					//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
					exit;
				}
				imagedestroy($img);
		}
	}
	else{
		echo '<script>alert(\'forbidden3\');</script>';
		//echo '<script>location.replace("https://www.isic.co.kr/");</script>';
		exit;
	}
	//Rotate function.
	function rotate1($img, $degrees){
		$rotated_img = imagerotate($img, -$degrees, 0);
		return $rotated_img;
	}
	//Crop function.
	function crop1($img, $x1, $y1, $x2, $y2, $scale){
		$x1 = $x1 / $scale;
		$y1 = $y1 / $scale;
		$x2 = $x2 / $scale;
		$y2 = $y2 / $scale;
		floor($x1);
		floor($y1);
		//Beware coordinates overflow.
		floor($x2);
		floor($y2);
		$w1 = abs($x2 - $x1);
		$h1 = abs($y2 - $y1);
		$croped_img = imagecreatetruecolor($w1, $h1);
		if($x1 < $x2 && $y1 < $y2){
			imagecopy($croped_img, $img, 0,0, $x1, $y1, $w1, $h1);
		}
		else if($x1 > $x2 && $y1 < $y2){
			imagecopy($croped_img, $img, 0,0, $x2, $y1, $w1, $h1);
		}
		else if($x1 < $x2 && $y1 > $y2){
			imagecopy($croped_img, $img, 0,0, $x1, $y2, $w1, $h1);
		}
		else if($x1 > $x2 && $y1 > $y2){
			imagecopy($croped_img, $img, 0,0, $x2, $y2, $w1, $h1);
		}
		else{
			return false;
		}
		return $croped_img;
	}
	function createimage($ext,$tmp_name){
		$createdimage;
		//echo $ext;
		switch($ext) {
			case 'jpeg':
			case 'jpg':
				//Is this really jpeg?
				$size = getimagesize($tmp_name.'.'.$ext);
				if($size[2] === 2){
					$createdimage = imagecreatefromjpeg($tmp_name.'.'.$ext);
					if ($createdimage === false){
						return 3;
					}
					else{
						break;
					}
				}
				else{
					return 1;
				}
			case 'png':
				//Is this really png?
				$size = getimagesize($tmp_name.'.'.$ext);
				if($size[2] === 3){
					$createdimage = @imagecreatefrompng($tmp_name.'.'.$ext);
					if ($createdimage === false){
						return 3;
					}
					else{
						break;
					}
				}
				else{
					return 1;
				}
			default:
				return 2;
		}
		return $createdimage;
	}
?>
<?php
	//echo $now;
?>