<script>self.close();</script>
<?php 
// 200615 check
session_start();


	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$officeId = (int)$_SESSION['officeId'];
	$currentDate = date("d/m/Y");
	/*
	echo '<pre>';
	print_r($_GET);
	echo '</pre>';*/
	//exit;
	$dateType = '';
	$searchType2 ='';
	$searchDate = 'no';
	$searchTwo = 'no';
	$searchFname = 'no';
	$searchLname ='no';
	$dateStartO  = '';
	$dateEndO = '';
	$engFnameO = '';
	$engLnameO = '';
	$searchTypeValue2O = '';
	$saveAsFile ='no';
	if(isset($_GET['file']) && $_GET['file'] === 'yes'){
		$saveAsFile ='yes';
	}else{}
	$dateTypeArray= array(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
	if(isset($_GET['dateType']) && !empty($_GET['dateType'])){
		$dateType  = preg_replace("/<|\/|_|>/","",$_GET['dateType']);
		$searchMode = 'searchMode';
		switch($dateType){
			case 'regDate':
				$dateTypeArray[0] ='selected';
				break;
			case 'approvedDate':
				$dateTypeArray[1] ='selected';
				break;
			case 'paidDate':
				$dateTypeArray[2] ='selected';
				break;
			case 'issuedDate':
				$dateTypeArray[3] ='selected';
				break;
			default:
				break;
		}
	}else{}	
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeIdSel= preg_replace("/<|\/|_|>/","", $_GET['officeId'] );
	}
	else{}
	if(isset($_GET['cardType']) && !empty($_GET['cardType'])){
		$cardType  = preg_replace("/<|\/|_|>/","",$_GET['cardType']);
		//$searchMode = 'searchMode';
	}else{}			
	if(isset($_GET['dateStart']) && !empty($_GET['dateStart']) && isset($_GET['dateEnd']) && !empty($_GET['dateEnd']) ){
		$searchDate ='yes';	
	}else{}
	
	if($searchDate ==='yes'){
		$dateStartO = preg_replace("/<|_|>/","",$_GET['dateStart']);
		$dateEndO = preg_replace("/<|_|>/","",$_GET['dateEnd']);
		$dateStartArray = explode('/',$dateStartO);
		$dateEndArray = explode('/',$dateEndO);
		$dateStart = $dateStartArray[2].'-'.$dateStartArray[1].'-'.$dateStartArray[0];
		$dateEnd = $dateEndArray[2].'-'.$dateEndArray[1].'-'.$dateEndArray[0];
	}else{}
	
	if(isset($_GET['searchType2Value']) && !empty($_GET['searchType2Value']) ){
		$searchTwo ='yes';
		$searchMode = 'searchMode';
	}else{}

	$searchType2Array= array(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
	if(isset($_GET['searchType2']) && !empty($_GET['searchType2'])){
		$searchType2 = preg_replace("/<|\/|_|>/","",$_GET['searchType2']);
		
		switch($searchType2){
			case 'born':		
				$searchType2Array[0] ='selected';
				break;
			case 'email':
				$searchType2Array[1] ='selected';
				break;
			case 'cid':
				$searchType2Array[2] ='selected';
				break;
			default:
				break;
		}
	}else{}	
	
	if($searchTwo ==='yes'){
		$searchTypeValue2O = preg_replace("/<|_|>/","",$_GET['searchType2Value']);
		if($searchType2 === 'born'){
			$searchTypeValue2Array = explode('/',$searchTypeValue2O);
			if(isset($searchTypeValue2Array[0])){
				if($searchTypeValue2Array[0][0] === '0'){
					$birthDaySearch =substr($searchTypeValue2Array[0],1,1);
				}else{
					$birthDaySearch =$searchTypeValue2Array[0];
				}
			}else{
				$birthDaySearch = '0';
			}
			if(isset($searchTypeValue2Array[1])){
				if($searchTypeValue2Array[1][0] === '0'){
					$birthMonthSearch =substr($searchTypeValue2Array[1],1,1);
				}else{
					$birthMonthSearch = $searchTypeValue2Array[1];
				}
			}else{
				$birthMonthSearch = '0';
			}
			if(isset($searchTypeValue2Array[2])){
				$birthYearSearch =  $searchTypeValue2Array[2];
			}else{
				$birthYearSearch =  '0';
			}
		}else{}
	}else{}
	if (isset($_GET['engFname']) && !empty($_GET['engFname'])){
		$engFnameO =  preg_replace("/<|_|>/","",$_GET['engFname']);
		$searchFname = 'yes';
		$searchMode = 'searchMode';
		
	}else{}
	if (isset($_GET['engLname']) && !empty($_GET['engLname'])){
		$engLnameO =  preg_replace("/<|_|>/","",$_GET['engLname']);
		$searchLname = 'yes';
		$searchMode = 'searchMode';
		
	}else{}
	$searchType = array("ASC","DESC");
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	$goParent ='/../';
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';
	$loadAppOnce = 100; // how many applications read from DB at once
	if (isset($searchMode) && $searchMode === 'searchMode'){
		try {
			require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
			$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':id', $_SESSION['id']);
			$stmt->bindParam(':no', $_SESSION['idNo']);
			$stmt->execute();
			if($stmt->rowCount() === 1 ){
				$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else {
				
				if(isset($_SESSION)){
					session_destroy();
				}else{}
				//print_r($stmt->errorInfo());
				echo '<script>alert(\'Please Login 99\');</script>';
				echo '<script>location.replace("/std/issuing/login.php");</script>';
				exit;
			}
			$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':officeId', $_SESSION['officeId']);
			$stmt->execute();
			if($stmt->rowCount() === 1 ){
				$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else {
				
				if(isset($_SESSION)){
					session_destroy();
				}else{}
				//print_r($stmt->errorInfo());
				echo '<script>alert(\'Please Login 99\');</script>';
				echo '<script>location.replace("/std/issuing/login.php");</script>';
				exit;
			}
			if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports === 'yes'  && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
			} else {
				echo '<script>alert(\'Access Denied1\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
			}
			$query = "SELECT count(appNo) as total FROM $tablename07";
			$stmt = $db->prepare($query);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$total = $result->total;
			$appReader = ceil($total/$loadAppOnce);
			$appStart = 0;
			$appCount = 0;
			for($i=0; $i<$appReader; $i++){
				if($appCount > 900){ //prevent over 1000 result
					break;
				}else{}
				$appStart = $i*$loadAppOnce;
				$appEnd = ($i+1)*$loadAppOnce;		
				$query = "SELECT appNo,birthDay,birthMonth,birthYear,engFname,engLname,email,customerId,encId FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE 1=1";
				if($searchDate === 'yes'){
					switch($dateType){
						case 'regDate':
							$query .=" AND date(regDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'approvedDate':
							$query .=" AND date(approvedDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'paidDate':
							$query .=" AND date(paidDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'issuedDate':
							$query .=" AND date(issuedDate) BETWEEN :dateStart AND :dateEnd";
							break;
						default:
							break;
					}
				}else{}
				if($cardType === 'ALL'){
				}else if(isset($cardType) && !empty($cardType)){
					$query .=" AND cardType = :cardType";
				}else{}
				if($officeId === 1){
					if($officeIdSel === 'ALL'){
					} else if ($officeIdSel === '1'){
						$query .= " AND (officeId = :inputValue11 OR officeId = 990)";
					} else {
						$query .= " AND officeId = :inputValue11";
					}
				} else {
					$query .= " AND officeId = :inputValue11";
				}
				$query .= " ORDER BY $tablename07.appNo $searchTypeSel LIMIT $appStart, $loadAppOnce";
				$stmt = $db->prepare($query);
				if($searchDate === 'yes'){
					switch($dateType){
						case 'regDate':
						case 'approvedDate':
						case 'paidDate':
						case 'issuedDate':
							$stmt->bindParam(':dateStart', $dateStart);
							$stmt->bindParam(':dateEnd', $dateEnd);
							break;
						default:
							break;
					}
				}else{}
				if($cardType === 'ALL'){
				}else if(isset($cardType) && !empty($cardType)){
					$stmt->bindParam(':cardType', $cardType);
				}else {}
				if($officeId === 1){
					if($officeIdSel === 'ALL'){
					}else{
						$stmt->bindParam(':inputValue11', $officeIdSel);
					}
				}else{
					$stmt->bindParam(':inputValue11', $officeId);
				}
				$stmt->execute();
				/*
				echo '<br/>';
				//echo $query;
				echo '<br/>';
				echo $stmt->rowCount();
				echo '<br/>';
				echo $searchDate.' '.$searchTwo.' '.$searchFname.' '.$searchLname;
				echo '<br/>';
				*/
				//print_r($stmt->errorInfo());
				while($result = $stmt->fetch(PDO::FETCH_OBJ)){				
					$isRight = 'no';
					if($searchDate ==='yes' && $searchTwo ==='no' && $searchFname === 'no' && $searchLname === 'no'){
						$isRight ='yes';
					}else{
						$isRight ='no';
					}
					if($searchTwo === 'yes'){
						switch($searchType2){
							case 'born':		
								if(decrypt1($result->birthDay, $result->encId) === $birthDaySearch && decrypt1($result->birthMonth, $result->encId) === $birthMonthSearch && decrypt1($result->birthYear, $result->encId) ===$birthYearSearch){
									$isRight ='yes';
								}else{
									$isRight ='no';
								}
								break;
							case 'email':
								if(strtolower(decrypt1($result->email, $result->encId)) === strtolower($searchTypeValue2O)){
									$isRight ='yes';
								}
								else{
									$isRight ='no';
								}
								break;
							case 'cid':
								if($result->customerId === $searchTypeValue2O){
									$isRight ='yes';
								}
								else{
									$isRight ='no';
								}
								break;
							default:
								break;
						}
					}else{}
					if($searchFname === 'yes'){
						if(strtolower(decrypt1($result->engFname, $result->encId)) === strtolower($engFnameO)){
							$isRight ='yes';
						}
						else{
							$isRight ='no';
						}
					}else{}
					if($searchLname ==='yes'){
						if(strtolower(decrypt1($result->engLname, $result->encId)) === strtolower($engLnameO)){
							$isRight ='yes';
						}
						else{
							$isRight ='no';
						}
					}else{}
				
					if($isRight === 'yes'){
						//echo " $appStart $appEnd ";
						//echo " $result->appNo ";
						$query2 = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE appNo = $result->appNo;";
						$stmt2 = $db->prepare($query2);
						$stmt2->execute();
						$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
						if(isset($result2->cardPrice) && (float)$result2->cardPrice > 0){
							$paymentAmount = sprintf("%.2f",(float)$result2->cardPrice + (float)$result2->shippingPrice);
						}else{
							$paymentAmount =0;
						}
						/*
						$applist[$appCount] = array (
						'appNo' => $result2->appNo, 
						'engFname' => decrypt1($result2->engFname, $result2->encId),
						'engLname' => decrypt1($result2->engLname, $result2->encId), 
						'photoName' => $result2->photoName, 
						'cardType' => $result2->cardType,
						'issuedDate' => $result2->issuedDate, 
						'printedDate' => $result2->printedDate,
						'school' => decrypt1($result2->school, $result2->encId), 
						'birthYear' => decrypt1($result2->birthYear, $result2->encId), 
						'birthMonth' => decrypt1($result2->birthMonth, $result2->encId), 
						'birthDay' => decrypt1($result2->birthDay, $result2->encId), 
						'name1' => decrypt1($result2->name1, $result2->encId), 
						'officeId' => $result2->officeId, 
						'status' => $result2->status, 
						'regDate' => $result2->regDate, 
						'paidDate' => $result2->paidDate,
						'approvedDate' => $result2->approvedDate,
						'customerId' => $result2->customerId,
						'address' => decrypt1($result2->address, $result2->encId), 
						'city' => decrypt1($result2->city, $result2->encId), 
						'zipcode' => decrypt1($result2->zipcode, $result2->encId),
						'orderNo' => $result2->orderNo,
						'cardSerialNum' => $result2->cardSerialNum,
						'paymentAmount' => $paymentAmount,
						'revaNum' => $result2->revaNum,
						'paymentType' => $result2->paymentType);
						*/
						$applist[$appCount] = array (
						'cardType' => $result2->cardType, 
						'name' => decrypt1($result2->engFname, $result2->encId).' '.decrypt1($result2->engLname, $result2->encId),
						'cardSerialNum' => $result2->cardSerialNum, 
						'email' => decrypt1($result2->email, $result2->encId), 
						'emailNews' => $result2->emailNews);
						$appCount = $appCount + 1;
						
					}else{}
				}
			}
			$db= NULL;
		}
		catch (PDOExeception $e){
			echo "Error: ".$e->getMessage();
			$db= NULL;
			exit;
		}
		if(isset($applist[0]) && !empty($applist[0])){
			$now = date('YmdHis');
			ob_end_clean();
			$fp = fopen('php://output', 'w');
			//header('Content-Type: text/csv; charset=utf-8');
			header("Content-Type:application/csv"); 
			header('Content-Disposition: attachment; filename="'.'file'.$now.'.csv";');
			echo "\xEF\xBB\xBF";
			//$array = array($applist);
			//fputcsv($fp, $applist);
			foreach($applist[0] as $key => $arr) {
				fwrite($fp,$key);
				fwrite($fp,',');
			}
			fwrite($fp,"\r\n");	
			//fwrite($fp,$dbColumnNames1);
			
			for($i=0;$i<$appCount;$i++){
				/*
				foreach($applist[$i] as $arr) {
					if($arr){
						fwrite($fp,$arr);
						fwrite($fp,',');
					}else{
						fwrite($fp,'NULL');
						fwrite($fp,',');
					}
				}
				fwrite($fp,"\r\n");
				*/
				fputcsv($fp, $applist[$i]);
			}
			
			fclose($fp);
		}else{}
	}else{}
?>