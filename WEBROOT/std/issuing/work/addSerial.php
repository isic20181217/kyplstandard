<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>addStock</title>
<?php
// 210510 add excel file process
$goParent ='/..';
$goParent2 ='/../..';
$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
require __DIR__.$goParent2.'/req.php';
require __DIR__.$goParent2.$reqDir1.'/_require1/excelAddon.php';	
// 200814 add serial number check;
// 200813 add serialnumber length check
// 200604 checked
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($_SESSION['user_type'])
		{
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (isset($_POST['formName']))
	{
		$formName =$_POST['formName'];
	}	
	else
	{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_FILES['file1']['tmp_name']) && !empty($_FILES['file1']['tmp_name']))
	{
		if(is_uploaded_file($_FILES['file1']['tmp_name']))
		{
			$name = $_FILES['file1']['name'];	
			$orifilename = pathinfo($name);
			$ext = $orifilename['extension'];
			$ext = strtolower($ext);
			switch($ext)
			{
				case 'csv':
					$handle = fopen($_FILES['file1']['tmp_name'], "r");				
					$i = 0;
					while(($data = fgetcsv($handle,",")) !== FALSE)
					{
						//echo $i.' ';
						//echo $i.' 0 '.$data[0][0].' 1 '.$data[0][1].' 2 '.$data[0][2].' 3 '.$data[0][3].' 4 '.$data[0][4].' ';
						if($data[0][0].$data[0][1].$data[0][2]==chr(hexdec('EF')).chr(hexdec('BB')).chr(hexdec('BF')))
						{
						   $data[0] = substr($data[0],3);
						   //echo $data[0][0];
						}
						else
						{
							//echo ' ok ';
							//echo $data[0][0];
						}						
						$fileArray[$i]= $data[0];
						$i++;
						//print_r ($data[0]);
						//echo '<br/>';
					}
					//echo '<pre>';
					//print_r($fileArray);
					//echo '</pre>';
					//exit;

					//$fileArray = file($_FILES['file1']['tmp_name']);
					break;
				case 'xls':
				case 'xlsx':
					/* 
					use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
					use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
					use PhpOffice\PhpSpreadsheet\IOFactory;
					*/
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['file1']['tmp_name']);
					$spreadData = $spreadsheet-> getActiveSheet()->toArray();
					/*
					echo '<pre>';
					print_r($spreadData);
					echo '</pre>';
					*/
					break;
				default:
					echo "<script> window.history.go(-1); </script>";
					echo "<script>alert('Can not upload {$ext}');</script>";
					exit;
			}
		}
		else
		{
			echo "<script> window.history.go(-1); </script>";
			echo '<script>alert(\'File Upload Error2\');</script>';
			exit;
		}
	}
	else
	{
		echo '<script>alert(\'Not Enough Values(File)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if(isset($_POST['serialName']) && !empty($_POST['serialName']))
	{
		$serialName = preg_replace("/<|\/|_|>/"," ",$_POST['serialName']);	
	}
	else
	{
		echo '<script>alert(\'Not Enough Values(serialName)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if(isset($_POST['serialDesc']) && !empty($_POST['serialDesc'])){
		$serialDesc = preg_replace("/<|\/|_|>/"," ",$_POST['serialDesc']);	
	}
	else
	{
		echo '<script>alert(\'Not Enough Values(serialDesc)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if(isset($_POST['serialType1']) && !empty($_POST['serialType1'])){
		$curSerialType1 = $_POST['serialType1'];
	}
	else
	{
		echo '<script>alert(\'Not Enough Values(Serial Type)\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}			
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	$addedBy= $_SESSION['valid_user'].'_'.time();
	if(isset($usePhpSpreadsheet) && $usePhpSpreadsheet === 'yes')
	{
		$arraySize = count($spreadData);
		$serialNumStart = $spreadData[1][0];
		$serialNumStart = str_replace(array("\n", "\r", " ", ",", ";"),'',$serialNumStart);
		$serialNumEnd = $spreadData[$arraySize-1][0];
		$serialNumEnd = str_replace(array("\n", "\r", " ", ",", ";"),'',$serialNumEnd);
	}
	else
	{
		$arraySize = count($fileArray);
		$serialNumStart = $fileArray[0];
		$serialNumStart = str_replace(array("\n", "\r", " ", ",", ";"),'',$serialNumStart);
		$serialNumEnd = $fileArray[$arraySize-1];
		$serialNumEnd = str_replace(array("\n", "\r", " ", ",", ";"),'',$serialNumEnd);
	}
	$used = 0;
	echo 'Start '.$serialNumStart;
	echo '<br />';
	echo 'END '.$serialNumEnd;
	echo '<br />';
	echo 'Total '.$arraySize;
	echo '<br />';
	echo 'Please Wait...<br />';
	$okSerial = 0;
	try 
	{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$db->beginTransaction();
		for ($i=0;$i<$arraySize;$i++)
		{
			echo $i.'<br/>';
			$serialNum = ' ';
			if(isset($usePhpSpreadsheet) && $usePhpSpreadsheet === 'yes')
			{
				$serialNum = $spreadData[$i][0];
			}
			else
			{
				$serialNum = $fileArray[$i];
			}
			$serialNum = str_replace(array("\n", "\r", " ", ",", ";"),'',$serialNum);
			$type ='notOk';
			if (strlen($serialNum) == 14)
			{} 
			else 
			{
				echo strlen($serialNum);
				echo '<script>alert(\'Serialnumber problem333('.$serialNum.')\');</script>';
				exit;
			}
			switch($serialNum[13])
			{
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
					break;
				default:
						echo '<script>alert(\'Serialnumber problem22('.$serialNum.')\');</script>';
						//echo "<script> window.history.go(-1); </script>";
						exit;
			}
			for($j=1; $j<13;$j++)
			{
				switch($serialNum[$j])
				{
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case '0':
						break;
					default:
						echo '<script>alert(\'Serialnumber problem2('.$serialNum.')\');</script>';
						//echo "<script> window.history.go(-1); </script>";
						exit;
				}
			}
			switch($serialNum[0])
			{
				case 'S':
					if($curSerialType1 === 'ISIC')
					{
						$type = 'ISIC';
					}
					else
					{
						$db->rollBack();
						$db= NULL;
						echo '<script>alert(\'serialType Not match('.$serialNum.')\');</script>';
						//echo "<script> window.history.go(-1); </script>";
						exit;
					}
					break;
				case 'T':
					if($curSerialType1 === 'ITIC')
					{
						$type = 'ITIC';
					}
					else
					{
						$db->rollBack();
						$db= NULL;
						echo '<script>alert(\'serialType Not match('.$serialNum.')\');</script>';
						//echo "<script> window.history.go(-1); </script>";
						exit;
					}
					break;
				case 'Y':
					if($curSerialType1 === 'IYTC')
					{
						$type = 'IYTC';
					}
					else
					{
						$db->rollBack();
						$db= NULL;
						echo '<script>alert(\'serialType Not match('.$serialNum.')\');</script>';
						//echo "<script> window.history.go(-1); </script>";
						exit;
					}
					break;
				default:
					$db->rollBack();
					$db= NULL;
					echo '<script>alert(\'serialType Not match('.$serialNum.')\');</script>';
					//echo "<script> window.history.go(-1); </script>";
					exit;
			}
			if($type === 'notOK')
			{
				continue;
			}
			else
			{}
			$query = "INSERT INTO $tablename11 (";
			$query .= "serType, serialNum, addedDate,addedBy, serialName)";
			$query .= " VALUES (:inputValue1, :inputValue2, now(), :inputValue3,:inputValue4)";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $type);
			$stmt->bindParam(':inputValue2', $serialNum);
			$stmt->bindParam(':inputValue3', $addedBy);
			$stmt->bindParam(':inputValue4', $serialName);
			if($stmt->execute())
			{
				echo "{$serialNum}<br />";
				ob_flush();
				flush();
				if ($okSerial == 0)
				{
					$serialNumStart = $serialNum;
				}else if($okSerial > 0)
				{
					$serialNumEnd = $serialNum;
				}
				else
				{
					echo '<script>alert(\'INSERT ERROR12\');</script>';
					$db->rollBack();
					$db= NULL;
					echo "<script> window.history.go(-1); </script>";
					exit;
				}
				$okSerial++;
			}
			else 
			{
				print_r($stmt->errorInfo());
				echo " ERROR at {$serialNum}<br />";
			}
		}
		if ($okSerial > 2)
		{
			$query = "INSERT INTO $tablename10 (";
			$query .= "
			serialType,
			serialName, 
			serialDesc,
			serialNumStart, 
			serialNumEnd, 
			used, 
			total, 
			addedBy, 
			addedDate
			)";
			$query .= " VALUES (
			:inputValue1, 
			:inputValue2, 
			:inputValue3, 
			:inputValue4, 
			:inputValue5, 
			:inputValue6, 
			:inputValue7, 
			:inputValue8, 
			now())";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $type);
			$stmt->bindParam(':inputValue2', $serialName);
			$stmt->bindParam(':inputValue3', $serialDesc);
			$stmt->bindParam(':inputValue4', $serialNumStart);
			$stmt->bindParam(':inputValue5', $serialNumEnd);
			$stmt->bindParam(':inputValue6', $used);
			$stmt->bindParam(':inputValue7', $okSerial);
			$stmt->bindParam(':inputValue8', $addedBy);
			if($stmt->execute())
			{
				echo "completed<br />";
			}
			else 
			{
				echo '<script>alert(\'INSERT ERROR1\');</script>';
				$db->rollBack();
				$db= NULL;
				print_r($stmt->errorInfo());
				//echo "<script> window.history.go(-1); </script>";
				exit;
			}
			$db->commit();
			$target ='../main_content.php?menu=cardSerial';
			$db=NULL;
		}
		else
		{
			echo '<script>alert(\'INSERT ERROR22\');</script>';
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}
	}
	catch (PDOExeception $e)
	{
		//echo "Error: ".$e->getMessage();
		echo '<script>alert(\'INSERT error3\');</script>';
		$db= NULL;
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
?>
<script>
	window.onload = function()
	{
		document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	</form> 
</body>
</html>