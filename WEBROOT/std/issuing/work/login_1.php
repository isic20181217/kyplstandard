<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<?php 
// 201114 check
	if (empty($_POST['id']))
	{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	else
	{}
	$inputid = $_POST['id'];
	$inputpwd =$_POST['passwd'];
	$userIP = $_SERVER['REMOTE_ADDR'];
	$now = time();
	$today = date("Y-m-d H:i:s",$now);
	try 
	{
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
		require __DIR__.$goParent2.'/req.php';
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT no,id,pwd,role,officeId FROM $tablename06 WHERE id = :searchValue1 AND activeStaff =:activeStaff";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $inputid);
		$activeStaff ='yes';
		$stmt->bindParam(':activeStaff', $activeStaff);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		if($result && $inputid === $result->id && password_verify($inputpwd, $result->pwd))
		{
			$query = "INSERT INTO $tablename08 (";
			$query .= "id, loginDate, userIP)";
			$query .= " VALUES (:inputValue1, :loginDate, :inputValue2)";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $inputid);
			$stmt->bindParam(':inputValue2', $userIP);
			$stmt->bindParam(':loginDate', $today);
			if($stmt->execute())
			{
				session_start();
				$_SESSION['valid_user'] =$inputid;
				if ($result->role === 'manager')
				{
					$_SESSION['user_type'] = 'Manager';
					$_SESSION['id'] = $result->id;
					$_SESSION['idNo'] = $result->no;
					$target ='../main_content.php';
				}
				else if ($result->role === 'issuer')
				{
					$_SESSION['user_type'] = 'Issuer';
					$_SESSION['id'] = $result->id;
					$_SESSION['idNo'] = $result->no;
					$target ='../main_content.php';
				}
				else if ($result->role === 'viewer')
				{
					$_SESSION['user_type'] = 'Viewer';
					$_SESSION['id'] = $result->id;
					$_SESSION['idNo'] = $result->no;
					$target ='../main_content.php';
				}
				else
				{
					echo '<script>alert(\'Login ERROR2_R\');</script>';
					echo '<script>location.replace("/std/issuing/login.php");</script>';
					exit;
				}
				$userIP = $_SERVER['REMOTE_ADDR'];
				$_SESSION['staffUserIP'] = $userIP;
				$_SESSION['officeId'] = $result->officeId;
				if(strlen($inputpwd) < 4)
				{
					$_SESSION['warning1'] = 'YES';
		  
				}
				else
				{}			
			}
			else 
			{
				echo '<script>alert(\'Login ERROR1 (Check ID and Password)\');</script>';
				echo '<script>location.replace("/std/issuing/login.php");</script>';
				exit;
			}
			$query2 = "SELECT name,officeDesc FROM $tablename12 WHERE officeId = :inputValue1 AND activeOffice = :activeOffice";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':inputValue1', $_SESSION['officeId']);
			$activeOffice1 ='yes';
			$stmt2->bindParam(':activeOffice', $activeOffice1);
			$stmt2->execute();
			if($result2 = $stmt2->fetch(PDO::FETCH_OBJ))
			{
				$_SESSION['officeName'] = $result2->name;
				$_SESSION['officeDesc'] = $result2->officeDesc;
			}
			else
			{
				echo '<script>alert(\'Login ERROR4\');</script>';
				if(isset($_SESSION))
				{
					session_destroy();
				}
				else
				{}
				echo '<script>location.replace("/std/issuing/login.php");</script>';
				exit;
			}
		}
		else {
			if(isset($_SESSION))
			{
				session_destroy();
			}
			else
			{}
			echo '<script>alert(\'ID or Password Not mach\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$db= NULL;
	}
	catch (PDOExeception $e)
	{
		//echo "Error: ".$e->getMessage();
		if(isset($_SESSION))
		{
			session_destroy();
		}
		else
		{}
		$db= NULL;
		exit;
	}
?>
<script>
	window.onload = function(){
		document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	</form> 
</body>
</html>