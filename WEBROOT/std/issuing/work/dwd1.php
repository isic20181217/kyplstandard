<?php
// 200813 fix typo
// download proof documents
// 200612 check		
session_start();
if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
{
	$user = $_SESSION['valid_user'];
	switch($user_type = $_SESSION['user_type'])
	{
		case 'Manager':
			break;
		default:
			echo '<script>alert(\'Please login.\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
	}
}
else
{
	echo '<script>alert(\'Please login.\');</script>';
	echo '<script>location.replace("/std/index.php");</script>';
	exit;
}
if (isset($_GET['fname']) && !empty($_GET['fname']) && isset($_GET['appNo']) && !empty($_GET['appNo']))
{
	$filename = preg_replace("/<|\/|>/","",$_GET['fname']);
	$appNo = preg_replace("/<|\/|>/","",$_GET['appNo']);
} 
else 
{
	echo '<script>alert(\'forbidden2\');</script>';
	echo '<script>location.replace("/std/index.php");</script>';
	exit;
}
$goParent ='/..';
$goParent2 ='/../..';
$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
require __DIR__.$goParent2.'/req.php';
require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
try 
{
	require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
	$query = "SELECT docuDir FROM $tablename07 WHERE appNo = :appNo;";
	$stmt = $db->prepare($query);
	$stmt->bindParam(':appNo', $appNo);
	$stmt->execute();
	if($result = $stmt->fetch(PDO::FETCH_OBJ))
	{
		$docuDir = $result->docuDir;
	}
	else
	{
		$docuDir ='';
	}
}
catch (PDOExeception $e)
{
	echo "Error: ".$e->getMessage();
	$db= NULL;
	exit;
}
$path = __DIR__.$goParent2.$reqDir1.$storageDir.$docuDir. $filename;
/*
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($path));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($path));
ob_clean();
flush();
readfile($path);
exit;
*/
$isFileExists = file_exists($path);
if($isFileExists)
{
	$fullName=pathinfo($filename);
	if(isset($fullName['extension']))
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		//header('Content-Disposition: attachment; filename=yourfile1.'.$fullName['extension']);
		header('Content-Disposition: attachment; filename='.basename($path));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path));
		ob_clean();
		flush();
		readfile($path);
	}
	else
	{}
}
else
{}

exit;
?>