<?php
// file download from /files/
// 200928 add new
session_start();
if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
{
	$user = $_SESSION['valid_user'];
	switch($user_type = $_SESSION['user_type'])
	{
		case 'Manager':
			break;
		default:
			echo '<script>alert(\'Please login.\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
	}
}
else
{
	echo '<script>alert(\'Please login.\');</script>';
	echo '<script>location.replace("/std/index.php");</script>';
	exit;
}
if (isset($_GET['fname']) && !empty($_GET['fname']))
{
	$filename = preg_replace("/<|\/|>/","",$_GET['fname']);
	switch($filename)
	{
		case 'bulk':
			$filename1 = '190724_ISIC_BulkUploadFormat_2.xlsx';
			break;
		case 'kyplsoft':
			$filename1 = 'KyplSoftwareConditions-eng.pdf';
			break;
		default:
			exit;
	}
} 
else 
{
	echo '<script>alert(\'forbidden2\');</script>';
	echo '<script>location.replace("/std/index.php");</script>';
	exit;
}
$goParent ='/..';
$goParent2 ='/../..';
$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
require __DIR__.$goParent2.'/req.php';
require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
$path = __DIR__.$goParent2.$reqDir1.'/files/'. $filename1;
$isFileExists = file_exists($path);
if($isFileExists)
{
	$fullName=pathinfo($filename1);
	if(isset($fullName['extension']))
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		//header('Content-Disposition: attachment; filename=yourfile1.'.$fullName['extension']);
		header('Content-Disposition: attachment; filename='.basename($path));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path));
		ob_clean();
		flush();
		readfile($path);
	}
	else
	{
		echo 'no1';
	}
}
else
{
	echo 'no';
}

exit;
?>