<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<?php 
// 201114 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($user_type = $_SESSION['user_type'])
		{
			case 'Issuer':
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['id1']) && !empty($_POST['id1']) && isset($_POST['password1']) && !empty($_POST['password1']) )
	{
		
		$newId = preg_replace("/<|\/|_|>/","", $_POST['id1']);
		$newPassword = $_POST['password1'];
		if(strlen($newId) < 4){
			echo '<script>alert(\'id is too short(longer then 3)\');</script>';
			echo '<script>location.replace("/std/issuing/main_content.php?menu=staffId");</script>';
		}
		else
		{}
		if(strlen($newPassword) < 4){
			echo '<script>alert(\'password is too short(longer then 3)\');</script>';
			echo '<script>location.replace("/std/issuing/main_content.php?menu=staffId");</script>';
		}else
		{
			$newPassword = password_hash($newPassword, PASSWORD_BCRYPT);
		}
	}
	else
	{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	$modifyUser = $_SESSION['valid_user'];
	try 
	{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT * FROM $tablename06 WHERE id = :modifyUser;";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':modifyUser', $modifyUser);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		if($stmt->rowCount() === 1)
		{
			$query = "UPDATE $tablename06 SET";
			$query .= " id=:newId, pwd=:newPassword, modifiedDate=now()";
			$query .= " WHERE no=:no1";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':newId', $newId);
			$stmt->bindParam(':newPassword', $newPassword);
			$stmt->bindParam(':no1', $result->no);
			if($stmt->execute())
			{
				switch($_SESSION['user_type'])
				{
					case 'Manager';
					case 'Issuer';
						$target ='../main_content.php?menu=staffid';
						break;
					default:
						echo '<script>alert(\'You are not issuer.\');</script>';
						echo '<script>location.replace("../../index.php");</script>';
						exit;
				}
				$_SESSION['warning1'] = 'NO';
				echo 'complete';
				$target ='../main_content.php?menu=staffId';
				$db= NULL;
			}
			else 
			{
				$db= NULL;
				echo '<script>alert(\'modify ERROR1\');</script>';
				echo "<script> window.history.go(-1); </script>";
				exit;
			}	
		}
		else 
		{
			$db= NULL;
			echo '<script>alert(\'modify ERROR2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$db= NULL;
		$_SESSION['valid_user'] = $newId;
		$_SESSION['id'] = $newId;
	}
	catch (PDOExeception $e)
	{
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo '<script>alert(\'modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	</form> 
</body>
</html>