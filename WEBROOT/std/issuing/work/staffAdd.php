<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>addStaff</title>
<?php
// 200615 check

	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				try {
					$goParent ='/..';
					$goParent2 ='/../..';
					$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
					require __DIR__.$goParent2.'/req.php';
					require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
					$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
					$stmt = $db->prepare($query);
					$stmt->bindParam(':id', $_SESSION['id']);
					$stmt->bindParam(':no', $_SESSION['idNo']);
					$stmt->execute();
					if($stmt->rowCount() === 1 ){
						$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
					} else {
						if(isset($_SESSION)){
							session_destroy();
						}else{}
						//print_r($stmt->errorInfo());
						echo '<script>alert(\'Please Login 99\');</script>';
						echo '<script>location.replace("/std/issuing/login.php");</script>';
						exit;
					}
					$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
					$stmt = $db->prepare($query);
					$stmt->bindParam(':officeId', $_SESSION['officeId']);
					$stmt->execute();
					if($stmt->rowCount() === 1 ){
						$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
					} else {
						if(isset($_SESSION)){
							session_destroy();
						}else{}
						//print_r($stmt->errorInfo());
						echo '<script>alert(\'Please Login 99\');</script>';
						echo '<script>location.replace("/std/issuing/login.php");</script>';
						exit;
					}	
					$db= NULL;
				}
				catch (PDOExeception $e){
					//echo "Error: ".$e->getMessage();
					if(isset($_SESSION)){
						session_destroy();
					}else{}
					$db= NULL;
					echo '<script>alert(\'Please Login 999\');</script>';
					echo '<script>location.replace("/std/index.php");</script>';
					exit;
				}
				if(isset($resultUserInfo->sMenuIo) && $resultUserInfo->sMenuIo ==='yes' && isset($resultOfficeInfo->menuIo) && $resultOfficeInfo->menuIo ==='yes' ){
				}else{
					echo '<script>alert(\'Please Login 9999\');</script>';
					echo '<script>location.replace("/std/index.php");</script>';
					exit;
				}
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (isset($_POST['formName'])){
		$formName= preg_replace("/<|\/|_|>/","",$_POST['formName'] );
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['officeId']) && !empty($_POST['officeId'])){
		$officeId= preg_replace("/<|\/|_|>/","",$_POST['officeId'] );
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['id1']) && !empty($_POST['id1'])){
		$id1 = $_POST['id1'];
		if(strlen($id1) < 4){
			echo '<script>alert(\'id is too short(longer then 3)\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}else if (strlen($id1) > 20){
			echo '<script>alert(\'id is too long(shorter then 21)\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}else{
			$id1= preg_replace("/<|\/|_|>/","",$_POST['id1'] );
		}
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['password1']) && !empty($_POST['password1'])){
		$password1 = $_POST['password1'];
		if(strlen($password1) < 4){
			echo '<script>alert(\'password is too short(longer then 3)\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}else if (strlen($password1) > 20){
			echo '<script>alert(\'password is too long(shorter then 21)\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}else{
			$password1 = password_hash($password1, PASSWORD_BCRYPT);
		}
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	$today = date("Y-m-d H:i:s");
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$db->beginTransaction();		
		$query = "SELECT no FROM $tablename06 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->execute();
		if($stmt->rowCount() > 0){
			$db= NULL;
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Forbidden\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}else{}	
		/*
		$query = "SELECT officeId FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->execute();
		if($stmt->rowCount() > 0){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			$db= NULL;
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Forbidden\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		*/		
		
		$query = "INSERT INTO $tablename06 (modifiedDate, modifiedBy";
		$query .= ", id";
		$query .= ", pwd ";
		$query .= ", role ";
		$query .= ", officeId ";
		$query .= ", activeStaff ";
		$query .= ", sCanUrl ";
		$query .= ", sCanCsv ";
		$query .= ", sCanValidate ";
		$query .= ", sCanIssue ";
		$query .= ", sCanPrint ";
		$query .= ", sCanPrintLabel ";
		$query .= ", sMenuIssuing ";
		$query .= ", sMenuReports ";
		$query .= ", sMenuSerial ";
		$query .= ", sMenuIo";
		$query .= ", sMenuApplication ";
		$query .=")";
		$query .= " VALUES (";
		$query .= ":modifiedDate";
		$query .= ", :modifiedBy ";
		$query .= ", :id";
		$query .= ", :pwd ";
		$query .= ", :role ";
		$query .= ", :officeId ";
		$query .= ", :activeStaff ";
		$query .= ", :sCanUrl ";
		$query .= ", :sCanCsv ";
		$query .= ", :sCanValidate ";
		$query .= ", :sCanIssue ";
		$query .= ", :sCanPrint ";
		$query .= ", :sCanPrintLabel ";
		$query .= ", :sMenuIssuing ";
		$query .= ", :sMenuReports ";
		$query .= ", :sMenuSerial ";
		$query .= ", :sMenuIo ";
		$query .= ", :sMenuApplication ";
		$query .=")";
		echo $query;
		$role ='issuer';
		$activeStaff ='yes';
		$sCanUrl ='yes';
		$sCanCsv ='yes';
		$sCanValidate ='yes';
		$sCanIssue ='yes';
		$sCanPrint ='yes';
		$sCanPrintLabel ='no';
		$sMenuIssuing ='yes';
		$sMenuReports ='yes';
		$sMenuSerial ='no';
		$sMenuIo ='no';
		$sMenuApplication = 'no';
		$stmt = $db->prepare($query);
		$stmt->bindParam(':modifiedDate', $today);
		$stmt->bindParam(':modifiedBy', $_SESSION['valid_user']);
		$stmt->bindParam(':activeStaff', $activeStaff);
		$stmt->bindParam(':id', $id1);
		$stmt->bindParam(':pwd', $password1);
		$stmt->bindParam(':role', $role);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->bindParam(':sCanUrl', $sCanUrl);
		$stmt->bindParam(':sCanCsv', $sCanCsv);
		$stmt->bindParam(':sCanValidate', $sCanValidate);
		$stmt->bindParam(':sCanIssue', $sCanIssue);
		$stmt->bindParam(':sCanPrint', $sCanPrint);
		$stmt->bindParam(':sCanPrintLabel', $sCanPrintLabel);
		$stmt->bindParam(':sMenuIssuing', $sMenuIssuing);
		$stmt->bindParam(':sMenuReports', $sMenuReports);
		$stmt->bindParam(':sMenuSerial', $sMenuSerial);
		$stmt->bindParam(':sMenuIo', $sMenuIo);
		$stmt->bindParam(':sMenuApplication', $sMenuApplication);
		if($stmt->execute()){
		}else {
			//print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'Error2\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$target ='../main_content.php?menu=issuingOfficeDetail&officeId='.$officeId;
		$db->commit();
		$db= NULL;
		echo '<script>location.replace("'.$target.'");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php echo $target ?>" method="post"> 
	<input type="submit">
	</form> 
</body>
</html>