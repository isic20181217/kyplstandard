<?php
// 200616 check		
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['appSettingId']) && !empty($_POST['appSettingId']){
		$appSettingId = preg_replace("/<|>/","",$_POST['appSettingId']);
	} else {
		echo '<script>alert(\'forbidden2\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$today = date("Y-m-d H:i:s");   
	$modifiedBy= $_SESSION['valid_user'];
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';

	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$db->beginTransaction();
		$query = "SELECT appNo FROM $tablename07 WHERE appSettingIdApp = :appSettingIdApp";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingIdApp', $appSettingId);
		$stmt->execute();
		//echo $query;
		//print_r($stmt->errorInfo());
		if($stmt->rowCount() == 0){
		}else{
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'This application is still using (appNo='.$result->appNo.') \');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$query = "DELETE FROM $tablename27 WHERE appSettingId = :appSettingId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingId', $appSettingId);
		$stmt->execute();
		if($stmt->rowCount() == 1){
		}
		else {
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'DELETE ERROR3\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}	
		$db->commit();
		$db= NULL;
		echo '<script>alert(\'Application Delete Complete\');</script>';
		echo '<script>location.replace("/std/issuing/main_content.php?menu=appUrlList");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		echo '<script>alert(\'DB error3\');</script>';
		$db= NULL;
		exit;
	}
?>