<?php 
// 210701 add expiredDate table process
// 210605 add issuedList table process
// 210605 noneedserial -> do not add monthlycalc +1
// 201114 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Issuer':
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['id']) && isset($_POST['type']) && isset($_POST['cardStatus']) && isset($_POST['issuedBy']) && isset($_POST['issueType'])){
		$appNo = preg_replace("/<|\/|_|>/","",$_POST['id']);
		$cardType = preg_replace("/<|\/|_|>/","",$_POST['type']);
		$cardStatus = preg_replace("/<|\/|_|>/","",$_POST['cardStatus']);
		$issuedBy = preg_replace("/<|\/|_|>/","",$_POST['issuedBy']);
		$issueType =preg_replace("/<|\/|_|>/","",$_POST['issueType']);
		$response1 = '999';
		$response2 = '999';
	}
	else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$manualIssuing = 'no';
	if(isset($_POST['cardSerialNum']) && !empty($_POST['cardSerialNum']) && isset($_POST['manualIssuing']) && $_POST['manualIssuing'] === 'yes'){
		//$cardSerialNum = $_POST['cardSerialNum'];
		$cardSerialNum = preg_replace("/<|\/|_|>/","",$_POST['cardSerialNum']);
		//echo $cardSerialNum;
		$manualIssuing = 'yes';
	}else{
		//echo 'nonono';
	}
	/*
	if($_SERVER['SERVER_NAME'] === 'www.isic.com.tw'){
	}else{
		echo '<script>alert(\'forbidden2\');</script>';
		echo '<script>location.replace("/twn/index.php");</script>';
		exit;
	}*/
	$now1 = time();
	$currentDate = date("Y-m-d H:i:s",$now1);
	$currentYear = date("Y",$now1);
	$currentMonth = date("m",$now1);
	$serialType ='no';
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/function.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	try {
		//issue start
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename06 WHERE id = :id AND no = :no";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $_SESSION['id']);
		$stmt->bindParam(':no', $_SESSION['idNo']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultUserInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		$query = "SELECT * FROM $tablename12 WHERE officeId = :officeId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':officeId', $_SESSION['officeId']);
		$stmt->execute();
		if($stmt->rowCount() === 1 ){
			$resultOfficeInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}
		else {
			
			if(isset($_SESSION)){
				session_destroy();
			}else{}
			//print_r($stmt->errorInfo());
			echo '<script>alert(\'Please Login 99\');</script>';
			echo '<script>location.replace("/std/issuing/login.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied1\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' ){
		} else {
			echo '<script>alert(\'Access Denied2\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$modifiedBy = 'issue2'.$_SESSION['valid_user'];
		$db->beginTransaction();

		$query1 = "SELECT payStatus,status,cardType,oriSerialNum FROM $tablename07 LEFT JOIN  $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE ($tablename07.appNo=:inputValue1);"; //paystatus,approval check
		if ($_SESSION['officeId'] === '1'){
			//$query .= " AND ((officeId = 1) OR (officeId = 990))";
		}else{
			$query .= " AND ($tablename07.officeId = :officeId)";
		}		
		$stmt1 = $db->prepare($query1);
		$stmt1->bindParam(':inputValue1', $appNo);
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':officeId', $_SESSION['officeId']);
		}
		if($stmt1->execute()){
			$result0 = $stmt1->fetch(PDO::FETCH_OBJ);
		}else {
			//print_r($stmt1->errorInfo());
			//echo "<script> window.history.go(-1); </script>";
			//echo '<script>alert(\'modify ERROR2\');</script>';
			$db->rollBack();
			$db= NULL;
			exit;
		}
		$query = "SELECT serialName,validityStart,validityEnd,validityStart11,validityEnd11,needSerial,ccdbSend FROM $tablename23 WHERE url = :searchValue1";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $result0->cardType);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			$db->rollBack();
			$db= NULL;
			exit;
		}
		if ($appSettingInfo->validityStart === 'validityStartFix'){
			$validityStart  = $validityStartFix;
		}else if ($appSettingInfo->validityStart === 'validityStart1'){
			$validityStart  = $validityStart1;
		}else{
			$validityStart  = $appSettingInfo->validityStart11;
		}
		if ($appSettingInfo->validityEnd === 'validityEndFix'){
			$validityEnd = $validityEndFix;
		}else if ($appSettingInfo->validityEnd === 'validityEnd1'){
			$validityEnd = $validityEnd1;
		}else{
			$validityEnd = $appSettingInfo->validityEnd11;
		}
		if(isset($validityEnd) && !empty($validityEnd))
		{
			$validityEnd2 = explode('/',$validityEnd);
			$validityEnd3 = $validityEnd2[1].'-'.$validityEnd2[0];
			$validityEnd4 = date("Y-m-t H:i:s", strtotime($validityEnd3));
			$validityEndDate = date("Y-m-d H:i:s", strtotime($validityEnd4)+(60*60*24)-1);
		}
		else
		{
			$db->rollBack();
			echo 'validityEnd Error';
			//$target ='../main_content.php?menu=documentApproval';
			//$db=NULL;
			exit;
		}		
		if($appSettingInfo->needSerial === 'yes'){
			if($result0->payStatus ==='paid' && $result0->status ==='approved' && $result0->cardType != 'REVA' && $manualIssuing !='yes'){
				$querySerial = "SELECT serId FROM $tablename11 WHERE (serialName=:searchValue1) AND (serAppNo is NULL) ORDER BY serId ASC Limit 1"; //get serial number1
				$stmtSerial = $db->prepare($querySerial);
				$stmtSerial->bindParam(':searchValue1', $appSettingInfo->serialName);
				$stmtSerial->execute();
				if($stmtSerial->rowCount() == 1){
					$serialResult1 = $stmtSerial->fetch(PDO::FETCH_OBJ);
					$serId = $serialResult1->serId;
				}else {
					echo 'noSerialNumber';
					$db->rollBack();
					$db= NULL;
					exit;
				}
				
				$query2 = "UPDATE $tablename11 SET"; //record serial number1
				$query2 .= " serAppNo=:inputValue1, usedDate=:usedDate, usedBy=:inputValue2";
				$query2 .= " WHERE serId=:searchValue1";
				$stmt2 = $db->prepare($query2);
				$stmt2->bindParam(':inputValue1', $appNo);
				$stmt2->bindParam(':usedDate', $currentDate);
				$stmt2->bindParam(':inputValue2', $modifiedBy);
				$stmt2->bindParam(':searchValue1', $serId);
				$stmt2->execute();
				if($stmt2->rowCount() == 1){
					echo 'ok';
				}else {
					echo 'noSerialNumber';
					$db->rollBack();
					$db= NULL;
					exit;
				}
				$query3 = "SELECT serialNum FROM $tablename11 WHERE (serAppNo=:inputValue1);"; // get serial number2
				$stmt3 = $db->prepare($query3);
				$stmt3->bindParam(':inputValue1', $appNo);
				$stmt3->execute();
				if($stmt3->rowCount() >0){
					echo 'everything';
					$serialResult = $stmt3->fetch(PDO::FETCH_OBJ);
					$cardSerialNum = $serialResult->serialNum;
					switch(substr($cardSerialNum,0,1))
					{
						case 'S':
							$serialType = 'ISIC';
							break;
						case 'T':
							$serialType = 'ITIC';
							break;
						case 'Y':
							$serialType = 'IYTC';
							break;
						default:
							break;
					}
				}else {
					$db->rollBack();
					$db= NULL;
					exit;
				}
				$result = $stmt3->fetch(PDO::FETCH_OBJ);
				$query4 = "UPDATE $tablename07 SET"; // give serial number to application
				$query4 .= " cardSerialNum=:inputValue9, validityStart=:inputValue11, validityEnd=:inputValue12, issuedDate=:issuedDate, modifiedBy=:inputValue13, expiredDate =:expiredDate";
				$query4 .= " WHERE appNo=:inputValue100";
				$stmt4 = $db->prepare($query4);
				$stmt4->bindParam(':inputValue9', $cardSerialNum);
				$stmt4->bindParam(':inputValue11', $validityStart);
				$stmt4->bindParam(':inputValue12', $validityEnd);
				$stmt4->bindParam(':issuedDate', $currentDate);
				$stmt4->bindParam(':expiredDate', $validityEndDate);
				$stmt4->bindParam(':inputValue13', $modifiedBy);
				$stmt4->bindParam(':inputValue100', $appNo);
				if($stmt4->execute()){
					//$db->commit();
					echo 'fi';
				}else {
					//print_r($stmt4->errorInfo());
					echo "<script> window.history.go(-1); </script>";
					echo '<script>alert(\'modify ERROR5\');</script>';
					$db->rollBack();
					$db= NULL;
					exit;
				}
			}else if($result0->payStatus ==='paid' && $result0->status ==='approved' && $result0->cardType != 'REVA' && $manualIssuing ==='yes'){
				//echo 'manual';
				$query2 = "UPDATE $tablename11 SET"; //get serial number1
				$query2 .= " serAppNo=:inputValue1, usedDate=:usedDate, usedBy=:inputValue2";
				$query2 .= " WHERE serialNum =:searchValue1 AND serialName =:searchValue2";
				$stmt2 = $db->prepare($query2);
				$stmt2->bindParam(':inputValue1', $appNo);
				$stmt2->bindParam(':inputValue2', $modifiedBy);
				$stmt2->bindParam(':usedDate', $currentDate);
				$stmt2->bindParam(':searchValue1', $cardSerialNum);
				$stmt2->bindParam(':searchValue2', $appSettingInfo->serialName);
				$stmt2->execute();
				//echo $appSettingInfo->serialName;
				//print_r($stmt2->errorInfo());
				if($stmt2->rowCount() == 1){
					//print_r($stmt2->errorInfo());
					echo 'ok';
					//exit;
				}else {
					$db->rollBack();
					$db= NULL;
					exit;
				}
				/*
				add update serialNumber to serialstock db 
				
				
				$query3 = "SELECT serialNum FROM $tablename11 WHERE (serAppNo=:inputValue1);"; // get serial number2
				$stmt3 = $db->prepare($query3);
				$stmt3->bindParam(':inputValue1', $appNo);
				if($stmt3->execute()){
					echo 'everything';
				}else {
					$db->rollBack();
					$db= NULL;
					exit;
				}*/
				//echo 'ok';
				echo 'everything';
				//$result = $stmt3->fetch(PDO::FETCH_OBJ);
				$query4 = "UPDATE $tablename07 SET"; // give serial number to application
				$query4 .= " cardSerialNum=:inputValue9, validityStart=:inputValue11, validityEnd=:inputValue12, issuedDate=:issuedDate, modifiedBy=:inputValue13";
				$query4 .= " WHERE appNo=:inputValue100";
				$stmt4 = $db->prepare($query4);
				$stmt4->bindParam(':inputValue9', $cardSerialNum);
				$stmt4->bindParam(':inputValue11', $validityStart);
				$stmt4->bindParam(':inputValue12', $validityEnd);
				$stmt4->bindParam(':issuedDate', $currentDate);
				$stmt4->bindParam(':inputValue13', $modifiedBy);
				$stmt4->bindParam(':inputValue100', $appNo);
				if($stmt4->execute()){
					//$db->commit();
					echo 'fi';
				}else {
					//print_r($stmt4->errorInfo());
					echo "<script> window.history.go(-1); </script>";
					echo '<script>alert(\'modify ERROR5\');</script>';
					$db->rollBack();
					$db= NULL;
					exit;
				}			
			}else if($result0->payStatus ==='paid' && $result0->status ==='approved' && $result0->cardType === 'REVA'){
				$oriSerialNum = $result0->oriSerialNum;
				$query5 = "SELECT photoName FROM $tablename07 WHERE cardSerialNum = :searchValue1;";
				$stmt5 = $db->prepare($query5);
				$stmt5->bindParam(':searchValue1', $oriSerialNum);
				$stmt5->execute();
				if ($stmt5->rowCount() > 0){
					$result5 = $stmt5->fetch(PDO::FETCH_OBJ);
					$thisPhotoName = $result5->photoName;
					$query4 = "UPDATE $tablename07 SET photoName = '$thisPhotoName'";
				}else{
					$query4 = "UPDATE $tablename07 SET";
				}
				//$query4 = "UPDATE $tablename07 SET"; // give serial number to application
				$query4 .= " validityStart=:inputValue11, validityEnd=:inputValue12, issuedDate=:issuedDate, modifiedBy=:inputValue13";
				$query4 .= " WHERE appNo=:inputValue100";
				$stmt4 = $db->prepare($query4);
				$stmt4->bindParam(':inputValue11', $validityStart);
				$stmt4->bindParam(':inputValue12', $validityEnd);
				$stmt4->bindParam(':issuedDate', $currentDate);
				$stmt4->bindParam(':inputValue13', $modifiedBy);
				$stmt4->bindParam(':inputValue100', $appNo);
				if($stmt4->execute()){
					echo 'okeverything';
				}else {
					//print_r($stmt4->errorInfo());
					//echo "<script> window.history.go(-1); </script>";
					//echo '<script>alert(\'modify ERROR5\');</script>';
					$db->rollBack();
					$db= NULL;
					exit;
				}
				$query5 = "UPDATE $tablename20 SET"; // give serial number to application
				$query5 .= " revaNum=:inputValue9, revaStart=:inputValue11, revaEnd=:inputValue12, revaDate=:revaDate, revaBy=:inputValue13";
				$query5 .= " WHERE revaAppNo=:inputValue100";
				$stmt5 = $db->prepare($query5);
				$stmt5->bindParam(':inputValue9', $revaNum);
				$stmt5->bindParam(':inputValue11', $validityStart);
				$stmt5->bindParam(':inputValue12', $validityEnd);
				$stmt5->bindParam(':revaDate', $currentDate);
				$stmt5->bindParam(':inputValue13', $_SESSION['valid_user']);
				$stmt5->bindParam(':inputValue100', $appNo);
				if($stmt5->execute()){
					//$db->commit();
					//$db= NULL;
					echo 'fi';
				}else {
					//print_r($stmt5->errorInfo());
					//echo "<script> window.history.go(-1); </script>";
					//echo '<script>alert(\'modify ERROR6\');</script>';
					$db->rollBack();
					$db= NULL;
					exit;
				}				
				
			}else{
				//echo "<script> window.history.go(-1); </script>";
				//print_r($stmt2->errorInfo());
				$db->rollBack();
				//$target ='../main_content.php?menu=documentApproval';
				$db=NULL;
				exit;
			}
		}else if ($appSettingInfo->needSerial === 'no'){
			if($result0->payStatus ==='paid' && $result0->status ==='approved' && $result0->cardType != 'REVA'){
				echo 'ok';
				echo 'everything';
				$cardSerialNum ='NoNeed';
				$query4 = "UPDATE $tablename07 SET"; // give serial number to application
				$query4 .= " cardSerialNum=:inputValue9, validityStart=:inputValue11, validityEnd=:inputValue12, issuedDate=:issuedDate, modifiedBy=:inputValue13";
				$query4 .= " WHERE appNo=:inputValue100";
				$stmt4 = $db->prepare($query4);
				$stmt4->bindParam(':inputValue9', $cardSerialNum);
				$stmt4->bindParam(':inputValue11', $validityStart);
				$stmt4->bindParam(':inputValue12', $validityEnd);
				$stmt4->bindParam(':inputValue13', $modifiedBy);
				$stmt4->bindParam(':issuedDate', $currentDate);
				$stmt4->bindParam(':inputValue100', $appNo);
				if($stmt4->execute()){
					//$db->commit();
					echo 'fi';
				}else {
					//print_r($stmt4->errorInfo());
					echo "<script> window.history.go(-1); </script>";
					echo '<script>alert(\'modify ERROR5\');</script>';
					$db->rollBack();
					$db= NULL;
					exit;
				}			
			}else{
				//echo "<script> window.history.go(-1); </script>";
				//print_r($stmt2->errorInfo());
				$db->rollBack();
				//$target ='../main_content.php?menu=documentApproval';
				$db=NULL;
				exit;
			}						
		}else{
			$db->rollBack();
			echo 'needSerialError';
			//$target ='../main_content.php?menu=documentApproval';
			//$db=NULL;
			exit;
		}
		

		// make a Issued application List
		if($appSettingInfo->needSerial === 'yes'){
			$queryIssued1 = "SELECT officeId, cardPrice, shippingPrice, changePrice, promoGroupId, paidDate, issuedDate FROM $tablename07 WHERE appNo=:appNo";
			$stmtIssued1 = $db->prepare($queryIssued1);
			$stmtIssued1->bindParam(':appNo', $appNo);
			if($stmtIssued1->execute())
			{
				$resultIssued1 = $stmtIssued1->fetch(PDO::FETCH_OBJ);
			}
			else 
			{	
				//echo '123';
				//print_r($db->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;
			}
			$queryIssued = "INSERT INTO $tablename32 (";
			$queryIssued .= " iss_AppNo";
			$queryIssued .= ", iss_SerialNum";
			$queryIssued .= ", iss_SerialType";
			$queryIssued .= ", iss_ValidityEnd";
			$queryIssued .= ", iss_OfficeId";
			$queryIssued .= ", iss_CardPrice";
			$queryIssued .= ", iss_ShippingPrice";
			$queryIssued .= ", iss_PaidDate";
			$queryIssued .= ", iss_IssuedDate";
			if(isset($resultIssued1->changePrice) && !empty($resultIssued1->changePrice))
			{
				$queryIssued .= ", iss_ChangePrice";
			}
			else
			{}
			if(isset($resultIssued1->promoGroupId) && !empty($resultIssued1->promoGroupId))
			{
				$queryIssued .= ", iss_PromoId";
			}
			else
			{}	
			$queryIssued .= ", iss_ModifiedDate";	
			$queryIssued .= ", iss_ModifiedBy";		
			$queryIssued .=")";
			$queryIssued .= " VALUES (";
			$queryIssued .= ":iss_AppNo";
			$queryIssued .= ", :iss_SerialNum";
			$queryIssued .= ", :iss_SerialType";
			$queryIssued .= ", :iss_ValidityEnd";
			$queryIssued .= ", :iss_OfficeId";
			$queryIssued .= ", :iss_CardPrice";
			$queryIssued .= ", :iss_ShippingPrice";
			$queryIssued .= ", :iss_PaidDate";
			$queryIssued .= ", :iss_IssuedDate";
			if(isset($resultIssued1->changePrice) && !empty($resultIssued1->changePrice))
			{
				$queryIssued .= ", :iss_ChangePrice";
			}
			else
			{}
			if(isset($resultIssued1->promoGroupId) && !empty($resultIssued1->promoGroupId))
			{
				$queryIssued .= ", :iss_PromoId";
			}
			else
			{}
			$queryIssued .= ", :iss_ModifiedDate";	
			$queryIssued .= ", :iss_ModifiedBy";	
			$queryIssued .=")";
			$stmtIssued = $db->prepare($queryIssued);
			$stmtIssued->bindParam(':iss_AppNo', $appNo);
			$stmtIssued->bindParam(':iss_SerialNum', $cardSerialNum);
			$stmtIssued->bindParam(':iss_SerialType', $serialType);
			$stmtIssued->bindParam(':iss_ValidityEnd', $validityEndDate);
			$stmtIssued->bindParam(':iss_OfficeId', $resultIssued1->officeId);
			$stmtIssued->bindParam(':iss_CardPrice', $resultIssued1->cardPrice);
			$stmtIssued->bindParam(':iss_ShippingPrice', $resultIssued1->shippingPrice);
			$stmtIssued->bindParam(':iss_PaidDate', $resultIssued1->paidDate);
			$stmtIssued->bindParam(':iss_IssuedDate', $currentDate);
			if(isset($resultIssued1->changePrice) && !empty($resultIssued1->changePrice))
			{
				$stmtIssued->bindParam(':iss_ChangePrice', $resultIssued1->changePrice);
			}
			else
			{}
			if(isset($resultIssued1->promoGroupId) && !empty($resultIssued1->promoGroupId))
			{
				$stmtIssued->bindParam(':iss_PromoId', $resultIssued1->promoGroupId);
			}
			else
			{}
			$stmtIssued->bindParam(':iss_ModifiedDate', $currentDate);
			$stmtIssued->bindParam(':iss_ModifiedBy', $modifiedBy);
			//$stmtIssued->execute();
			//echo $stmtIssued->execute();
			if($stmtIssued->execute())
			{
				//echo '1234';
			}
			else 
			{	
				//echo $queryIssued;
				//echo '123';
				print_r($db->errorInfo());
				$db->rollBack();
				$db= NULL;
				exit;
			}
			$queryMonthCalc = "SELECT idMonthly,mCount FROM $tablename31 WHERE mYear = :mYear AND mMonth = :mMonth AND mYearMonth =:mYearMonth";
			$stmtMonthCalc = $db->prepare($queryMonthCalc);
			$currentYearMonth = $currentYear.$currentMonth;
			$stmtMonthCalc->bindParam(':mYear', $currentYear);
			$stmtMonthCalc->bindParam(':mMonth', $currentMonth);
			$stmtMonthCalc->bindParam(':mYearMonth', $currentYearMonth);
			$stmtMonthCalc->execute();
			
			/*
			echo $queryMonthCalc;
			echo '///';
			echo $currentYear;
			echo '///';
			echo $currentMonth;
			echo '///';
			echo $currentYearMonth;
			echo '///';
			echo $stmtMonthCalc->rowCount();
			*/
			if($stmtMonthCalc->rowCount() === 1)
			{
				$resultMonthCalc = $stmtMonthCalc->fetch(PDO::FETCH_OBJ);
				$newMCount = (int)$resultMonthCalc->mCount + 1;
				$queryMonthCalc2 = "UPDATE $tablename31 SET";
				$queryMonthCalc2 .= " mCount =:mCount";
				$queryMonthCalc2 .= " ,modifiedDate =:modifiedDate";
				$queryMonthCalc2 .= " ,modifiedBy =:modifiedBy";
				$queryMonthCalc2 .= " WHERE idMonthly = :idMonthly";
				$stmtMonthCalc2 = $db->prepare($queryMonthCalc2);
				$stmtMonthCalc2->bindParam(':mCount', $newMCount, PDO::PARAM_INT);
				$stmtMonthCalc2->bindParam(':modifiedDate', $currentDate);
				$stmtMonthCalc2->bindParam(':modifiedBy', $modifiedBy);
				$stmtMonthCalc2->bindParam(':idMonthly', $resultMonthCalc->idMonthly);

				if($stmtMonthCalc2->execute())
				{
				}
				else 
				{
					$db->rollBack();
					$db= NULL;
					exit;
				}
				$db->commit();
				//$db= NULL;
				echo 'ne';
			}
			else 
			{
				$newMCount = 1;
				$queryMonthCalc2 = "INSERT INTO $tablename31 (";
				$queryMonthCalc2 .= " mYearMonth";
				$queryMonthCalc2 .= ", mYear";
				$queryMonthCalc2 .= ", mMonth";
				$queryMonthCalc2 .= ", mCount";
				$queryMonthCalc2 .= ", modifiedBy";
				$queryMonthCalc2 .= ", modifiedDate";
				$queryMonthCalc2 .=")";
				$queryMonthCalc2 .= " VALUES (";
				$queryMonthCalc2 .= ":mYearMonth";
				$queryMonthCalc2 .= ", :mYear";
				$queryMonthCalc2 .= ", :mMonth";
				$queryMonthCalc2 .= ", :mCount";
				$queryMonthCalc2 .= ", :modifiedBy";
				$queryMonthCalc2 .= ", :modifiedDate";
				$queryMonthCalc2 .=")";
				$stmtMonthCalc2 = $db->prepare($queryMonthCalc2);
				$stmtMonthCalc2->bindParam(':mYearMonth', $currentYearMonth);
				$stmtMonthCalc2->bindParam(':mYear', $currentYear);
				$stmtMonthCalc2->bindParam(':mMonth', $currentMonth);
				$stmtMonthCalc2->bindParam(':mCount', $newMCount, PDO::PARAM_INT);
				$stmtMonthCalc2->bindParam(':modifiedBy', $modifiedBy);
				$stmtMonthCalc2->bindParam(':modifiedDate', $currentDate);
				if($stmtMonthCalc2->execute())
				{
					//echo '1234';
				}
				else 
				{	
					//echo '123';
					//print_r($db->errorInfo());
					$db->rollBack();
					$db= NULL;
					exit;
				}
				$db->commit();
				$db= NULL;
				echo 'ne';
			}
		}
		else
		{
			$db->commit();
			$db= NULL;
			echo 'ne';
		}
		///// issue end////
		
		//// start ccdb part ///
		if ($appSettingInfo->ccdbSend === 'no'){
			 echo '990990';
			 $responseStatus = 'ok';
		} else if  ($appSettingInfo->ccdbSend === 'yes'){
			//// get data for ccdb ////
			require __DIR__.$goParent2.$reqDir1.'/_require1/outValidKey2.php';	
			$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE appNo=:searchValue100 AND cardType=:searchValue101 AND issuedDate IS NOT NULL";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchValue100', $appNo);
			$stmt->bindParam(':searchValue101', $cardType);
			$stmt->execute();
			if($stmt->rowCount() === 1){
				$result = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else {
				$db = NULL;
				exit;
			}
			$id1 = $id2388387491f87;
			$pwd1 = $pwd232324481;
			if($cardType != 'REVA'){
				$cardNumber = $result->cardSerialNum;
			}else{
				$cardNumber = $result->oriSerialNum;
				$stickerNumber = $result->revaNum;
			}
			if(substr($cardNumber,0,1) === 'S'){
				$cardType1 = 'S';
			}else if(substr($cardNumber,0,1) === 'T') {
				$cardType1 = 'T';
			}else if(substr($cardNumber,0,1) === 'Y') {
				$cardType1 = 'Y';
			}else {
				$db = NULL;
				exit;
			}
			//$cardStatus GET
			if($nameStyle1 === 'FL'){
				$printedName = decrypt1($result->engFname,$result->encId).' '.decrypt1($result->engLname,$result->encId);
			}else if($nameStyle1 === 'LF'){
				$printedName = decrypt1($result->engLname,$result->encId).' '.decrypt1($result->engFname,$result->encId);
			}else{
				$printedName = decrypt1($result->engFname,$result->encId).' '.decrypt1($result->engLname,$result->encId);
			}
				//exit;
			$firstName = decrypt1($result->engFname,$result->encId);
			$lastName = decrypt1($result->engLname,$result->encId);
				$birthDay = decrypt1($result->birthDay,$result->encId);
				$birthMonth = decrypt1($result->birthMonth,$result->encId);
				$birthYear = decrypt1($result->birthYear,$result->encId);
				if (strlen($birthDay) == 1){
					$birthDay = '0'.$birthDay;
				}else {}
				if (strlen($birthMonth) == 1){
					$birthMonth = '0'.$birthMonth;
				}else {}
			$dateOfBirth = $birthDay.'/'.$birthMonth.'/'.$birthYear;
			$validFrom = $result->validityStart;
			$validTo = $result->validityEnd;
			$institutionName = decrypt1($result->school,$result->encId);
			//$issuedBy $issuerName + GET
			$issuedBy = $issuerName.$issuedBy;
			$email = decrypt1($result->email,$result->encId);
			$filePath='';
			$issuedOn = substr($result->issuedDate,8,2).'/'.substr($result->issuedDate,5,2).'/'.substr($result->issuedDate,0,4);
			//$issueType GET
			if($cardType != 'REVA'){		
				$photoContent = file_get_contents(__DIR__.$goParent2.$reqDir1.$storageDir.$result->photoName);
				$img =  imagecreatefromstring($photoContent); 
				$imgSize = getimagesize(__DIR__.$goParent2.$reqDir1.$storageDir.$result->photoName);
				$newImg = imagecreatetruecolor(413,413);
				$white  = imagecolorallocate($newImg,255,255,255);
				imagefilledrectangle($newImg,0,0,413,413,$white);
				if($imgSize[0] > $imgSize[1]){		
					$recalWidth = 413;
					$recalHeight = round($imgSize[1] * 413 / $imgSize[0]);
					imagecopyresampled($newImg,$img,0,(431-$recalHeight)/2,0,0,$recalWidth,$recalHeight,$imgSize[0],$imgSize[1]);
				
				}else if ($imgSize[0] < $imgSize[1]){
					$recalWidth = round($imgSize[0] *413 / $imgSize[1]);
					$recalHeight = 413;
					imagecopyresampled($newImg,$img,(431-$recalWidth)/2,0,0,0,$recalWidth,$recalHeight,$imgSize[0],$imgSize[1]);
				
				}else{
					$recalWidth = 413;
					$recalHeight = 413;
					imagecopyresampled($newImg,$img,0,0,0,0,$recalWidth,$recalHeight,$imgSize[0],$imgSize[1]);
				}
				ob_start(); // Let's start output buffering.
					imagejpeg($newImg); //This will normally output the image, but because of ob_start(), it won't.
					$contents = ob_get_contents(); //Instead, output above is saved to $contents
				ob_end_clean(); //End the output buffer.
				$photoReaded = base64_encode($contents);
			}else{}
			//// get data for ccdb end ////
			//// send data to ccdb start ////
			if($cardType != 'REVA'){
				$post = http_build_query([
					'id' => $id1,
					'pwd' => $pwd1,
					'cardNumber' => $cardNumber,
					'cardType' => $cardType1,
					'cardStatus' => $cardStatus,
					'printedName' => $printedName,
					'firstName' => $firstName,
					'lastName' => $lastName,
					'dateOfBirth' => $dateOfBirth,
					'validFrom' => $validFrom,
					'validTo' => $validTo,
					'institutionName' => $institutionName,
					'issuedBy' => $issuedBy,
					'email' => $email,
					'filePath' => $filePath,
					'issuedOn' => $issuedOn,
					'issueType' => $issueType
				]);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERPWD, $id1 . ":" . $pwd1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_URL, $url33);
				$response1 = curl_exec($ch);
				curl_close($ch);
				if(isset($response1)){
					/*
					echo '<pre>';
					print_r($response1);
					echo '</pre>';
					*/
				}else{
					$response1 = 'noReponse';
				}
				//echo $response1;
				$responseStatus ='notOk';
				switch($response1){
					case '201':
					case '200':
						$responseStatus = 'ok';
						break;
					default:
						$responseStatus ='notOk';
						break;
				}
				if($responseStatus === 'ok'){
					$post = http_build_query([
						'id' => $id1,
						'pwd' => $pwd1,
						'cardNumber' => $cardNumber,
						'filePath' => $photoReaded
					]);
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_USERPWD, $id1 . ":" . $pwd1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_URL, $urlPhoto);
					$response2 = curl_exec($ch);
					curl_close($ch);
					if(isset($response2)){
						/*
						echo '<pre>';
						print_r($response2);
						echo '</pre>';
						*/
					}else{
						$response2 = 'noReponse';
					}
				}else{}
			}else{
				$post = http_build_query([
					'id' => $id1,
					'pwd' => $pwd1,
					'cardNumber' => $cardNumber,
					'stickerNumber' => $stickerNumber,
					'validFrom' => $validFrom,
					'validTo' => $validTo
				]);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERPWD, $id1 . ":" . $pwd1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_URL, $urlReva);
				$response2 = curl_exec($ch);
				curl_close($ch);
				if(isset($response1)){
				}else{
					$response1 = 'noReponse';
				}	
				switch($response1){
					case '201':
						$responseStatus = 'ok';
						break;
					default:
						$responseStatus ='notOk';
						break;
				}			
			}
			//// send data to ccdb end ////
			//// evaluate response and add sendData start ///
			if($cardType !='REVA'){
				switch($response1){
					case '201':
						//echo '<script>alert(\'Create Complete\');</script>';
						echo '201';
						break;
					case '200':
						//echo '<script>alert(\'Update Complete\');</script>';
						echo '200';
						break;
					case '400':
						//echo '<script>alert(\'Bad Request\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						echo '400';
						break;
					default:
						echo '999';
						//echo '<script>alert(\'No Response 1\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						break;
				}
				switch($response2){
					case '200':
						echo '200';
						//echo '<script>alert(\'Update Complete\');</script>';
						break;
					case '400':
						echo '400';
						//echo '<script>alert(\'Bad Request\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						break;
					default:
						echo '999';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						break;
				}
			}else{
				switch($response1){
					case '201':
						//echo '<script>alert(\'Create Complete\');</script>';
						echo '201';
						break;
					case '400':
						//echo '<script>alert(\'Bad Request\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						echo '400';
						break;
					case '404':
						//echo '<script>alert(\'Not Found\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						echo '404';
						break;
					default:
						//echo '<script>alert(\'No Response 2\');</script>';
						//echo '<script>location.replace("/twn/issuingKYP/main_issuer.php?menu=issued");</script>';
						echo '999';
						break;
				}
			}
			//// evaluate response and add sendData end ///
			
		}else{}
		
		// start update send date// 
		/// even if we don't need to send date always update send date for issued.php to work.
		if($responseStatus === 'ok'){
			$query0 = "UPDATE $tablename07 SET"; //approve 
			$query0 .= " sendDate=:sendDate, modifiedDate=:modifiedDate, modifiedBy=:inputValue8";
			$query0 .= " WHERE appNo=:searchValue100";
			$stmt2 = $db->prepare($query0);
			$stmt2->bindParam(':inputValue8', $modifiedBy);
			$stmt2->bindParam(':sendDate', $currentDate);
			$stmt2->bindParam(':modifiedDate', $currentDate);
			$stmt2->bindParam(':searchValue100', $appNo);
			if($stmt2->execute()){
			}
			else {
				//print_r($stmt2->errorInfo());
				$db= NULL;
			}
			//$db->commit();
			$db= NULL;
		}else{}
		
		// end update send date//
	//// end ccdb part ///
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		exit;
	}
?>