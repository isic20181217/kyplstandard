<?php 

session_start();
// 210510 add excel support
// 200615 check

	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	*/
	if(isset($_POST['formName']) && !empty($_POST['formName'])){
		$formName  = preg_replace("/<|\/|_|>/","",$_POST['formName']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;		
	}
	if(isset($_POST['stock']) && !empty($_POST['stock'])){
		$stockId  = preg_replace("/<|\/|_|>/","",$_POST['stock']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;			
	}	
	if(isset($_POST['addedBy']) && !empty($_POST['addedBy'])){
		$addedBy  = preg_replace("/<|\/|_|>/","",$_POST['addedBy']);
	}else{
		echo '<script>location.replace("/std/index.php");</script>';
		exit;	
	}	
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	//require __DIR__.$goParent2.$reqDir1.'/_require1/excelAddon.php';
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename10 WHERE stockId =:stockId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':stockId', $stockId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$stockInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		if(isset($stockInfo->serialType) && !empty($stockInfo->serialType) && isset($stockInfo->serialName) && !empty($stockInfo->serialName) && isset($stockInfo->serialDesc) && !empty($stockInfo->serialDesc) && isset($stockInfo->addedDate) && !empty($stockInfo->addedDate) && isset($stockInfo->addedBy) && !empty($stockInfo->addedBy)){
		}else{
			echo '<script>alert(\'serial stock Info Error\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$timeStamp = @strtotime($stockInfo->addedDate);
		if(isset($timeStamp) && !empty($timeStamp) ){
		}else{
			echo '<script>alert(\'AddedDate Error\');</script>';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		$stockInfo->addedDate = date("Ymd_His",$timeStamp);
		$stockInfo->serialDesc = preg_replace('/\\\\|\/|:|\*|\?|\"|<|>|\|/',"_",$stockInfo->serialDesc);
		$fileName = 'Delete'.$stockInfo->serialType.'_'.$stockInfo->serialName.'_'.$stockInfo->serialDesc.'_'.$stockInfo->addedDate;
		//echo $fileName;
		$query2 = "SELECT * FROM $tablename11 WHERE addedBy =:addedBy";
		$stmt2 = $db->prepare($query2);
		$stmt2->bindParam(':addedBy', $stockInfo->addedBy);
		$stmt2->execute();
		$serialCount = 0;
		while($result2 = $stmt2->fetch(PDO::FETCH_OBJ)){
			$serialList[$serialCount] = array (
			'serialNum' => $result2->serialNum, 
			'serAppNo' => $result2->serAppNo, 		
			'usedDate' => $result2->usedDate, 
			'usedBy' => $result2->usedBy, 
			'returned' => $result2->returned);
			$serialCount = $serialCount + 1;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	/*
	echo '<pre>';
	print_r($serialList);
	echo '</pre>';
	*/
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
	if(isset($usePhpSpreadsheet) && $usePhpSpreadsheet === 'yes')
	{
		

		require __DIR__.$goParent2.$reqDir1.'/_require1/excelAddon.php';

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$cells = array(
			'A' => array(20, 'serialNum', 'SerialNumber'),
			'B' => array(20, 'serAppNo',  'ApplicationNo'),
			'C' => array(20, 'usedDate', 'UsedDate'),
			'D' => array(20, 'usedBy', 'UsedBy'),
			'E' => array(20, 'returned', 'Returned')
		);
		foreach ($cells as $key => $val) {
			$cellName = $key.'1';

			$sheet->getColumnDimension($key)->setWidth($val[0]);
			$sheet->getRowDimension('1')->setRowHeight(25);
			$sheet->setCellValue($cellName, $val[2]);
			$sheet->getStyle($cellName)->getFont()->setBold(true);
			$sheet->getStyle($cellName)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle($cellName)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		}

		for ($i = 2; $row = array_shift($serialList); $i++) {
			foreach ($cells as $key => $val) {
				$sheet->setCellValue($key.$i, $row[$val[1]]);
			}
		}
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.$fileName.'.xlsx";');

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}
	else
	{
		if(isset($serialList[0]) && !empty($serialList[0])){
			ob_end_clean();
			$fp = fopen('php://output', 'w');
			header("Content-Type:application/csv"); 
			header('Content-Disposition: attachment; filename="'.$fileName.'.csv";');
			echo "\xEF\xBB\xBF"; // utf-8
			foreach($serialList[0] as $key => $arr) {
				fwrite($fp,$key);
				fwrite($fp,',');
			}
			fwrite($fp,"\r\n");	
			for($i=0;$i<$serialCount;$i++){
				fputcsv($fp, $serialList[$i]);
			}
			
			fclose($fp);
		}else{}
	}
?>