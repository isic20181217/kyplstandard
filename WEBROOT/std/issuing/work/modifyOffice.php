<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>modifyOffice</title>
<?php
// 201114 check
	session_start();
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'Please login\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	} 
	if (isset($_POST['formName'])){
		$formName = preg_replace("/<|\/|_|>/","",$_POST['formName'] );
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['activeOffice']) && !empty($_POST['activeOffice'])){
		$activeOffice= preg_replace("/<|\/|_|>/","",$_POST['activeOffice']);
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['officeId']) && !empty($_POST['officeId'])){
		$officeId= preg_replace("/<|\/|_|>/","",$_POST['officeId']);
	}else{
		echo '<script>alert(\'forbidden\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if (isset($_POST['name']) && !empty($_POST['name'])){
		$name= preg_replace("/<|\/|_|>/","",$_POST['name'] );
	}else{
		echo '<script>alert(\'Not Enough Values name\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['officeDesc']) && !empty($_POST['officeDesc'])){
		$officeDesc= preg_replace("/<|\/|_|>/","",$_POST['officeDesc'] );
	}else{
		echo '<script>alert(\'Not Enough Values officeDesc\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['address']) && !empty($_POST['address'])){
		$address= preg_replace("/<|\/|_|>/","",$_POST['address'] );
	}else{
		$address = ' ';
		/*
		echo '<script>alert(\'Not Enough Values address\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['city']) && !empty($_POST['city'])){
		$city= preg_replace("/<|\/|_|>/","",$_POST['city'] );
	}else{
		$city = ' ';
		/*
		echo '<script>alert(\'Not Enough Values city\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
		$zipcode= preg_replace("/<|\/|_|>/","",$_POST['zipcode'] );
	}else{
		$zipcode = ' ' ;
		/*
		echo '<script>alert(\'Not Enough Values zipcode\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['tel']) && !empty($_POST['tel'])){
		$tel= preg_replace("/<|\/|_|>/","",$_POST['tel'] );
	}else{
		$tel = ' ' ;
		/*
		echo '<script>alert(\'Not Enough Values tel \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['contact']) && !empty($_POST['contact'])){
		$contact= preg_replace("/<|\/|_|>/","",$_POST['contact'] );
	}else{
		$contact = ' ' ;
		/*
		echo '<script>alert(\'Not Enough Values contact \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['email']) && !empty($_POST['email'])){
		$email= preg_replace("/<|\/|_|>/","",$_POST['email'] );
	}else{
		$email = ' ' ;
		/*
		echo '<script>alert(\'Not Enough Values email\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}
	if (isset($_POST['workHour']) && !empty($_POST['workHour'])){
		$workHour= preg_replace("/<|\/|_|>/","",$_POST['workHour'] );
	}else{
		$workHour = ' ';
		/*
		echo '<script>alert(\'Not Enough Values workHour \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
		*/
	}

	if (isset($_POST['canUrl']) && !empty($_POST['canUrl'])){
		$canUrl= preg_replace("/<|\/|_|>/","",$_POST['canUrl'] );
	}else{
		echo '<script>alert(\'Not Enough Values canUrl \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['canCsv']) && !empty($_POST['canCsv'])){
		$canCsv= preg_replace("/<|\/|_|>/","",$_POST['canCsv'] );
	}else{
		echo '<script>alert(\'Not Enough Values canCsv \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['canValidate']) && !empty($_POST['canValidate'])){
		$canValidate= preg_replace("/<|\/|_|>/","",$_POST['canValidate'] );
	}else{
		echo '<script>alert(\'Not Enough Values canValidate \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['canIssue']) && !empty($_POST['canIssue'])){
		$canIssue= preg_replace("/<|\/|_|>/","",$_POST['canIssue'] );
	}else{
		echo '<script>alert(\'Not Enough Values canIssue \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['canPrint']) && !empty($_POST['canPrint'])){
		$canPrint= preg_replace("/<|\/|_|>/","",$_POST['canPrint'] );
	}else{
		echo '<script>alert(\'Not Enough Values canPrint \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['canPrintLabel']) && !empty($_POST['canPrintLabel'])){
		$canPrintLabel= preg_replace("/<|\/|_|>/","",$_POST['canPrintLabel'] );
	}else{
		echo '<script>alert(\'Not Enough Values canPrintLabel \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	/*
	if (isset($_POST['menuIssuing']) && !empty($_POST['menuIssuing'])){
		$menuIssuing= preg_replace("/<|\/|_|>/","",$_POST['menuIssuing'] );
	}else{
		echo '<script>alert(\'Not Enough Values menuIssuing \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['menuReports']) && !empty($_POST['menuReports'])){
		$menuReports= preg_replace("/<|\/|_|>/","",$_POST['menuReports'] );
	}else{
		echo '<script>alert(\'Not Enough Values menuReports \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['menuSerial']) && !empty($_POST['menuSerial'])){
		$menuSerial= preg_replace("/<|\/|_|>/","",$_POST['menuSerial'] );
	}else{
		echo '<script>alert(\'Not Enough Values menuSerial \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['menuIo']) && !empty($_POST['menuIo'])){
		$menuIo= preg_replace("/<|\/|_|>/","",$_POST['menuIo'] );
	}else{
		echo '<script>alert(\'Not Enough Values menuIo \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if (isset($_POST['menuApplication']) && !empty($_POST['menuApplication'])){
		$menuApplication= preg_replace("/<|\/|_|>/","",$_POST['menuApplication'] );
	}else{
		echo '<script>alert(\'Not Enough Values menuApplication \');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
*/	

	$today = date("Y-m-d H:i:s");
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$db->beginTransaction();		
		$query = "UPDATE $tablename12 SET ";
		/*
		$query .= "
		activeOffice=:activeOffice,
		name=:name,
		officeDesc =:officeDesc,
		address=:address, 
		city=:city,
		zipcode=:zipcode,
		contact=:contact, 
		email=:email, 
		workHour=:workHour,
		tel=:tel, 
		canUrl =:canUrl,
		canCsv =:canCsv,
		canValidate =:canValidate,
		canIssue=:canIssue,
		canPrint=:canPrint,
		canPrintLabel=:canPrintLabel,
		menuIssuing=:menuIssuing,
		menuReports=:menuReports,
		menuSerial=:menuSerial,
		menuIo=:menuIo,
		menuApplication=:menuApplication,
		modifiedDate=:modifiedDate, 
		modifiedBy=:modifiedBy";*/
		$query .= "
		activeOffice=:activeOffice,
		name=:name,
		officeDesc =:officeDesc,
		address=:address, 
		city=:city,
		zipcode=:zipcode,
		contact=:contact, 
		email=:email, 
		workHour=:workHour,
		tel=:tel, 
		canUrl =:canUrl,
		canCsv =:canCsv,
		canValidate =:canValidate,
		canIssue=:canIssue,
		canPrint=:canPrint,
		canPrintLabel=:canPrintLabel,
		modifiedDate=:modifiedDate, 
		modifiedBy=:modifiedBy";
		$query .= " WHERE officeId = :officeId;";
		//echo $query;
		$stmt = $db->prepare($query);
		$stmt->bindParam(':activeOffice', $activeOffice);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':officeDesc', $officeDesc);
		$stmt->bindParam(':address', $address);
		$stmt->bindParam(':city', $city);
		$stmt->bindParam(':zipcode', $zipcode);
		$stmt->bindParam(':contact', $contact);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':workHour', $workHour);
		$stmt->bindParam(':tel', $tel);
		
		$stmt->bindParam(':canUrl', $canUrl);
		$stmt->bindParam(':canCsv', $canCsv);
		$stmt->bindParam(':canValidate', $canValidate);
		$stmt->bindParam(':canIssue', $canIssue);
		$stmt->bindParam(':canPrint', $canPrint);
		$stmt->bindParam(':canPrintLabel', $canPrintLabel);
		/*
		$stmt->bindParam(':menuIssuing', $menuIssuing);
		$stmt->bindParam(':menuReports', $menuReports);
		$stmt->bindParam(':menuSerial', $menuSerial);
		$stmt->bindParam(':menuIo', $menuIo);
		$stmt->bindParam(':menuApplication', $menuApplication);
		*/
		$stmt->bindParam(':modifiedDate', $today);
		$stmt->bindParam(':modifiedBy', $_SESSION['valid_user']);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
		}else {
			print_r($stmt->errorInfo());
			$db->rollBack();
			$db= NULL;
			echo '<script>alert(\'Error2\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}
		$target ='?menu='.$formName.'&officeId='.$_POST['officeId'];
		$db->commit();
		$db= NULL;
		echo '<script>location.replace("/std/issuing/main_content.php'.$target.'");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db->rollBack();
		$db= NULL;
		echo '<script>alert(\'modify error2\');</script>';
		echo '<script>window.history.back();</script>';
		exit;
	}
?>
<script>
	window.onload = function(){
		location.replace("/std/issuing/main_content.php");
	}
</script>
</head>
<body>
</body>
</html>