<?php 
// 201114 add

	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type'])
		{
			case 'Manager':
			case 'Issuer':
			case 'Viewer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?>
<?php
	$menuArr['issuingOne'] ='';
	$menuArr['reports']  ='';
	$menuArr['setup']  ='';
	$menuArr['manager'] ='';
	$menuArr['io'] ='';
	
	if(isset($_GET['menu']) && !empty($_GET['menu']))
	{
			switch($_GET['menu'])
			{
				case 'appFileUpload':
				case 'issuingOne':
				case 'bulkList'://bulk upload
					$menuArr['issuingOne'] = 'active';
					break;
				case 'daily':
				case 'issued': // ccdb error app list
				case 'monthly':
				case 'saveReport';
					$menuArr['reports'] = 'active';
					break;
				case 'staffId':
				case 'designList2':
				case 'designEditor2':	
					$menuArr['manager'] = 'active';
					$menuArr['io'] ='';
					break;
				case 'designEditorNew':
				case 'cardSerialNew':
				case 'appUrlNew':
					$menuArr['manager'] = 'active';
					break;
				case 'cardSerial':
				case 'checkSerial':
					$menuArr['manager'] = 'active';
					break;
				case 'issuingOfficeNew':
				case 'issuingOffice':
				case 'issuingOfficeDetail':
				case 'staffNew':
				case 'staffDetail':
					$menuArr['manager'] = 'active';
					break;
				case 'appUrlList':
				case 'appUrlDetail':
					$menuArr['manager'] = 'active';
					break;
				case 'promoList':
				case 'promoNew':
				case 'promoDetail':
					$menuArr['manager'] = 'active';
					break;
				case 'designEditor1':
				case 'designList':
					$menuArr['manager'] = 'active';
					break;
				default:
					break;
			}
	}
	else
	{
		$menuArr['issuingOne'] = 'active';
	}
?>
<!-- menu start-->
<div id="topMenu" class="row m0">	
	<div id="cssmenu">
		<ul>
			<?php 
				if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing ==='yes' && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' )
				{
			?>
			<li class="<?php echo $menuArr['issuingOne']?>"><a href="./main_content.php?menu=issuingOne">Applications to issue</a>
				<ul>
					<li><a href="./main_content.php?menu=issuingOne">Online applications</a></li>
					<?php 
						if(isset($resultUserInfo->sCanCsv) && $resultUserInfo->sCanCsv ==='yes' && isset($resultOfficeInfo->canCsv) && $resultOfficeInfo->canCsv ==='yes' )
						{
					?>
					<li><a href="./main_content.php?menu=bulkList">Application bulk upload</a>
						<ul>
							<li><a href="./main_content.php?menu=appFileUpload">Bulk upload new applications</a></li>
							<li><a href="./main_content.php?menu=bulkList">Bulk uploaded applications list</a></li>
							
						</ul>
					</li>
					<?php
						} 
						else 
						{}
					?>
				</ul>
			</li>
			<?php 
				} 
				else 
				{}
			?>
			<?php 
				if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports ==='yes' && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
			?>
			<li class="<?php echo $menuArr['reports']?>"><a href="./main_content.php?menu=daily">Issued data report</a>
				<ul>
				  <li><a href="./main_content.php?menu=daily">Issued data</a></li>
				  <li><a href="./main_content.php?menu=issued">Manual data upload to CCDB</a></li>
				  <li><a href="./main_content.php?menu=monthly">Monthly sales report</a></li>
				  <li><a href="./main_content.php?menu=saveReport">Download report File</a></li>
<?php
	if($user_type === 'Manager')
	{
?>
				<li><a href="./main_content.php?menu=accountSerialM">Account data by serials</a></li>
<?php
	}
	else
	{
?>
				<li><a href="./main_content.php?menu=accountSerial">Account data by serials</a></li>
<?php
	}
?>

				  
				</ul>
			</li>
			<?php 
				} else {}
			?>
			<?php 
				if(isset($resultUserInfo->sMenuSerial) && $resultUserInfo->sMenuSerial ==='yes' && isset($resultOfficeInfo->menuSerial) && $resultOfficeInfo->menuSerial ==='yes' ){
			?>
			<?php 
				} else {}
			?>
			<?php 
				if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication ==='yes' && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
			?>
			<?php 
				} else {}
			?>
			<?php
				if ($user_type === 'Manager')
				{
			?>
			<li class="<?php echo $menuArr['manager']?>" id="managerMenu"><a href="#">System management</a>
				<ul>
					<li><a href="./main_content.php?menu=designEditorNew&setup=yes">Initial use setup <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=designEditorNew&setup=yes">Upload card design</a></li>
							<li><a href="./main_content.php?menu=designList2&setup=yes">Print test(MainOffice)</a></li>
							<li><a href="./main_content.php?menu=cardSerialNew&setup=yes">Upload serial</a></li>
							<li><a href="./main_content.php?menu=appUrlNew&setup=yes&initialSetup=yes">Create Application</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=designList">Manage card design <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=designEditorNew&setup=yes">Add new card design</a></li>
							<li><a href="./main_content.php?menu=designList">Card design list</a></li>
							<li><a href="./main_content.php?menu=designList2&setup=yes">Print test(MainOffice)</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=cardSerial">Manage serial <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=cardSerialNew&setup=yes">Add new serial</a></li>
							<li><a href="./main_content.php?menu=cardSerial">Serial list</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=appUrlList">Manage application <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=appUrlNew&setup=yes">Add new application</a></li>
							<li><a href="./main_content.php?menu=appUrlList">Application list</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=issuingOffice">Manage IO <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=issuingOfficeNew">Add new IO</a></li>
							<li><a href="./main_content.php?menu=issuingOffice">IO list</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=promoList">Manage promotion <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=promoNew">Add new promotion</a></li>
							<li><a href="./main_content.php?menu=promoList">Promotion list</a></li>
						</ul>
					</li>
					<li><a href="./main_content.php?menu=schoolName">School name management<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<ul>
							<li><a href="./main_content.php?menu=schoolName">Add new school name</a></li>
							<li><a href="./main_content.php?menu=schoolNameList">School name list</a></li>
						</ul>
					</li>
					<!--
					<li><a href="./main_content.php?menu=oldApps">Old Apps</a></li>
					<li><a href="./main_content.php?menu=expiredApps">Expired Apps</a></li>
					<li><a href="./main_content.php?menu=search">Search</a></li>
					-->
				</ul>
			</li>
			<?php 
				} 
				else 
				{	
			?>
			<li class="<?php echo $menuArr['io']?>" id="managerMenu"><a href="#">IO setup</a>
				<ul>
					<li><a href="./main_content.php?menu=staffId">Login password setup<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</li>
					<?php 
						if(isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint ==='yes' && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' )
						{
					?>
					<li><a href="./main_content.php?menu=designList2&setup=yes">Card design <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</li>
					<?php
						}
						else 
						{}
					?>
					<li><a href="./main_content.php?menu=accountSerial">Serial <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</li>
					<li><a href="./main_content.php?menu=appUrlList">Application <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</li>
				</ul>
			</li>				
			<?php
				}
			?>
		</ul>
	</div>
</div>
<!-- menu end --> 
