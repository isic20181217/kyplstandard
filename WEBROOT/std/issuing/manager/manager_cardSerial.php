<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['hidden']) && $_GET['hidden'] === 'yes'){
		$hiddenSel = 'yes';
	}
	else{
		$hiddenSel = 'no';
	}
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query2 = "SELECT stockId FROM $tablename10";
		if($hiddenSel === 'yes'){
			$query2 .= " WHERE hidden = 'yes'";
		}else{
			$query2 .= " WHERE hidden != 'yes' or hidden IS NULL";
		}
		$stmt2 = $db->prepare($query2);
		$stmt2->bindParam(':addedBy', $result->addedBy);
		$stmt2->execute();
		//echo $query2;
		//paging start
		$howmany = 10; // show how many for once
		$howmanypage = 10; // show how many pages for once
		$maxsize = $stmt2->rowCount();
		$maxpage = floor($maxsize / $howmany); //total 
		//echo ' '.$maxsize.' '.$maxpage.' ';
		if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1){
			$pagenum = $_GET['pagenum'];
		}
		else{
			$pagenum = 1; // current pagenumber
		}
		$page = ($pagenum-1)*$howmany;
		if($maxsize % $howmany > 0){
			$maxpage = $maxpage + 1; //if the number of result is 110 and the number of result for one page is 10, show result 101~110 on page 11
			}
		if ($pagenum > $maxpage){ 
			$pagenum = $maxpage;
		}
		else{}
		$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
		if($maxsize % ($howmany*$howmanypage) > 0){
			$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
		}
		else{}
		
		$currentgroup = floor($pagenum / $howmanypage); //current page's group
		if($pagenum % $howmanypage > 0){
			$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
			$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
		else{
			$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
		}
		//paging end		
		$query = "SELECT * FROM $tablename10";
		if($hiddenSel === 'yes'){
			$query .= " WHERE hidden = 'yes'";
		}else{
			$query .= " WHERE hidden != 'yes' or hidden IS NULL";
		}
		$query .= " ORDER BY stockId $searchTypeSel LIMIT $page, $howmany";
		$stmt = $db->prepare($query);
		$stmt->execute();
		$count1 = 0;
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$count1++;
			$stockList[$count1] = $result;
			$stockList[$count1]->avail = 0;
			$stockList[$count1]->total = 0;
			$stockList[$count1]->serialNumStart =' ';
			$stockList[$count1]->serialNumEnd =' ';
			$query2 = "SELECT count(serId) AS total FROM $tablename11 WHERE addedBy =:addedBy";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $stockList[$count1]->addedBy);
			$stmt2->execute();
			$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
			$stockList[$count1]->total = $result2->total;
			$query2 = "SELECT count(serId) AS avail FROM $tablename11 WHERE addedBy =:addedBy AND serAppNo IS NULL AND voided IS NULL";
			//$query2 = "SELECT count(serId) AS avail FROM $tablename11 WHERE addedBy =:addedBy AND serAppNo IS NULL  AND returned !='yes';";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $stockList[$count1]->addedBy);
			$stmt2->execute();
			$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
			$stockList[$count1]->avail = $result2->avail;
			
			$query2 = "SELECT serialNum FROM $tablename11 WHERE addedBy =:addedBy ORDER BY serId ASC LIMIT 1";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $stockList[$count1]->addedBy);
			$stmt2->execute();
			$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
			if(isset($result2->serialNum) && !empty($result2->serialNum)){
				$stockList[$count1]->serialNumStart = $result2->serialNum;
			}else{}
			$query2 = "SELECT serialNum FROM $tablename11 WHERE addedBy =:addedBy ORDER BY serId DESC LIMIT 1";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $stockList[$count1]->addedBy);
			$stmt2->execute();
			$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
			if(isset($result2->serialNum) && !empty($result2->serialNum)){
				$stockList[$count1]->serialNumEnd = $result2->serialNum;
			}else{}
			/*
			echo '<pre>';
			print_r($result2);
			echo '</pre>';
			*/
		}
		
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?> 
<!-- content start -->
<div id="contents">
<h1>Manage serial  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Serial list</span><a href="http://std.aretede.com/mockup/issuing/ManageSerial_stock.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>
  
	
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
            <th class="text-center thGrey">Serial Type</th>
            <th class="text-center thGrey">Serial Name</th>
			<th class="text-center thGrey">Serial Desc</th>
            <th class="text-center thGrey">Start</th>
            <th class="text-center thGrey">End</th>
            <th class="text-center thGrey">Available</th>
            <th class="text-center thGrey">Total</th>
            <th class="text-center thGrey">Added</th>
        </tr>
		<?php for($i=1;$i<$count1+1;$i++){ ?>
        <tr>
          <td class="text-center"><?php echo $stockList[$i]->serialType; ?></td>
          <td class="text-center"><a href="./main_content.php?menu=cardSerialDetail&stock=<?php echo $stockList[$i]->stockId ?>"><?php echo $stockList[$i]->serialName; ?></a></td>
		  <td class="text-center"><?php echo $stockList[$i]->serialDesc; ?></td>
          <td class="text-center"><?php echo $stockList[$i]->serialNumStart; ?></td>
          <td class="text-center"><?php echo $stockList[$i]->serialNumEnd; ?></td>
          <td class="text-right"><?php echo $stockList[$i]->avail; ?></td>
          <td class="text-center"><?php echo $stockList[$i]->total; ?></td>
          <td class="text-center"><?php echo $stockList[$i]->addedDate; ?></td>
        </tr>
		<?php } ?>
    </table>
    <!-- result end -->
	<div class="btnDiv">
		<a href="./main_content.php?menu=cardSerialNew" role="button" class="btn btn-kyp">Add <i class="fa fa-caret-right"></i></a>
		<?php		
		if($hiddenSel === 'yes'){
		?>
			<a href="./main_content.php?menu=cardSerial&hidden=no" role="button" class="btn btn-kyp">Current Serials</a>
		<?php
		}else{
		?>
			<a href="./main_content.php?menu=cardSerial&hidden=yes" role="button" class="btn btn-kyp">Hidden Serials</a>
		<?php
		}
		?>
	</div>
	<!-- Paging Start -->
	<?php 
		if($count1 === 0){
		}else{
	?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=cardSerial&pagenum=1&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=cardSerial&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=cardSerial&pagenum=$pagingno&searchType=$searchTypeSel&hidden=$hiddenSel\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=cardSerial&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=cardSerial&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
	<?php } ?>
    <!-- pagingEnd -->
</div>
<!-- content end -->