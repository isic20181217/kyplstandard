<?php
	// 210630
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($_SESSION['user_type'])
		{
			case 'Manager';
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' )
	{} 
	else 
	{
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sCanUrl) && $resultUserInfo->sCanUrl === 'yes'  && isset($resultOfficeInfo->canUrl) && $resultOfficeInfo->canUrl ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanCsv) && $resultUserInfo->sCanCsv === 'yes'  && isset($resultOfficeInfo->canCsv) && $resultOfficeInfo->canCsv ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanValidate) && $resultUserInfo->sCanValidate === 'yes'  && isset($resultOfficeInfo->canValidate) && $resultOfficeInfo->canValidate ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanPrintLabel) && $resultUserInfo->sCanPrintLabel === 'yes'  && isset($resultOfficeInfo->canPrintLabel) && $resultOfficeInfo->canPrintLabel ==='yes' )
	{} 
	else 
	{
		echo '<script>alert(\'Access Denied1111\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}						   
	$now1 = time();
	$rowcount1 =0;
	$totalCount =0;
	$currentDate = date("d/m/Y",$now1);
	$officeId = (int)$_SESSION['officeId'];
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeIdSel= preg_replace("/<|\/|_|>/","", $_GET['officeId'] );
	}
	else{
		$officeIdSel = 'ALL';
	}
	if(isset($_GET['orderType']) && !empty($_GET['orderType'])){
		$orderType= preg_replace("/<|\/|_|>/","", $_GET['orderType'] );
	}
	else{
		$orderType = 'ASC';
	}
	if(isset($_GET['startDate']) && !empty($_GET['startDate'])){
		$startDate= preg_replace("/<|_|>/","",$_GET['startDate'] );
		$startDateSel = explode('/',$startDate);
		if(sizeof($startDateSel) === 3){
		}
		else{
			$startDate = $currentDate;
		}
	}
	else{
		$startDate = $currentDate;
	}
	if(isset($_GET['endDate']) && !empty($_GET['endDate'])){
		$endDate= preg_replace("/<|_|>/","",$_GET['endDate'] );
		$endDateSel = explode('/',$endDate);
		if(sizeof($endDateSel) === 3){
		}
		else{
			$endDate = $currentDate;
		}
	}
	else{
		$endDate = $currentDate;
	}
	$startDateSel = explode('/',$startDate);
	$searchYear = $startDateSel[2];
	$searchMonth = $startDateSel[1];
	$searchDay = $startDateSel[0];
	$endDateSel = explode('/',$endDate);
	$searchYear2 = $endDateSel[2];
	$searchMonth2 = $endDateSel[1];
	$searchDay2 = $endDateSel[0];
	$searchStart = $searchYear.'-'.$searchMonth.'-'.$searchDay;
	$searchEnd = $searchYear2.'-'.$searchMonth2.'-'.$searchDay2;
	$timeLength = strtotime($searchEnd) - strtotime($searchStart);
	if((int)$timeLength > 60*60*24*365)
	{
		echo '<script>alert(\'The date interval must be no more than 365 days.\');</script>';
		$startDate = $currentDate;
		$endDate = $currentDate;
		$startDateSel = explode('/',$startDate);
		$searchYear = $startDateSel[2];
		$searchMonth = $startDateSel[1];
		$searchDay = $startDateSel[0];
		$endDateSel = explode('/',$endDate);
		$searchYear2 = $endDateSel[2];
		$searchMonth2 = $endDateSel[1];
		$searchDay2 = $endDateSel[0];
		$searchStart = $searchYear.'-'.$searchMonth.'-'.$searchDay;
		$searchEnd = $searchYear2.'-'.$searchMonth2.'-'.$searchDay2;
		$timeLength = strtotime($searchEnd) - strtotime($searchStart);
	}
	else
	{}

	$searchBySerialType ='no';
	$searchSerialType ='all';
	if(isset($_GET['serialType']))
	{
		switch($_GET['serialType'])
		{
			case 'ISIC':
				$searchSerialType ='ISIC';
				$searchBySerialType ='yes';
				break;
			case 'ITIC':
				$searchSerialType ='ITIC';
				$searchBySerialType ='yes';
				break;
			case 'IYTC':
				$searchSerialType ='IYTC';
				$searchBySerialType ='yes';
				break;
			default:
				break;
				
		}
	}
	else
	{}
	$serialCheck ='no';
	$serialNum ='';
	if(isset($_GET['serialNum']) && !empty($_GET['serialNum'])){
		$serialNum = preg_replace("/<|_|>/","",$_GET['serialNum'] );
		$serialCheck ='yes';
	}
	else
	{}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		// paging calculation
		$query = "SELECT COUNT(iss_No) as totalCount FROM $tablename32 WHERE 1=1";
		if($officeId === 1){
			if($officeIdSel === 'ALL'){
			} else if ($officeIdSel === '1'){
				$query .= " AND (iss_OfficeId = :iss_OfficeId OR iss_OfficeId = 990)";
			} else {
				$query .= " AND iss_OfficeId = :iss_OfficeId";
			}
		} else {
			$query .= " AND iss_OfficeId = :iss_OfficeId";
		}
		if($searchBySerialType === 'yes')
		{
			$query .= " AND (iss_SerialType = :iss_SerialType)";
		}
		else
		{}
		if($serialCheck ==='yes')
		{
			$query .= " AND (iss_SerialNum = :iss_SerialNum)";
		}
		else
		{}
		//$query .= " AND year(issuedDate) = :inputValue2 AND month(issuedDate) = :inputValue3 AND day(issuedDate) = :inputValue4";
		$query .= " AND DATE(iss_IssuedDate) BETWEEN :searchStart AND :searchEnd";
		$query .= " ORDER BY iss_IssuedDate";
		if($orderType === 'DESC'){ //Do not put GET data directly into the query.
			$query .= " DESC";
		}else{
			$query .= " ASC";
		}
		$stmt = $db->prepare($query);
		if($officeId === 1){
			if($officeIdSel === 'ALL'){
			}else{
				$stmt->bindParam(':iss_OfficeId', $officeIdSel);
			}
		}else{
			$stmt->bindParam(':iss_OfficeId', $officeId);
		}
		if($searchBySerialType === 'yes')
		{
			$stmt->bindParam(':iss_SerialType', $searchSerialType);
		}
		else
		{}
		if($serialCheck ==='yes')
		{
			$stmt->bindParam(':iss_SerialNum', $serialNum);
		}
		else
		{}
		/*
		$stmt->bindParam(':inputValue2', $searchYear);
		$stmt->bindParam(':inputValue3', $searchMonth);
		$stmt->bindParam(':inputValue4', $searchDay);*/
		$stmt->bindParam(':searchStart', $searchStart);
		$stmt->bindParam(':searchEnd', $searchEnd);
		$stmt->execute();
		while($result = $stmt->fetch(PDO::FETCH_OBJ))
		{
			$howmany = 20; // show how many Things for once
			$howmanypage = 10; // show how many pages for once
			$maxsize = $result->totalCount;
			$totalCount = $maxsize;
			$maxpage = floor($maxsize / $howmany); //total application 
		}
		if($totalCount >0)
		{
			//echo ' '.$maxsize.' '.$maxpage.' ';
			if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1)
			{
				$pagenum = preg_replace("/<|\/|_|>/","",$_GET['pagenum']); // current pagenumber
			}
			else
			{
				$pagenum = 1; // current pagenumber
			}
			$page = ($pagenum-1)*$howmany;
			if($maxsize % $howmany > 0)
			{
				$maxpage = $maxpage + 1; //if the number of things is 110 and the number of things for one page is 10, show thing 101~110 on page 11
			}
			else
			{}
			if ($pagenum > $maxpage){ 
				$pagenum = $maxpage;
			}
			else
			{}
			$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
			if($maxsize % ($howmany*$howmanypage) > 0)
			{
				$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
			}
			else{}
			$currentgroup = floor($pagenum / $howmanypage); //current page's group
			if($pagenum % $howmanypage > 0)
			{
				$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
				$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
			else
			{
				$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
			}
			
			// paging end
			$query = "SELECT * FROM $tablename32 WHERE 1=1";
			if($officeId === 1){
				if($officeIdSel === 'ALL'){
				} else if ($officeIdSel === '1'){
					$query .= " AND (iss_OfficeId = :iss_OfficeId OR iss_OfficeId = 990)";
				} else {
					$query .= " AND iss_OfficeId = :iss_OfficeId";
				}
			} else {
				$query .= " AND iss_OfficeId = :iss_OfficeId";
			}
			if($searchBySerialType === 'yes')
			{
				$query .= " AND (iss_SerialType = :iss_SerialType)";
			}
			else
			{}
			if($serialCheck ==='yes')
			{
				$query .= " AND (iss_SerialNum = :iss_SerialNum)";
			}
			else
			{}
			//$query .= " AND year(issuedDate) = :inputValue2 AND month(issuedDate) = :inputValue3 AND day(issuedDate) = :inputValue4";
			$query .= " AND DATE(iss_IssuedDate) BETWEEN :searchStart AND :searchEnd";
			$query .= " ORDER BY iss_IssuedDate ";
			if($orderType === 'DESC'){ //Do not put GET data directly into the query.
				$query .= " DESC";
			}else{
				$query .= " ASC";
			}
			$query .= " LIMIT :page, :howmany";
			$stmt = $db->prepare($query);
			if($officeId === 1){
				if($officeIdSel === 'ALL'){
				}else{
					$stmt->bindParam(':iss_OfficeId', $officeIdSel);
				}
			}else{
				$stmt->bindParam(':iss_OfficeId', $officeId);
			}
			if($searchBySerialType === 'yes')
			{
				$stmt->bindParam(':iss_SerialType', $searchSerialType);
			}
			else
			{}
			if($serialCheck ==='yes')
			{
				$stmt->bindParam(':iss_SerialNum', $serialNum);
			}
			else
			{}
			/*
			$stmt->bindParam(':inputValue2', $searchYear);
			$stmt->bindParam(':inputValue3', $searchMonth);
			$stmt->bindParam(':inputValue4', $searchDay);*/
			$stmt->bindParam(':searchStart', $searchStart);
			$stmt->bindParam(':searchEnd', $searchEnd);
			$stmt->bindParam(':page', $page, PDO::PARAM_INT);
			$stmt->bindParam(':howmany', $howmany, PDO::PARAM_INT);
			$stmt->execute();
			$rowcount1 = $stmt->rowCount();
			$count1 = 0;
			//echo "$officeId ";
			while($result = $stmt->fetch(PDO::FETCH_OBJ)){
				
				$appList[$count1] = $result;
				$count1++;
			}
			//print_r($stmt->errorInfo());
		}
		else
		{
			$count1 =0;
		}
																						
		$query = "SELECT officeId,name FROM $tablename12";
		//$valueYes1 = 'yes';
		$stmt = $db->prepare($query);
		//$stmt->bindParam(':activeOffice', $valueYes1);
		$stmt->execute();
		if($stmt->rowCount() >= 1){
			$officeCount = $stmt->rowCount();
			$i =0;
			while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
				$officeList[$i] = $result0;
				$i++;
			}
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}		
		$db= NULL;
	}
	catch (PDOExeception $e)
	{
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}

?>
<!-- 내용 영역 시작 -->
<div id="contents">

<h1>Account data by serials</h1>

    <!-- Search Start -->
    <div class="searchDiv">
	<form name="accountSerialForm" id="accountSerialForm">
	<input type="hidden" name="menu" type="menu" value="<?php echo $menu?>">
		<table>
			<tr>
				<td>
					<label>Serial type</label>
				</td>
				<td>
<?php 
	$selection1 = '';
	$selection2 = '';
	$selection3 = '';
	$selection4 = '';
	if($searchSerialType=== 'ISIC')
	{
		$selection2 = 'selected';
	} 
	else if ($searchSerialType=== 'ITIC') 
	{
		$selection3 = 'selected';
	} 
	else if ($searchSerialType=== 'IYTC') 
	{
		$selection4 = 'selected';
	} 
	else 
	{
		$selection1 = 'selected';
	}
?>
					<select name="serialType" id="serialType" class="form-control">
						<option <?php echo $selection1?> value="all">ALL</option>
						<option <?php echo $selection2?> value="ISIC">ISIC</option>
						<option <?php echo $selection3?> value="ITIC">ITIC</option>
						<option <?php echo $selection4?> value="IYTC">IYTC</option>
					</select>
				</td>
				<td>
					<label class="ml20">Office ID</label>
				</td>
				<td>
					<?php 
						if($officeId === 1){ 
					?>
					<select class="form-control" name="officeId" id="officeId">
						<option value="ALL">ALL</option>
						<?php 
							for($i=0;$i<$officeCount;$i++)
							{
								$selectThis = '';
								if($officeIdSel === $officeList[$i]->officeId)
								{
									$selectThis ='selected';
								}
								else
								{}						
						?>
							<option <?php echo $selectThis; ?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
						<?php 
							}
						?>
					</select>
					<?php 
					} 
					else 
					{} 
					?>
				<td><label class="ml20">Issued date</label></td>
				<td>
				  <input type="text" class="form-control formYoonCa p-5" name="startDate" id="startDate" placeholder="dd/mm/yyyy" value="<?php echo $startDate?>" maxlength="10">
				  ~
				  <input type="text" class="form-control formYoonCa p-5" name="endDate" id="endDate" placeholder="dd/mm/yyyy" value="<?php echo $endDate?>" maxlength="10">
				</td>
				<td><label class="ml20">Serial no.</label></td>
				<td><input type="text" name="serialNum" id="serialNum" class="form-control" value="<?php echo $serialNum?>" maxlength="14"></td>
				<td><button type="submit" class="btn btn-kyp btn-sm ml20"><i class="fab fa-sistrix"></i> Search</button></td>
		  </tr>
		</table>
		</form>
    </div>
    <!-- Search End -->


    <!-- result start -->
		<table class="table table-bordered">
			<tr>
				<th rowspan="2" class="text-center thGrey"></th>
				<th rowspan="2" class="text-center thGrey">SerialType</th>
				<th rowspan="2" class="text-center thGrey">PaidDate</th>
				<th rowspan="2" class="text-center thGrey">Issued date</th>
				<th rowspan="2" class="text-center thGrey">ValidityEnd</th>
				<th rowspan="2" class="text-center thGrey">Serial no.</th>
				<th colspan="4" class="text-center thGrey">Payment Amount</th>
			</tr>
			<tr>
			  <th class="text-center thGrey">Card price</th>
			  <th class="text-center thGrey">Shipping price</th>
			  <th class="text-center thGrey">Discount(PromoId)</th>
			  <th class="text-center thGrey">Total</th>
			</tr>
<?php 
$totalCardPrice = 0;
$totalShippingPrice = 0;
$totalChangePrice = 0;
$totalFinalPrice = 0;
for($i=0;$i<$count1;$i++)
{ 
?>
			<tr>
				<td class="text-center"><?php echo $appList[$i]->iss_AppNo; ?></td>
				<td class="text-center"><?php echo $appList[$i]->iss_SerialType; ?></td>
				<td class="text-center"><?php echo $appList[$i]->iss_PaidDate; ?></td>
				<td class="text-center"><?php echo $appList[$i]->iss_IssuedDate; ?></td>
				<td class="text-center"><?php echo $appList[$i]->iss_ValidityEnd; ?></td>
				<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $appList[$i]->iss_AppNo; ?>"><?php echo $appList[$i]->iss_SerialNum; ?></td>
				<td class="text-right"><?php echo $appList[$i]->iss_CardPrice; ?></td>
				<td class="text-right"><?php echo $appList[$i]->iss_ShippingPrice; ?></td>
				<td class="text-right"><?php echo $appList[$i]->iss_ChangePrice; ?> (<a href="./main_content.php?menu=promoDetail&gId=<?php echo $appList[$i]->iss_PromoId ?>"><?php echo $appList[$i]->iss_PromoId ?></a>)</td>
				<?php
					$totalPrice = 0;
					$totalPrice = (float)$appList[$i]->iss_CardPrice+(float)$appList[$i]->iss_ShippingPrice-(float)$appList[$i]->iss_ChangePrice;
					$totalCardPrice = (float)$totalCardPrice + (float)$appList[$i]->iss_CardPrice;
					$totalShippingPrice = (float)$totalShippingPrice + (float)$appList[$i]->iss_ShippingPrice;
					$totalChangePrice = (float)$totalChangePrice + (float)$appList[$i]->iss_ChangePrice;
					$totalFinalPrice = (float)$totalFinalPrice + (float)$appList[$i]->iss_CardPrice + (float)$appList[$i]->iss_ShippingPrice + (float)$appList[$i]->iss_ChangePrice;
				?>
				<td class="text-right"><strong><?php echo $totalPrice?></strong></td>
			</tr>
<?php 
}
?>
			<tr class="thGrey">
				<td colspan="6" class="text-center">Total</td>
				<td class="text-right"><?php echo $totalCardPrice?></td>
				<td class="text-right"><?php echo $totalShippingPrice?></td>
				<td class="text-right"><?php echo $totalChangePrice?></td>
				<td class="text-right"><strong><?php echo $totalFinalPrice?></strong></td>
			</tr>
		</table>
    <!-- result end -->
 	<!-- Paging Start -->
<?php 
if($rowcount1 > 0)
{
?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=<?php echo $menu?>&pagenum=1&orderType=<?php echo $orderType?>&serialType=<?php echo $searchSerialType?>&officeId=<?php echo $officeIdSel?>&startDate=<?php echo $startDate?>&endDate=<?php echo $endDate?>&serialNum=<?php echo $serialNum?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=<?php echo $menu?>&pagenum=<?php echo $previouspage; ?>&orderType=<?php echo $orderType?>&serialType=<?php echo $searchSerialType?>&officeId=<?php echo $officeIdSel?>&startDate=<?php echo $startDate?>&endDate=<?php echo $endDate?>&serialNum=<?php echo $serialNum?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=$menu&pagenum=$pagingno&orderType=$orderType&serialType=$searchSerialType&officeId=$officeIdSel&startDate=$startDate&endDate=$endDate&serialNum=$serialNum\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=<?php echo $menu?>&pagenum=<?php echo $nextpage; ?>&orderType=<?php echo $orderType?>&serialType=<?php echo $searchSerialType?>&officeId=<?php echo $officeIdSel?>&startDate=<?php echo $startDate?>&endDate=<?php echo $endDate?>&serialNum=<?php echo $serialNum?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=<?php echo $menu?>&pagenum=<?php echo $maxpage; ?>&orderType=<?php echo $orderType?>&serialType=<?php echo $searchSerialType?>&officeId=<?php echo $officeIdSel?>&startDate=<?php echo $startDate?>&endDate=<?php echo $endDate?>&serialNum=<?php echo $serialNum?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
<?php 
}
else
{} 
?>
    <!-- pagingEnd -->
</div>
<!-- 내용 영역 끝 -->