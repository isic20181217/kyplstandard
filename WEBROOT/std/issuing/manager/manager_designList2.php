<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$hiddenSel = 'hiddenSel';
	if(isset($_GET['cardType']) ){
		$searchCardType = 'yes';
		switch($_GET['cardType']){
			case 'ISIC':
				$cardType='ISIC';
				break;
			case 'ITIC':
				$cardType='ITIC';
				break;
			case 'IYTC':
				$cardType='IYTC';
				break;
			default :
				$searchCardType = 'no';
				$cardType ='ALL';
				break;
		}
	} else {
		$searchCardType = 'no';
		$cardType ='ALL';
	}
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	if(isset($_GET['isActive']) && $_GET['isActive'] === 'no'){
		$activeDesign = 'no';
	}
	else{
		$activeDesign = 'yes';
	}
	$dateStartO = '';
	$dateEndO = '';
	if(isset($_GET['dateStart']) && !empty($_GET['dateStart']) && isset($_GET['dateEnd']) && !empty($_GET['dateEnd']) ){
		$searchDate ='yes';	
	}else{
		$searchDate ='no';
	}
	
	if($searchDate ==='yes'){
		$dateStartO = preg_replace("/<|_|>/","",$_GET['dateStart']);
		$dateEndO = preg_replace("/<|_|>/","",$_GET['dateEnd']);
		$dateStartArray = explode('/',$dateStartO);
		$dateEndArray = explode('/',$dateEndO);
		$dateStart = $dateStartArray[2].'-'.$dateStartArray[1].'-'.$dateStartArray[0];
		$dateEnd = $dateEndArray[2].'-'.$dateEndArray[1].'-'.$dateEndArray[0];
	}else{
	}
	
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query2 = "SELECT designId FROM $tablename30";
		if($activeDesign === 'yes'){
			$query2 .= " WHERE activeDesign = 'yes'";
		}else{
			$query2 .= " WHERE (activeDesign != 'yes' or activeDesign IS NULL)";
		}
		if($searchCardType === 'yes'){
			$query2 .= " AND cardType = :cardType";
		} else { }
		if($searchDate === 'yes'){
			$query2.=" AND date(addedDate) BETWEEN :dateStart AND :dateEnd";
		} else { }
		$query2.=" ORDER BY designNo ASC";
		$stmt2 = $db->prepare($query2);
		if($searchCardType === 'yes'){
		$stmt2->bindParam(':cardType', $cardType);
		} else { }
		if($searchDate === 'yes'){
			$stmt2->bindParam(':dateStart', $dateStart);
			$stmt2->bindParam(':dateEnd', $dateEnd);
		} else { }
		$stmt2->execute();
		//echo $query2;
		//paging start
		$howmany = 10; // show how many for once
		$howmanypage = 10; // show how many pages for once
		$maxsize = $stmt2->rowCount();
		$maxpage = floor($maxsize / $howmany); //total 
		//echo ' '.$maxsize.' '.$maxpage.' ';
		if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1){
			$pagenum = $_GET['pagenum'];
		}
		else{
			$pagenum = 1; // current pagenumber
		}
		$page = ($pagenum-1)*$howmany;
		if($maxsize % $howmany > 0){
			$maxpage = $maxpage + 1; //if the number of result is 110 and the number of result for one page is 10, show result 101~110 on page 11
			}
		if ($pagenum > $maxpage){ 
			$pagenum = $maxpage;
		}
		else{}
		$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
		if($maxsize % ($howmany*$howmanypage) > 0){
			$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
		}
		else{}
		
		$currentgroup = floor($pagenum / $howmanypage); //current page's group
		if($pagenum % $howmanypage > 0){
			$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
			$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
		else{
			$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
		}
		//paging end		
		$query = "SELECT * FROM $tablename30";
		if($activeDesign === 'yes'){
			$query .= " WHERE activeDesign = 'yes'";
		}else{
			$query .= " WHERE activeDesign != 'yes' or activeDesign IS NULL";
		}
		if($searchCardType === 'yes'){
			$query .= " AND cardType = :cardType";
		} else { }
		if($searchDate === 'yes'){
			$query .=" AND date(addedDate) BETWEEN :dateStart AND :dateEnd";
		} else { }
		//$query .= " ORDER BY designId $searchTypeSel LIMIT $page, $howmany";
		$query .= " ORDER BY designNo ASC LIMIT :page, :howmany";
		$stmt = $db->prepare($query);
		if($searchCardType === 'yes'){
		$stmt->bindParam(':cardType', $cardType);
		} else { }
		if($searchDate === 'yes'){
			$stmt->bindParam(':dateStart', $dateStart);
			$stmt->bindParam(':dateEnd', $dateEnd);
		} else { }
		$stmt->bindParam(':page', $page, PDO::PARAM_INT);
		$stmt->bindParam(':howmany', $howmany, PDO::PARAM_INT);
		//echo ' '.$page.' '.$howmany.' ';
		$stmt->execute();
		//echo $query;
		$count1 = 0;
		//print_r($stmt->errorInfo());
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$count1++;
			$designList[$count1] = $result;
		}
		/*
		echo '<pre>';
		print_r($designList);
		echo '</pre>';
		*/
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?> 
<!-- content start -->
<div id="contents">
<h1>Card print test</h1>
	<!-- search start -->
	<form name="searchInSearch" id="searchInSearch" action="./main_content.php" method="GET">
		<input type="hidden" name="menu" id="menu" value="designList2">
		<div class="searchDiv">
				<div class="row">
					<div class="col-xs-10">
						<label>Active</label>
						<select class="form-control formYoon" id="isActive" name="isActive">
								<?php 
									if(isset($activeDesign)){ 
										$selectYes =' ';
										$selectNo =' ';
										switch($activeDesign){
											case 'yes':
												$selectYes ='selected';
												break;
											case 'no':
												$selectNo ='selected';
												break;
											default:
												break;
										}
									}else{}										
								?>
								<option <?php echo $selectYes?> value="yes">Yes</option>
								<option <?php echo $selectNo?> value="no">No</option>
						</select>
						<label>Card Type</label>
						<select class="form-control formYoon" id="cardType" name="cardType">
								<?php 
									if(isset($cardType)){ 
										$selectISIC =' ';
										$selectITIC =' ';
										$selectIYTC =' ';
										switch($cardType){
											case 'ISIC':
												$selectISIC ='selected';
												break;
											case 'ITIC':
												$selectITIC ='selected';
												break;
											case 'IYTC':
												$selectIYTC ='selected';
												break;
											default:
												break;
										}
									}else{}										
								?>
								<option value="All">All</option>
								<option <?php echo $selectISIC?> value="ISIC">ISIC</option>
								<option <?php echo $selectITIC?> value="ITIC">ITIC</option>
								<option <?php echo $selectIYTC?> value="IYTC">IYTC</option>
						</select>
						<label>Added date</label>
						<input type="text" class="form-control formYoonCa" placeholder="dd/mm/yyyy" value="<?php echo $dateStartO ?>" id="dateStart" name="dateStart" style="width: 110px;"> ~
						<input type="text" class="form-control formYoonCa" placeholder="dd/mm/yyyy" value="<?php echo $dateEndO ?>" id="dateEnd" name="dateEnd" style="width: 110px;">
						<button type="submit" class="btn btn-kyp ml20"><i class="fab fa-sistrix"></i> Search</button>
					</div>
				</div>
		</div>
	</form>
	<!-- search end -->

	
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
            <th class="text-center thGrey"> </th>
            <th class="text-center thGrey">DesignNo</th>
			<th class="text-center thGrey">Type</th>
            <th class="text-center thGrey">AddedDate</th>
        </tr>
		<?php for($i=1;$i<$count1+1;$i++){ ?>
        <tr>
			<td class="text-center"></td>
			<td class="text-center"><a href="./main_content.php?menu=designEditor2&designId=<?php echo $designList[$i]->designId ?>"><?php echo $designList[$i]->designNo; ?></a></td>
			<td class="text-center"><?php echo $designList[$i]->cardType; ?></td>
			<td class="text-center"><?php echo $designList[$i]->addedDate; ?></td>
        </tr>
		<?php } ?>
    </table>
    <!-- result end -->
	<!-- Paging Start -->
	<?php 
		if($count1 === 0){
		}else{
	?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=designList2&pagenum=1&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=designList2&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=designList2&pagenum=$pagingno&searchType=$searchTypeSel&hidden=$hiddenSel\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=designList2&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=designList2&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>&hidden=<?php echo $hiddenSel?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
	<?php } ?>
    <!-- pagingEnd -->
</div>
<!-- content end -->