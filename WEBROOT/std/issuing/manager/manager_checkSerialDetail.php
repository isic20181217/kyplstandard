<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?>
<?php
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT * FROM $tablename11 WHERE serType='none'";
		if(isset($_POST['serialNum'])){
			$query .= " OR serialNum = :searchValue1";
		}else{}
		if(isset($_POST['appId'])){
			$query .= " OR serAppNo = :searchValue2";
		}else{}
		$stmt = $db->prepare($query);
		if(isset($_POST['serialNum'])){
			$stmt->bindParam(':searchValue1', $_POST['serialNum']);
		}else{}
		if(isset($_POST['appId'])){
			$stmt->bindParam(':searchValue2', $_POST['appId']);
		}else{}		
		$stmt->execute();
		if($stmt->rowCount()>0){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$type = $result->serType;
			$serialName = $result->serialName;
			$serialNum = $result->serialNum;
			$appId = $result->serAppNo;
			$addedDate = $result->addedDate;
			$usedDate = $result->usedDate;
			$returned = $result->returned;
			$db= NULL;
		}
		else{
			//print_r($stmt->errorInfo());
			$type = '';
			$serialName = '';
			$serialNum = '';
			$appId = '';
			$addedDate = '';
			$usedDate = '';
			$returned = '';
			$db= NULL;
		}
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?>
<!-- Contents -->
<form name="checkSerialDetail" method="POST" action="./main_content.php?menu=checkSerialDetail">
<div id="contents">
<h1>Check serial numbers <i class="fas fa-angle-double-right"></i> <span class="h1Sub">ard Stock Manager</span></h1>
    <!-- Data Table -->
    <table class="table table-bordered">
        <tr>
            <th class="text-center thGrey"></th>
            <th class="text-center thGrey">Type</th>
			<th class="text-center thGrey">Serial Name</th>
            <th class="text-center thGrey">Serial Number</th>
            <th class="text-center thGrey">Application ID</th>
            <th class="text-center thGrey">Added Date</th>
			<th class="text-center thGrey">Used Date</th>
			<th class="text-center thGrey">Returned</th>
			<th class="text-center thGrey"></th>
        </tr>
        <tr>
          <td class="text-center"></td>
          <td class="text-center"><?php echo $type; ?></td>
		  <td class="text-center"><?php echo $serialName; ?></td>
          <td class="text-center"><input type="text" name="serialNum" value="<?php echo $serialNum;?>"></td>
          <td class="text-center"><input type="text" name="appId" value="<?php echo $appId; ?>"> <button type="submit" class="btn btn-kyp">Make Null</button></td>
          <td class="text-center"><?php echo $addedDate; ?></td>
		  <td class="text-center"><?php echo $usedDate; ?></td>
		  <td class="text-center"><?php echo $returned; ?></td>
		  <td class="text-center"><button type="submit" class="btn btn-kyp">Delete</button></td>
        </tr>
    </table>
    <!-- Data Table End -->
	<div class="btnDiv">
		<button type="submit" class="btn btn-kyp">Search</button>
    </div>
</div>
</form>
<!-- Contents End -->