<?php 
// 210510 add excel file accept
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?> 
<!-- content start -->
<div id="contents">
  <h1>Manage serial <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Add serials</span><a href="http://std.aretede.com/mockup/issuing/ManageSerial_stockAdd.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>


<form id="serialAddForm" name="serialAddForm" method="POST" action="./work/addSerial.php" enctype="multipart/form-data">
<input type="hidden" name="formName" value="addCardSerial">	
	
	<div class="row">
		<div class="col-xs-4"><h2><i class="far fa-list-alt"></i> Serial number information</h2></div>
		<div class="col-xs-8 text-right small pt-5" style="color: #999;">* Use the same serial name you enter here when you create the Application URL.</div>
	</div>
	
	<table class="table table-bordered">   
		<tr>
		  <th class="text-center thGrey">Serial Name</th>
		  <td colspan="3"><input type="text" class="form-control" name="serialName" maxlength="25"></td>
        </tr>
		<tr>
		  <th class="text-center thGrey">Serial Type</th>
		  <td colspan="3">
			<select class="form-control" name="serialType1" id="serialType1">
				<option value="ISIC">ISIC</option> 
				<option value="ITIC">ITIC</option> 
				<option value="IYTC">IYTC</option> 
			</select>
			</td>
        </tr>
		<tr>
		  <th class="text-center thGrey">Serial Descrption</th>
		  <td colspan="3"><input type="text" class="form-control" name="serialDesc" maxlength="45"></td>
        </tr>
<?php 
	if(isset($usePhpSpreadsheet) && $usePhpSpreadsheet === 'yes')
	{
?>
		<tr>
		  <th class="text-center thGrey">Serial Excel file</th>
		  <td colspan="3"><input type="file" class="form-control" name="file1" id="file1" accept=".xls, .xlsx"></td>
	    </tr>
<?php
	}
	else
	{
?>
		<tr>
		  <th class="text-center thGrey">Serial CSV file</th>
		  <td colspan="3"><input type="file" class="form-control" name="file1" id="file1" accept=".csv"></td>
	    </tr>
<?php
	}
?>

    </table>
    <div class="btnDiv row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3"><button type="submit" class="btn btn-kyp btn-block">Add</button></div>
		<div class="col-sm-3"><a href="./main_content.php?menu=cardSerial" role="button" class="btn btn-kyp btn-block">Go to serial list</a></div>
		<div class="col-sm-3"></div>
    </div>
</form>
</div>
<!-- content end -->