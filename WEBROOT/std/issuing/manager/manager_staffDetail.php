<?php 
//200608 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['officeId']) && !empty($_GET['officeId']) ){
		$officeId = preg_replace("/<|\/|_|>/","",$_GET['officeId']);
	}else{
		echo '<script>alert(\'Forbidden\');</script>';
		//echo '1';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['idNo']) && !empty($_GET['idNo']) ){
		$idNo = preg_replace("/<|\/|_|>/","",$_GET['idNo']);
	}else{
		echo '<script>alert(\'Forbidden\');</script>';
		//echo '2';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['id']) && !empty($_GET['id']) ){
		$id = preg_replace("/<|\/|_|>/","",$_GET['id']);
	}else{
		echo '<script>alert(\'Forbidden\');</script>';
		//echo '3';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT * FROM $tablename06 WHERE id =:id AND officeId = :officeId AND no =:idNo";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':idNo', $idNo);
		$stmt->bindParam(':officeId', $officeId);
		$stmt->execute();
		if($stmt->rowCount() > 0){
			$resultInfo = $stmt->fetch(PDO::FETCH_OBJ);
		} else {
			$db= NULL;
			echo '<script>alert(\'forbidden.\');</script>';
			//echo '4';
			echo '<script>location.replace("/std/index.php");</script>';
			exit;
		}
		
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	/*
	echo '<pre>';
	print_r($resultInfo);
	echo '</pre>';
	*/
	$db=NULL;
?> 
<!-- content start-->
<script type="text/javascript" src="./manager/manager_staffDetail.js"></script>
<form id="staffDetail" name="staffDetail" method="POST">
<input type="hidden" name="formName" value="staffDetail">
<input type="hidden" name="officeId" value="<?php echo $resultInfo->officeId ?>">
<input type="hidden" name="idNo" value="<?php echo $resultInfo->no ?>">
<div id="contents">
<h1>Staff Id <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Setting</span></h1>     
	<h2 class="pt30"><i class="fas fa-user"></i> ID/PW Setting</h2>
	<table class="table table-bordered">
		<tr>
			<th class="text-center thGrey">Id</th>
			<td><input type="text" class="form-control" name="id1" id="id1" value="<?php echo $resultInfo->id?>" maxlength="20" placeholder="4~20Character"></td>
			<th class="text-center thGrey">Password</th>
			<td><input type="password" class="form-control" name="password1" id="password1" value="" maxlength="20" placeholder="4~20Character"></td>
		</tr>
		<!--
		<tr>
			<th class="text-center thGrey">Active Staff</th>
			<td class="w30p">
			<?php echo $resultInfo->activeStaff; ?>
				<select name="activeStaff" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'activeStaff';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>
			</td>
			<th class="text-center thGrey"></th>
			<td></td>
		</tr>
		-->
	</table>
    

	<!--
	<h2><i class="far fa-list-alt"></i> Authorization</h2>
	<table class="table table-bordered">   
		<tr>
			<th class="text-center thGrey w20p">URL Application</th>
			<td class="w30p">
			<?php //echo $resultInfo->sCanUrl; ?>
				<select name="sCanUrl" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanUrl';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>
			</td>
			<th class="text-center thGrey">CSV Application</th>
			<td><?php //echo $resultInfo->sCanCsv; ?>
				<select name="sCanCsv" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanCsv';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>	
			</td>
		</tr>
		<tr>
			<th class="text-center thGrey">Validation</th>
			<td><?php// echo $resultInfo->sCanValidate; ?>
				<select name="sCanValidate" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanValidate';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="text-center thGrey">Issuing</th>
			<td><?php //echo $resultInfo->sCanIssue; ?>
				<select name="sCanIssue" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanIssue';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		<tr>
			<th class="text-center thGrey">Printing(Card)</th>
			<td><?php //echo $resultInfo->sCanPrint; ?>
				<select name="sCanPrint" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanPrint';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="text-center thGrey">Printing(Label)</th>
			<td><?php //echo $resultInfo->sCanPrintLabel; ?>
				<select name="sCanPrintLabel" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sCanPrintLabel';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		<tr>
			<th class="text-center thGrey">Menu Issuing</th>
			<td><?php //echo $resultInfo->sMenuIssuing; ?>
				<select name="sMenuIssuing" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sMenuIssuing';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="text-center thGrey">Menu Reports</th>
			<td><?php //echo $resultInfo->sMenuReports; ?>
				<select name="sMenuReports" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sMenuReports';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		<tr>
			<th class="text-center thGrey">Menu Serial</th>
			<td><?php //echo $resultInfo->sMenuSerial; ?>
				<select name="sMenuSerial" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sMenuSerial';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="text-center thGrey">Menu IO</th>
			<td><?php //echo $resultInfo->sMenuIo; ?>
				<select name="sMenuIo" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sMenuIo';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		<tr>
			<th class="text-center thGrey">Menu Application</th>
			<td><?php //echo $resultInfo->sMenuApplication; ?>
				<select name="sMenuApplication" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'sMenuApplication';
						if(isset($resultInfo->$selectionType) && $resultInfo->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="text-center thGrey"></th>
			<td></td>
		</tr>
    </table>
	-->
	<div class="btnDiv">
		<button type="button" class="btn btn-kyp" onclick="submit1(1);">Save</button>
		<a href="./main_content.php?menu=issuingOfficeDetail&officeId=<?php echo $resultInfo->officeId ?>" role="button" class="btn btn-kyp">Back to list</a>
	</div>
</div>
</form>
<!-- content end -->
