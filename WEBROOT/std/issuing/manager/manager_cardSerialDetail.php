<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';

		exit;
	}
	if(isset($_GET['stock'])){
		$stockId= preg_replace("/<|\/|_|>/","",$_GET['stock'] ); 
	}
	else{
		echo '<script>alert(\'no id.\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
	$searchTypeSel = 'ASC';
	}
	else{
	$searchTypeSel = 'DESC';
	}
	$rowCount1 =0;
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT * FROM $tablename10 WHERE stockId =:stockId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':stockId', $stockId);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$addedBy = $result->addedBy;
		}else{
			echo 'no Result1';
			exit;
		}

		if(isset($result->addedBy) && !empty($result->addedBy)){
			$query2 = "SELECT serId FROM $tablename11 WHERE addedBy =:addedBy";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $addedBy);
			$stmt2->execute();
			
			//paging start
			$howmany = 100; // show how many for once
			$howmanypage = 10; // show how many pages for once
			$maxsize = $stmt2->rowCount();
			$maxpage = floor($maxsize / $howmany); //total 
			//echo ' '.$maxsize.' '.$maxpage.' ';
			if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1){
				$pagenum = preg_replace("/<|\/|_|>/","",$_GET['pagenum']); // current pagenumber
			}
			else{
				$pagenum = 1; // current pagenumber
			}
			$page = ($pagenum-1)*$howmany;
			if($maxsize % $howmany > 0){
				$maxpage = $maxpage + 1; //if the number of result is 110 and the number of result for one page is 10, show result 101~110 on page 11
				}
			if ($pagenum > $maxpage){ 
				$pagenum = $maxpage;
			}
			else{}
			$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
			if($maxsize % ($howmany*$howmanypage) > 0){
				$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
			}
			else{}
			
			$currentgroup = floor($pagenum / $howmanypage); //current page's group
			if($pagenum % $howmanypage > 0){
				$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
				$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
				}
			else{
				$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
			}
			//paging end
			
			$query2 = "SELECT * FROM $tablename11 WHERE addedBy =:addedBy";
			//$query2 .= " ORDER BY serId $searchTypeSel LIMIT $page, $howmany";
			$query2 .= " ORDER BY serId $searchTypeSel LIMIT :page, :howmany";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':addedBy', $addedBy);
			$stmt2->bindParam(':page', $page, PDO::PARAM_INT);
			$stmt2->bindParam(':howmany', $howmany, PDO::PARAM_INT);
			$stmt2->execute();
			/*
			echo $query2;
			echo '<br/>';
			echo $addedBy;
			echo '<br/>';
			echo $page;
			echo '<br/>';
			echo $howmany;
			echo '<br/>';
			echo $stmt2->rowCount();
			echo '<br/>';
			*/
			if($stmt2->rowCount()>0){
				$rowCount1 = $stmt2->rowCount();
			}else{
				echo 'no Result2';
				print_r($stmt->errorInfo());
				$rowCount1 =0;
				//exit;
			}
			$count1 = 0;
			while($result2 = $stmt2->fetch(PDO::FETCH_OBJ)){
				$count1++;
				$serialList[$count1] = $result2;
			}
		}else{
			echo 'no Result3';
			exit;
		}
		
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?> 
<script src="./manager/manager_cardSerialDetail.js?ver=1"></script>
<!-- content start -->
<div id="contents">
<h1>Manage serial <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Stock details</span></h1>
	
  
<!-- menu description end -->
	<form name="cardSerialDetailForm" id="cardSerialDetailForm" method="post" action="">
		<input type="hidden" name="formName" value="cardSerialDetail">	
		<input type="hidden" name="stock" value="<?php echo $stockId ?>">	
		<input type="hidden" name="addedBy" value="<?php echo $result->addedBy?>">
		<div>
			<button type="button" class="btn btn-kyp" onclick="backToList(1)">Back</button>
			<a href="./main_content.php?menu=cardSerialDetail&searchType=ASC&stock=<?php echo $stockId ?>" role="button" class="btn btn-kyp">ASC</a>
			<a href="./main_content.php?menu=cardSerialDetail&searchType=DESC&stock=<?php echo $stockId ?>" role="button" class="btn btn-kyp">DESC</a>
			<button type="button" class="btn btn-kyp" onclick="saveAsCsv(1)">SaveAsCsv</button>
			<?php 
				if(isset($result->hidden) && $result->hidden ==='yes'){
			?>
				<button type="button" class="btn btn-kyp" onclick="unhide(5)">UnHide</button>
			<?php 
				}else{
			?>
				<button type="button" class="btn btn-kyp" onclick="hide(5)">Hide</button>
			<?php
				}
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button type="button" class="btn btn-kyp" onclick="deleteUnUsed(2)">DeleteUnusedSerial</button>
		</div>
		<br/>
		<!-- result start -->
		<table class="table table-bordered">
			<tr>
				<th class="text-center thGrey">Type</th>
				<th class="text-center thGrey">Name</th>
				<th class="text-center thGrey">Num</th>
				<th class="text-center thGrey">AppNo</th>
				<th class="text-center thGrey">Added</th>
				<th class="text-center thGrey">UsedDate</th>
				<th class="text-center thGrey">Used</th>
				<th class="text-center thGrey">Returned</th>
			</tr>
			<?php for($i=1;$i<$count1+1;$i++){ ?>
			<tr>
				<td class="text-center"><?php echo $serialList[$i]->serType; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->serialName; ?></a></td>
				<td class="text-center"><?php echo $serialList[$i]->serialNum; ?></td>
				<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $serialList[$i]->serAppNo; ?>"><?php echo $serialList[$i]->serAppNo; ?></a></td>
				<td class="text-center"><?php echo $serialList[$i]->addedBy; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->usedDate; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->usedBy; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->returned; ?></td>
			</tr>
			<?php } ?>
		</table>
	</form>
    <!-- result end -->
	
	 <!-- Paging Start -->
	<?php 
		if($rowCount1 === 0){
		}else{
	?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=cardSerialDetail&pagenum=1&searchType=<?php echo $searchTypeSel?>&stock=<?php echo $stockId?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=cardSerialDetail&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>&stock=<?php echo $stockId?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=cardSerialDetail&pagenum=$pagingno&searchType=$searchTypeSel&stock=$stockId\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=cardSerialDetail&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>&stock=<?php echo $stockId?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=cardSerialDetail&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>&stock=<?php echo $stockId?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
	<?php } ?>
    <!-- pagingEnd -->
</div>
<!-- content end -->