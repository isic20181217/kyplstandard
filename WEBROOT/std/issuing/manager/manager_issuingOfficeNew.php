<?php 
// 201114 check 
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?> 
<!-- content start -->
<form id="officeAddForm" name="officeAddForm" method="POST" action="./work/addOffice.php">
<input type="hidden" name="formName" value="issuingOfficeNew">
<div id="contents">
  <h1>Manage IOs <i class="fas fa-angle-double-right"></i> <span class="h1Sub">New IO register</span><a href="http://std.aretede.com/mockup/issuing/ManageIo_add.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>
	

	
<div class="row">
	<div class="col-xs-6"><h2>IO information</h2></div>
	<div class="col-xs-6 text-right">※ Items marked with * must be filled.</div>
</div>
	<table class="table table-bordered">   
        <tr>
          <th class="thGrey w20p">IO Name *</th>
          <td class="w30p"><input type="text" class="form-control" name="name"></td>        
          <th class="thGrey w20p">IO Description *</th>
          <td><input type="text" class="form-control" name="officeDesc"  /></td>
        </tr>
		<tr>
		  <th class="thGrey">Contact person</th>
		  <td><input type="text" class="form-control" name="contact"  /></td>
		  <th class="thGrey">Email</th>
		  <td><input type="text" class="form-control" name="email"  /></td>
	    </tr>
		<tr>
		  <th class="thGrey">Work Hour</th>
		  <td><input type="text" class="form-control" name="workHour"  /></td>
		  <th class="thGrey">Telephone Number</th>
		  <td><input type="text" class="form-control" name="tel"  /></td>
	    </tr>
		<tr>
		  <th class="thGrey">City</th>
		  <td><input type="text" class="form-control" name="city"  /></td>
		  <th class="thGrey">Zipcode</th>
		  <td><input type="text" class="form-control" name="zipcode"  /></td>
	    </tr>
		<tr>
		  <th class="thGrey">Address</th>
		  <td colspan="3"><input type="text" class="form-control" name="address"  /></td>
        </tr>
    </table>
    <div class="btnDiv">
      <button type="submit" class="btn btn-kyp">Register</button>
	  <a href="./main_content.php?menu=issuingOffice" role="button" class="btn btn-kyp">Back to list</a>
    </div>
</div>
<!-- content end -->