<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['activeOfficeSel']) && !empty($_GET['activeOfficeSel']) ){
		$activeOfficeSel = preg_replace("/<|\/|_|>/","",$_GET['activeOfficeSel']);
	}else{
		$activeOfficeSel = 'yes';
	}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		//// office list read start
		$queryOfficeList = "SELECT * FROM $tablename12 WHERE activeOffice = :activeOffice";
		$stmtOfficeList = $db->prepare($queryOfficeList);
		$stmtOfficeList->bindParam(':activeOffice', $activeOfficeSel);
		$stmtOfficeList-> execute();
		$officeListCount = 0;
		if($stmtOfficeList->rowCount() > 0){
			while($resultOfficeList = $stmtOfficeList->fetch(PDO::FETCH_OBJ) ){
				$officeList[$officeListCount] = $resultOfficeList;
				$officeListCount++;
			}
		}else{}
		//// office list read end
		/*
		echo '<pre>';
		print_r($officeList);
		echo '</pre>';
		*/
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'error1';
		exit;
	}
?> 
<!-- content start -->

<div id="contents">
  <h1>Manage IOs <i class="fas fa-angle-double-right"></i> <span class="h1Sub">IOs list</span></h1>
    <!-- search start -->
	<form name="issuingOfficeForm" id="issuingOfficeForm" method="GET" action="./main_content.php">
		<input type="hidden" name="menu" value="issuingOffice">
		<div class="searchDiv">
			<label>Active office</label>
			<select name="activeOfficeSel" class="form-control formYoon">
			<?php
				if(isset($activeOfficeSel) && $activeOfficeSel === 'yes'){
					$activeOfficeYes = 'selected';
					$activeOfficeNo = ' ';
				}else if (isset($activeOfficeSel) && $activeOfficeSel === 'no'){
					$activeOfficeYes = '';
					$activeOfficeNo = 'selected';
				}else{
					$activeOfficeYes = 'selected';
					$activeOfficeNo = ' ';
				}
			?>
				<option <?php echo $activeOfficeYes ?> value="yes">YES</option>
				<option <?php echo $activeOfficeNo ?> value="no">no</option>
			</select>
		  <button type="submit" class="btn btn-kyp"><i class="fab fa-sistrix"></i> Search</button>
			
			<a href="./main_content.php?menu=issuingOfficeNew" role="button" class="btn btn-kyp fr">Add New IO <i class="fa fa-caret-right"></i></a>    
		</div>
	</form>

    <!-- search end -->
    
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey">ID</th>
            <th class="text-center thGrey">City</th>
            <th class="text-center thGrey">IO</th>
            <th class="text-center thGrey">Contact person</th>
            <th class="text-center thGrey">Tel. No.</th>
            <th class="text-center thGrey">Email</th>
            <th class="text-center thGrey">Desc</th>
        </tr>
		<?php 
			for($i=0; $i<$officeListCount; $i++){?>
        <tr>
			<td class="text-center"><?php echo $officeList[$i]->officeId; ?></td>
			<td class="text-center"><?php echo $officeList[$i]->city; ?></td>
			<td class="text-center"><a href="./main_content.php?menu=issuingOfficeDetail&officeId=<?php echo $officeList[$i]->officeId;?>"><?php echo $officeList[$i]->name; ?></a></td>
			<td class="text-center"><?php echo $officeList[$i]->contact; ?></td>
			<td class="text-center"><?php echo $officeList[$i]->tel; ?></td>
			<td class="text-center"><?php echo $officeList[$i]->email; ?></td>
			<td class="text-center"><?php echo $officeList[$i]->officeDesc; ?></td>
		<?php } ?>
        </tr>
    </table>
    <!-- result end -->
</div>

<!-- content end -->