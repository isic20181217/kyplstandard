<?php 
// 201114 check

	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
?>
<script src="./manager/manager_designEditorNew.js?ver=1"></script>
<script src="css/js/jquery.min.js"></script>
<script src="css/js/bootstrap.min.js"></script>
	<style type="text/css">
		.editorTable {width: 100%;}
		.editorTable th {width: 22%; vertical-align: text-top;}
		.tableDiv {width:100%; /*height:700px; margin-top:4px; overflow-x:hidden; overflow-y:scroll;*/ font-size:90%;}
		.active {color: #444;}
		.color-title {color: #006364;}
	</style>

<!-- content start -->
<div id="contents">
	
  <h1>Manage card design <i class="fas fa-angle-double-right"></i> <span class="h1Sub">New design Upload</span><a href="http://std.aretede.com/mockup/issuing/setup_cardDesignNew.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>
	
						 
									   
																									
																																		   
																								 
	  
							 
	
<form method="POST" id="designEditorNew" name="designEditorNew" action="" method="POST"  enctype="multipart/form-data">
<input type="hidden" id="formName" name="formName" value="designEditorNew">
      <table class="table table-bordered">
			<tr>
				<th class="text-center thGrey">Card image</th>
				<td colspan="3">
					<div class="row">
						<div class="col-sm-4">
							<?php
								if(isset($result->bgImgName)){
									echo $result->bgImgName.'.jpg';
								}else{}
							?>
							<p class="pt10"><input type="file" class="form-control"  name="bgImg" id="bgImg" accept=".jpg"></p>
						</div>
						<div class="col-sm-8">
							<ul class="pt10">
								<li>Suitable size :450Pixel*284Pixel</li>
								<li>Available jpg file</li>
								<li>File name: Use small letters and numbers only</li>
								<li>File extention: Use small letters</li>
							</ul>	
						</div>
					</div>		
			</tr>
			<tr>
				<th class="text-center thGrey w20p">Design No</th>
				<td class="w30p">
					<input type="text" id="designNo" name="designNo" maxlength="25" class="form-control">
				</td>
				<th class="text-center thGrey w20p" id="cardType" name="cardType">Card type</th>
				<td>
					<select class="form-control" id="cardTYpe" name="cardType">
						<option value="ISIC">ISIC</option>
						<option value="ITIC">ITIC</option>
						<option value="IYTC">IYTC</option>
					</select>
				</td>
			</tr>
			<tr>
				<th class="text-center thGrey w20p">Active</th>
				<td>
					<select class="form-control" name="activeDesign" id="activeDesign">
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					</select>
				</td>
				<th class="text-center thGrey w20p">Added Date</th>
				<td class="w30p">
					<input type="text" id="addedDate" name="addedDate" maxlength="10" class="form-control" placeholder="dd/mm/yyyy">
				</td>
			</tr>
		</table>
	<div class="row">
			<div class="text-center pt30 mt10">
			<button type="button" class="btn btn-kyp" style="width: 300px;" onclick="submit1(1)">Save</button>
			<a href="main_content.php?menu=designList" role="button" class="btn btn-kyp" style="width: 300px;">Go to card design list</a>
			</div>
	</div>
		

</form>
</div>

<!-- content end -->
<?php 
?>