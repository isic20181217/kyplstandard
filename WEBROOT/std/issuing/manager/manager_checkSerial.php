<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$searchMode = 'searchMode';
	$selCardType='ISIC';
	if(isset($_GET['selCardType']) && !empty($_GET['selCardType'])){
		$selCardType = $_GET['selCardType'];
	}else{}
	$selOrderType='DESC';
	if(isset($_GET['selOrderType']) && !empty($_GET['selOrderType'])){
		$selOrderType = $_GET['selOrderType'];
	}else{}
	$howMany = 10;
	if(isset($_GET['howMany']) && !empty($_GET['howMany'])){
		$howMany = (int)$_GET['howMany'];
	}else{}
	$serialName = '';
	if(isset($_GET['serialName']) && !empty($_GET['serialName'])){
		//$serialName = $_GET['serialName'];
		$serialName = preg_replace("/<|\/|>/"," ",$_GET['serialName']);//accept '_'
	}else{}
	$selStatus='NOTUSED';
	if(isset($_GET['selStatus']) && !empty($_GET['selStatus'])){
		$selStatus = $_GET['selStatus'];
	}else{}
	$goParent ='/../';
	$goParent2 ='/../../';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	if (isset($searchMode) && $searchMode === 'searchMode'){
		try {
			require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
			$query = "SELECT * FROM $tablename11 WHERE serType=:inputValue1";
			$query .= " AND serAppNo";
			if ($selStatus === 'USED'){		
				$query .= " IS NOT NULL";
			}else{
				$query .= " IS NULL";
			}
			if(isset($serialName) && !empty($serialName)){
				$query .= " AND serialName =:inputValue2";
			}else{}				
			$query .= " ORDER BY serId";
			if ($selOrderType === 'DESC'){		
				$query .= " DESC";
			}else{
				$query .= " ASC";
			}
			$query .= " LIMIT 0, :inputValue111;";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $selCardType);
			if(isset($serialName) && !empty($serialName)){
				$stmt->bindParam(':inputValue2', $serialName);
			}else{}		
			$stmt->bindParam(':inputValue111', $howMany, PDO::PARAM_INT);
			$stmt->execute();
			$count1 = 0;
			while($result = $stmt->fetch(PDO::FETCH_OBJ)){
				$serialList[$count1] = $result;
				$count1 = $count1 + 1;
			}
			$db= NULL;
		}
		catch (PDOExeception $e){
			//echo "Error: ".$e->getMessage();
			$db= NULL;
			exit;
		}
	}else{}	
?>
<script>
	function submit1(index){
		switch (index){
			case 1:
				if(checkvalue1() == 1){
					document.checkSerialForm.action='./main_manager.php';
					document.checkSerialForm.submit();
				}
				else{
				}
				break;
			default :
				break;
		}

	 }
	 function checkvalue1(){
		return 1;
	 }
</script>
<!-- content start -->
<form id="checkSerialForm" name="checkSerialForm" method="GET" action="./main_content.php">
	<input type="hidden" name="searchMode" value="searchMode">
	<input type="hidden" name="menu" value="checkSerial">
	<div id="contents">
    <h1>Manage serial  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Check serial numbers</span><a href="http://std.aretede.com/mockup/issuing/ManageSerial_checkSerialNo.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>
		

		<div class="searchDiv">
			<!-- Search start-->
			<div>
				<label>Serial type</label>
					<select class="form-control formYoon" name="selCardType" id="selCardType">
						<?php 
							if ($selCardType === 'ITIC'){
								echo "<option value =\"ISIC\">ISIC</option>";
								echo "<option selected value =\"ITIC\">ITIC</option>";
								echo "<option value =\"IYTC\">IYTC</option>";
							}
							else if ($selCardType === 'IYTC'){
								echo "<option value =\"ISIC\">ISIC</option>";
								echo "<option value =\"ITIC\">ITIC</option>";
								echo "<option selected value =\"IYTC\">IYTC</option>";
							}else {
								echo "<option selected value =\"ISIC\">ISIC</option>";
								echo "<option value =\"ITIC\">ITIC</option>";
								echo "<option value =\"IYTC\">IYTC</option>";
							}
						?>
					</select>
				<label>How many Serial?</label>
				<input type="text" class="form-control formYoon" id="howMany" name="howMany" value="<?php echo $howMany; ?>" style="width: 50px;">
				<label>Serial name</label>
				<input type="text" class="form-control formYoon" id="serialName" name="serialName" value="<?php echo $serialName; ?>" style="width: 120px;">
				<label>Order style</label>
					<select class="form-control formYoon" name="selOrderType" id="selOrderType">
						<?php 
							if ($selOrderType === 'DESC'){
								echo "<option selected value =\"DESC\">DESC</option>";
								echo "<option value =\"ASC\">ASC</option>";
							}
							else {
								echo "<option value =\"DESC\">DESC</option>";
								echo "<option selected value =\"ASC\">ASC</option>";
							}
						?>
					</select>
				<label>Status</label>
					<select class="form-control formYoon" name="selStatus" id="selStatus">
						<?php 
							if ($selStatus === 'USED'){
								echo "<option selected value =\"USED\">Used</option>";
								echo "<option value =\"NOTUSED\">Not Used</option>";
							}
							else {
								echo "<option value =\"USED\">Used</option>";
								echo "<option selected value =\"NOTUSED\">Not Used</option>";
							}
						?>
					</select>
				<button type="submit" class="btn btn-kyp" ><i class="fab fa-sistrix"></i> Search</button>
			</div>
		</div>
	
		<!-- Search End --> 
		<!-- result start -->
		<table class="table table-bordered">
			<tr>
				<th class="text-center thGrey">SerialName</th>
				<th class="text-center thGrey">SerialNum</th>
				<th class="text-center thGrey">Application ID</th>
			</tr>
			<?php for($i=0;$i<$count1;$i++){ ?>
			<tr>
				<td class="text-center"><?php echo $serialList[$i]->serialName; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->serialNum; ?></td>
				<td class="text-center"><?php echo $serialList[$i]->serAppNo;?></td>
			</tr>
			<?php } ?>
		</table>
		 <!-- result End --> 
	</div>
</form>