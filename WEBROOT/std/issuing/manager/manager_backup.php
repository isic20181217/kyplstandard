<?php 
// 200604 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}

	$currentYear = date("Y");
	$currentYear = (int)$currentYear;
	$currentMonth = date("m");
	$currentMonth = (int)$currentMonth;
	$currentDay = date("d");
	$currentDay = (int)$currentDay;
	$startYear = 2018;
?>
<!-- content start -->
<form name="backupForm" id="backupForm" method="POST" action="./work/applyBackup1.php">
<input type="hidden" name="formName" value="managerBackupForm">
<input type="hidden" name="menu" value="backup">
<input type="hidden" name="searchMode" value="searchMode">
<div id="contents">
<h1>Setup <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Backup data</span></h1>

    <!-- search start -->
    <div class="searchDiv">
        <label>Year</label>
        <select name="searchYear" class="form-control formYoon">
		<?php for($i=$currentYear; $i>$startYear -10; $i--) { 
			$selectThis = '';
			if($i === $currentYear){
				$selectThis ='selected';
			}else{}
		?>
            <option <?php echo $selectThis; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
        </select>
        <label>Month</label>
        <select name="searchMonth" class="form-control formYoon">
			<option value="ALL">All</option>
		<?php for($i=1; $i<13; $i++) { 
			$selectThis = '';
			if($i === (int)$currentMonth){
				$selectThis ='selected';
			}else{}
		?>
            <option <?php echo $selectThis; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
        </select>
        <label>Day</label>
        <select name="searchDay" class="form-control formYoon">
			<option value="ALL">All</option>
		<?php for($i=1; $i<32; $i++) { 
			$selectThis = '';
			if($i === (int)$currentDay){
				$selectThis ='selected';
			}else{}
			
		?>
            <option <?php echo $selectThis; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
        </select>
      	<button type="submit" class="btn btn-kyp">Save as CSV</button>
    </div>
    <!-- search end -->
</div>
</form>
<!-- content end -->