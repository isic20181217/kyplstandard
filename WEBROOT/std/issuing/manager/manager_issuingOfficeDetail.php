<?php 
// 201114 check 
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeId= preg_replace("/<|\/|_|>/","",$_GET['officeId'] );
	}else{
		echo '<script>alert(\'forbidden.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$officeNoId = 'no';
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT * FROM $tablename12 WHERE officeId = :searchValue1;";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $officeId);
		$stmt->execute();
		if($result = $stmt->fetch(PDO::FETCH_OBJ)){
		}
		else{
			$db= NULL;
			print_r($stmt->errorInfo());
			echo '<script>alert(\'Error1\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			exit;
		}
		
		$count1 = 0;
		$query = "SELECT * FROM $tablename06 WHERE officeId = :searchValue1 AND (role = 'issuer' or role='manager')";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $officeId);
		$stmt->execute();
		if($stmt->rowCount() > 0){
			while($result2 = $stmt->fetch(PDO::FETCH_OBJ)){
				$staffList[$count1] = $result2;
				$count1 = $count1 +1;
			}
		}
		else{
			//$db= NULL;
			//echo '<script>alert(\'No Id\');</script>';
			//echo "<script> window.history.go(-1); </script>";
			//exit;
			$officeNoId = 'yes';
		}
		
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	$officeId = $result->officeId;
	$name = $result->name;
	$address = $result->address;
	$city = $result->city;
	$tel= $result->tel;
	$zipcode= $result->zipcode;
	$contact= $result->contact;
	$email= $result->email;
	$workHour= $result->workHour;
	/*
	echo '<pre>';
	print_r($result);
	echo '</pre>';
	*/
	$db=NULL;
?> 
<!-- content start-->
<form id="officeDetailForm" name="officeDetailForm" method="POST" action="./work/modifyOffice.php">
<input type="hidden" name="formName" value="issuingOfficeDetail">
<input type="hidden" name="officeId" value="<?php echo $officeId ?>">
<div id="contents">
  <h1>Manage IOs <i class="fas fa-angle-double-right"></i> <span class="h1Sub">IO settings</span></h1>
	
						
																													  
													 
	 
 
<div class="row">
	<div class="col-xs-6"><h2>IO information</h2></div>
	<div class="col-xs-6 text-right">※ Items marked with * must be filled.</div>
</div>

<table class="table table-bordered">
	<tr>
		<th class="thGrey w20p">ActiveOffice</th>
		<td class="w30p">
			<select name="activeOffice" class="form-control formYoon">
				<?php
					$selectionYes = ' ';
					$selectionNo = ' ';
					$selectionType = 'activeOffice';
					if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
						$selectionYes ='selected';
						$selectionNo = ' ';
					}else{
						$selectionYes = '';
						$selectionNo = 'selected';
					}
				?>
				<option <?php echo $selectionNo ?>value="no>">no</option>
				<option <?php echo $selectionYes ?> value="yes">yes</option>
			</select>		
		</td>        
		<th class="thGrey w20p">Office Id</th>
		<td><?php echo $officeId ?></td>
	</tr>   
	<tr>
		<th class="thGrey w20p">IO Name *</th>
		<td class="w30p">
		<input type="text" class="form-control" name="name" value="<?php echo $name; ?>" />
		</td>        
		<th class="thGrey w20p">IO Description *</th>
		<td><input type="text" class="form-control" name="officeDesc" value="<?php echo $result->officeDesc; ?>" /></td>
	</tr>
	<tr>
		<th class="thGrey">Contact person</th>
		<td><input type="text" class="form-control" name="contact" value="<?php echo $contact; ?>" /></td>
		<th class="thGrey">Email</th>
		<td><input type="text" class="form-control" name="email" value="<?php echo $email; ?>" /></td>
	</tr>
	<tr>
		<th class="thGrey">Work Hour</th>
		<td><input type="text" class="form-control" name="workHour" value="<?php echo $workHour; ?>" /></td>
		<th class="thGrey">Telephone Number</th>
		<td><input type="text" class="form-control" name="tel" value="<?php echo $tel; ?>" /></td>
	</tr>
	<tr>
		<th class="thGrey">City</th>
		<td><input type="text" class="form-control" name="city" value="<?php echo $city; ?>" /></td>
		<th class="thGrey">Zipcode</th>
		<td><input type="text" class="form-control" name="zipcode" value="<?php echo $zipcode; ?>" /></td>
	</tr>
	<tr>
		<th class="thGrey">Address</th>
		<td colspan="3"><input type="text" class="form-control" name="address" value="<?php echo $address; ?>" /></td>
	</tr>
</table>
      
	<h2 class="pt30">Issuing system login ID Setting</h2>
	<table class="table table-bordered">
	<?php if($officeNoId === 'yes'){ ?>
		<tr>
			<th class="thGrey w20p">ID (issuer)</th>
			<td class="w30p"><a class="btn btn-kyp" href="./main_content.php?menu=staffNew&officeId=<?php echo $result->officeId?>">Issuer ID Create</a></td>
			<th class="thGrey w20p"></th>
			<td></td>
		</tr>
	<?php }else{ ?>
		<?php for($i=0; $i < $count1; $i++){ ?>
			<tr>
				<th class="thGrey w20p">ID (<?php echo $staffList[$i]->role?>)</th>
				<td class="w30p"><?php echo $staffList[$i]->id?></td>
				<td><a class="btn btn-kyp" href="./main_content.php?menu=staffDetail&officeId=<?php echo $result->officeId?>&idNo=<?php echo $staffList[$i]->no?>&id=<?php echo $staffList[$i]->id?>">Modify</a></td>
			</tr>
		<?php } ?>
	<?php } ?>
	</table>
    

	
  <h2 class="pt-30">IO Function Setting <small> - If you do not want to use any function below, select ‘NO’ and click the ‘Save’ button.</small></h2>
	<table class="table table-bordered">   
		<tr>
			<th class="thGrey w20p">To use the online application to view customer information and issue cards.</th>
			<td class="w30p">
				<div class="row">
					<div class="col-sm-6"><?php echo $result->canUrl; ?></div>
					<div class="col-sm-6">
						<select name="canUrl" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canUrl';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>
					</div>
				</div>
			</td>
			<th class="thGrey w20p">To bulk upload online applications with CSV files.</th>
			<td>
				<div class="row">
					<div class="col-sm-6"><?php echo $result->canCsv; ?></div>
					<div class="col-sm-6">
						<select name="canCsv" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canCsv';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>	
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<th class="thGrey">To check online applications and approve certifications directly.</th>
			<td>
				<div class="row">
					<div class="col-xs-6">
						<?php echo $result->canValidate; ?>
					</div>
					<div class="col-xs-6">
						<select name="canValidate" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canValidate';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>
					</div>
				</div>
				<small>* If you click 'No', the main office will approve the certification.</small>
			</td>
			<th class="thGrey">To issue cards for applications that have been approved and paid.</th>
			<td>
				<div class="row">
					<div class="col-sm-6"><?php echo $result->canIssue; ?></div>
					<div class="col-sm-6">
						<select name="canIssue" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canIssue';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>	
					</div>
				</div>		
			</td>
		</tr>
		<tr>
			<th class="thGrey">To use an auto card printer.</th>
			<td>
				<div class="row">
					<div class="col-sm-6"><?php echo $result->canPrint; ?></div>
					<div class="col-sm-6">
						<select name="canPrint" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canPrint';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>	
					</div>
				</div>		
			</td>
			<th class="thGrey">To use a label card printer.</th>
			<td>
				<div class="row">
					<div class="col-sm-6"><?php echo $result->canPrintLabel; ?></div>
					<div class="col-sm-6">
						<select name="canPrintLabel" class="form-control">
							<?php
								$selectionYes = ' ';
								$selectionNo = ' ';
								$selectionType = 'canPrintLabel';
								if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
									$selectionYes ='selected';
									$selectionNo = ' ';
								}else{
									$selectionYes = '';
									$selectionNo = 'selected';
								}
							?>
							<option <?php echo $selectionNo ?>value="no>">no</option>
							<option <?php echo $selectionYes ?> value="yes">yes</option>
						</select>	
					</div>
				</div>		
			</td>
		</tr>
		<!--
		<tr>
			<th class="thGrey">Menu Issuing</th>
			<td><?php //echo $result->menuIssuing; ?>
				<select name="menuIssuing" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'menuIssuing';
						if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="thGrey">Menu Reports</th>
			<td><?php //echo $result->menuReports; ?>
				<select name="menuReports" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'menuReports';
						if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		-->
		<!--
		<tr>
			<th class="thGrey">Menu Serial</th>
			<td><?php //echo $result->menuSerial; ?>
				<select name="menuSerial" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'menuSerial';
						if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="thGrey">Menu IO</th>
			<td><?php //echo $result->menuIo; ?>
				<select name="menuIo" class="form-control formYoon">
					<?php
						/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'menuIo';
						if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
		</tr>
		<tr>
			<th class="thGrey">Menu Application</th>
			<td><?php //echo $result->menuApplication; ?>
				<select name="menuApplication" class="form-control formYoon">
					<?php
					/*
						$selectionYes = ' ';
						$selectionNo = ' ';
						$selectionType = 'menuApplication';
						if(isset($result->$selectionType) && $result->$selectionType === 'yes'){
							$selectionYes ='selected';
							$selectionNo = ' ';
						}else{
							$selectionYes = '';
							$selectionNo = 'selected';
						}
						*/
					?>
					<option <?php //echo $selectionNo ?>value="no>">no</option>
					<option <?php //echo $selectionYes ?> value="yes">yes</option>
				</select>			
			</td>
			<th class="thGrey"></th>
			<td></td>
		</tr>
		-->
    </table>
	<div class="btnDiv">
		<button type="submit" class="btn btn-kyp">Save</button>
		<a href="./main_content.php?menu=issuingOffice" role="button" class="btn btn-kyp">Back to list</a>
	</div>
</div>
</form>
<!-- content end -->
