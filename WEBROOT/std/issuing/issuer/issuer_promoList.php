<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename27";
		$stmt = $db->prepare($query);
		$stmt->execute();
		$promoCount =0;
		if($stmt->rowCount() > 0){
			while($result = $stmt->fetch(PDO::FETCH_OBJ)){
				$promoList[$promoCount]= $result;
				$queryTotal = "SELECT * FROM $tablename26 WHERE promoCodeGroup = :searchValue1";
				$stmtTotal = $db->prepare($queryTotal);
				$stmtTotal->bindParam(':searchValue1', $promoList[$promoCount]->promoGroupId);
				$stmtTotal->execute();
				if($stmtTotal->rowCount() > 0){
					//print_r($stmtTotal->errorInfo());
					$totalValue = $stmtTotal->rowCount();
					$promoList[$promoCount]->total = $totalValue; 
					$queryUsed = "SELECT * FROM $tablename26 WHERE promoCodeGroup = :searchValue1 AND promoUsed = 'yes'";
					$stmtUsed = $db->prepare($queryUsed);
					$stmtUsed->bindParam(':searchValue1', $promoList[$promoCount]->promoGroupId);
					$stmtUsed->execute();
					if($stmtUsed->rowCount() > 0){
						//print_r($stmtUsed->errorInfo());
						$usedValue = $stmtUsed->rowCount(); 
						$promoList[$promoCount]->used = $usedValue;
					}else{
						$promoList[$promoCount]->used = 0;
					}
				}else{
					$promoList[$promoCount]->used = 0;
					$promoList[$promoCount]->total = 0;
				}
				$promoCount++;
			}
		}else{
			$db= NULL;
		}
		$db= NULL;
		/*
		echo '<pre>';
		print_r($promoList);
		echo '</pre>';
		*/
?>
<!-- content Start-->
<div id="contents">
  <div class="row">
    <div class="col-sm-6">
        <h1>Manage promotion  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Promotion list</span></h1>
    </div>
    <div class="col-sm-6 text-right">
      <a href="./main_content.php?menu=promoNew" role="button" class="btn btn-kyp fr">Add New promotion <i class="fa fa-caret-right"></i></a>
    </div>
  </div>
    
    <!-- result Start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey"></th>
            <th class="text-center thGrey">PromoName</th>
			<th class="text-center thGrey">Value</th>
			<th class="text-center thGrey">Type</th>
            <th class="text-center thGrey">Date</th>
			<th class="text-center thGrey">Stock</th>
			<th class="text-center thGrey">Active</th>
        </tr>
		<?php for($i=0;$i<$promoCount;$i++){?>
        <tr>
			<td class="text-center"><?php echo $i+1?></td>
			<td class="text-center">
				<a href="./main_content.php?menu=promoDetail&gId=<?php echo $promoList[$i]->promoGroupId?>"><?php echo $promoList[$i]->promoGroupName?></a>
			</td>
			<td class="text-center"><?php echo $promoList[$i]->promoDiscount?></td>
			<td class="text-center"><?php echo $promoList[$i]->promoType?></td>
			<td class="text-center"><?php echo $promoList[$i]->modifiedDate?></td>
			<td class="text-center"><?php echo $promoList[$i]->used?>/<?php echo $promoList[$i]->total?></td>
			<td class="text-center"><?php echo $promoList[$i]->promoActive?></td>
        </tr>
		<?php } ?>
    </table>
    <!-- result End -->
	
    <!-- paging Start -->              
    <!-- paging end -->
	
</div>
<!-- content End -->

