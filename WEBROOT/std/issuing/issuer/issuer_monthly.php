<?php 
///201114 check		
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports === 'yes'  && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$officeId = (int)$_SESSION['officeId'];
	$currentYear = date("Y");
	$currentYear = (int)$currentYear;
	$reportStartYear = 2019;
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeIdSel= preg_replace("/<|\/|_|>/","", $_GET['officeId'] );
	}
	else{
		$officeIdSel = 'ALL';
	}
	if(isset($_GET['searchType']) && !empty($_GET['searchType'])){
		$searchTypeSel  = preg_replace("/<|\/|_|>/","",$_GET['searchType']);
	}
	else{
		$searchTypeSel = 'ALL';
	}
	if(isset($_GET['searchYear']) && !empty($_GET['searchYear'])){
		$searchYear  = preg_replace("/<|\/|_|>/","",$_GET['searchYear']);
	}
	else{
		$searchYear = $currentYear;
	}
	$issuedCardTot = 0;
	$issuedCardTot2 = 0;
	$issuedCardNum = array();	 
	$issuedCardNum2 = array();	
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		for($i=1;$i<13;$i++){
			$issuedCardNum[$i] = 0;
			$query = "SELECT count(appNo) as total FROM $tablename07 WHERE year(issuedDate) = :inputValue2 AND month(issuedDate) = :inputValue3";
			if($searchTypeSel === 'ALL'){
			}else{
				$query .= " AND cardType = :inputValue1";
			}
			if($officeId === 1){
				if($officeIdSel === 'ALL'){
				} else if ($officeIdSel === '1'){
					$query .= " AND (officeId = :inputValue11 OR officeId = 990)";
				} else {
					$query .= " AND officeId = :inputValue11";
				}
			} else {
				$query .= " AND officeId = :inputValue11";
			}
			$stmt = $db->prepare($query);
			if($searchTypeSel === 'ALL'){
			} else {
				$stmt->bindParam(':inputValue1', $searchTypeSel);	
			}
			if($officeId === 1){
				if($officeIdSel === 'ALL'){
				}else{
					$stmt->bindParam(':inputValue11', $officeIdSel);
				}
			}else{
				$stmt->bindParam(':inputValue11', $officeId);
			}
			$stmt->bindParam(':inputValue2', $searchYear);
			$stmt->bindParam(':inputValue3', $i);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$issuedCardNum[$i] = $result->total;
			$query2 = "SELECT mCount FROM $tablename31 WHERE mYear =  :mYear AND mMonth = :mMonth";
			$stmt2 = $db->prepare($query2);
			$stmt2->bindParam(':mYear', $searchYear);
			$searchMonth = $i;
			if(strlen((string)$i) < 2)
			{
				$searchMonth = '0'.$searchMonth;
			}
			else
			{}
			$stmt2->bindParam(':mMonth', $searchMonth);
			$stmt2->execute();
			if($stmt2->rowCount() === 1)
			{
				$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
				$issuedCardNum2[$i] = $result2->mCount;
			}
			else if ($stmt2->rowCount() > 1)
			{
				echo 'DB error';
				echo $searchYear.$searchMonth;
				$db= NULL;
				exit;
			}
			else
			{
				$issuedCardNum2[$i]  ='0';
			}
		}
		$queryAppSetting = "SELECT appSettingId, url FROM $tablename23 order by appSettingId";
		$stmtAppSetting = $db->prepare($queryAppSetting);
		$stmtAppSetting->execute();
		$appSettingCount =0;
		if($stmtAppSetting->rowCount() > 0){
			while($result = $stmtAppSetting->fetch(PDO::FETCH_OBJ)){
				$appSettingList[$appSettingCount]= $result;
				$appSettingCount++;
			}
		}else{
			$db= NULL;
		}
		$query = "SELECT officeId,name FROM $tablename12";
		$stmt = $db->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() >= 1){
			$officeCount = $stmt->rowCount();
			$i =0;
			while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
				$officeList[$i] = $result0;
				$i++;
			}
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}	
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?>
<!-- content start -->
<form name="monthlyForm" id="monthlyForm" method="GET" action="./main_content.php">
<input type="hidden" name="menu" value="monthly">
<input type="hidden" name="searchMode" value="searchMode">
<div id="contents">
<h1>Monthly sales report</h1>

    <!-- search start -->
    <div class="searchDiv">
		<?php 
			if($officeId === 1){ 
		?>
		<label>Office</label>
		<select class="form-control formYoon" name="officeId" id="officeId">
			<option value="ALL">ALL</option>
			<?php 
				for($i=0;$i<$officeCount;$i++){
					$selectThis = '';
					if($officeIdSel === $officeList[$i]->officeId){
						$selectThis ='selected';
					}else{}						
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
			<?php 
				}
			?>
		</select>
		<?php } else {} ?>
        <label>Card type</label>
        <select name="searchType" class="form-control formYoon">
			<option value="ALL">ALL</option>
			<?php for($i=0; $i< $appSettingCount; $i++) { 
				$selectThis = '';
				if($searchTypeSel === $appSettingList[$i]->url){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $appSettingList[$i]->url ?>"><?php echo $appSettingList[$i]->url ?></option>
			<?php } ?>
        </select>
        <label>Year</label>
        <select name="searchYear" class="form-control formYoon">
		<?php for($i=$currentYear; $i>$reportStartYear -1; $i--) { 
			$selectThis = '';
			if($i === $searchYear){
				$selectThis ='selected';
			}else{}
		?>
            <option <?php echo $selectThis; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
        </select>
      	<button type="submit" class="btn btn-kyp"><i class="fab fa-sistrix"></i> Search</button>
    </div>
    <!-- search end -->
    
    <!-- result start-->
    <table class="table table-bordered">
        <tr>
            <th colspan="2" class="text-center thGrey">&nbsp;</th>
            <th class="text-center thGrey">Jan</th>
            <th class="text-center thGrey">Feb</th>
            <th class="text-center thGrey">Mar</th>
            <th class="text-center thGrey">Apr</th>
            <th class="text-center thGrey">May</th>
            <th class="text-center thGrey">Jun</th>
            <th class="text-center thGrey">Jul</th>
            <th class="text-center thGrey">Aug</th>
            <th class="text-center thGrey">Sep</th>
            <th class="text-center thGrey">Oct</th>
            <th class="text-center thGrey">Nov</th>
            <th class="text-center thGrey">Dec</th>
            <th class="text-center thGrey">Total</th>
        </tr>
		<tr>
			<td class="text-center"><?php echo $searchTypeSel; ?> </td>
			<td class="text-center">Issued</td>
			<?php for($i=1;$i<13;$i++){?>
			<td class="text-right"><?php echo $issuedCardNum[$i]; ?>(<?php echo $issuedCardNum2[$i]?>)</td>
			<?php 
				$issuedCardTot = $issuedCardTot + $issuedCardNum[$i];
				$issuedCardTot2 = $issuedCardTot2 + (int)$issuedCardNum2[$i];
				} 
			?>
			<td class="text-right"><?php echo $issuedCardTot; ?>(<?php echo $issuedCardTot2?>)</td>

		</tr>
    </table>
    <!-- result end -->
</div>
</form>
<!-- content end -->