<?php 
//201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
 	}
	require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
	$initialSetup ='no';
	if(isset($_GET['initialSetup']) && $_GET['initialSetup']=== 'yes')
	{
		$initialSetup ='yes';
	}
	else
	{}
	$query = "SELECT * FROM $tablename30 WHERE activeDesign = :activeDesign";
	$stmt = $db->prepare($query);
	$paramYes = 'yes';
	$stmt->bindParam(':activeDesign', $paramYes);
	$stmt->execute();
	//echo $query;
	//echo $stmt->rowCount();
	if($stmt->rowCount() >= 1 ){
		$stmt->execute();
		$designCount = $stmt->rowCount();
		$i = 0;
		while($result4 = $stmt->fetch(PDO::FETCH_OBJ)){
			$designList[$i] = $result4;
			$i++;
		}
	}else{
		//echo 'nodesign';
		$designCount =0;
	}
?>
<script src="./issuer/issuer_appUrlNew.js?ver=1"></script>
<!-- content start -->
<form name="addUrl" id="addUrl" method="POST">
<div id="contents">
<?php
	if($initialSetup === 'yes')
	{
?>
	<h1>Application create</h1>
<?php 
	}
	else 
	{
?>
	<h1>Manage applications  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Add new application</span></h1>
	
<?php 
	}
?>
	<h2>New online application URL createion &amp; setting</h2>

	
	<table class="table table-bordered">   
		<tr>
			<th class="text-left thGrey w20p">Type of ID card to be used</th>
			<td class="w30p">
				<select class="form-control" name="serialType" id="serialType">
					<option value="ISIC">ISIC</option>
					<option value="ITIC">ITIC</option>
					<option value="IYTC">IYTC</option>
				</select>
			</td>   				
			<th class="text-left thGrey w20p">Serial Name</th>
			<td>
				<input type="text" class="form-control"  name="serialName" id="serialName" maxlength="20" value="">
				</td>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Printing the serial number</th>
			<td>
				<select class="form-control" name="needSerial" id="needSerial">
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Real time updating to CCDB</th>
			<td>
				<select class="form-control" name="ccdbSend" id="ccdbSend">
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
				</select>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Printer type</th>
			<td>
				<select class="form-control" name="issueType" id="issueType">
					<option value="auto">Auto</option>
					<option value="manual">Manual</option>
				</select>
			</td>
			<th class="text-left thGrey">Id type</th>
			<td>
				<select class="form-control" name="issueType2" id="issueType2">
					<option value="P">Plastic</option>
					<option value="V">Virtual</option>
				</select>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Using online payment</th>
			<td>
				<select class="form-control" name="payment" id="payment">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">ID price</th>
			<td><?php echo $localCurrency?> <input type="text" class="form-control formYoonNm" style="width: 90px;" maxlength="5" id="price" name="price"> <small style="color: #999;">* e.g - 9.00</small></td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Creation URL address</th>
			<td colspan="3"><?php echo $thisSiteDomain?>/app/app01.php?appSet=
				<input type="text" class="form-control formYoon" placeholder="keyword" / name="url" id="url"> <small style="color: #999;">* Alphabets and numeric characters only.</small></td>
		</tr>
		<tr>
			<th class="text-left thGrey">Application Name</th>
			<td colspan="3">
			  <input type="text" class="form-control"  name="appName" id="appName" maxlength="25">
		  </td>	
	    </tr>
		<tr>
			<th class="text-left thGrey w20p">Card design number</th>
			<td class="w30p">
				<select class="form-control" name="designId" id="designId">
					<?php 
						for($i=0;$i<$designCount;$i++){
							$selected='';
							if(isset($designList[$i]->designId) && isset($result->designId) && $designList[$i]->designId === $result->designId){
								$selected='selected';
							} else {
								$selected='';
							}
					?>
						<option <?php echo $selected ?> value="<?php echo $designList[$i]->designId?>"><?php echo $designList[$i]->designNo?></option>
					<?php 
						}
					?>
				</select>
			</td>        
			<th class="text-left thGrey w20p"></th>
			<td>
			</td>
		</tr>
    </table>
	
	<h2 class="pt20"> Application details settings</h2>
	<table class="table table-bordered">
		<tr>
		  <th class="text-left thGrey w20p">Use name in local language field	</th>
		  <td class="w30p">
				<select class="form-control" name="localName" id="localName">
					<option value="yes">Yes</option>
					<option selected value="no">No</option>
				</select>
			</td>
		  <th class="text-left w20p thGrey">Use gender field	</th>
		  <td>
				<select class="form-control" name="gender" id="gender">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Accept online applications of children under 16	</th>
			<td>
				<select class="form-control" name="underAge" id="underAge">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Use school name field	</th>
			<td>
				<select class="form-control" name="school" id="school">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Use certification(proof documents) attachment	</th>
			<td>
				<select class="form-control" name="proofStatus" id="proofStatus">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Use identification attachment	</th>
			<td>
				<select class="form-control" name="proofBirth" id="proofBirth">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
	</table>

	<h2 class="pt20">Information settings for postal delivery</h2>

	<table class="table table-bordered">
		<tr>
			<th class="text-left w20p thGrey">Use  the 1st address field</th>
			<td class="w30p">
				<select class="form-control" name="address" id="address">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left w20p thGrey">Use  the 2nd address field</th>
			<td>
				<select class="form-control" name="address2" id="address2">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use city field</th>
			<td>
				<select class="form-control" name="city" id="city">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Use country field</th>
			<td>
				<select class="form-control" name="Country" id="Country">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use postal code field</th>
			<td>
				<select class="form-control" name="postal" id="postal">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Use nationality field</th>
			<td>
				<select class="form-control" name="nationality" id="nationality">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use phone number field</th>
			<td>
				<select class="form-control" name="phone" id="phone">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Use email field</th>
			<td>
				<select class="form-control" name="email" id="email">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
		</table>

		<table class="table table-bordered">
		<tr>
			<th colspan="2" class="text-left w20p thGrey">Total post delivery type</th>
			<td>
				<input type="text" class="form-control formYoon" name="postType" id="postType" value="3" maxlength="20" style="width: 100px;">
				<small style="color: #999"> * Enter a number between 1 to 4. ( Classify 4 types of the delivery method below and designate each shipping fee. )</small>
			</td>
		</tr>
			
		<tr>
			<th rowspan="3" class="text-left thGrey">Post type 1</th>
			<th class="text-left thGrey">Name</th>
			<td><input type="text" class="form-control" name="post1Name" id="post1Name" value="visit" maxlength="20"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Description</th>
			<td><input type="text" class="form-control" name="post1Desc" id="post1Desc" value="Visiting Office" maxlength="200"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Price</th>
			<td><input type="text" class="form-control formYoon" name="post1Price" id="post1Price" value="0" maxlength="20"></td>
		  </tr>
			
		<tr>
			<th rowspan="3" class="text-left thGrey">Post type 2</th>
			<th class="text-left thGrey">Name</th>
			<td><input type="text" class="form-control formYoon" name="post2Name" id="post2Name" value="regular" maxlength="20"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Description</th>
			<td><input type="text" class="form-control" name="post2Desc" id="post2Desc" value="Regular Posting" maxlength="200"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Price</th>
			<td><input type="text" class="form-control formYoon" name="post2Price" id="post2Price" value="0" maxlength="20"></td>
		  </tr>
			
		<tr>
			<th rowspan="3" class="text-left thGrey">Post type 3</th>
			<th class="text-left thGrey">Name</th>
			<td><input type="text" class="form-control formYoon" name="post3Name" id="post3Name" value="express" maxlength="20"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Description</th>
			<td><input type="text" class="form-control" name="post3Desc" id="post3Desc" value="Express Posting" maxlength="200"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Price</th>
			<td><input type="text" class="form-control formYoon" name="post3Price" id="post3Price" value="0" maxlength="20"></td>
		  </tr>
			
		<tr>
			<th rowspan="3" class="text-left thGrey">Post type 4</th>
			<th class="text-left thGrey">Name</th>
			<td><input type="text" class="form-control formYoon" name="post4Name" id="post4Name" value="none" maxlength="20"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Description</th>
			<td><input type="text" class="form-control" name="post4Desc" id="post4Desc" value="none" maxlength="200"></td>
		  </tr>
		<tr>
		  <th class="text-left thGrey">Price</th>
			<td><input type="text" class="form-control formYoon" name="post4Price" id="post4Price" value="0" maxlength="20"></td>
		  </tr>
		</table>

		<h2 class="pt20">Regulations and issuance settings</h2>
		<table class="table table-bordered">
		<tr>
			<th class="text-left w20p thGrey">Terms &amp; conditions</th>
				<td colspan="3">
					<div style="height: 500px;">
						<iframe src="/std/app/App_terms.html" scrolling="yes" style="height: 500px;" class="form-control agreeIframe"></iframe>
					</div>
			<!--<textarea class="form-control" rows="5"><?php //echo $result->termsText?></textarea>-->
			</td>
		</tr>
		<tr>
			<th class="text-left w20p thGrey">Privacy policy</th>
				<td colspan="3">
				<div style="height: 500px;">
					<iframe src="/std/app/App_privacy.html" scrolling="yes" style="height: 500px;" class="form-control agreeIframe"></iframe>
				</div>
			<!--<textarea class="form-control" rows="5"><?php //echo $result->privacyText?></textarea>-->
			</td>
		</tr>
		<tr>
		  <th class="text-left w20p thGrey">Guide to terms &amp; condition</th>
		  <td class="w30p">
				<select class="form-control" name="terms" id="terms">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
		  </td>
		  <th class="text-left w20p thGrey">Guide to privacy policy</th>
			<td>
				<select class="form-control" name="privacy" id="privacy">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Agree to permit email newsletter</th>
			<td>
				<select class="form-control" name="news" id="news">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Validity Type</th>
			<td>
				<select class="form-control" name="validityStart" id="validityStart">
					<!--<option value="validityStartFix"><?php //echo $validityStartFix.' ~ '.$validityEndFix ?></option>-->
					<option value="validityStart1">1Year</option>
					<option value="validityStartC">Custom Value</option>
				</select>
			</td>
		</tr>
		</table>

		<h2 class="pt20">Custom validity settings</h2>
		<table class="table table-bordered">
		<tr>
		  <th class="text-left w20p thGrey">Valid from</th>
		  <td class="w30p">
				<input type="text" class="form-control" name="validityStart11" id="validityStart11" value="mm/yyyy" maxlength="7">
			</td>
		  <th class="text-left w20p thGrey">Valid until</th>
			<td>
				<input type="text" class="form-control" name="validityEnd11" id="validityEnd11" value="mm/yyyy" maxlength="7">
			</td>
		</tr>	
		</table>

		<h2 class="pt20">Vat related information settings</h2>
		<table class="table table-bordered">
		<tr>
		  <th class="text-left w20p thGrey">Vat Number</th>
		  <td class="w30p">
				<select class="form-control" name="vatNumber" id="vatNumber">
					<option value="no">No</option>
					<option value="yes">Yes</option>
				</select>
			</td>
		  <th class="text-left w20p thGrey">Associate Number</th>
			<td>
				<select class="form-control" name="associateNumber" id="associateNumber">
					<option value="yes">Yes</option>
					<option selected value="no">No</option>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Associate Name</th>
			<td>
				<select class="form-control" name="associateName" id="associateName">
					<option value="yes">Yes</option>
					<option selected value="no">No</option>
				</select>
			</td>
			<th class="text-left thGrey">Associate Relation</th>
			<td>
				<select class="form-control" name="associateRelation" id="associateRelation">
					<option value="yes">Yes</option>
					<option selected  value="no">No</option>
				</select>
			</td>
		</tr>	
    </table>

	<h2 class="pt20">Promotion settings</h2>
	<table class="table table-bordered">   
		<tr>
			<th class="text-left thGrey w20p">Using promotion code</th>
			<td class="w30p">
				<select class="form-control" name="promo" id="promo">
					<option value="no">No</option>
					<option value="yes">Yes</option>
				</select>
			</td>
			<th class="text-left thGrey w20p">Promotion Group Id</th>
			<td><input type="text" class="form-control" id="promoGroupId" name="promoGroupId" maxlength="20"></td>
		</tr>
    </table>
	
    <div class="btnDiv">
      <button type="button" class="btn btn-kyp" onclick="submit1(1)">Create</button>
      <a href="./main_content.php?menu=appUrlList" role="button" class="btn btn-kyp">Go to application list</a>
    </div>
</div>
</form>
<!-- content End -->

