<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
				break;
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'Please login.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports === 'yes'  && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$officeId = (int)$_SESSION['officeId'];
	$currentDate = date("d/m/Y");	
	/*
	echo '<pre>';
	print_r($_GET);
	echo '</pre>';
	*/
	$dateType = '';
	$searchType2 ='';
	$searchDate = 'no';
	$searchTwo = 'no';
	$searchFname = 'no';
	$searchLname ='no';
	$dateStartO  = '';
	$dateEndO = '';
	$engFnameO = '';
	$engLnameO = '';
	$searchTypeValue2O = '';
	$saveAsFile ='no';
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeIdSel= preg_replace("/<|\/|_|>/","", $_GET['officeId'] );
	}
	else{
		$officeIdSel = 'ALL';
	}
	if(isset($_GET['file']) && $_GET['file'] === 'yes'){
		$saveAsFile ='yes';
	}else{}
	$dateTypeArray= array(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
	if(isset($_GET['cardType']) && !empty($_GET['cardType'])){
		$cardType  = preg_replace("/<|\/|_|>/","",$_GET['cardType']);
		//$searchMode = 'searchMode';
	}else{
		$cardType= 'initial';
	}	
	if(isset($_GET['dateType']) && !empty($_GET['dateType'])){
		$dateType  = preg_replace("/<|\/|_|>/","",$_GET['dateType']);
		$searchMode = 'searchMode';
		switch($dateType){
			case 'regDate':
				$dateTypeArray[0] ='selected';
				break;
			case 'approvedDate':
				$dateTypeArray[1] ='selected';
				break;
			case 'paidDate':
				$dateTypeArray[2] ='selected';
				break;
			case 'issuedDate':
				$dateTypeArray[3] ='selected';
				break;
			default:
				break;
		}
	}else{}	
		
	if(isset($_GET['dateStart']) && !empty($_GET['dateStart']) && isset($_GET['dateEnd']) && !empty($_GET['dateEnd']) ){
		$searchDate ='yes';	
	}else{}
	
	if($searchDate ==='yes'){
		$dateStartO = preg_replace("/<|_|>/","",$_GET['dateStart']);
		$dateEndO = preg_replace("/<|_|>/","",$_GET['dateEnd']);
		$dateStartArray = explode('/',$dateStartO);
		$dateEndArray = explode('/',$dateEndO);
		$dateStart = $dateStartArray[2].'-'.$dateStartArray[1].'-'.$dateStartArray[0];
		$dateEnd = $dateEndArray[2].'-'.$dateEndArray[1].'-'.$dateEndArray[0];
	}else{}
	
	if(isset($_GET['searchType2Value']) && !empty($_GET['searchType2Value']) ){
		$searchTwo ='yes';
		$searchMode = 'searchMode';
	}else{}

	$searchType2Array= array(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
	if(isset($_GET['searchType2']) && !empty($_GET['searchType2'])){
		$searchType2 = preg_replace("/<|\/|_|>/","",$_GET['searchType2']);
		
		switch($searchType2){
			case 'born':		
				$searchType2Array[0] ='selected';
				break;
			case 'email':
				$searchType2Array[1] ='selected';
				break;
			default:
				break;
		}
	}else{}	
	
	if($searchTwo ==='yes'){
		$searchTypeValue2O = preg_replace("/<|_|>/","",$_GET['searchType2Value']);
		if($searchType2 === 'born'){
			$searchTypeValue2Array = explode('/',$searchTypeValue2O);
			if(isset($searchTypeValue2Array[0])){
				if($searchTypeValue2Array[0][0] === '0'){
					$birthDaySearch =substr($searchTypeValue2Array[0],1,1);
				}else{
					$birthDaySearch =$searchTypeValue2Array[0];
				}
			}else{
				$birthDaySearch = '0';
			}
			if(isset($searchTypeValue2Array[1])){
				if($searchTypeValue2Array[1][0] === '0'){
					$birthMonthSearch =substr($searchTypeValue2Array[1],1,1);
				}else{
					$birthMonthSearch = $searchTypeValue2Array[1];
				}
			}else{
				$birthMonthSearch = '0';
			}
			if(isset($searchTypeValue2Array[2])){
				$birthYearSearch =  $searchTypeValue2Array[2];
			}else{
				$birthYearSearch =  '0';
			}
		}else{}
	}else{}
	if (isset($_GET['engFname']) && !empty($_GET['engFname'])){
		$engFnameO =  preg_replace("/<|_|>/","",$_GET['engFname']);
		$searchFname = 'yes';
		$searchMode = 'searchMode';
		
	}else{}
	if (isset($_GET['engLname']) && !empty($_GET['engLname'])){
		$engLnameO =  preg_replace("/<|_|>/","",$_GET['engLname']);
		$searchLname = 'yes';
		$searchMode = 'searchMode';
		
	}else{}
	$searchType = array("ASC","DESC");
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	$loadAppOnce = 100; // how many applications read from DB at once

	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$queryAppSetting = "SELECT appSettingId, url FROM $tablename23 order by appSettingId";
		$stmtAppSetting = $db->prepare($queryAppSetting);
		$stmtAppSetting->execute();
		$appSettingCount =0;
		if($stmtAppSetting->rowCount() > 0){
			while($result = $stmtAppSetting->fetch(PDO::FETCH_OBJ)){
				$appSettingList[$appSettingCount]= $result;
				$appSettingCount++;
			}
		}else{
			//$db= NULL;
		}
		$query = "SELECT officeId,name FROM $tablename12";
		$stmt = $db->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() >= 1){
			$officeCount = $stmt->rowCount();
			$i =0;
			while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
				$officeList[$i] = $result0;
				$i++;
			}
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}		
		if (isset($searchMode) && $searchMode === 'searchMode'){
			$query = "SELECT count(appNo) as total FROM $tablename07";
			$stmt = $db->prepare($query);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$total = $result->total;
			$appReader = ceil($total/$loadAppOnce);
			$appStart = 0;
			$appCount = 0;
			for($i=0; $i<$appReader; $i++){
				if($appCount > 900){ //prevent over 1000 result
					break;
				}else{}
				$appStart = $i*$loadAppOnce;
				$appEnd = ($i+1)*$loadAppOnce;		
				$query = "SELECT appNo,birthDay,birthMonth,birthYear,engFname,engLname,email,customerId,encId FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE 1=1";
				if($searchDate === 'yes'){
					switch($dateType){
						case 'regDate':
							$query .=" AND date(regDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'approvedDate':
							$query .=" AND date(approvedDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'paidDate':
							$query .=" AND date(paidDate) BETWEEN :dateStart AND :dateEnd";
							break;
						case 'issuedDate':
							$query .=" AND date(issuedDate) BETWEEN :dateStart AND :dateEnd";
							break;
						default:
							break;
					}
				}else{}
				if($cardType === 'ALL'){
				}else if(isset($cardType) && !empty($cardType)){
					$query .=" AND cardType = :cardType";
				}else{}
				/*
				if ($_SESSION['officeId'] === '1'){
					//$query .= " AND ((officeId = 1) OR (officeId = 990));";
				}else{
					$query .= " AND (officeId = :officeId);";
				}*/
				if($officeId === 1){
					if($officeIdSel === 'ALL'){
					} else if ($officeIdSel === '1'){
						$query .= " AND (officeId = :inputValue11 OR officeId = 990)";
					} else {
						$query .= " AND officeId = :inputValue11";
					}
				} else {
					$query .= " AND officeId = :inputValue11";
				}
				$query .= " ORDER BY $tablename07.appNo $searchTypeSel LIMIT $appStart, $loadAppOnce";
				$stmt = $db->prepare($query);
				if($searchDate === 'yes'){
					switch($dateType){
						case 'regDate':
						case 'approvedDate':
						case 'paidDate':
						case 'issuedDate':
							$stmt->bindParam(':dateStart', $dateStart);
							$stmt->bindParam(':dateEnd', $dateEnd);
							break;
						default:
							break;
					}
				}else{}
				if($cardType === 'ALL'){
				}else if(isset($cardType) && !empty($cardType)){
					$stmt->bindParam(':cardType', $cardType);
				}else {}
				if($officeId === 1){
					if($officeIdSel === 'ALL'){
					}else{
						$stmt->bindParam(':inputValue11', $officeIdSel);
					}
				}else{
					$stmt->bindParam(':inputValue11', $officeId);
				}
				//echo $query;
				$stmt->execute();
				/*
				echo '<br/>';
				//echo $query;
				echo '<br/>';
				echo $stmt->rowCount();
				echo '<br/>';
				echo $searchDate.' '.$searchTwo.' '.$searchFname.' '.$searchLname;
				echo '<br/>';
				*/
				//print_r($stmt->errorInfo());
				while($result = $stmt->fetch(PDO::FETCH_OBJ)){				
					$isRight = 'no';
					if($searchDate ==='yes' && $searchTwo ==='no' && $searchFname === 'no' && $searchLname === 'no'){
						$isRight ='yes';
					}else{
						$isRight ='no';
					}
					if($searchTwo === 'yes'){
						switch($searchType2){
							case 'born':		
								if(decrypt1($result->birthDay, $result->encId) === $birthDaySearch && decrypt1($result->birthMonth, $result->encId) === $birthMonthSearch && decrypt1($result->birthYear, $result->encId) ===$birthYearSearch){
									$isRight ='yes';
								}else{
									$isRight ='no';
								}
								break;
							case 'email':
								if(strtolower(decrypt1($result->email, $result->encId)) === strtolower($searchTypeValue2O)){
									$isRight ='yes';
								}
								else{
									$isRight ='no';
								}
								break;
							case 'cid':
								if($result->customerId === $searchTypeValue2O){
									$isRight ='yes';
								}
								else{
									$isRight ='no';
								}
								break;
							default:
								break;
						}
					}else{}
					if($searchFname === 'yes'){
						if(strtolower(decrypt1($result->engFname, $result->encId)) === strtolower($engFnameO)){
							$isRight ='yes';
						}
						else{
							$isRight ='no';
						}
					}else{}
					if($searchLname ==='yes'){
						if(strtolower(decrypt1($result->engLname, $result->encId)) === strtolower($engLnameO)){
							$isRight ='yes';
						}
						else{
							$isRight ='no';
						}
					}else{}
				
					if($isRight === 'yes'){
						//echo " $appStart $appEnd ";
						//echo " $result->appNo ";
						$query2 = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE appNo = $result->appNo;";
						$stmt2 = $db->prepare($query2);
						$stmt2->execute();
						$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
					
						$applist[$appCount] = array (
						'appNo' => $result2->appNo, 
						'engFname' => decrypt1($result2->engFname, $result2->encId),
						'engLname' => decrypt1($result2->engLname, $result2->encId), 
						'email' => decrypt1($result2->email, $result2->encId), 
						'emailNews' => $result2->emailNews, 
						'cardSerialNum' => $result2->cardSerialNum, 
						'photoName' => $result2->photoName, 
						'cardType' => $result2->cardType,
						'issuedDate' => $result2->issuedDate, 
						'printedDate' => $result2->printedDate,
						'school' => decrypt1($result2->school, $result2->encId), 
						'birthYear' => decrypt1($result2->birthYear, $result2->encId), 
						'birthMonth' => decrypt1($result2->birthMonth, $result2->encId), 
						'birthDay' => decrypt1($result2->birthDay, $result2->encId), 
						'name1' => decrypt1($result2->name1, $result2->encId), 
						'officeId' => $result2->officeId, 
						'status' => $result2->status, 
						'regDate' => $result2->regDate, 
						'paidDate' => $result2->paidDate,
						'approvedDate' => $result2->approvedDate,
						'customerId' => $result2->customerId,
						'address' => decrypt1($result2->address, $result2->encId), 
						'city' => decrypt1($result2->city, $result2->encId), 
						'postal' => decrypt1($result2->postal, $result2->encId));
						$appCount = $appCount + 1;
					}else{}
				}
			}		
			$db= NULL;
		}else{}	
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}

	/*
	echo '<pre>';
	print_r($applist);
	echo '</pre>';
	*/
?>
<?php $today2 = date("d \/ m \/ Y"); ?>
<script type="text/javascript" src="./issuer/issuer_saveReport.js"></script>
<!-- content start-->
<form name="searchInSearch" id="searchInSearch" action="./main_content.php" method="GET">
<div id="contents">
<h1>Search for report
<?php if (isset($searchMode) && $searchMode === 'searchMode'){ ?>
(<?php echo $appCount.' Applications'; ?>
	<?php 
	if($appCount > 999){
		echo ' [Cannot show more than 1000] ';
	}else{}
	?>
)
<?php }else{}?>
</h1>

    <!-- search start -->
	
	<input type="hidden" name="menu" id="menu" value="saveReport">
  
    
    
	<div class="searchDiv">
		<?php 
			if($officeId === 1){ 
		?>
		<label>Office</label>
		<select class="form-control formYoon" name="officeId" id="officeId">
			<option value="ALL">ALL</option>
			<?php 
				for($i=0;$i<$officeCount;$i++){
					$selectThis = '';
					if($officeIdSel === $officeList[$i]->officeId){
						$selectThis ='selected';
					}else{}						
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
			<?php 
				}
			?>
		</select>
		<?php } else {} ?>
    <label>Card type</label>
			<select name="cardType" class="form-control formYoon">
				<option value="ALL">ALL</option>
			<?php for($i=0; $i< $appSettingCount; $i++) { 
				$selectThis = '';
				if($cardType === $appSettingList[$i]->url){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $appSettingList[$i]->url; ?>"><?php echo $appSettingList[$i]->url; ?></option>
			<?php } ?>
			</select>
    
    <label class="ml15">Date</label>
	   
		<label>
			<select class="form-control formYoon input-sm m0" name="dateType" id="dateType">
				<option <?php echo $dateTypeArray[0] ?> value="regDate">Registration date</option>
				<option <?php echo $dateTypeArray[1] ?> value="approvedDate">Approved date</option>
				<option <?php echo $dateTypeArray[2] ?> value="paidDate">Payment date</option>
				<option <?php echo $dateTypeArray[3] ?> value="issuedDate">Issued date</option>
			</select>
		</label>
		<input type="text" class="form-control formYoonCa input-sm" placeholder="dd/mm/yyyy" name="dateStart" id="dateStart" value="<?php echo $dateStartO?>"> ~ <input type="text" class="form-control formYoonCa input-sm" placeholder="dd/mm/yyyy" name="dateEnd" id="dateEnd" value="<?php echo $dateEndO?>"> 
    <br>
    <br>
		<label>
			<select class="form-control formYoon input-sm m0" name="searchType2" id="searchType2">
				<option <?php echo $searchType2Array[0] ?> value="born">Born(dd/mm/yyyy)</option>
				<option <?php echo $searchType2Array[1] ?> value="email">Email</option>
			</select>
		</label>
		<input type="text" class="form-control formYoonCa input-sm" name="searchType2Value" value="<?php echo $searchTypeValue2O; ?>" style="width: 180px; margin: 0;">
    
	   
		<label class="ml15">First name</label>
		<input type="text" class="form-control formYoonCa input-sm" name="engFname" value="<?php echo $engFnameO; ?>" style="width: 180px;">
		<label class="ml15">Last name</label>
		<input type="text" class="form-control formYoonCa input-sm" name="engLname" value="<?php echo $engLnameO; ?>" style="width: 180px;">

		<label class="ml15">Order</label>
			<select name="searchType" class="form-control formYoon input-sm">
			<?php for($i=0; $i< count($searchType); $i++) { 
				$selectThis = '';
				if($searchTypeSel === $searchType[$i]){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $searchType[$i]; ?>"><?php echo $searchType[$i]; ?></option>
			<?php } ?>
		</select>
    
		<button type="button" class="btn btn-kyp btn-sm ml20" onclick="submit01(1)"><i class="fab fa-sistrix"></i> Search</button>
		<br/>
		
															
						
																				  
																																
	</div>
	
    <!-- search end -->
    
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
            <!--<th class="text-center thGrey"></th>-->
            <th class="text-center thGrey">Card</th>
			<th class="text-center thGrey">regDate</th>
			<th class="text-center thGrey">approveDate</th>
			<th class="text-center thGrey">paidDate</th>
			<th class="text-center thGrey">issuedDate</th>
			<th class="text-center thGrey">Born</th>
            <th class="text-center thGrey">Name</th>
            <th class="text-center thGrey">cardSerialNum</th>
            <th class="text-center thGrey">Email</th>
			<th class="text-center thGrey">EmailNews</th>
        </tr>
		<?php if (isset($searchMode) && $searchMode === 'searchMode'){ ?>
		<?php for ($i = 0; $i < $appCount; $i++){ ?>
        <tr class="small">
			<!--<td class="text-center"><input type="checkbox"></td>-->
			<td class="text-center"><?php echo $applist[$i]['cardType'] ?></td>
			<td class="text-center"><?php echo substr($applist[$i]['regDate'],0,10) ?></td>
			<td class="text-center"><?php echo substr($applist[$i]['approvedDate'],0,10) ?></td>
			<td class="text-center"><?php echo substr($applist[$i]['paidDate'],0,10) ?></td>
			<td class="text-center"><?php echo substr($applist[$i]['issuedDate'],0,10) ?></td>
			<td class="text-center"><?php echo $applist[$i]['birthDay'] ?>/<?php echo $applist[$i]['birthMonth'] ?>/<?php echo $applist[$i]['birthYear'] ?></td>
			<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $applist[$i]['appNo']; ?>"><?php echo $applist[$i]['engFname'].' '.$applist[$i]['engLname']; ?></a></td>
			<td class="text-center"><?php echo $applist[$i]['cardSerialNum'] ?></td>
			<td class="text-center"><?php echo $applist[$i]['email']; ?></td>
			<td class="text-center"><?php echo $applist[$i]['emailNews']; ?></td>
        </tr>
		<?php } ?>
		<?php }else{} ?>
    </table>
    <!-- result end -->
    
    <div class="btnDiv">
      <input type="hidden" name="searchMode" value="searchMode">
      <?php $pagenum = 1; ?>
      <input type="hidden" id="pagenum" name="pagenum" value="<?php echo $pagenum ?>">
      <button type="button" class="btn btn-kyp btn-sm ml15" onclick="saveAsCsv(1)">Report file download (Save as CSV)</button>
    </div>
</div>
</form>
<!-- content end -->