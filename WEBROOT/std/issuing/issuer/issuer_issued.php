<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($_SESSION['user_type'])
		{
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/app/app01.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/app/app01.php");</script>';
		exit;
	} 
	$officeId = (int)$_SESSION['officeId'];
	$currentDate = date("d/m/Y");
	try 
	{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		/*
		$query = "SELECT * FROM $tablename23 WHERE appSettingIdApp = ";
		$stmt=$db->prepare($query);
		$stmt->execute();
		while($result = $stmt->fetch(PDO::FETCH_OBJ))
		{
			echo '<pre>';
			print_r($result);
			echo '</pre>';
		}
		*/
		$query = "SELECT appNo,cardSerialNum,cardType,issuedDate,encId,revaNum,oriSerialNum,appSettingIdApp FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE issuedDate IS NOT NULL AND sendDate IS NULL ORDER BY issuedDate DESC";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':inputValue11', $officeId);
		$stmt->execute();
		//echo 'total'.$stmt->rowCount();
		//echo '<br/>';
		$count1 =0;
		while($result = $stmt->fetch(PDO::FETCH_OBJ))
		{
			
			//$queryAppSetting = "SELECT issueType2,ccdbSend FROM $tablename23 ";
			$queryAppSetting = "SELECT issueType2,ccdbSend FROM $tablename23 WHERE appSettingId =:appSettingIdApp";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			$stmtAppSetting->bindParam(':appSettingIdApp', $result->appSettingIdApp);
			$stmtAppSetting->execute();
			//echo 'appid'.$result->appSettingIdApp.' ';
			//echo $stmtAppSetting->rowCount().'<br/>';
			if($stmtAppSetting->rowCount() === 1)
			{
				$appSettingInfo = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				if(isset($appSettingInfo->ccdbSend) &&  $appSettingInfo->ccdbSend ==='yes')
				{
				//if(isset($appSettingInfo->ccdbSend)){	
					$count1++;
					$appList[$count1] = $result;
					$appList[$count1]->issueType2 = $appSettingInfo->issueType2;	
										
				}
				else
				{
					//echo '<br>'.$result->appNo.'<br>';
					
				}
				
				//echo '1<br/>';
				
			}
			else
			{
				if(isset($appSettingInfo->ccdbSend) &&  $appSettingInfo->ccdbSend ==='yes')
				{
				//if(isset($appSettingInfo->ccdbSend)){	
					$count1++;
					$appList[$count1] = $result;	
					$appList[$count1]->issueType2 = 'P';
					
				}
				else
				{
					//echo '<br>'.$result->appNo.'<br>';
				}
				//echo '2<br/>';
			}				
		}
		$db= NULL;
	}
	catch (PDOExeception $e)
	{
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?>
<!-- content start -->

<div id="contents">
<h1>Manual data upload to CCDB <span class="h1Sub">- For the case of uploading error (<?php echo $count1?>)</span></h1>
<form name="issuedData" id="issuedData" action="./work/sendApplyAll.php" method="POST"  enctype="multipart/form-data">
<input type="hidden" name="formName" value="issued">
<input type="hidden" name="cardStatus" value="1">
<input type="hidden" name="appCount" value="<?php echo $count1;?>">
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey"></th>
            <th class="text-center thGrey">appNo</th>
            <th class="text-center thGrey">cardType</th>
            <th class="text-center thGrey">cardSerialNum</th>
            <th class="text-center thGrey">issuedDate</th>
            <th class="text-center thGrey"><!--<input type="submit" value="Send All">--></th>
        </tr>
		<?php for($i=1;$i<$count1+1;$i++){ ?>
        <tr>
			<input type="hidden" name="appNo<?php echo $i?>" value="<?php echo $appList[$i]->appNo ?>">
			<input type="hidden" name="cardType<?php echo $i?>" value="<?php echo $appList[$i]->cardType ?>">
			<td class="text-center"><?php echo $i ?></td>
			<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $appList[$i]->appNo; ?>"><?php echo $appList[$i]->appNo; ?></a></td>
			<td class="text-center"><?php echo $appList[$i]->cardType; ?></td>
			<td class="text-center">
			<?php 
				if($appList[$i]->cardType != 'REVA'){
					echo $appList[$i]->cardSerialNum;
				}else{
					echo $appList[$i]->oriSerialNum;
					echo '<br/>';
					echo $appList[$i]->revaNum;
				}
			?>
			</td>
			<td class="text-center"><?php echo $appList[$i]->issuedDate; ?></td>
			<td class="text-center"><a href="./work/sendApply3.php?appNo=<?php echo $appList[$i]->appNo ?>&formName=issued&cardType=<?php echo $appList[$i]->cardType ?>&cardStatus=1&issuedBy=<?php echo $_SESSION['valid_user'] ?>&issueType=<?php echo $appList[$i]->issueType2 ?>" role="button" class="btn btn-kyp">Send</a>
			<!--<a href="./work/sendApply3.php?appNo=<?php //echo $appList[$i]->appNo ?>&formName=issued&cardType=<?php //echo $appList[$i]->cardType ?>&cardStatus=1&issuedBy=<?php //echo $issuerName ?>&issueType=<?php //echo $appList[$i]->issueType2 ?>" role="button" class="btn btn-kyp">Send(test)</a>--></td>  
        </tr>
		<?php } ?>
    </table>
    <!-- result end -->
    </div>
</form>
<!-- content end  -->