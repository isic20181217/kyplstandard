// 210518 add appNo for docuDir
// 200601 check				
var widthlimit = 130;
var heightlimit = 130;
var ratio =1.25;
var tempdir='./orig/'; //currently not using. use .old and save in same directory
var uploaddir='./orig/'; //currently not using. use .old and save in same directory
var status1='apply_manager';

function classItem(){
	this.c;
	this.ctx;
	this.img;
	this.w;
	this.h;
	this.x1;
	this.y1;
	this.x2;
	this.y2;
	this.newx1;
	this.newy1;
	this.newx2;
	this.newy2;
	this.wcalc;
	this.hcalc;
	this.scale;
	this.rotateAng;
	this.phase;
	this.filename;
	this.croped;
	this.filenameName;
	this.filenameExt;
}
var photocanvas = new Array();
//make default canvas
window.onload =function(){
	var maxnum = Number(document.getElementById('maxnum').value);
	//console.log(maxnum);
	for(i=1;i<maxnum+1;i++){
		//console.log(i);
		var filename = document.getElementById('filename'+String(i)).value;
		var filenameName = document.getElementById('filenameName'+String(i)).value;
		var filenameExt = document.getElementById('filenameExt'+String(i)).value;
		var appNo = document.getElementById('appNo'+String(i)).value;
		imgload(i,'prev'+String(i),filename,filenameName,filenameExt,appNo);
		//console.log('filename'+String(i));
		//console.log(filename);
	}
	
}
function imgload(canvasNum,canvasName,filename,filenameName,filenameExt,appNo){
	//console.log('load'+canvasNum);
	var timestamp1 = new Date().getTime();
	photocanvas[canvasNum] = new classItem();
	photocanvas[canvasNum].phase = '0';
	//console.log(photocanvas[canvasNum].phase);
	photocanvas[canvasNum].c=document.getElementById(canvasName);
	photocanvas[canvasNum].ctx=photocanvas[canvasNum].c.getContext('2d');
	//console.log('onloa1d'+canvasNum);
	photocanvas[canvasNum].img = new Image();
	photocanvas[canvasNum].img.onload = function(msg, url, lineNo, columnNo, error){
		//console.log('onload'+canvasNum);
		photocanvas[canvasNum].w = photocanvas[canvasNum].img.width;
		photocanvas[canvasNum].h = photocanvas[canvasNum].img.height;
		
		// when the image is too small to resize.
		if (photocanvas[canvasNum].w < widthlimit || photocanvas[canvasNum].h < heightlimit){
			photocanvas[canvasNum].scale = 1;
			photocanvas[canvasNum].wcalc = photocanvas[canvasNum].w;
			photocanvas[canvasNum].hcalc = photocanvas[canvasNum].h;
		}
		else{
			if (photocanvas[canvasNum].w < photocanvas[canvasNum].h){
				photocanvas[canvasNum].scale = heightlimit / photocanvas[canvasNum].h;
				photocanvas[canvasNum].wcalc = photocanvas[canvasNum].scale * photocanvas[canvasNum].w;
				photocanvas[canvasNum].wcalc = Math.round(photocanvas[canvasNum].wcalc);
				photocanvas[canvasNum].hcalc = heightlimit;
			}
			//
			else if(photocanvas[canvasNum].w > photocanvas[canvasNum].h){
				photocanvas[canvasNum].scale = widthlimit / photocanvas[canvasNum].w;
				photocanvas[canvasNum].hcalc = photocanvas[canvasNum].scale * photocanvas[canvasNum].h;
				photocanvas[canvasNum].hcalc = Math.round(photocanvas[canvasNum].hcalc);
				photocanvas[canvasNum].wcalc = widthlimit;
			}
			//when width = height
			else{
				photocanvas[canvasNum].scale = heightlimit / photocanvas[canvasNum].h;
				photocanvas[canvasNum].wcalc = widthlimit;
				photocanvas[canvasNum].hcalc = widthlimit;
			}
		}
		
		photocanvas[canvasNum].ctx.clearRect(0,0,9999,9999);
		photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,0,0, photocanvas[canvasNum].wcalc,photocanvas[canvasNum].hcalc);
		photocanvas[canvasNum].rotateAng = 0;
		photocanvas[canvasNum].filename = filename;
		//console.log(filename);
		photocanvas[canvasNum].filenameName = filenameName;
		//console.log(filenameName);
		photocanvas[canvasNum].filenameExt =  filenameExt;
		photocanvas[canvasNum].appNo =  appNo;
		//console.log(filenameExt);
		document.getElementById('divPrev'+String(canvasNum)).style.display = "none";
		unselect1(canvasNum)
		//console.log('onload');
	}
	photocanvas[canvasNum].img.onerror = function(msg, url, lineNo, columnNo, error){
		document.getElementById('prev'+String(canvasNum)).style.display = "none";
		//console.log('onerror');
	}
	photocanvas[canvasNum].img.src = filename+'&time2='+timestamp1;
	//console.log(filename+'&time2='+timestamp1);
}
function callEditor01(canvasNum){
	
	var request = new XMLHttpRequest();
	request.open("POST", "./work/imgedit.php");
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var sendValue1 = 'status='+status1+'&tempdir='+tempdir+'&uploaddir='+uploaddir+'&filename='+photocanvas[canvasNum].filename+'&filenameName='+photocanvas[canvasNum].filenameName+'&filenameExt='+photocanvas[canvasNum].filenameExt;
	sendValue1 += '&x1='+photocanvas[canvasNum].newx1+'&y1='+photocanvas[canvasNum].newy1+'&x2='+photocanvas[canvasNum].newx2+'&y2='+photocanvas[canvasNum].newy2;
	sendValue1 += '&rotate='+photocanvas[canvasNum].rotateAng+'&scale='+photocanvas[canvasNum].scale+'&appNo='+photocanvas[canvasNum].appNo;
	//console.log(sendValue1);
	request.send(sendValue1);
	request.onreadystatechange = function() {
		if ( request.readyState === 4 && request.status === 200 ) { // request end (4), success (200)
		//imgload(i,'prev'+String(i),filename,filenameName,filenameExt);
			var timestamp2 = new Date().getTime();
			//console.log(canvasNum+' '+'prev'+canvasNum+' '+document.getElementById('filename'+String(canvasNum)).value+' '+document.getElementById('filenameName'+String(canvasNum)).value+' '+document.getElementById('filenameExt'+String(canvasNum)).value);
			imgload(canvasNum,'prev'+canvasNum,document.getElementById('filename'+String(canvasNum)).value,document.getElementById('filenameName'+String(canvasNum)).value,document.getElementById('filenameExt'+String(canvasNum)).value);
			//console.log('ok');
			
			//console.log(request.responseText);
		}
		else{
			//console.log('error');
			//console.log(canvasNum+' '+'prev'+canvasNum+' '+photocanvas[canvasNum].filename+' '+photocanvas[canvasNum].filenameName+' '+photocanvas[canvasNum].filenameExt);
		}
	}
	//console.log(canvasNum+' '+'prev'+canvasNum+' '+photocanvas[canvasNum].filename+' '+photocanvas[canvasNum].filenameName+' '+photocanvas[canvasNum].filenameExt);
}

// draw crop red square
function point(canvasNum,event){
	//console.log(photocanvas[canvasNum].phase);
	if (photocanvas[canvasNum].phase == '3'){
	}
	else if (photocanvas[canvasNum].phase == '1'){
		//2nd Crop point.
		photocanvas[canvasNum].x2 = event.offsetX;
		photocanvas[canvasNum].y2 = event.offsetY;
		//console.log('realx2_'+photocanvas[canvasNum].x2+' '+photocanvas[canvasNum].y2);
		//caution for x1= x2
		if(photocanvas[canvasNum].x2 == photocanvas[canvasNum].x1){
			photocanvas[canvasNum].x2= photocanvas[canvasNum].x2+1;
		}
		else{}
		//caution for y1= y2
		if(photocanvas[canvasNum].y2 == photocanvas[canvasNum].y1){
			photocanvas[canvasNum].y2 = photocanvas[canvasNum].y2+1;
		}
		else{}
		document.getElementById('crop_button'+String(canvasNum)).disabled = false;
		rotate1(4,canvasNum);
		
	}
	else{
		//1st crop point.
		photocanvas[canvasNum].x1 = event.offsetX;
		photocanvas[canvasNum].y1 = event.offsetY;
		//console.log('realx1_'+photocanvas[canvasNum].x1+' '+photocanvas[canvasNum].y1);
		//console.log('2nd '+photocanvas[canvasNum].x1+' '+photocanvas[canvasNum].y1);
		photocanvas[canvasNum].phase = '1';
		rotate1(4,canvasNum);
		document.getElementById('rotate_left'+String(canvasNum)).disabled = true;
		document.getElementById('rotate_right'+String(canvasNum)).disabled = true;
	}
}
function unselect1(canvasNum){
	//reset everything
	photocanvas[canvasNum].x1 = '';
	photocanvas[canvasNum].y1 = '';
	photocanvas[canvasNum].x2 = '';
	photocanvas[canvasNum].y2 = '';

	photocanvas[canvasNum].phase = '0';
	photocanvas[canvasNum].ctx.clearRect(0,0,9999,9999);	
	photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,0,0,photocanvas[canvasNum].wcalc,photocanvas[canvasNum].hcalc);	
	photocanvas[canvasNum].rotateAng = 0;
	document.getElementById('rotate_left'+String(canvasNum)).disabled = false;
	document.getElementById('rotate_right'+String(canvasNum)).disabled = false;	
	document.getElementById('crop_button'+String(canvasNum)).disabled = true;
	photocanvas[canvasNum].croped = '0';
}		
//rotate image
function rotate1(type,canvasNum){
	photocanvas[canvasNum].ctx.clearRect(0,0,9999,9999);
	photocanvas[canvasNum].ctx.save();
	photocanvas[canvasNum].ctx.translate(widthlimit/2,heightlimit/2);
	if (type == 1){
		switch(photocanvas[canvasNum].rotateAng){
			case 0:
				photocanvas[canvasNum].rotateAng = 270;
				break;
			case 90:
				photocanvas[canvasNum].rotateAng = 0;
				break;
			case 180:
				photocanvas[canvasNum].rotateAng = 90;
				break;
			case 270:
				photocanvas[canvasNum].rotateAng = 180;
				break;
			default:
				break;
		}
		photocanvas[canvasNum].ctx.rotate(photocanvas[canvasNum].rotateAng*Math.PI/180);
	}
	else if (type == 2){
		switch(photocanvas[canvasNum].rotateAng){
			case 0:
				photocanvas[canvasNum].rotateAng = 90;
				break;
			case 90:
				photocanvas[canvasNum].rotateAng = 180;
				break;
			case 180:
				photocanvas[canvasNum].rotateAng = 270;
				break;
			case 270:
				photocanvas[canvasNum].rotateAng = 0;
				break;
			default:
				break;
		}
		photocanvas[canvasNum].ctx.rotate(photocanvas[canvasNum].rotateAng*Math.PI/180);
	}
	else if(type == 3 || type == 4){
		photocanvas[canvasNum].ctx.rotate(photocanvas[canvasNum].rotateAng*Math.PI/180);
	}
	else{}
	photocanvas[canvasNum].ctx.translate(-widthlimit / 2,-heightlimit / 2);
	//view croped preview//
	if (type == 3){
		var scale = photocanvas[canvasNum].scale;
		var sourceX1 = photocanvas[canvasNum].newx1 / scale;
		var sourceY1 = photocanvas[canvasNum].newy1 / scale;
 		var sourceX2 = photocanvas[canvasNum].newx2 / scale;
		var sourceY2 = photocanvas[canvasNum].newy2 / scale;
		//console.log('source_1 '+photocanvas[canvasNum].newx1+' '+photocanvas[canvasNum].newy1+' '+photocanvas[canvasNum].newx2+' '+photocanvas[canvasNum].newy2+' '+scale);
		sourceX1 = Math.floor(sourceX1);
		sourceY1 = Math.floor(sourceY1); 
		sourceX2 = Math.floor(sourceX2);
		sourceY2 = Math.floor(sourceY2);
		//console.log('source_2 '+sourceX1+' '+sourceY1+' '+sourceX2+' '+sourceY2+' '+scale);
		var sourceWidth = Math.abs(sourceX2 - sourceX1);
		var sourceHeight = Math.abs(sourceY2 - sourceY1);
		if (sourceWidth > sourceHeight){
			var destHeight =  Math.round(widthlimit  /  sourceWidth * sourceHeight);
			var destWidth = widthlimit;
		}
		else if(sourceWidth < sourceHeight){
			var destWidth =  Math.round(heightlimit  /  sourceHeight * sourceWidth);
			var destHeight = heightlimit;
		}
		if(sourceX1 < sourceX2 && sourceY1 < sourceY2){
			photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,sourceX1,sourceY1,sourceWidth,sourceHeight,0,0,destWidth,destHeight);
		}
		else if(sourceX1 > sourceX2 && sourceY1 < sourceY2){
			photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,sourceX2,sourceY1,sourceWidth,sourceHeight,0,0,destWidth,destHeight);
		}
		else if(sourceX1 < sourceX2 && sourceY1 > sourceY2){
			photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,sourceX1,sourceY2,sourceWidth,sourceHeight,0,0,destWidth,destHeight);
		}
		else if(sourceX1 > sourceX2 && sourceY1 > sourceY2){
			photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,sourceX2,sourceY2,sourceWidth,sourceHeight,0,0,destWidth,destHeight);
		}
		else{
			return false;
		}
	}
	/////////////////////////
	//just preview//
	else {
		photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,0,0,photocanvas[canvasNum].wcalc,photocanvas[canvasNum].hcalc);
	}
	//show croppoint on rotated preview
	if (photocanvas[canvasNum].x2 > 0 && photocanvas[canvasNum].y2 > 0 && type != 3 ){
		switch(photocanvas[canvasNum].rotateAng){
			case 90:
				photocanvas[canvasNum].newx1 = photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newy1 = heightlimit-photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newx2 = photocanvas[canvasNum].y2;
				photocanvas[canvasNum].newy2 = heightlimit-photocanvas[canvasNum].x2;
				var redheight = Math.abs(photocanvas[canvasNum].newy2-photocanvas[canvasNum].newy1);
				var redwidth = redheight * ratio;
				var redwidth = Math.round(redwidth);
				//console.log('90');
				break;
			case 180:
				photocanvas[canvasNum].newx1 = widthlimit-photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newy1 = heightlimit-photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newx2 = widthlimit-photocanvas[canvasNum].x2;
				photocanvas[canvasNum].newy2 = heightlimit-photocanvas[canvasNum].y2;
				var redwidth = Math.abs(photocanvas[canvasNum].newx2-photocanvas[canvasNum].newx1);
				var redheight = redwidth * ratio;
				var redheight = Math.round(redheight);
				//console.log('180');
				break;
			case 270:
				photocanvas[canvasNum].newx1 = widthlimit-photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newy1 = photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newx2 = widthlimit-photocanvas[canvasNum].y2;
				photocanvas[canvasNum].newy2 = photocanvas[canvasNum].x2;
				var redheight = Math.abs(photocanvas[canvasNum].newy2-photocanvas[canvasNum].newy1);
				var redwidth = redheight * ratio;
				var redwidth = Math.round(redwidth);
				//console.log('270');
				break;
			default:
				photocanvas[canvasNum].newx1 = photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newy1 = photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newx2 = photocanvas[canvasNum].x2;
				photocanvas[canvasNum].newy2 = photocanvas[canvasNum].y2;
				var redwidth = Math.abs(photocanvas[canvasNum].newx2-photocanvas[canvasNum].newx1);
				var redheight = redwidth * ratio;
				var redheight = Math.round(redheight);
				//console.log('0');
				break;
		}
		var startx = photocanvas[canvasNum].newx1;
		var starty = photocanvas[canvasNum].newy1;
		if(photocanvas[canvasNum].newx1 < photocanvas[canvasNum].newx2 && photocanvas[canvasNum].newy1 < photocanvas[canvasNum].newy2){
			photocanvas[canvasNum].newx2 = photocanvas[canvasNum].newx1+ redwidth;
			photocanvas[canvasNum].newy2 = photocanvas[canvasNum].newy1 + redheight;
			//console.log('1');
		}
		else if(photocanvas[canvasNum].newx1 > photocanvas[canvasNum].newx2 && photocanvas[canvasNum].newy1 < photocanvas[canvasNum].newy2){
			redwidth = -redwidth;
			photocanvas[canvasNum].newx2 = photocanvas[canvasNum].newx1 + redwidth;
			photocanvas[canvasNum].newy2 = photocanvas[canvasNum].newy1 + redheight;
			//console.log('2');
		}
		else if(photocanvas[canvasNum].newx1 < photocanvas[canvasNum].newx2 && photocanvas[canvasNum].newy1 > photocanvas[canvasNum].newy2){
			redheight= -redheight;
			photocanvas[canvasNum].newx2 = photocanvas[canvasNum].newx1 +redwidth;
			photocanvas[canvasNum].newy2 = photocanvas[canvasNum].newy1 + redheight;
			//console.log('3');
		}
		else if(photocanvas[canvasNum].newx1 > photocanvas[canvasNum].newx2 && photocanvas[canvasNum].newy1 > photocanvas[canvasNum].newy2){
			redwidth = -redwidth;
			redheight= -redheight;
			photocanvas[canvasNum].newx2 = photocanvas[canvasNum].newx1 + redwidth;
			photocanvas[canvasNum].newy2 = photocanvas[canvasNum].newy1 + redheight;
			//console.log('4');
		}
		else{
			photocanvas[canvasNum].newx2 = photocanvas[canvasNum].newx2;
			photocanvas[canvasNum].newy2 = photocanvas[canvasNum].newy1 + redheight;
		}
			//console.log('changed_2nd '+photocanvas[canvasNum].newx2+' '+photocanvas[canvasNum].newy2);
		//if the cropping rectalgle exceeds the canvas boundary, the cropping rectangle stops;
		if (photocanvas[canvasNum].newx2 < widthlimit && photocanvas[canvasNum].newx2 > 0 && photocanvas[canvasNum].newy2 < heightlimit && photocanvas[canvasNum].newy2 > 0){	
			//photocanvas[canvasNum].x2 = photocanvas[canvasNum].newx2;
			//photocanvas[canvasNum].y2 = photocanvas[canvasNum].newy2;		
			photocanvas[canvasNum].ctx.beginPath();
			photocanvas[canvasNum].ctx.rect(startx,starty,redwidth,redheight);
			photocanvas[canvasNum].ctx.lineWidth = 2;
			photocanvas[canvasNum].ctx.strokeStyle = 'red';
			photocanvas[canvasNum].ctx.stroke();
			photocanvas[canvasNum].ctx.closePath();
			//console.log('changed_'+photocanvas[canvasNum].newx1+' '+photocanvas[canvasNum].newy1+' '+photocanvas[canvasNum].newx2+' '+photocanvas[canvasNum].newy2);
		}
		else{ //keep the first crop point.
			photocanvas[canvasNum].ctx.beginPath();
			photocanvas[canvasNum].ctx.rect(photocanvas[canvasNum].newx1,photocanvas[canvasNum].newy1,2,2);
			photocanvas[canvasNum].ctx.lineWidth = 2;
			photocanvas[canvasNum].ctx.strokeStyle = 'red';
			photocanvas[canvasNum].ctx.stroke();
			photocanvas[canvasNum].ctx.closePath();
		}
	}
	else if (photocanvas[canvasNum].x1 > 0 && photocanvas[canvasNum].y1 > 0 && type != 3 ){
		
		switch(photocanvas[canvasNum].rotateAng){
			case 90:
				photocanvas[canvasNum].newx1 = photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newy1 = heightlimit-photocanvas[canvasNum].x1;
				break;
			case 180:
				photocanvas[canvasNum].newx1 = widthlimit-photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newy1 = heightlimit-photocanvas[canvasNum].y1;
				break;
			case 270:
				photocanvas[canvasNum].newx1 = widthlimit-photocanvas[canvasNum].y1;
				photocanvas[canvasNum].newy1 = photocanvas[canvasNum].x1;
				break;
			default:
				photocanvas[canvasNum].newx1 = photocanvas[canvasNum].x1;
				photocanvas[canvasNum].newy1 = photocanvas[canvasNum].y1;
				break;
		}
		//console.log('changed_1_'+photocanvas[canvasNum].newx1+' '+photocanvas[canvasNum].newy1);
		photocanvas[canvasNum].ctx.beginPath();
		photocanvas[canvasNum].ctx.rect(photocanvas[canvasNum].newx1,photocanvas[canvasNum].newy1,2,2);
		photocanvas[canvasNum].ctx.lineWidth = 2;
		photocanvas[canvasNum].ctx.strokeStyle = 'red';
		photocanvas[canvasNum].ctx.stroke();
		photocanvas[canvasNum].ctx.closePath();
	}
	else{};
	photocanvas[canvasNum].ctx.restore();
}
function crop1(canvasNum){
	if (photocanvas[canvasNum].y2 > 0){
		document.getElementById('rotate_left'+String(canvasNum)).disabled = true;
		document.getElementById('rotate_right'+String(canvasNum)).disabled = true;
		document.getElementById('crop_button'+String(canvasNum)).disabled = true;
		photocanvas[canvasNum].croped = 'croped'
		rotate1(3,canvasNum);
		photocanvas[canvasNum].phase = '3';
	}
	else{
		//alert('Please select area for cropping.');
	}
}