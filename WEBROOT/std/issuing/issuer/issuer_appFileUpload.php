<?php 
///201114 check		
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sCanCsv) && $resultUserInfo->sCanCsv === 'yes'  && isset($resultOfficeInfo->canCsv) && $resultOfficeInfo->canCsv ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied2\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	
	$_SESSION['uploadApp1'] = 'start';
?>
<script type="text/javascript" src="./issuer/issuer_appFileUpload.js?ver=1"></script>
<!-- content Start -->
<div id="contents">
  <div class="row">
    <div class="col-sm-8"><h1>Bulk uploaded applications <i class="fas fa-angle-double-right"></i> <span class="h1Sub">New applications bulk upload</span></h1></div>
    <div class="col-sm-4 text-right"><a href="./work/dwd2.php?fname=bulk"><i class="far fa-file-excel"></i> Excel form dowload <i class="fa fa-caret-right"></i></a></div>
  </div>

	<form name="fileUploadForm" id="fileUploadForm" method="POST" enctype="multipart/form-data">
		<table class="table table-bordered">
			<tr>
				<th class="text-left thGrey w20p">File Upload( CSV )</th>
				<td class="text-center">	<input class="form-control input-sm" type="file" name="file1" id="file1" accept=".csv"></td>
			</tr>
			<tr>
				<th class="text-left thGrey w20p">Photo Upload( ZIP )</th>
				<td class="text-center">	<input class="form-control input-sm" type="file" name="file2" id="file2" accept=".zip"></td>
			</tr>
			<tr>
				<th class="text-left thGrey w20p">Delimiter </th>
				<td>
					<select class="form-control input-sm" name="delimiter">
						<option value=";">Semicolon ;</option>
						<option value=",">Comma ,</option>
					</select>
				</td>
			</tr>
		</table>
		<div class="btnDiv">
			<button type="button" class="btn btn-kyp" onclick="submit1(1);">Upload</button>
			<button type="button" class="btn btn-kyp" onclick="checkFileSize1(1);">CheckFileSize</button>
			<button type="button" class="btn btn-kyp" onclick="checkZipFile1(1);">CheckZipFile</button>
		</div>
	</form>
</div>

<!-- content end -->