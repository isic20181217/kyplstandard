<?php 
// 210606 add changePrice
// 210606 add appNo for imgReader
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['no'])){
		$id1= preg_replace("/<|\/|_|>/","",$_GET['no'] );
		//$id1 = $_GET['no'];
		$oriSerialNum = '1';
	}
	else if (isset($_GET['oriSerialNum'])){
		$oriSerialNum= preg_replace("/<|\/|_|>/","",$_GET['oriSerialNum'] );
		$id1 = '1';
		//$oriSerialNum = $_GET['oriSerialNum'];
	}
	else{
		echo '<script>alert(\'no id.\');</script>';
		echo "<script> window.history.go(-1); </script>";
		exit;
	}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';	
		$query = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo LEFT JOIN $tablename24 ON $tablename07.appNo = $tablename24.extraAppNo WHERE (($tablename07.appNo = :searchValue1) OR (cardSerialNum = :searchValue2))";
		if ($_SESSION['officeId'] === '1'){
			//$query .= " AND ((officeId = 1) OR (officeId = 990))";
		}else{
			$query .= " AND (officeId = :officeId)";
		}
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $id1);
		$stmt->bindParam(':searchValue2', $oriSerialNum);
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':officeId', $_SESSION['officeId']);
		}
		$stmt->execute();
		if($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$id1 = $result->appNo;
			$customerId = $result->customerId;
			$issuedDate = $result->issuedDate;
			$approvedDate = $result->approvedDate;
			$paidDate = $result->paidDate;
			$sendDate = $result->sendDate;
			$printedDate = $result->printedDate;
			$status = $result->status;
			$payStatus = $result->payStatus;
			$orderNo = $result->orderNo;
			$receiveType = $result->receiveType;
			$cardType = $result->cardType;
			$appSettingIdApp = $result->appSettingIdApp;
			$photoName = $result->photoName;
			$gender = $result->gender;
			$nationality = (int)$result->nationality;
			$emailNews = $result->emailNews;
			$proof1Name = $result->proof1Name;
			$proof2Name = $result->proof2Name;
			$proof3Name = $result->proof3Name;
			$proof4Name = $result->proof4Name;
			$countryId = $result->countryId;
			$officeId = $result->officeId;
			$appNo = $result->appNo;
			$regDate = $result->regDate;
			$cardPrice = $result->cardPrice;
			$shippingPrice = $result->shippingPrice;
			$changePrice = $result->changePrice;
			$validityStart = $result->validityStart;
			$validityEnd = $result->validityEnd;
			$revaStart =$result->revaStart;
			$revaEnd =$result->revaEnd;
			$revaDate =$result->revaDate;
			$revaNum = $result->revaNum;
			$cardSerialNum = $result->cardSerialNum;
			$oriSerialNum = $result->oriSerialNum;
			
			//possibly encrypted data
			$name1= $result->name1;
			$engLname = $result->engLname;
			$engFname = $result->engFname;
			$birthYear = $result->birthYear;
			$birthMonth = $result->birthMonth;
			$birthDay = $result->birthDay;
			$email = $result->email;			
			$phone = $result->phone;
			$comment = $result->comment;
			//$mobile = $result->mobile;
			$school = '';
			$modifiedBy= $result->modifiedBy;
			$createdBy= $result->createdBy;
			$modifiedDate= $result->modifiedDate;									   
			if($cardType != 'IYTC'){
				$school = $result->school;	
			}else{}
			$city = '';
			$address = '';
			$address2 = '';
			$country ='';
			$postal = '';
			if($receiveType !='visit'){
				//echo '5523';
				$city = $result->city;
				$address = $result->address;
				$postal = $result->postal;
				$country= $result->country;
				$address2 = $result->address2;
			}else{}
			//echo $result->vatNumber;
			$vatNumber='';
			if(isset($result->vatNumber) && !empty($result->vatNumber)){
				$vatNumber= $result->vatNumber;
			}else{}
			//echo $vatNumber;
			$promo ='';
			if(isset($result->promo) && !empty($result->promo)){
				$promo= $result->promo;
			}else{}
			$associateNumber = '';
			if(isset($result->associateNumber) && !empty($result->associateNumber)){
				$associateNumber= $result->associateNumber;
			}else{}
			$associateName ='';
			if(isset($result->associateName)&& !empty($result->associateName)){
				$associateName= $result->associateName;
			}else{}
			$associateRelation ='';
			if(isset($result->associateRelation) && !empty($result->associateRelation)){
				$associateRelation= $result->associateRelation;
			}else{}
			for($extraLoop=1;$extraLoop<11;$extraLoop++){
				$currentExtra = 'extra'.$extraLoop.'Value';
				if(isset($result->$currentExtra) && !empty($result->$currentExtra)){
					$extraList[$extraLoop] = $result->$currentExtra;
				}else{}
			}			
			if($result->encDate && $result->encId){
				$encId = $result->encId;
				$engFname = decrypt1($engFname, $encId); 
				$engLname = decrypt1($engLname, $encId);
				$name1 =decrypt1($name1, $encId); 
				$birthYear = decrypt1($birthYear, $encId);
				$birthMonth = decrypt1($birthMonth, $encId);
				$birthDay = decrypt1($birthDay, $encId);		
				$email = decrypt1($email, $encId);
				$phone = decrypt1($phone, $encId);
				$comment = decrypt1($comment, $encId);
				if(isset($createdBy) && !empty($createdBy))
				{
					$createdBy = decrypt1($createdBy, $encId);
				}
				else
				{}
				//$mobile = decrypt1($mobile, $encId);			
				if($cardType != 'IYTC'){
					$school = decrypt1($school, $encId);
				}else{}
				if($receiveType !='visit'){
					//echo '1123';
					$city = decrypt1($city, $encId);
					$address = decrypt1($address, $encId);
					$address = str_replace("\""," ",$address);
					$address2 = decrypt1($address2, $encId);
					$address2 = str_replace("\""," ",$address2);
					$country = decrypt1($country, $encId);
					$postal = decrypt1($postal, $encId);
				}else{}
				//$vatNumber= '';
				//if(isset($result->vatNumber) && !empty($result->vatNumber)){
			//		$vatNumber = decrypt1($vatNumber, $encId);
				//}else{}
				if(isset($result->associateNumber) && !empty($result->associateNumber)){
					$associateNumber = decrypt1($associateNumber, $encId);
				}else{}
				if(isset($result->associateName) && !empty($result->associateName)){
					$associateName = decrypt1($associateName, $encId);
				}else{}
				if(isset($result->associateRelation) && !empty($result->associateRelation)){
					$associateRelation = decrypt1($associateRelation, $encId);
				}else{}
				for($extraLoop=1;$extraLoop<11;$extraLoop++){
					$currentExtra = 'extra'.$extraLoop.'Value';
					if(isset($result->$currentExtra) && !empty($result->$currentExtra)){
						$extraList[$extraLoop] = decrypt1($extraList[$extraLoop] , $encId);
					}else{}
				}
				
				$query5 = "SELECT * FROM $tablename22 WHERE payLogAppNo = :searchValue1";
				$stmt5 = $db->prepare($query5);
				$stmt5->bindParam(':searchValue1', $id1);
				$stmt5->execute();
				if($stmt5->rowCount()>0){
					$payLogCount =0;
					while($payLogResult = $stmt5->fetch(PDO::FETCH_OBJ)){
						$payLog[$payLogCount]['payLogType'] = $payLogResult->payLogType;
						$payLog[$payLogCount]['payLogDate'] = $payLogResult->payLogDate;
						$payLog[$payLogCount]['payLogIp'] = $payLogResult->payLogStatus;
						$payLogCount = $payLogCount+1;
					}																			
				}else{}		
			}else{
			$db= NULL;
			echo '<script>alert(\'enc Error\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
			}
		}else{
			$db= NULL;
			echo '<script>alert(\'no id2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
		//$db= NULL;
		$query = "SELECT issueType2 FROM $tablename23 WHERE appSettingId =:appSettingIdApp";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingIdApp', $appSettingIdApp);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);
/*
			echo '<pre>';
			print_r($result);
			echo '</pre>';
*/			
		}else{
			echo 'No AppSettingInfo : '.$cardType;
			$db= NULL;
			exit;
		}
		/*
		if(isset($receiveType) && $receiveType === 'virtual'){
			$appSettingInfo->issueType2 = 'v';
		}else{}*/
		$query = "SELECT officeId,name FROM $tablename12";
		//$valueYes1 = 'yes';
		$stmt = $db->prepare($query);
		//$stmt->bindParam(':activeOffice', $valueYes1);
		$officeList = array();
		$stmt->execute();
		if($stmt->rowCount() >= 1){
			$officeCount = $stmt->rowCount();
			$iOffice =0;
			while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
				$officeList[$iOffice] = $result0;
				$iOffice++;
			}
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}		
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	function read_img($index,$name,$ext,$appNo){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index."&appNo=".$appNo;
		return $src;
	}	

?>
<script type="text/javascript" src="./issuer/issuer_searchDetail.js"></script>
<!-- content Start -->
<form name="searchDetail" id="searchDetail" action="" method="POST" enctype="multipart/form-data">
<input type="hidden" name="appNo" id="appNo" value="<?php echo $id1; ?>">
<input type="hidden" name="formName" id="formName" value="searchDetail">
<input type="hidden" name="cardType" id="cardType" value="<?php echo $cardType; ?>">
<input type="hidden" name="appSettingIdApp" id="appSettingIdApp" value="<?php echo $appSettingIdApp; ?>">
<input type="hidden" name="issuedBy" id="issuedBy" value="<?php echo $_SESSION['valid_user']; ?>">			
<input type="hidden" name="issueType2" id="issueType2" value="<?php echo $appSettingInfo->issueType2; ?>">																				  
<input type="hidden" name="receiveType" id="receiveType" value="<?php echo $receiveType; ?>">
<div id="contents">
<h1>Application details</h1>
<!-- menu description start -->
<!-- menu description end -->
	
	
	<h2>Application information</h2>
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey w20p">AppNo</th>
			<td class="w30p"><?php echo $id1?></td>
			<th class="text-center thGrey">CardType</th>
			<td class="w30p"><?php echo $cardType ?></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Approved Date</th>
			<td class="w30p"><?php echo $approvedDate; ?></td>
			<th class="text-center thGrey">Issued Date</th>
			<td class="w30p"><?php echo $issuedDate; ?></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Created By</th>
			<td class="w30p">			
				<?php 
					if (isset($createdBy)){
						echo $createdBy;
					}else{} 
				?>
			</td>
			<th class="text-center thGrey">Card Receive</th>
			<td class="w30p"><?php echo $receiveType; ?></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Modified By</th>
			<td class="w30p"><?php echo $modifiedBy; ?></td>
			<th class="text-center thGrey">Modified Date</th>
			<td class="w30p"><?php echo $modifiedDate; ?></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Order Number</th>
			<td class="w30p"><?php echo $orderNo; ?></td>
			<th class="text-center thGrey">Paid Date</th>
			<td class="w30p"><?php echo $paidDate; ?></td>
        </tr>
		<tr>
			<th class="text-center thGrey">Payment Log</th>
			<td colspan="3">
				<?php 
				if(isset($payLogCount) && $payLogCount >0){
					for($i=0;$i<$payLogCount;$i++){
						echo $payLog[$i]['payLogDate'];
						echo ' | ';
						echo $payLog[$i]['payLogIp'];
						echo ' | ';
						echo $payLog[$i]['payLogType'];
						echo '<br/>';
					}
				}else{}
				?>
			</td>
		</tr>
  </table>
  
  <h2 class="pt20">Personal Details</h2>
    <table class="table table-bordered">
        <tr>
          <th rowspan="4" class="text-center thGrey w20p">Photo</th>
          <td rowspan="4" class="text-center w30p">
			<?php if(isset($photoName)) {?>
			<?php echo $photoName;?>
			<?php $photoName = pathinfo($photoName); ?>

			<p><img height="150" id="preview" src="<?php echo read_img('1',$photoName['filename'],$photoName['extension'],$id1);?>"></p>
			<?php }else{} ?>
            <p class="mx"><input type="file" name="photoName" id="photoName" class="form-control"></p>
          </td>
          <th class="text-center thGrey w20p">Name in Local Language</th>
          <td class="w30p"><input type="text" class="form-control" name="name1" id="name1" value="<?php echo $name1; ?>"></td>
        </tr>
        <tr>
          <th class="text-center thGrey w20p">First name</th>
          <td class="w30p"><input type="text" class="form-control" name="engFname" id="engFname" maxlength="20" value="<?php echo $engFname; ?>"></td>
        </tr>
        <tr>
          <th class="text-center thGrey">Last name</th>
          <td><input type="text" class="form-control" name="engLname" id="engLname" maxlength="20" value="<?php echo $engLname; ?>"></td>   
        </tr>
        <tr>
          <th class="text-center thGrey">Born <span class="normal small">(DDMMYYYY)</span></th>
          <td>
            <div class="row">
              <div class="col-xs-4 pr3">
                <input type="text" class="form-control" maxlength="2" name="birthDay" id="birthDay" value="<?php echo $birthDay ?>" >
              </div>
              <div class="col-xs-4 pr3 pl3">
                <input type="text" class="form-control" maxlength="2" name="birthMonth" id="birthMonth" value="<?php echo $birthMonth ?>" >
              </div>
              <div class="col-xs-4 pl3">
                <input type="text" class="form-control" maxlength="4" name="birthYear" id="birthYear" value="<?php echo $birthYear ?>" >
              </div>
            </div>
            <!--<p class="m0">(Proof of date of birth: <a href="#" target="_blank"><?php //echo $result->proof1Name; ?></a>)</p>-->
          </td>
        </tr>
        <tr>
          <th class="text-center thGrey">Gender</th>
          <td><select class="form-control" name="gender" id="gender">
            <?php 
					if ('f' == $gender){
						echo "<option selected value =\"f\">Female</option>";
						echo "<option value =\"m\">Male</option>";
					}
					else if ('m' == $gender){
						echo "<option value =\"f\">Female</option>";
						echo "<option selected value =\"m\">Male</option>";
					}
					else{
						echo "<option selected value =\" \">Deleted</option>";
						echo "<option value =\"f\">Female</option>";
						echo "<option value =\"m\">Male</option>";
					}
				?>
          </select></td>
          <th class="text-center thGrey">Nationality</th>
          <td><?php //echo $countryId;;?>
                    <select class="form-control" name="nationality" id="nationality">
					<?php 
						if($nationality >0){
							//echo $nationality;
						}else{
							$nationality = (int)$countryId;
						}
						for($i=1;$i<241;$i++){
							if ($i === $nationality){
								echo "<option selected value =\"{$i}\">{$countryName[$i]}</option>";
							}
							else{
								echo "<option value =\"{$i}\">{$countryName[$i]}</option>";
							}
						}
					?>
                </select>
          </td>
        </tr>
        <tr>
          <th class="text-center thGrey">Email</th>
          <td><div class="row">
            <div class="col-xs-8 pr0">
              <input type="text" class="form-control" name="email" id="email" value="<?php echo $email; ?>">
            </div>
            <div class="col-xs-4">
              <select class="form-control" name="emailNews" id="emailNews">
			  <?php 
					if ($emailNews === 'yes'){
						echo "<option selected value =\"yes\">Y</option>";
						echo "<option value =\"no\">N</option>";
					}
					else{
						echo "<option value =\"yes\">Y</option>";
						echo "<option selected value =\"no\">N</option>";
					}
				?>
              </select>
            </div>
          </div></td>
          <th class="text-center thGrey">Phone no.</th>
    		  <td class="w30p"><input type="text" class="form-control" name="phone" id="phone" value="<?php echo $phone; ?>"></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Studies/Teaches at</th>
			<td><input type="text" class="form-control" name="school" id="school" value="<?php echo $school; ?>"></td>
			<th class="text-center thGrey">Proof documents</th>
			<td>
				<p class="mb-3"><a href="./work/dwd1.php?fname=<?php echo $proof1Name?>&appNo=<?php echo $id1;?>" target="_blank"><?php echo $proof1Name?></a></p>
				<p class="mx"><input type="file" class="form-control" name="proof1Name" id="proof1Name"></p>
				<p class="mb-3"><a href="./work/dwd1.php?fname=<?php echo $proof2Name?>&appNo=<?php echo $id1;?>" target="_blank"><?php echo $proof2Name?></a></p>
				<p class="mx"><input type="file" class="form-control" name="proof2Name" id="proof2Name"></p>
				<p class="mb-3"><a href="./work/dwd1.php?fname=<?php echo $proof3Name?>&appNo=<?php echo $id1;?>" target="_blank"><?php echo $proof3Name?></a></p>
				<p class="mx"><input type="file" class="form-control" name="proof3Name" id="proof3Name"></p>
				<p class="mb-3"><a href="./work/dwd1.php?fname=<?php echo $proof4Name?>&appNo=<?php echo $id1;?>" target="_blank"><?php echo $proof4Name?></a></p>
				<p class="mx"><input type="file" class="form-control" name="proof4Name" id="proof4Name"></p>
			</td>
        </tr>
        <tr>
			<th class="text-center thGrey">Vat Number</th>
			<td><input type="text" class="form-control" name="vatNumber" id="vatNumber" value="<?php echo $vatNumber; ?>"></td>
			<th class="text-center thGrey">Promo</th>
			<td><input type="text" class="form-control" name="promo" id="promo" value="<?php echo $promo; ?>"></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Associate Number</th>
			<td><input type="text" class="form-control" name="associateNumber" id="associateNumber" value="<?php echo $associateNumber; ?>"></td>
			<th class="text-center thGrey">Associate Name</th>
			<td><input type="text" class="form-control" name="associateName" id="associateName" value="<?php echo $associateName; ?>"></td>
        </tr>
        <tr>
			<th class="text-center thGrey">Associate Relation</th>
			<td><input type="text" class="form-control" name="associateRelation" id="associateRelation" value="<?php echo $associateRelation; ?>"></td>
			<th class="text-center thGrey"></th>
			<td></td>
        </tr>
		<?php 
			for($extraLoop=1;$extraLoop<11;$extraLoop++){
				$currentExtra = 'extra'.$extraLoop.'Value';
				if(isset($result->$currentExtra) && !empty($result->$currentExtra)){
					$currentExtraName = 'extra'.$extraLoop.'Name';
		?>
		<tr>
			<th class="text-center thGrey"><?php echo $result->$currentExtraName ?></th>
			<td><input type="text" class="form-control" name="<?php echo $currentExtra ?>" id="<?php echo $currentExtra ?>" value="<?php echo $extraList[$extraLoop] ?>"></td>
			<th class="text-center thGrey"></th>
			<td></td>
        </tr>
		<?php 
			}else{}
			}
		?>
        <tr>
          <th class="text-center thGrey">Password</th>
          <td colspan="2" class="no-border-right"><input type="text" class="form-control" name="newPassword" id="newPassword" value=""></td>
		      <td class="no-border-left"><button type="button" class="btn btn-kyp" onclick="randomMaker(1);">Generate password</button></td>
		</tr>
    </table>

	<h2 class="pt20">Address</h2>
    <table class="table table-bordered">
        <tr>
          <th class="text-center thGrey w20p">Address</th>
          <td colspan="3"><input type="text" class="form-control" name="address" id="address" value="<?php echo $address; ?>"></td>
        </tr>
		<tr>
          <th class="text-center thGrey w20p">Address2</th>
          <td colspan="3"><input type="text" class="form-control" name="address2" id="address2" value="<?php echo $address2; ?>"></td>
        </tr>
        <tr>
          <th class="text-center thGrey w20p">City</th>
          <td class="w30p"><input type="text" class="form-control" name="city" id="city" value="<?php echo $city; ?>"></td>
          <th class="text-center thGrey w20p">Country</th>
          <td class="w30p"><input type="text" class="form-control" name="country" id="country1" value="<?php echo $country; ?>"></td>
        </tr>
        <tr>
          <th class="text-center thGrey w20p">Zipcode</th>
          <td><input type="text" class="form-control" name="postal" id="postal" value="<?php echo $postal; ?>"></td>
          <th class="text-center thGrey w20p">&nbsp;</th>
          <td>&nbsp;</td>
        </tr>
    </table>

	<h2 class="pt20">ID data</h2>
    <table class="table table-bordered">
        <tr>
          <th class="text-center thGrey w20p">Application No.</th>
          <td class="w30p"><?php echo $countryId.'_'.$officeId.'_'.$appNo; ?></td>
          <th class="text-center thGrey w20p">Applied date</th>
          <td><?php echo $regDate; ?></td>
        </tr>
        <tr>
          <th class="text-center thGrey">Payment amount</th>
          <td><?php echo $cardPrice + $shippingPrice; ?> (<?php echo $cardPrice; ?> + shipping <?php echo $shippingPrice; ?>)</td>
          <th class="text-center thGrey">Validity</th>
          <td>
          	<input type="text" class="form-control formYoonCa" name="validityStart" id="validityStart" value="<?php echo $validityStart; ?>"> ~ <input type="text" class="form-control formYoonCa"  name="validityEnd" id="validityEnd" value="<?php echo $validityEnd; ?>">
          </td>
		 </tr>
		<tr>
          <th class="text-center thGrey w20p">Card Serial Number</th>
          <td ><?php echo $cardSerialNum; ?></td>
		  <th class="text-center thGrey w20p">Customer ID</th>
          <td ><?php echo $customerId; ?></td>
        </tr>
		<tr>
          <th class="text-center thGrey w20p">Office ID</th>
			<td>
				<?php 
				if($_SESSION['officeId'] === '1'){ 
				?>
				<select class="form-control" name="officeId" id="officeId">
					<option value="990">WEB</option>
					<?php 
					for($i=0;$i<$officeCount;$i++)
					{
						$selectThis = '';
						if($officeId === $officeList[$i]->officeId)
						{
							$selectThis ='selected';
						}
						else
						{}						
					?>
					<option <?php echo $selectThis; ?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
					<?php 
					}
					?>
				</select>
				<?php 
				} 
				else 
				{
					echo $officeId;
				}			
				?>
			</td>
		  <th class="text-center thGrey w20p"></th>
          <td ></td>
        </tr>
    </table>
	<h2 class="pt20">Revalidation Data</h2>
	<table class="table table-bordered">
		<tr>
			<th class="text-center thGrey w20p">Original Serial Number</th>
			<td class="w30p"><input type="text" class="form-control" name="oriSerialNum" id="oriSerialNum" value="<?php echo $oriSerialNum; ?>"></td>
			<th class="text-center thGrey w20p">Revalidaion Number</th>
			<td class="w30p"><input type="text" class="form-control" name="revaNum" id="revaNum" value="<?php echo $revaNum; ?>"></td></td>
		</tr>
		<tr>
			<th class="text-center thGrey">Revalidation Date</th>
			<td><?php echo $revaDate?></td>
			<th class="text-center thGrey">Revalidation Validity</th>
			<td>
			<input type="text" class="form-control formYoonCa" name="revaStart" id="revaStart" value="<?php echo $revaStart; ?>"> ~ <input type="text" class="form-control formYoonCa"  name="revaEnd" id="revaEnd" value="<?php echo $revaEnd; ?>">
			</td>
		</tr>
	</table>	
	<h2 class="pt20">Comments</h2>
	<div>
		<textarea rows="7" class="form-control" maxlength="50" id="comment" name="comment"><?php echo $comment; ?></textarea>
	</div>
    
    <div class="btnDiv">
        <button type="button" class="btn btn-kyp" onclick="submit1(1);">Save</button>
		<?php if($payStatus ==='paid'){ ?>
		<?php }else{?>
			<button type="button" class="btn btn-kyp2" onclick="paid1(1);">Paid</button>
		<?php } ?>
		<?php if($status ==='initial'){?>
			<?php if (isset($resultUserInfo->sCanValidate) && $resultUserInfo->sCanValidate === 'yes'  && isset($resultOfficeInfo->canValidate) && $resultOfficeInfo->canValidate ==='yes' ){ ?>
		<button type="button" class="btn btn-kyp2" onclick="approve1(1,<?php echo $appNo;?>,<?php echo '\''.$cardType.'\''; ?>);">Approve</button>
			<?php }else{}?>
		<?php }else{} ?>
		<?php if($status ==='approved' && $payStatus ==='paid' && $cardSerialNum != NULL){?>
			<?php if (isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' ){ ?>
		<button type="button" class="btn btn-kyp" onclick="printApply(<?php echo $appNo;?>,1);">Card Print</button>
		<button type="button" class="btn btn-kyp" onclick="printApply(<?php echo $appNo;?>,2);">Label Print</button>
		<button type="button" class="btn btn-kyp" onclick="undoIssuing(<?php echo $appNo;?>,1);">Undo Issuing</button>
			<?php }else{}?>
		<?php }else{} ?>
		<?php if(isset($cardSerialNum) && !empty($cardSerialNum)){ ?>
			<?php if(isset($result->sendDate) && !empty($result->sendDate)){?>
				<button type="button" class="btn btn-kyp" onclick="sendApply3(1);">Resend to CCDB(<?php echo $result->sendDate?>)</button>
			<?php }else{ ?>
				<button type="button" class="btn btn-kyp" onclick="sendApply3(1);">Send to CCDB</button>
			<?php }?>						
		<?php }else{} ?>
		<button type="button" class="btn btn-danger" onclick="deleteApply(1);">Delete</button>
        <button type="button" class="btn btn-kyp" onclick="goBack(1);">Back to list</button>
    </div>
</div>
</form>
<div id="result1" style="display:none;">
<?php 

	echo '<pre>';
	print_r($result);
	echo '</pre>';
	
?>
</div>
<!-- content end -->