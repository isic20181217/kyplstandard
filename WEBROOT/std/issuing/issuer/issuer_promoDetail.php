<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['gId']) && !empty($_GET['gId'])){
		$gId = preg_replace("/<|\/|_|>/","", $_GET['gId']);
	}else{
		echo 'Access Denied1';
		exit;
	}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	
	require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
	$query = "SELECT * FROM $tablename27 WHERE promoGroupId =:searchValue1";
	$stmt = $db->prepare($query);
	$stmt->bindParam(':searchValue1', $gId);
	$stmt->execute();
	//print_r($stmt->errorInfo());
	if($stmt->rowCount() == 1){
		$promoCount =0;
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$promoList[$promoCount]= $result;
			$queryTotal = "SELECT * FROM $tablename26 WHERE promoCodeGroup = :searchValue1";
			$stmtTotal = $db->prepare($queryTotal);
			$stmtTotal->bindParam(':searchValue1', $promoList[$promoCount]->promoGroupId);
			$stmtTotal->execute();
			//print_r($stmtTotal->errorInfo());
			if($stmtTotal->rowCount() > 0){
				
				$totalValue = $stmtTotal->rowCount();
				$promoList[$promoCount]->total = $totalValue; 
				$queryUsed = "SELECT * FROM $tablename26 WHERE promoCodeGroup = :searchValue1 AND promoUsed = 'yes'";
				$stmtUsed = $db->prepare($queryUsed);
				$stmtUsed->bindParam(':searchValue1', $promoList[$promoCount]->promoGroupId);
				$stmtUsed->execute();
				//print_r($stmtUsed->errorInfo());
				if($stmtUsed->rowCount() > 0){
					
					$usedValue = $stmtUsed->rowCount(); 
					$promoList[$promoCount]->used = $usedValue;
				}else{
					$promoList[$promoCount]->used = 0;
				}
			}else{
				$promoList[$promoCount]->used = 0;
				$promoList[$promoCount]->total = 0;
			}
			$promoCount++;
		}
	}else{
		$db= NULL;
	}
	$db = NULL;
	/*
	echo '<pre>';
	print_r($promoList);
	echo '</pre>';
	*/	
?>
<script src="./issuer/issuer_promoDetail.js?ver=2"></script>
<!-- content start -->

<div id="contents">
<button data-toggle="collapse" data-target="#tip" class="collapsible btn btn-kyp2 btn-sm"><i class="fas fa-question"></i></button>

<h1>Manage promotion  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Promotion details</span></h1>

<!-- menu Description start -->
<div id="tip" class="collapse content">
</div>
<!-- menu description End -->

    <!-- search Start -->
    <!-- search End -->
    
    <!-- result Start -->
	<form name="modifyPromo" id="modifyPromo" method="POST" enctype="multipart/form-data">
	<h2>Setting</h2>
	<table class="table table-bordered">
		<tr>
			<th class="text-left thGrey">promoActive</th>
			<td class="w30p">
				<select class="form-control" name="promoActive" id="promoActive">
					<?php 
						$thisColumn1 = 'promoActive';
						if(isset($promoList[0]->$thisColumn1) && $promoList[0]->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">promoType</th>
			<td>
				<input type="hidden" class="form-control"  name="promoType" id="promoType" maxlength="20" readonly value="<?php echo $promoList[0]->promoType?>"><?php echo $promoList[0]->promoType?>
			</td>	
	    </tr>
		<tr>
			<th class="text-left thGrey w20p">promoGroupName</th>
			<td>
				<input type="text" class="form-control"  name="promoGroupName" id="promoGroupName" maxlength="20" value="<?php echo $promoList[0]->promoGroupName?>">
				<small style="color: #999;">* Alphabets and numeric characters only.</small>
			</td>
			<th class="text-left thGrey w20p">promoGroupId</th>
			<td>
				<input type="hidden" class="form-control"   name="promoGroupId" id="promoGroupId" maxlength="20" readonly value="<?php echo $promoList[0]->promoGroupId?>"><?php echo $promoList[0]->promoGroupId?>
			</td>			
		</tr>
		<tr>
			<th class="text-left thGrey w20p">Discount</th>
			<td>
				<input type="text" class="form-control"  name="promoDiscount" id="promoDiscount" maxlength="20" value="<?php echo $promoList[0]->promoDiscount?>">
				<small style="color: #999;">* Numeric characters only.</small>												  
			</td>
			<th class="text-left thGrey w20p">Modified Info</th>
			<td>
				<?php echo $promoList[0]->modifiedDate?><br/><?php echo $promoList[0]->modifiedBy?>
			</td>			
		</tr>
		<tr>
			<th class="text-left thGrey w20p">used</th>
			<td>
				<?php echo $promoList[0]->used?>
			</td>
			<th class="text-left thGrey w20p">total</th>
			<td>
				<?php echo $promoList[0]->total?>
			</td>			
		</tr>
		<tr>
			<th class="text-left thGrey w20p">Code CSV</th>
			<td>
				<input type="file" class="form-control" name="file1" id="file1" accept=".csv"></td>
			</td>
			<th class="text-left thGrey w20p"></th>
			<td>
			</td>			
		</tr>
	</table>
    <!-- result End -->
	
    <!-- paging Start -->              
    <!-- paging end -->
	<div class="btnDiv">
		<button type="button" class="btn btn-kyp" onclick="submit1(1)">Modify</button>
		<button type="button" class="btn btn-kyp" onclick="delete1(1)">Delete</button>
		<button type="button" class="btn btn-kyp" onclick="goBack(1)">Back to list</button>
	</div>
	<div class="btnDiv">
		<button type="button" class="btn btn-kyp" onclick="viewCode1(1)">View Codes</button>
		<button type="button" class="btn btn-kyp" onclick="addCode1(1)">Add Codes</button>
		<button type="button" class="btn btn-kyp" onclick="deleteCode1(1)">Delete Unused Codes</button>
		<button type="button" class="btn btn-kyp" onclick="deleteCodeAll1(1)">Delete All Codes</button>
	</div>
	</form>
</div>

<!-- content End -->

