<?php
// 210630 get expiredMonth from setting.php
// 210629 change issuedDate to expiredDate
///200603 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$currentDate = date("Y-m-s");
	$searchType = array("ASC","DESC");
	//$oldAppMonth = setting.php
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT appNo FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (appNo IS NOT NULL)";
		//$query = "SELECT appNo FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (regDate IS NOT NULL) ";
		//echo $_SESSION['officeId'] ;
		if ($_SESSION['officeId'] === '1'){
			$query .= " AND ((officeId = 1) OR (officeId = 990))";
		}else{
			$query .= " AND (officeId = :searchValue1)";
		}
		$query .=" AND (expiredDate <= date_add(now(), interval -$expiredMonth month))";
		$stmt = $db->prepare($query);
		//echo $query;
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
		}
		$stmt->execute();
		$howmany = 20; // show how many applications for once
		$howmanypage = 10; // show how many pages for once
		$maxsize = $stmt->rowCount();
		$maxpage = floor($maxsize / $howmany); //total application
				//echo ' '.$maxsize.' '.$maxpage.' ';
		if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1){
			$pagenum = $_GET['pagenum'];
		}
		else{
			$pagenum = 1; // current pagenumber
		}
		$page = ($pagenum-1)*$howmany;
		if($maxsize % $howmany > 0){
			$maxpage = $maxpage + 1; //if the number of applications is 110 and the number of applications for one page is 10, show application 101~110 on page 11
			}
		if ($pagenum > $maxpage){
			$pagenum = $maxpage;
		}
		else{}
		$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
		if($maxsize % ($howmany*$howmanypage) > 0){
			$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group.
		}
		else{}

		$currentgroup = floor($pagenum / $howmanypage); //current page's group
		if($pagenum % $howmanypage > 0){
			$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
			$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
		else{
			$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
		}
		$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (appNo IS NOT NULL)";
		//$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (regDate IS NOT NULL) ";
		if ($_SESSION['officeId'] === '1'){
			$query .= " AND ((officeId = 1) OR (officeId = 990))";
		}else{
			$query .= " AND (officeId = :searchValue1)";
		}
		$query .=" AND expiredDate <= date_add(now(), interval -$expiredMonth month)";
		$query .= " ORDER BY $tablename07.appNo $searchTypeSel LIMIT $page, $howmany";
		$stmt = $db->prepare($query);
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
		}
		$stmt->execute();
		$rowcount1 = $stmt->rowCount();
		$count1 = 0;
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$applist[$count1] = array (
			'appNo' => $result->appNo,
			'engFname' => decrypt1($result->engFname, $result->encId),
			'engLname' => decrypt1($result->engLname, $result->encId),
			'photoName' => $result->photoName,
			'cardType' => $result->cardType,
			'school' => decrypt1($result->school, $result->encId),
			'birthYear' => decrypt1($result->birthYear, $result->encId),
			'birthMonth' => decrypt1($result->birthMonth, $result->encId),
			'birthDay' => decrypt1($result->birthDay, $result->encId),
			'name1' => decrypt1($result->name1, $result->encId),
			'officeId' => $result->officeId,
			'proof1Name' => $result->proof1Name,
			'proof2Name' => $result->proof2Name,
			'proof3Name' => $result->proof3Name,
			'proof4Name' => $result->proof4Name,
			'payStatus' =>$result->payStatus,
			'status' => $result->status,
			'payStatus' => $result->payStatus,
			'regDate' => $result->regDate,
			'issuedDate' => $result->issuedDate,
			'expiredDate' => $result->expiredDate,
			'address' => decrypt1($result->address, $result->encId),
			'city' => decrypt1($result->city, $result->encId),
			'postal' => decrypt1($result->postal, $result->encId),
			'cardSerialNum' => $result->cardSerialNum,
			'revaNum' => $result->revaNum,
			'oriSerialNum' => $result->oriSerialNum);
			$count1 = $count1 +1;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?>
<script src="./issuer/issuer_expiredApps.js"></script>
<!-- Content Start -->
<div id="contents">
    <!-- search Start -->
	<form method="get" name="expiredAppsForm" id="expiredAppsForm">
		<input type="hidden" name="formName" value="expiredApps">
		<input type="hidden" id="maxnum" value="<?php echo $rowcount1; ?>">
    <h1>Expired Apps <span class="h1Sub"> (<?php echo $maxsize.' Applications'; ?>)</span> <i class="far fa-question-circle qButton" data-toggle="collapse" data-target="#tip"></i><!-- Click question mark button : Show menu description --></h1>

		<!-- Menu description -->
		<div id="tip" class="collapse content">
			<p>On this menu, you can see the expired applications that have been submitted more than 2years ago.</p>
			<ol class="m-0">
				<li>Select the way of showing the search results between “ASC(ascending order)” and “DESC(descending order).”</li>
				<li>To see and edit the details of an application, click the name of the application you want from the list.</li>
			</ol>
		</div>
		<!-- menu description end -->
	<div class="searchDiv">
		<label>Order</label>
			<select name="searchType" class="form-control formYoon" onchange="moveTo(<?php echo $pagenum?>,value)">
			<?php for($i=0; $i< count($searchType); $i++) {
				$selectThis = '';
				if($searchTypeSel === $searchType[$i]){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $searchType[$i]; ?>"><?php echo $searchType[$i]; ?></option>
			<?php } ?>
		</select>
	</div>
	</form>
    <!-- search End -->

    <!-- result Start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey">&nbsp;</th>
            <th class="text-center thGrey">Card Type</th>
            <th class="text-center thGrey">Name</th>
            <th class="text-center thGrey">ExpiredDate</th>
            <th class="text-center thGrey">Studies/Teaches at</th>
            <th class="text-center thGrey">Born</th>
            <th class="text-center thGrey">Documents</th>
            <th class="text-center thGrey">Status</th>
        </tr>
		<?php for ($i = 0; $i < $rowcount1; $i++){ ?>
        <tr>
			<td class="text-center"><?php echo $applist[$i]['appNo']?></td>
			<?php
				if ($applist[$i]['cardType'] === 'REVA') {
					switch(substr($applist[$i]['oriSerialNum'],0,1)){
						case 'S' :
								$revCardType = 'ISIC';
								break;
						case 'T' :
								$revCardType = 'ITIC';
								break;
						case 'Y' :
								$revCardType = 'IYTC';
								break;
						default:
								$revCardType = 'ERR';
								break;
					}
					$thisCardType = $revCardType.' '.$applist[$i]['cardType'];
				} else {
					$thisCardType = $applist[$i]['cardType'];
				}
			?>
            <td class="text-center"><?php echo $thisCardType; ?></td>
            <td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $applist[$i]['appNo']; ?>"><?php echo $applist[$i]['name1']; ?><br /><?php echo $applist[$i]['engFname'].' / '.$applist[$i]['engLname']; ?></a></td>
            <td class="text-center"><?php echo $applist[$i]['expiredDate'] ?></td>
            <td class="text-center"><?php echo $applist[$i]['school'] ?></td>
            <td class="text-center"><?php echo $applist[$i]['birthDay'].'/'. $applist[$i]['birthMonth'].'/'. $applist[$i]['birthYear']; ?></td>
			<td class="text-center">
				<?php if(isset($applist[$i]['proof1Name']) && !empty($applist[$i]['proof1Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof1Name']?>" target="_blank">Doc1</a><br>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof2Name']) && !empty($applist[$i]['proof2Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof2Name']?>" target="_blank">Doc2</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof3Name']) && !empty($applist[$i]['proof3Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof3Name']?>" target="_blank">Doc3</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof4Name']) && !empty($applist[$i]['proof4Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof4Name']?>" target="_blank">Doc4</a>
				<?php }else{} ?>
			</td>
            <td class="text-center"><?php echo $applist[$i]['status'] ?>/<?php echo $applist[$i]['payStatus'] ?></td>
        </tr>
		<?php } ?>
    </table>
    <!-- result End -->
 	<!-- Paging Start -->
	<?php
		if($rowcount1 === 0){
		}else{
	?>
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=expiredApps&pagenum=1&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-left"></i></a></li>
			<?php
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=expiredApps&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-left"></i></a></li>
			<?php
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=expiredApps&pagenum=$pagingno&searchType=$searchTypeSel\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=expiredApps&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=expiredApps&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>

    </div>
	<?php } ?>
    <!-- pagingEnd -->

</div>
</form>
<!-- Conent End -->
