<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager';
			//case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}	
	$searchType = array("ASC","DESC");
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC'){
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	$searchCardUrl = '';
	if (isset($_GET['searchCardUrl']) && !empty($_GET['searchCardUrl'])){
		$searchCardUrl= preg_replace("/<|\/|_|>/","",$_GET['searchCardUrl'] );
	}else{}
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		if(isset($searchCardUrl) && !empty($searchCardUrl) ){
			$query = "SELECT appSettingId,url FROM $tablename23 WHERE url = :url";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':url', $searchCardUrl);
			$stmt->execute();
			if($stmt->rowCount() === 1){
				$searchCardInfo = $stmt->fetch(PDO::FETCH_OBJ);
			}else{
				$db= NULL;
				echo '<script>alert(\'Wrong Card Url\');</script>';
				echo "<script> window.history.go(-1); </script>";
				exit;
			}
		}else{}		
		$query = "SELECT appNo FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL)";
		if ($_SESSION['officeId'] === '1'){
			$query .= " AND ((officeId != 1) AND (officeId != 990));";
		}else{
			//$query .= " AND (officeId = :searchValue1);";
		}
		if(isset($searchCardUrl) && !empty($searchCardUrl) ){
			$query .= " AND appSettingIdApp = :appSettingIdApp";
		}else{}		
		$stmt = $db->prepare($query);
		if(isset($searchCardUrl) && !empty($searchCardUrl) ){
			$stmt->bindParam(':appSettingIdApp', $searchCardInfo->appSettingIdApp);
		}else{}
		if ($_SESSION['officeId'] === '1'){
		}else{
			//$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
		}
		$stmt->execute();
		$howmany = 20; // show how many applications for once
		$howmanypage = 10; // show how many pages for once
		$maxsize = $stmt->rowCount();
		$maxpage = floor($maxsize / $howmany); //total application 
				//echo ' '.$maxsize.' '.$maxpage.' ';
		if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1){
			$pagenum = $_GET['pagenum'];
		}
		else{
			$pagenum = 1; // current pagenumber
		}
		$page = ($pagenum-1)*$howmany;
		if($maxsize % $howmany > 0){
			$maxpage = $maxpage + 1; //if the number of applications is 110 and the number of applications for one page is 10, show application 101~110 on page 11
			}
		if ($pagenum > $maxpage){ 
			$pagenum = $maxpage;
		}
		else{}
		$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
		if($maxsize % ($howmany*$howmanypage) > 0){
			$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
		}
		else{}
		
		$currentgroup = floor($pagenum / $howmanypage); //current page's group
		if($pagenum % $howmanypage > 0){
			$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
			$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
		else{
			$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
		}
		$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL) ";
		if ($_SESSION['officeId'] === '1'){
			$query .= " AND ((officeId != 1) AND (officeId != 990))";
		}else{
			//$query .= " AND (officeId = :searchValue1)";
		}
		if(isset($searchCardUrl) && !empty($searchCardUrl) ){
			$query .= " AND appSettingIdApp = :appSettingIdApp";
		}else{}	
		$query .= " ORDER BY $tablename07.appNo $searchTypeSel LIMIT $page, $howmany";
		$stmt = $db->prepare($query);
		if ($_SESSION['officeId'] === '1'){
		}else{
			$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
		}
		if(isset($searchCardUrl) && !empty($searchCardUrl) ){
			$stmt->bindParam(':appSettingIdApp', $searchCardInfo->appSettingId);
		}else{}
		$stmt->execute();
		$rowcount1 = $stmt->rowCount();
		$count1 = 0;
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$applist[$count1] = array (
			'appNo' => $result->appNo, 
			'engFname' => decrypt1($result->engFname, $result->encId), 
			'engLname' => decrypt1($result->engLname, $result->encId), 
			'photoName' => $result->photoName, 
			'cardType' => $result->cardType,
			'appSettingIdApp' =>$result->appSettingIdApp,
			'school' => decrypt1($result->school, $result->encId), 
			'birthYear' => decrypt1($result->birthYear, $result->encId), 
			'birthMonth' => decrypt1($result->birthMonth, $result->encId), 
			'birthDay' => decrypt1($result->birthDay, $result->encId), 
			'name1' => decrypt1($result->name1, $result->encId), 
			'officeId' => $result->officeId,
			'proof1Name' => $result->proof1Name, 
			'proof2Name' => $result->proof2Name, 
			'proof3Name' => $result->proof3Name, 
			'proof4Name' => $result->proof4Name, 
			'payStatus' =>$result->payStatus, 
			'status' => $result->status, 
			'regDate' => $result->regDate, 
			'address' => decrypt1($result->address, $result->encId), 
			'city' => decrypt1($result->city, $result->encId), 
			'postal' => decrypt1($result->postal, $result->encId), 
			'cardSerialNum' => $result->cardSerialNum, 
			'revaNum' => $result->revaNum, 
			'oriSerialNum' => $result->oriSerialNum);
			//$queryDesign = "SELECT issueType,issueType2 FROM $tablename23 WHERE url =:searchValue1";
			$queryAppSetting = "SELECT issueType,issueType2 FROM $tablename23 WHERE appSettingId =:appSettingId";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			//$stmtAppSetting->bindParam(':searchValue1', $applist[$count1]['cardType']);
			$stmtAppSetting->bindParam(':appSettingId', $applist[$count1]['appSettingIdApp']);
			$stmtAppSetting->execute();
			if($stmtAppSetting->rowCount() === 1){
				$appSettingInfo = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				$applist[$count1]['issueType'] = $appSettingInfo->issueType;
				$applist[$count1]['issueType2'] = $appSettingInfo->issueType2;
	/*
				echo '<pre>';
				print_r($result);
				echo '</pre>';
	*/			
			}else{
				echo 'No AppSettingInfo : '.$result->cardType;
				$db= NULL;
				exit;
			}
			$count1 = $count1 +1;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	function read_img($index,$name,$ext){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}
?>
<script src="./issuer/imgedit_2.js"></script>
<script src="./issuer/issuer_appFromIo.js"></script>
<!-- Content Start -->
<div id="contents">
	
	<h1>Application from IO
		<i class="fas fa-angle-double-right"></i> 
		<span class="h1Sub">
			List (<?php echo $maxsize.' Applications'; ?>)
		</span>
	</h1>
    <!-- search Start -->
	<div class="searchDiv">
	<form method="get" name="IssuingForm" id="IssuingForm">
		<input type="hidden" name="formName" value="appFromIo">				   
		<input type="hidden" id="maxnum" value="<?php echo $rowcount1; ?>">
		<label>Order</label>
			<select name="searchType" id="searchType" class="form-control formYoon">
			<?php for($i=0; $i< count($searchType); $i++) { 
				$selectThis = '';
				if($searchTypeSel === $searchType[$i]){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $searchType[$i]; ?>"><?php echo $searchType[$i]; ?></option>
			<?php } ?>
		</select>
		<label>CardType</label>
		<input type="text" class="form-control formYoonCa input-sm" placeholder="URL" name="searchCardUrl" id="searchCardUrl" value="<?php echo $searchCardUrl; ?>" >
		<button type="button" class="btn btn-kyp btn-sm ml20" onclick="search1(1)"><i class="fab fa-sistrix"></i> Search</button>
	</form>
	</div>
    <!-- search End -->
    
    <!-- result Start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey">&nbsp;</th>
            <th class="text-center thGrey">Card Type</th>
            <th class="text-center thGrey">Name</th>
            <th class="text-center thGrey">Photo</th>
            <th class="text-center thGrey">Studies/Teaches at</th>
            <th class="text-center thGrey">Born</th>
            <th class="text-center thGrey">Documents</th>
            <th class="text-center thGrey">Approval</th>
        </tr>
		<?php for ($i = 0; $i < $rowcount1; $i++){ ?>
        <tr>
			<td class="text-center"><?php echo $applist[$i]['appNo']?></td>
			<?php 
				if ($applist[$i]['cardType'] === 'REVA') { 
					switch(substr($applist[$i]['oriSerialNum'],0,1)){ 
						case 'S' :
								$revCardType = 'ISIC';
								break;
						case 'T' :
								$revCardType = 'ITIC';
								break;
						case 'Y' :
								$revCardType = 'IYTC';
								break;
						default:
								$revCardType = 'ERR';
								break;
					}
					$thisCardType = $revCardType.' '.$applist[$i]['cardType'];
				} else {
					$thisCardType = $applist[$i]['cardType'];
				}
			?>
            <td class="text-center"><?php echo $thisCardType; ?></td>
            <td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $applist[$i]['appNo']; ?>"><?php echo $applist[$i]['name1']; ?><br /><?php echo $applist[$i]['engFname'].' / '.$applist[$i]['engLname']; ?></a></td>
            <td class="text-center">
            	<p>
					<?php if(isset($applist[$i]['photoName'])) {?>
					<?php $photoName = pathinfo($applist[$i]['photoName']); ?>
					<?php }else{}?>
					<input type="hidden" id="filenameName<?php echo $i+1 ?>" value="<?php echo $photoName['filename']?>">	
					<input type="hidden" id="filenameExt<?php echo $i+1 ?>" value="<?php echo $photoName['extension']?>">	
					<input type="hidden" id="filename<?php echo $i+1 ?>" value="<?php echo read_img($i+1,$photoName['filename'],$photoName['extension']); ?>">	
					<div id="divPrev<?php echo $i+1;?>">None</div>
					<canvas id="prev<?php echo $i+1;?>" onclick="point(<?php echo $i+1;?>,event)" style="border:0px solid #ccc; cursor:crosshair;" width="130" height="130">HTML5 Support Required</canvas>
					<!--<img height="130" src="<?php //echo '../'.$storageDir.$applist[$i]['photoName']; ?>" class="approveListPhoto">-->
				</p>
				<?php if($applist[$i]['cardType'] === 'REVA'){ ?>
				<a href="./main_content.php?menu=searchDetail&oriSerialNum=<?php echo $applist[$i]['oriSerialNum']; ?>"><?php echo $applist[$i]['oriSerialNum']; ?></a>
				<?php } else { ?>
							<div>
								<button type="button" class="btn btn-kyp2 btn-xs" id="rotate_left<?php echo $i+1;?>" onclick="rotate1(1,<?php echo $i+1;?>);"><i class="fa fa-undo" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" id="rotate_right<?php echo $i+1;?>" onclick="rotate1(2,<?php echo $i+1;?>);"><i class="fa fa-redo" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" id="crop_button<?php echo $i+1;?>" onclick="crop1(<?php echo $i+1;?>);" disabled><i class="fa fa-crop" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" onclick="unselect1(<?php echo $i+1;?>);"><i class="fa fa-retweet" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" onclick="callEditor01(<?php echo $i+1;?>);">Save</button>
							</div>
				<?php } ?>
			</td>
            <td class="text-center"><?php echo $applist[$i]['school'] ?></td>
            <td class="text-center"><?php echo $applist[$i]['birthDay'].'/'. $applist[$i]['birthMonth'].'/'. $applist[$i]['birthYear']; ?></td>
			<td class="text-center">
				<?php if(isset($applist[$i]['proof1Name']) && !empty($applist[$i]['proof1Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof1Name']?>" target="_blank">Doc1</a><br>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof2Name']) && !empty($applist[$i]['proof2Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof2Name']?>" target="_blank">Doc2</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof3Name']) && !empty($applist[$i]['proof3Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof3Name']?>" target="_blank">Doc3</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof4Name']) && !empty($applist[$i]['proof4Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof4Name']?>" target="_blank">Doc4</a>
				<?php }else{} ?>
			</td>
            <td class="text-center">
			<?php 
			if($applist[$i]['cardSerialNum'] == null || empty($applist[$i]['cardSerialNum'])){
				if($applist[$i]['payStatus'] === 'paid'){ 
					echo "<p>Paid</p>";
				}else{
					echo "<p>Not Paid</p>";
				} 
			}else{}
			?>
			<?php if($applist[$i]['status'] != 'approved'){ ?>
			<a href="./work/approveApply.php?appNo=<?php echo $applist[$i]['appNo']; ?>&cardType=<?php echo $applist[$i]['cardType']; ?>" role="button" class="btn btn-kyp">Approve</a>			
			<!-- auto serial issuing start -->
			<?php }else if(isset($applist[$i]['cardSerialNum']) && $applist[$i]['cardType'] != 'REVA'){ ?>
			<?php 
				$cardSerialNum = substr($applist[$i]['cardSerialNum'], 0, 1).' '.substr($applist[$i]['cardSerialNum'], 1, 3).' '.substr($applist[$i]['cardSerialNum'], 4, 3).' '.substr($applist[$i]['cardSerialNum'], 7, 3).' '.
				substr($applist[$i]['cardSerialNum'], 10, 3).' '.substr($applist[$i]['cardSerialNum'], 13, 1);
			?>
			<p class="text-center"><?php echo $cardSerialNum; ?></p>
			<?php 
				switch($applist[$i]['cardType']){
					case 'ISIC':
						$cardTypeCode = '7';
						break;
					case 'ITIC':
						$cardTypeCode = '8';
						break;
					case 'IYTC':
						$cardTypeCode = '9';
						break;
					default :
						$cardTypeCode = '7';
						break;
				}
			?>
			<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
			<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="printApply(<?php echo $applist[$i]['appNo']; ?>,1);">Card Print</button></p>
			<!--<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="printApply(<?php //echo $applist[$i]['appNo']; ?>,<?php //echo $cardTypeCode ?>,2);">Label Print</button></p>-->
			<?php }else if($applist[$i]['cardType'] != 'REVA' && $applist[$i]['issueType']==='auto' && $applist[$i]['payStatus'] === 'paid'){ ?>
			<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
			<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
			<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
			<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $issuerName ?>">
			<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
			<!--<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,222)">Auto Issue<i class="fas fa-caret-right"></i></button></p>-->
			<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,222)">Auto Issue<i class="fas fa-caret-right"></i></button></p>
			<!-- auto serial issuing end -->
			<!-- manual serial issuing start -->
			<?php }else if($applist[$i]['cardType'] != 'REVA' && $applist[$i]['issueType']==='manual' && $applist[$i]['payStatus'] === 'paid'){ ?>
			<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
			<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
			<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
			<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $issuerName ?>">
			<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
				<p class="m0 pt10 text-center"><input type="text" class="form-control" maxlength="14" id="cardSerialNum<?php echo $applist[$i]['appNo']; ?>" name="cardSerialNum<?php echo $applist[$i]['appNo']; ?>"></div></p>
				<!--<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,333)">Manual Issue <i class="fas fa-caret-right"></i></button></p>-->
				<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,333)">Manual Issue<i class="fas fa-caret-right"></i></button></p>

			<!-- manual serial issuing end -->
			<!-- manual issue reva start -->
			<?php }else if($applist[$i]['cardType'] === 'REVA' && $applist[$i]['payStatus'] === 'paid'){ ?>
			<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
			<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
			<div class="row">
				<div class="col-xs-7 pr3"><input type="text" class="form-control" maxlength="7" id="revaNum<?php echo $applist[$i]['appNo']; ?>" name="revaNum<?php echo $applist[$i]['appNo']; ?>"></div>
				<!--<div class="col-xs-5 pl3"><button type="button" class="btn btn-kyp btn-block" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,111)">Register <i class="fas fa-caret-right"></i></button></div>-->
				<div class="col-xs-5 pl3"><button type="button" class="btn btn-kyp btn-block" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,111)">Register<i class="fas fa-caret-right"></i></button></div>
			</div>
			<!-- manual issue reva end -->
			<?php }else if (0 > 9){ ?>
			<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
			<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
			<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
			<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $issuerName ?>">
			<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
			<!--<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,111)">No Issue<i class="fas fa-caret-right"></i></button></p>-->
			<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,111)">No Issue<i class="fas fa-caret-right"></i></button></p>
			
			<?php } else{}?>
			</td>
        </tr>
		<?php } ?>
    </table>
    <!-- result End -->
 	<!-- Paging Start -->
	<?php 
		if($rowcount1 === 0){
		}else{
	?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=appFromIo&pagenum=1&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=appFromIo&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=appFromIo&pagenum=$pagingno&searchType=$searchTypeSel\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=appFromIo&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=appFromIo&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
	<?php } ?>
    <!-- pagingEnd -->
    
</div>
</form>
<!-- Conent End -->