<?php 
// 201114 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}

	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}

?>
<script src="./issuer/issuer_promoNew.js?ver=1"></script>
<!-- content start -->

<div id="contents">

<h1>Manage promotion  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Add new promotion</span></h1>

<!-- menu Description start -->
<div id="tip" class="collapse content">
</div>
<!-- menu description End -->

    <!-- search Start -->
    <!-- search End -->
    
    <!-- result Start -->
	<form name="addPromo" id="addPromo" method="POST" enctype="multipart/form-data">
	<h2>Setting</h2>
	<table class="table table-bordered">
		<tr>
			<th class="text-left thGrey">promoActive</th>
			<td>
				NO			
			</td>
			<th class="text-left thGrey">promoType</th>
			<td>
				<select class="form-control" name="promoType" id="promoType">
					<option selected value="single">Single</option>
					<option value="group">Group</option>
				</select>
			</td>	
	    </tr>
		<tr>
			<th class="text-left thGrey w20p">promoGroupName</th>
			<td>
				<input type="text" class="form-control"  name="promoGroupName" id="promoGroupName" maxlength="20" value="">
        <small style="color: #999;">* Alphabets and numeric characters only.</small>
			</td>
			<th class="text-left thGrey w20p">promoGroupId</th>
			<td>
				<input type="text" class="form-control"  name="promoGroupId" id="promoGroupId" maxlength="20" value="">
        <small style="color: #999;">* Alphabets and numeric characters only.</small>
			</td>			
		</tr>
		<tr>
			<th class="text-left thGrey w20p">Discount</th>
			<td>
				<input type="text" class="form-control"  name="promoDiscount" id="promoDiscount" maxlength="20" value="">
        <small style="color: #999;">* Numeric characters only.</small>
			</td>
			<th class="text-left thGrey w20p"></th>
			<td>
			</td>			
		</tr>
	</table>
    <!-- result End -->
	
    <!-- paging Start -->              
    <!-- paging end -->
	<div class="btnDiv">
		<button type="button" class="btn btn-kyp" onclick="submit1(1)">Add</button>
		<button type="button" class="btn btn-kyp" onclick="goBack(1)">Back to list</button>
	</div>
	</form>
</div>

<!-- content End -->

