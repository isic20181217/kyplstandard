<?php 
//200602 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		switch($_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuReports) && $resultUserInfo->sMenuReports === 'yes'  && isset($resultOfficeInfo->menuReports) && $resultOfficeInfo->menuReports ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$officeId = (int)$_SESSION['officeId'];
	$currentDate = date("d/m/Y");
	if(isset($_GET['officeId']) && !empty($_GET['officeId'])){
		$officeIdSel= preg_replace("/<|\/|_|>/","", $_GET['officeId'] );
	}
	else{
		$officeIdSel = 'ALL';
	}
	if(isset($_GET['searchType']) && !empty($_GET['searchType'])){
		$searchTypeSel= preg_replace("/<|\/|_|>/","", $_GET['searchType'] );
	}
	else{
		$searchTypeSel = 'ALL';
	}
	if(isset($_GET['startDate']) && !empty($_GET['startDate'])){
		$startDate= preg_replace("/<|_|>/","",$_GET['startDate'] );
		$startDateSel = explode('/',$startDate);
		if(sizeof($startDateSel) === 3){
		}
		else{
			$startDate = $currentDate;
		}
	}
	else{
		$startDate = $currentDate;

	}
	$startDateSel = explode('/',$startDate);
	$searchYear = $startDateSel[2];
	$searchMonth = $startDateSel[1];
	$searchDay = $startDateSel[0];
	//echo "$searchYear $searchMonth $searchDay <br/>";
	try {
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT appNo,cardType,cardSerialNum,engFname,engLname,school,phone,encId,regDate,issuedDate,status,payStatus,paidDate FROM $tablename07 WHERE 1=1";
		if($officeId === 1){
			if($officeIdSel === 'ALL'){
			} else if ($officeIdSel === '1'){
				$query .= " AND (officeId = :inputValue11 OR officeId = 990)";
			} else {
				$query .= " AND officeId = :inputValue11";
			}
		} else {
			$query .= " AND officeId = :inputValue11";
		}
		if($searchTypeSel === 'ALL'){
		}else{
			$query .= " AND cardType = :inputValue1";
		}
		$query .= " AND year(regDate) = :inputValue2 AND month(regDate) = :inputValue3 AND day(regDate) = :inputValue4";
		$stmt = $db->prepare($query);
		if($officeId === 1){
			if($officeIdSel === 'ALL'){
			}else{
				$stmt->bindParam(':inputValue11', $officeIdSel);
			}
		}else{
			$stmt->bindParam(':inputValue11', $officeId);
		}
		if($searchTypeSel === 'ALL'){
		}else{
			$stmt->bindParam(':inputValue1', $searchTypeSel);
		}
		$stmt->bindParam(':inputValue2', $searchYear);
		$stmt->bindParam(':inputValue3', $searchMonth);
		$stmt->bindParam(':inputValue4', $searchDay);
		$stmt->execute();
		$count1 = 0;
		$issuedCount1 =0;
		//echo "$officeId ";
		while($result = $stmt->fetch(PDO::FETCH_OBJ)){
			$count1++;
			$appList[$count1] = array (
			'appNo' => $result->appNo, 
			'engFname' => decrypt1($result->engFname, $result->encId), 
			'engLname' => decrypt1($result->engLname, $result->encId),
			'cardType' => $result->cardType, 
			'school' => decrypt1($result->school, $result->encId),
			'cardSerialNum' => $result->cardSerialNum, 
			'phone' => decrypt1($result->phone, $result->encId), 
			'regDate' => substr($result->regDate,2), 
			'issuedDate' => substr($result->issuedDate,2),
			'status' => $result->status,
			'payStatus' => $result->payStatus,
			'paidDate' => substr($result->paidDate,2)
			);
			if($result->issuedDate){
				$issuedCount1++;
			}else{}
		}
		//print_r($stmt->errorInfo());
		$queryAppSetting = "SELECT appSettingId, url FROM $tablename23 order by appSettingId";
		$stmtAppSetting = $db->prepare($queryAppSetting);
		$stmtAppSetting->execute();
		$appSettingCount =0;
		if($stmtAppSetting->rowCount() > 0){
			while($result = $stmtAppSetting->fetch(PDO::FETCH_OBJ)){
				$appSettingList[$appSettingCount]= $result;
				$appSettingCount++;
			}
		}else{
			$db= NULL;
		}
		$query = "SELECT officeId,name FROM $tablename12";
		$stmt = $db->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() >= 1){
			$officeCount = $stmt->rowCount();
			$i =0;
			while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
				$officeList[$i] = $result0;
				$i++;
			}
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
?>
<script type="text/javascript" src="./issuer/issuer_daily.js"></script>
<!-- content start -->
<form name="dailyForm" id="dailyForm" method="GET" action="./main_content.php">
<input type="hidden" name="menu" value="dailyAll">
<input type="hidden" name="searchMode" value="searchMode">
<div id="contents">
<h1>Daily application data(<?php echo $issuedCount1.' / '.$count1?>)</h1>

    <!-- search start -->
    <div class="searchDiv">
    	<div>
			<?php 
				if($officeId === 1){ 
			?>
			<label>Office</label>
			<select class="form-control formYoon" name="officeId" id="officeId">
				<option value="ALL">ALL</option>
				<?php 
					for($i=0;$i<$officeCount;$i++){
						$selectThis = '';
						if($officeIdSel === $officeList[$i]->officeId){
							$selectThis ='selected';
						}else{}						
				?>
					<option <?php echo $selectThis; ?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
				<?php 
					}
				?>
			</select>
			<?php } else {} ?>
			<label>Card type</label>
			<select name="searchType" class="form-control formYoon">
				<option value="ALL">ALL</option>
			<?php for($i=0; $i< $appSettingCount; $i++) { 
				$selectThis = '';
				if($searchTypeSel === $appSettingList[$i]->url){
					$selectThis ='selected';
				}else{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $appSettingList[$i]->url; ?>"><?php echo $appSettingList[$i]->url; ?></option>
			<?php } ?>
			</select>
			<label>Reg date</label>
			<input type="text" class="form-control formYoonCa" name="startDate" id="startDate" value="<?php echo $startDate; ?>" placeholder="dd/mm/yyyy" onchange="copyDate1(1)">
			<button type="submit" class="btn btn-kyp"><i class="fab fa-sistrix"></i> Search</button>
        </div>
    </div>
    <!-- search End-->
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey"></th>
			<th class="text-center thGrey">appNo</th>
            <th class="text-center thGrey">CardType</th>
            <th class="text-center thGrey">Name</th>
			<th class="text-center thGrey">Status</th>
            <th class="text-center thGrey">RegDate</th>
			<th class="text-center thGrey">PaidDate</th>
            <th class="text-center thGrey">IssuedDate</th>
        </tr>
		<?php for($i=1;$i<$count1+1;$i++){ ?>
        <tr>
			<td class="text-center"><?php echo $i ?></td>
			<td class="text-center"><?php echo $appList[$i]['appNo']; ?></td>
			<td class="text-center"><?php echo $appList[$i]['cardType']; ?></td>
			<?php if($nameStyle1==='FL'){ ?>
			<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $appList[$i]['appNo']; ?>"><?php echo $appList[$i]['engFname']; ?> / <?php echo $appList[$i]['engLname']; ?></a></td>
			<?php }else if($nameStyle1==='LF'){ ?>
			<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $appList[$i]['appNo']; ?>"><?php echo $appList[$i]['engLname']; ?> / <?php echo $appList[$i]['engFname']; ?></a></td>
			<?php }else{ ?>
			<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $appList[$i]['appNo']; ?>"><?php echo $appList[$i]['engFname']; ?> / <?php echo $appList[$i]['engLname']; ?></a></td>
			<?php } ?>
			<td class="text-center"><?php echo $appList[$i]['status']; ?> / <?php echo $appList[$i]['payStatus']; ?></td>
			<td class="text-center"><?php echo $appList[$i]['regDate']; ?></td>
			<td class="text-center"><?php echo $appList[$i]['paidDate']; ?></td>
			<td class="text-center"><?php echo $appList[$i]['issuedDate']; ?></td>
        </tr>
		<?php } ?>
    </table>
    <!-- result end -->
    </div>
</form>
<!-- content end -->