<?php 
// 200818 add date search
// 200604 check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($_SESSION['user_type'])
		{
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	$searchValue1 = '';
	$searchValue2 = '';
	$searchValue3 = '';
	$searchValue4 = '';
	$searchValue5 = '';
	$searchValue6 = '';
	$searchValue123 ='';
	$searchCardUrl = '';
	$startDate ='';
	$endDate ='';
	if (isset($_GET['born']) && !empty($_GET['born']))
	{
		$search1 = 'birthDay';
		$search2 = 'birthMonth';
		$search3 = 'birthYear';
		$born = explode('/',$_GET['born']);
		$searchValue1 = $born[0];
		$searchValue2 = $born[1];
		$searchValue3 = $born[2];
		$searchValue123 = $searchValue1.'/'.$searchValue2.'/'.$searchValue3;
		$searchMode = $_GET['searchMode'];
	}
	else
	{}
	if (isset($_GET['engFname']) && !empty($_GET['engFname']))
	{
		$search4 = 'engFname';
		$searchValue4= preg_replace("/<|\/|_|>/","",$_GET['engFname'] );
		$searchMode = $_GET['searchMode'];
		
	}
	else
	{}
	if (isset($_GET['engLname']) && !empty($_GET['engLname']))
	{
		$search5 = 'engLname';
		$searchValue5= preg_replace("/<|\/|_|>/","",$_GET['engLname'] );
		$searchMode = $_GET['searchMode'];
		
	}
	else
	{}
	if (isset($_GET['email']) && !empty($_GET['email']))
	{
		$search6 = 'email';
		$searchValue6= preg_replace("/<|\/|_|>/","",$_GET['email'] );
		$searchMode = $_GET['searchMode'];
	}
	else
	{}
	if (isset($_GET['searchCardUrl']) && !empty($_GET['searchCardUrl']))
	{
		$searchCardUrl= preg_replace("/<|\/|_|>/","",$_GET['searchCardUrl'] );
	}
	else
	{}
	$searchType = array("ASC","DESC");
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC')
	{
		$searchTypeSel = 'ASC';
	}
	else
	{
		$searchTypeSel = 'DESC';
	}
	$searchByDate = 'no';
	if(isset($_GET['startDate']) && !empty($_GET['startDate']))
	{
		$startDate= preg_replace("/<|_|>/","",$_GET['startDate'] );
		$startDateSel = explode('/',$startDate);
		if(sizeof($startDateSel) === 3)
		{
			$startDateSel = explode('/',$startDate);
			$searchYear = $startDateSel[2];
			$searchMonth = $startDateSel[1];
			$searchDay = $startDateSel[0];
			$searchByDate ='yes';
			$searchMode = $_GET['searchMode'];
			if(isset($_GET['endDate']) && !empty($_GET['endDate']))
			{
				$endDate= preg_replace("/<|_|>/","",$_GET['endDate'] );
				$endDateSel = explode('/',$endDate);
				if(sizeof($endDateSel) === 3)
				{
				}
				else
				{
					$endDate = $stardDate;
				}
			}
			else
			{
				$endDate = $startDate;
			}
			$endDateSel = explode('/',$endDate);
			$searchYear2 = $endDateSel[2];
			$searchMonth2 = $endDateSel[1];
			$searchDay2 = $endDateSel[0];
			$searchStart = $searchYear.'-'.$searchMonth.'-'.$searchDay;
			$searchEnd = $searchYear2.'-'.$searchMonth2.'-'.$searchDay2;
			
		}
		else
		{
			//$startDate = $currentDate;
		}
	}
	else
	{
		//$startDate = $currentDate;
	}

	$loadAppOnce = 100; // how many applications read from DB at once
	if (isset($searchMode) && $searchMode === 'searchMode')
	{
		try 
		{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
			if(isset($searchCardUrl) && !empty($searchCardUrl) )
			{
				$query = "SELECT appSettingId,url FROM $tablename23 WHERE url = :url";
				$stmt = $db->prepare($query);
				$stmt->bindParam(':url', $searchCardUrl);
				$stmt->execute();
				if($stmt->rowCount() === 1)
				{
					$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);
				}
				else
				{
					$db= NULL;
					echo '<script>alert(\'Wrong Card Url\');</script>';
					echo "<script> window.history.go(-1); </script>";
					exit;
				}
			}
			else
			{}
			$query = "SELECT count(appNo) as total FROM $tablename07 WHERE 1=1";
			if(isset($searchCardUrl) && !empty($searchCardUrl) )
			{
				$query .= " AND appSettingIdApp = :appSettingIdApp";
			}
			else
			{}
			if ($_SESSION['officeId'] === '1')
			{
			}
			else
			{
				$query .= " AND (officeId = :officeId)";
			}
			$stmt = $db->prepare($query);
			if(isset($searchCardUrl) && !empty($searchCardUrl) )
			{
				$stmt->bindParam(':appSettingIdApp', $appSettingInfo->appSettingId);
			}
			else
			{}
			if ($_SESSION['officeId'] === '1')
			{
			}
			else
			{
				$stmt->bindParam(':officeId', $_SESSION['officeId']);
			}
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$total = $result->total;
			$appReader = ceil($total/$loadAppOnce);
			$appStart = 0;
			$appCount = 0;
			for($i=0; $i<$appReader; $i++)
			{
				if($appCount > 500){ //show 500 app once
					echo '<script>alert(\'Max Result Limit 500.\');</script>';
					break;
				}
				else 
				{}
				$appStart = $i*$loadAppOnce;
				$appEnd = ($i+1)*$loadAppOnce;		
				if($appStart > 0)
				{
					$appStart = $appStart - 1;
				}
				else
				{}
				$query = "SELECT appNo,birthDay,birthMonth,birthYear,engFname,engLname,email,customerId,encId FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE 1=1";
				if(isset($searchCardUrl) && !empty($searchCardUrl) )
				{
					$query .= " AND appSettingIdApp = :appSettingIdApp";
				}
				else
				{}
				if ($_SESSION['officeId'] === '1')
				{
				}
				else
				{
					$query .= " AND (officeId = :officeId)";
				}
				if ($searchByDate === 'yes')
				{
					$query .= " AND DATE(regDate) BETWEEN :searchStart AND :searchEnd";
				}
				else 
				{
				}
				$query .=" ORDER BY $tablename07.appNo $searchTypeSel LIMIT $appStart, $loadAppOnce";
				$stmt = $db->prepare($query);
				if(isset($searchCardUrl) && !empty($searchCardUrl) )
				{
					$stmt->bindParam(':appSettingIdApp', $appSettingInfo->appSettingId);
				}
				else
				{}
				if ($_SESSION['officeId'] === '1')
				{
				}
				else
				{
					$stmt->bindParam(':officeId', $_SESSION['officeId']);
				}
				if ($searchByDate === 'yes')
				{
					$stmt->bindParam(':searchStart', $searchStart);
					$stmt->bindParam(':searchEnd', $searchEnd);
				}
				else 
				{
				}
				$stmt->execute();
				//echo $stmt->rowCount();
				while($result = $stmt->fetch(PDO::FETCH_OBJ))
				{				
					$isRight = 'no';
					if ($searchByDate === 'yes')
					{
						$isRight ='yes';
					}
					else 
					{}
					if (isset($search1)){
						if(decrypt1($result->birthDay, $result->encId) === $searchValue1 && decrypt1($result->birthMonth, $result->encId) === $searchValue2 && decrypt1($result->birthYear, $result->encId) ===$searchValue3)
						{
							$isRight ='yes';
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}
					if (isset($search4))
					{
						if(strtolower(decrypt1($result->engFname, $result->encId)) === strtolower($searchValue4))
						{
							$isRight ='yes';
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}
					if (isset($search5))
					{
						if(strtolower(decrypt1($result->engLname, $result->encId)) === strtolower($searchValue5))
						{
							$isRight ='yes';
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}
					if (isset($search6))
					{
						if(strtolower(decrypt1($result->email, $result->encId)) === strtolower($searchValue6))
						{
							$isRight ='yes';
						}
						else
						{
							$isRight ='no';
						}
					}
					if($isRight === 'yes')
					{
						$query2 = "SELECT * FROM $tablename07 WHERE appNo = $result->appNo;";
						$stmt2 = $db->prepare($query2);
						if ($_SESSION['officeId'] === '1')
						{
						}
						else
						{
							$query2 .= " AND (officeId = :officeId)";
						}
						$stmt2->bindParam(':officeId', $_SESSION['officeId']);
						$stmt2->execute();
						$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
						$applist[$appCount] = array (
						'appNo' => $result2->appNo, 
						'engFname' => decrypt1($result2->engFname, $result2->encId),
						'engLname' => decrypt1($result2->engLname, $result2->encId), 
						'photoName' => $result2->photoName, 
						'cardType' => $result2->cardType,
						'issuedDate' => $result2->issuedDate, 
						'printedDate' => $result2->printedDate,
						'school' => decrypt1($result2->school, $result2->encId), 
						'birthYear' => decrypt1($result2->birthYear, $result2->encId), 
						'birthMonth' => decrypt1($result2->birthMonth, $result2->encId), 
						'birthDay' => decrypt1($result2->birthDay, $result2->encId), 
						'name1' => decrypt1($result2->name1, $result2->encId), 
						'officeId' => $result2->officeId, 
						'status' => $result2->status, 
						'regDate' => $result2->regDate, 
						'address' => decrypt1($result2->address, $result2->encId), 
						'city' => decrypt1($result2->city, $result2->encId), 
						'postal' => decrypt1($result2->postal, $result2->encId));															 
						$appCount = $appCount + 1;
					}
					else
					{}
				}
			}
			$db= NULL;
		}
		catch (PDOExeception $e)
		{
			//echo "Error: ".$e->getMessage();
			$db= NULL;
			exit;
		}
	}
	else
	{}
	function read_img($index,$name,$ext)
	{
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}	
?>
<?php $today2 = date("d \/ m \/ Y"); ?>
<script type="text/javascript" src="./issuer/issuer_search.js"></script>
<!-- content start -->
<div id="contents">
<h1 class="pb-20">Search
<?php if (isset($searchMode) && $searchMode === 'searchMode'){ ?>
(<?php echo $appCount.' Applications'; ?>)
<?php }else{}?>
	<i class="far fa-question-circle qButton" data-toggle="collapse" data-target="#tip"></i><!-- Click question mark button : Show menu description -->
</h1>
	
<!-- menu description start -->
<div id="tip" class="collapse content">
	<p>On this menu, you can search for the applications you want.<br>
You can also see and edit the details of the applications.</p>
	<ol class="m-0">
		<li>To search applications, select and/or enter the scope of your search and click the “Search” button.</li>
		<li>To see and edit the details of an application, click the name of the application you want from the list.</li>
	</ol>
</div>
<!-- menu description end -->

    <!-- search start -->
	<form name="searchInSearch" id="searchInSearch" action="./main_content.php?menu=search" method="GET">
	<input type="hidden" name="menu" id="menu=" value="search">
	<div class="searchDiv">
		<label>Order</label>
			<select name="searchType" id="searchType" class="form-control formYoonCa">
			<?php 
				for($i=0; $i< count($searchType); $i++) 
				{ 
					$selectThis = '';
					if($searchTypeSel === $searchType[$i])
					{
						$selectThis ='selected';
					}
					else
					{}
			?>
				<option <?php echo $selectThis; ?> value="<?php echo $searchType[$i]; ?>"><?php echo $searchType[$i]; ?></option>
			<?php 
				} 
			?>
		</select>
		<label class="ml20">CardType &nbsp;&nbsp;</label>
		<input type="text" class="form-control formYoonCa input-sm" placeholder="URL" name="searchCardUrl" value="<?php echo $searchCardUrl; ?>" >
		<label class="ml20">Reg date</label>
			<input type="text" class="form-control formYoonCa input-sm" name="startDate" id="startDate" value="<?php echo $startDate; ?>" placeholder="dd/mm/yyyy">
			~ <input type="text" class="form-control formYoonCa input-sm" name="endDate" id="endDate" value="<?php echo $endDate; ?>" placeholder="dd/mm/yyyy">
		<br/>				
		<label>Born &nbsp;</label>
		<input type="text" class="form-control formYoonCa input-sm" placeholder="dd/mm/yyyy" name="born" value="<?php echo $searchValue123; ?>" > <!--<i class="far fa-calendar-alt fa-lg"></i>-->
		<label class="ml20">First name</label>
		<input type="text" class="form-control formYoonCa input-sm" name="engFname" value="<?php echo $searchValue4; ?>" >
		<label class="ml20">Last name</label>
		<input type="text" class="form-control formYoonCa input-sm" name="engLname" value="<?php echo $searchValue5; ?>" >
		<label class="ml20">Email</label>
		<input type="text" class="form-control formYoonCa input-sm" name="email" value="<?php echo $searchValue6; ?>" >
		<input type="hidden" name="searchMode" value="searchMode">
		<?php $pagenum = 1; ?>
		<input type="hidden" id="pagenum" name="pagenum" value="<?php echo $pagenum ?>">
		<button class="btn btn-kyp btn-sm ml20"><i class="fab fa-sistrix"></i> Search</button> (Max Result Limit 500)
	</div>
	</form>
    <!-- search end -->
    
    <!-- result start -->
    <table class="table table-bordered">
        <tr>
            <!--<th class="text-center thGrey"></th>-->
            <th class="text-center thGrey">Card type</th>
            <th class="text-center thGrey">Name</th>
            <th class="text-center thGrey w20p">Studies/Teaches at</th>
            <th class="text-center thGrey">Born</th>
            <th class="text-center thGrey">Photo</th>
            <th class="text-center thGrey">Address</th>
            <th class="text-center thGrey">Status</th>
            <th class="text-center thGrey">Applied date</th>
        </tr>
		<?php 
			if (isset($searchMode) && $searchMode === 'searchMode')
			{ 
		?>
			<?php 
				for ($i = 0; $i < $appCount; $i++)
				{ 
			?>
			<tr>
				<!--<td class="text-center"><input type="checkbox"></td>-->
				<td class="text-center"><?php echo $applist[$i]['cardType'] ?></td>

				<td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $applist[$i]['appNo']; ?>"><?php echo $applist[$i]['name1'] ?><br /><?php echo $applist[$i]['engFname'].' '.$applist[$i]['engLname']; ?></a></td>
				<td class="text-center"><?php echo $applist[$i]['school'] ?></td>
				<td class="text-center"><?php echo $applist[$i]['birthDay'].'/'. $applist[$i]['birthMonth'].'/'. $applist[$i]['birthYear']; ?></td>
				<td class="text-center">
				<?php 
					if(isset($applist[$i]['photoName'])) 
					{
				?>
					<?php $currentPhotoName = pathinfo($applist[$i]['photoName']); ?>
					<img height="150" src="<?php echo read_img('1',$currentPhotoName['filename'],$currentPhotoName['extension']);?>" class="printListPhoto">
				<?php 
					}
					else
					{} 
				?>
				</td>
				<td class="text-left"><?php echo $applist[$i]['address'].'<br/ >'.$applist[$i]['city'].'/'.$applist[$i]['postal']; ?></td>
				<td class="text-center">
				<?php echo $applist[$i]['status']; ?><br/>
				<?php if(isset($applist[$i]['issuedDate'])){echo 'issued';}?><br/>
				<?php if(isset($applist[$i]['printedDate'])){echo 'printed';}?><br/>
				</td>
				<td class="text-center"><?php echo $applist[$i]['regDate']; ?></td>
			</tr>
			<?php 
				} 
			?>
		<?php 
			}
			else
			{} 
		?>
    </table>
    <!-- result end -->
    
    <!--<div class="btnDiv"><button type="button" class="btn btn-kyp"><i class="far fa-file-excel"></i> Excel file download <i class="fas fa-download"></i></button></div>-->
</div>
<!-- content end -->