<?php 
// 210630 add img reader appNo for docuDir
// 201114 add
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type']))
	{
		switch($_SESSION['user_type'])
		{
			case 'Manager';
			case 'Issuer';
				break;
			default:
				echo '<script>alert(\'You are not issuer.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else
	{
		echo '<script>alert(\'Please login.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuIssuing) && $resultUserInfo->sMenuIssuing === 'yes'  && isset($resultOfficeInfo->menuIssuing) && $resultOfficeInfo->menuIssuing ==='yes' )
	{} 
	else 
	{
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sCanUrl) && $resultUserInfo->sCanUrl === 'yes'  && isset($resultOfficeInfo->canUrl) && $resultOfficeInfo->canUrl ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanCsv) && $resultUserInfo->sCanCsv === 'yes'  && isset($resultOfficeInfo->canCsv) && $resultOfficeInfo->canCsv ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanValidate) && $resultUserInfo->sCanValidate === 'yes'  && isset($resultOfficeInfo->canValidate) && $resultOfficeInfo->canValidate ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' )
	{} 
	else if (isset($resultUserInfo->sCanPrintLabel) && $resultUserInfo->sCanPrintLabel === 'yes'  && isset($resultOfficeInfo->canPrintLabel) && $resultOfficeInfo->canPrintLabel ==='yes' )
	{} 
	else 
	{
		echo '<script>alert(\'Access Denied1111\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}						   
	$searchType = array("ASC","DESC");
	if(isset($_GET['searchType']) && $_GET['searchType'] === 'ASC')
	{
		$searchTypeSel = 'ASC';
	}
	else{
		$searchTypeSel = 'DESC';
	}
	$now1 = time();
	$rowcount1 =0;
	$totalCount =0;
	$currentDate = date("d/m/Y",$now1);
	$searchByCardType ='no';
	$searchCardType ='all';
	if(isset($_GET['cardType']))
	{
		switch($_GET['cardType'])
		{
			case 'ISIC':
				$searchCardType ='ISIC';
				$searchByCardType ='yes';
				break;
			case 'ITIC':
				$searchCardType ='ITIC';
				$searchByCardType ='yes';
				break;
			case 'IYTC':
				$searchCardType ='ISIC';
				$searchByCardType ='yes';
				break;
			default:
				break;
				
		}
	}
	else
	{}	
	$searchByBorn = 'no';
	$born = '';
	if(isset($_GET['born']) && !empty($_GET['born'])){
		$born= preg_replace("/<|_|>/","",$_GET['born'] );
		$bornSel = explode('/',$born);
		if(sizeof($bornSel) === 3)
		{
			$bornYear = $bornSel[2];
			$bornMonth = $bornSel[1];
			$bornDay = $bornSel[0];
			$searchByBorn ='yes';
		}
		else
		{}
	}
	else
	{}
	$searchByFname ='no';
	if (isset($_GET['fname']) && !empty($_GET['fname']))
	{
		$searchFname= preg_replace("/<|\/|_|>/","",$_GET['fname'] );
		$searchByFname ='yes';
		
	}
	else
	{
		$searchFname ='';
	}
	$searchByLname ='no';
	if (isset($_GET['lname']) && !empty($_GET['lname']))
	{
		$searchLname= preg_replace("/<|\/|_|>/","",$_GET['lname'] );
		$searchByLname ='yes';
	}
	else
	{
		$searchLname ='';
	}
	$searchByAppSettingId ='no';
	$searchAppSettingId='all';
	if (isset($_GET['appSettingId']) && !empty($_GET['appSettingId']))
	{	
		$searchAppSettingId = preg_replace("/<|\/|_|>/","",$_GET['appSettingId'] );
		if($searchAppSettingId === 'all')
		{}
		else
		{
			$searchByAppSettingId ='yes';
		}
		
	}
	else
	{}	
	$searchByDate = 'no';
	$startDate ='';
	$endDate ='';
	if(isset($_GET['startDate']) && !empty($_GET['startDate']))
	{
		$startDate= preg_replace("/<|_|>/","",$_GET['startDate'] );
		$startDateSel = explode('/',$startDate);
		if(sizeof($startDateSel) === 3)
		{
			$searchYear = $startDateSel[2];
			$searchMonth = $startDateSel[1];
			$searchDay = $startDateSel[0];
			if(isset($_GET['endDate']) && !empty($_GET['endDate'])){
				$endDate= preg_replace("/<|_|>/","",$_GET['endDate'] );
				$endDateSel = explode('/',$endDate);
				if(sizeof($endDateSel) === 3)
				{
					$searchYear2 = $endDateSel[2];
					$searchMonth2 = $endDateSel[1];
					$searchDay2 = $endDateSel[0];
					$searchStart = $searchYear.'-'.$searchMonth.'-'.$searchDay;
					$searchEnd = $searchYear2.'-'.$searchMonth2.'-'.$searchDay2;
					$searchByDate ='yes';
				}
				else{}
			}
			else{}
		}
		else
		{}
	}
	else
	{}
	try 
	{
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		// get app setting list
		$queryAppSetting = "SELECT appSettingId, url FROM $tablename23 ORDER BY appSettingId";
		$stmtAppSetting = $db->prepare($queryAppSetting);
		$stmtAppSetting->execute();
		$appSettingCount =0;
		if($stmtAppSetting->rowCount() > 0)
		{
			while($result = $stmtAppSetting->fetch(PDO::FETCH_OBJ))
			{
				$appSettingList[$appSettingCount]= $result;
				$appSettingCount++;
			}
		}
		else
		{
			//$db= NULL;
		}
		if($searchByBorn === 'yes' || $searchByFname === 'yes'|| $searchByLname === 'yes')
		{
			// count application list
			$query = "SELECT appNo FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL) AND (hidden1 IS NOT NULL) ";
			if ($_SESSION['officeId'] === '1')
			{
				// Main office can search every application by name born search
				//$query .= " AND ((officeId = 1) OR (officeId = 990))";
			}
			else
			{
				$query .= " AND (officeId = :searchValue1)";
			}
			if($searchByCardType === 'yes')
			{
				$query .= " AND (serialTypeApp = :serialTypeApp)";
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$query .= " AND (appSettingIdApp = :appSettingIdApp)";
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$query .= " AND DATE(regDate) BETWEEN :searchStart AND :searchEnd";
			}
			else
			{}
			$stmt = $db->prepare($query);
			if ($_SESSION['officeId'] === '1'){
			}else{
				$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
			}
			if($searchByCardType === 'yes')
			{
				$stmt->bindParam(':serialTypeApp', $searchCardType);
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$stmt->bindParam(':appSettingIdApp', $searchAppSettingId);
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$stmt->bindParam(':searchStart', $searchStart);
				$stmt->bindParam(':searchEnd', $searchEnd);
			}
			else
			{}
			//echo $query;
			$stmt->execute();
			$maxsize = $stmt->rowCount();
			//echo $maxsize;
			$loadAppOnce = 100; // how many applications read from DB at once
			$total = $maxsize;
			$appReader = ceil($total/$loadAppOnce);
			$appStart = 0;
			$appCount = 0;
			for($i=0; $i<$appReader; $i++)
			{
				if($appCount > 500){ //show 500 app once
					echo '<script>alert(\'Max Result Limit 500.\');</script>';
					break;
				}
				else 
				{}
				$appStart = $i*$loadAppOnce;
				$appEnd = ($i+1)*$loadAppOnce;		
				if($appStart > 0)
				{
					$appStart = $appStart - 1;
				}
				else
				{}
				$query = "SELECT appNo, birthDay, birthMonth, birthYear, engFname, engLname, encId FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL) AND (hidden1 IS NOT NULL) ";
				if ($_SESSION['officeId'] === '1')
				{	
					// Main office can search every application by name born search
					// $query .= " AND ((officeId = 1) OR (officeId = 990))";
				}
				else
				{
					$query .= " AND (officeId = :searchValue1)";
				}
				if($searchByCardType === 'yes')
				{
					$query .= " AND (serialTypeApp = :serialTypeApp)";
				}
				else
				{}
				if($searchByAppSettingId === 'yes')
				{
					$query .= " AND (appSettingIdApp = :appSettingIdApp)";
				}
				else
				{}
				if($searchByDate === 'yes')
				{
					$query .= " AND DATE(regDate) BETWEEN :searchStart AND :searchEnd";
				}
				else
				{}
				$query .=" ORDER BY $tablename07.appNo LIMIT $appStart, $loadAppOnce";
				$stmt = $db->prepare($query);
				//echo $query;
				if ($_SESSION['officeId'] === '1'){
				}else{
					$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
				}
				if($searchByCardType === 'yes')
				{
					$stmt->bindParam(':serialTypeApp', $searchCardType);
				}
				else
				{}
				if($searchByAppSettingId === 'yes')
				{
					$stmt->bindParam(':appSettingIdApp', $searchAppSettingId);
				}
				else
				{}
				if($searchByDate === 'yes')
				{
					$stmt->bindParam(':searchStart', $searchStart);
					$stmt->bindParam(':searchEnd', $searchEnd);
				}
				else
				{}
				$stmt->execute();
				//echo $stmt->rowCount();
				while($result = $stmt->fetch(PDO::FETCH_OBJ))
				{				
					$isRight = 'yes';
					/*
					if ($searchByDate === 'yes')
					{
						$isRight ='yes';
					}
					else 
					{}
					*/
					if ($searchByBorn === 'yes'){
						if(decrypt1($result->birthDay, $result->encId) === $bornDay && decrypt1($result->birthMonth, $result->encId) === $bornMonth && decrypt1($result->birthYear, $result->encId) ===$bornYear)
						{
							$isRight ='yes';
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}
					if ($searchByFname === 'yes')
					{
						if(strtolower(decrypt1($result->engFname, $result->encId)) === strtolower($searchFname))
						{
							if($isRight ==='no')
							{}
							else{
								$isRight ='yes';
							}
							
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}
					if ($searchByLname === 'yes')
					{
						if(strtolower(decrypt1($result->engLname, $result->encId)) === strtolower($searchLname))
						{
							if($isRight ==='no')
							{}
							else{
								$isRight ='yes';
							}
						}
						else
						{
							$isRight ='no';
						}
					}
					else
					{}

					if($isRight === 'yes')
					{
						$query2 = "SELECT * FROM $tablename07 WHERE appNo = $result->appNo;";
						$stmt2 = $db->prepare($query2);
						if ($_SESSION['officeId'] === '1')
						{
							// Main office can search every application by name born search
							// $query2 .= " AND ((officeId = 1) OR (officeId = 990))";
						}
						else
						{
							$query2 .= " AND (officeId = :officeId)";
						}
						$stmt2->bindParam(':officeId', $_SESSION['officeId']);
						$stmt2->execute();
						$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
						$applist[$appCount] = array (
						'appNo' => $result2->appNo, 
						'engFname' => decrypt1($result2->engFname, $result2->encId), 
						'engLname' => decrypt1($result2->engLname, $result2->encId), 
						'photoName' => $result2->photoName, 
						'cardType' => $result2->cardType,
						'appSettingIdApp' =>$result2->appSettingIdApp,
						'school' => decrypt1($result2->school, $result2->encId), 
						'birthYear' => decrypt1($result2->birthYear, $result2->encId), 
						'birthMonth' => decrypt1($result2->birthMonth, $result2->encId), 
						'birthDay' => decrypt1($result2->birthDay, $result2->encId), 
						'name1' => decrypt1($result2->name1, $result2->encId), 
						'officeId' => $result2->officeId,
						'proof1Name' => $result2->proof1Name, 
						'proof2Name' => $result2->proof2Name, 
						'proof3Name' => $result2->proof3Name, 
						'proof4Name' => $result2->proof4Name, 
						'payStatus' =>$result2->payStatus, 
						'status' => $result2->status, 
						'regDate' => $result2->regDate, 
						'address' => decrypt1($result2->address, $result2->encId), 
						'city' => decrypt1($result2->city, $result2->encId), 
						'postal' => decrypt1($result2->postal, $result2->encId), 
						'cardSerialNum' => $result2->cardSerialNum);
						$queryAppSetting = "SELECT issueType,issueType2 FROM $tablename23 WHERE appSettingId =:appSettingId";
						$stmtAppSetting = $db->prepare($queryAppSetting);
						$stmtAppSetting->bindParam(':appSettingId', $applist[$appCount]['appSettingIdApp']);
						$stmtAppSetting->execute();
						if($stmtAppSetting->rowCount() === 1)
						{
							$appSettingInfo = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
							$applist[$appCount]['issueType'] = $appSettingInfo->issueType;
							$applist[$appCount]['issueType2'] = $appSettingInfo->issueType2;
				/*
							echo '<pre>';
							print_r($result);
							echo '</pre>';
					*/	
						//echo decrypt1($result->birthDay, $result->encId);
						}
						else
						{
							echo 'No AppSettingInfo : '.$result2->cardType;
							$db= NULL;
							exit;
						}						
						$appCount = $appCount + 1;
						$rowcount1 = $appCount;
						$totalCount = $rowcount1;
					}
					else
					{}
				}
			}
		}
		else
		{		
			// count application list
			$query = "SELECT appNo FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL) AND (hidden1 IS NOT NULL)";
			if ($_SESSION['officeId'] === '1')
			{
				$query .= " AND ((officeId = 1) OR (officeId = 990))";
			}
			else
			{
				$query .= " AND (officeId = :searchValue1)";
			}
			if($searchByCardType === 'yes')
			{
				$query .= " AND (serialTypeApp = :serialTypeApp)";
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$query .= " AND (appSettingIdApp = :appSettingIdApp)";
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$query .= " AND DATE(regDate) BETWEEN :searchStart AND :searchEnd";
			}
			else
			{}
			$stmt = $db->prepare($query);
			if ($_SESSION['officeId'] === '1'){
			}else{
				$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
			}
			if($searchByCardType === 'yes')
			{
				$stmt->bindParam(':serialTypeApp', $searchCardType);
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$stmt->bindParam(':appSettingIdApp', $searchAppSettingId);
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$stmt->bindParam(':searchStart', $searchStart);
				$stmt->bindParam(':searchEnd', $searchEnd);
			}
			else
			{}
			//echo $query;
			$stmt->execute();
			$howmany = 20; // show how many applications for once
			$howmanypage = 10; // show how many pages for once
			$maxsize = $stmt->rowCount();
			$totalCount = $maxsize;
			$maxpage = floor($maxsize / $howmany); //total application 
			//echo ' '.$maxsize.' '.$maxpage.' ';
			if(isset($_GET['pagenum']) && $_GET['pagenum'] > 1)
			{
				$pagenum = preg_replace("/<|\/|_|>/","",$_GET['pagenum']); // current pagenumber
			}
			else
			{
				$pagenum = 1; // current pagenumber
			}
			$page = ($pagenum-1)*$howmany;
			if($maxsize % $howmany > 0)
			{
				$maxpage = $maxpage + 1; //if the number of applications is 110 and the number of applications for one page is 10, show application 101~110 on page 11
			}
			else
			{}
			if ($pagenum > $maxpage){ 
				$pagenum = $maxpage;
			}
			else
			{}
			$maxgroup = floor($maxsize / ($howmany*$howmanypage)); // show how many pagegroup for once
			if($maxsize % ($howmany*$howmanypage) > 0)
			{
				$maxgroup = $maxgroup + 1;  // to showing 5 pages for once, page 11~14 needs third group. 
			}
			else{}
			$currentgroup = floor($pagenum / $howmanypage); //current page's group
			if($pagenum % $howmanypage > 0)
			{
				$currentgroup = $currentgroup + 1; // to showing 5 pages for once, page 11~14 needs third group.
				$currentgrouplocation = $pagenum%$howmanypage; //  when showing 5 pages for once, page 9 is forth in current group.
			}
			else
			{
				$currentgrouplocation = $howmanypage; // when showing 5 pages for once, page 10 is fifth in current group.
			}
			$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo WHERE (issuedDate IS NULL) AND (revaDate IS NULL) AND (hidden1 IS NOT NULL)";
			if ($_SESSION['officeId'] === '1')
			{
				$query .= " AND ((officeId = 1) OR (officeId = 990))";
			}else{
				$query .= " AND (officeId = :searchValue1)";
			}
			if($searchByCardType === 'yes')
			{
				$query .= " AND (serialTypeApp = :serialTypeApp)";
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$query .= " AND (appSettingIdApp = :appSettingIdApp)";
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$query .= " AND DATE(regDate) BETWEEN :searchStart AND :searchEnd";
			}
			else
			{}
			$query .= " ORDER BY $tablename07.appNo $searchTypeSel LIMIT :page, :howmany";
			$stmt = $db->prepare($query);
			if ($_SESSION['officeId'] === '1'){
			}else{
				$stmt->bindParam(':searchValue1', $_SESSION['officeId']);
			}
			if($searchByCardType === 'yes')
			{
				$stmt->bindParam(':serialTypeApp', $searchCardType);
			}
			else
			{}
			if($searchByAppSettingId === 'yes')
			{
				$stmt->bindParam(':appSettingIdApp', $searchAppSettingId);
			}
			else
			{}
			if($searchByDate === 'yes')
			{
				$stmt->bindParam(':searchStart', $searchStart);
				$stmt->bindParam(':searchEnd', $searchEnd);
			}
			else
			{}
			$stmt->bindParam(':page', $page, PDO::PARAM_INT);
			$stmt->bindParam(':howmany', $howmany, PDO::PARAM_INT);
			$stmt->execute();
			$rowcount1 = $stmt->rowCount();
			$count1 = 0;
			while($result = $stmt->fetch(PDO::FETCH_OBJ))
			{
				$applist[$count1] = array (
				'appNo' => $result->appNo, 
				'engFname' => decrypt1($result->engFname, $result->encId), 
				'engLname' => decrypt1($result->engLname, $result->encId), 
				'photoName' => $result->photoName, 
				'cardType' => $result->cardType,
				'appSettingIdApp' =>$result->appSettingIdApp,
				'school' => decrypt1($result->school, $result->encId), 
				'birthYear' => decrypt1($result->birthYear, $result->encId), 
				'birthMonth' => decrypt1($result->birthMonth, $result->encId), 
				'birthDay' => decrypt1($result->birthDay, $result->encId), 
				'name1' => decrypt1($result->name1, $result->encId), 
				'officeId' => $result->officeId,
				'proof1Name' => $result->proof1Name, 
				'proof2Name' => $result->proof2Name, 
				'proof3Name' => $result->proof3Name, 
				'proof4Name' => $result->proof4Name, 
				'payStatus' =>$result->payStatus, 
				'status' => $result->status, 
				'regDate' => $result->regDate, 
				'address' => decrypt1($result->address, $result->encId), 
				'city' => decrypt1($result->city, $result->encId), 
				'postal' => decrypt1($result->postal, $result->encId), 
				'cardSerialNum' => $result->cardSerialNum, 
				'revaNum' => $result->revaNum, 
				'oriSerialNum' => $result->oriSerialNum,
				'docuDir' => $result->docuDir);
				$queryAppSetting = "SELECT issueType,issueType2 FROM $tablename23 WHERE appSettingId =:appSettingId";
				$stmtAppSetting = $db->prepare($queryAppSetting);
				$stmtAppSetting->bindParam(':appSettingId', $applist[$count1]['appSettingIdApp']);
				$stmtAppSetting->execute();
				if($stmtAppSetting->rowCount() === 1)
				{
					$appSettingInfo = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
					$applist[$count1]['issueType'] = $appSettingInfo->issueType;
					$applist[$count1]['issueType2'] = $appSettingInfo->issueType2;
		/*
					echo '<pre>';
					print_r($result);
					echo '</pre>';
		*/			
				}
				else
				{
					echo 'No AppSettingInfo : '.$result->cardType;
					$db= NULL;
					exit;
				}
				$count1 = $count1 +1;
			}
		}
		$db= NULL;
	}
	catch (PDOExeception $e)
	{
		echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	function read_img($index,$name,$ext,$appNo)
	{
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index."&appNo=".$appNo;
		return $src;
	}
?>
<script src="./issuer/imgedit_2.js"></script>
<script src="./issuer/issuer_bulkList.js"></script>
<!-- Content Start -->

<div id="contents">
	
	<h1>Bulk uploaded applications 
		<i class="fas fa-angle-double-right"></i> 
		<span class="h1Sub">List (<?php echo $totalCount.' Applications'; ?>)
			<span class="pl-10 small">
				｜<a href="./main_content.php?menu=appFileUpload" role="link">New applications bulk upload <i class="fa fa-caret-right"></i></a>
			</span> 
		</span>
	</h1>

    <!-- search Start -->
	<form method="get" name="IssuingForm" id="IssuingForm">
		<input type="hidden" name="formName" value="issuingOne">				   
		<input type="hidden" id="maxnum" value="<?php echo $rowcount1; ?>">
		<div class="searchDiv">
      
      <table>
        <tr>
          <td width="140"><label>Card type</label></td>
          <td>
						<?php 
						$selection1 = '';
						$selection2 = '';
						$selection3 = '';
						$selection4 = '';
						if($searchCardType=== 'ISIC')
						{
						$selection2 = 'selected';
						} 
						else if ($searchCardType=== 'ITIC') 
						{
						$selection3 = 'selected';
						} 
						else if ($searchCardType=== 'IYTC') 
						{
						$selection4 = 'selected';
						} 
						else 
						{
						$selection1 = 'selected';
						}
						?>
						<select name="cardType" id="cardType" class="form-control">
							<option <?php echo $selection1?> value="all">ALL</option>
							<option <?php echo $selection2?> value="ISIC">ISIC</option>
							<option <?php echo $selection3?> value="ITIC">ITIC</option>
							<option <?php echo $selection4?> value="IYTC">IYTC</option>
						</select>
          </td>
          <td width="160"><label class="ml20">Registered Date</label></td>
          <td style="width: 250px;">
            <input type="text" class="form-control formYoonCa p-5" name="startDate" id="startDate" placeholder="dd/mm/yyyy" value="<?php echo $startDate?>">
            ~
            <input type="text" class="form-control formYoonCa p-5" name="endDate" id="endDate" placeholder="dd/mm/yyyy" value="<?php echo $endDate?>">
          </td>
          <td width="130"><label class="ml20">First name</label></td>
          <td><input type="text" class="form-control input-sm" name="engFname" id="engFname" value="<?php echo $searchFname?>" ></td>
          <td width="130"><label class="ml20">Last name</label></td>
          <td><input type="text" class="form-control input-sm" name="engLname" id="engLname" value="<?php echo $searchLname?>" ></td>
        </tr>
        <tr>
          <td colspan="9" class="p-5"></td>
        </tr>
        <tr>
          <td><label>Application URL</label></td>
          <td colspan="3">
						<select name="appSettingId" id="appSettingId" class="form-control">
							<option value="all">All</option>
							<?php 
							for($i=0;$i<$appSettingCount;$i++)
							{
							$selected ='';
							if($searchAppSettingId === $appSettingList[$i]->appSettingId)
							{
							$selected = 'selected';
							}
							else
							{}
							?>
							<option <?php echo $selected?> value="<?php echo $appSettingList[$i]->appSettingId?>"><?php echo $thisSiteDomain?>/std/app/app01.php?appSet=<?php echo $appSettingList[$i]->url?></option>
							<?php
							}
							?>
						</select>
          </td>
          <td><label class="ml20">Born</label></td>
          <td><input type="text" class="form-control input-sm" placeholder="dd/mm/yyyy" name="born" id="born" value="<?php echo $born?>" ></td>
          <td colspan="2" class="pl-30"><button type="button" class="btn btn-kyp btn-sm" onclick="search1(1)"><i class="fab fa-sistrix"></i> Search</button> <small>(Max Result Limit 500)</small></td>
        </tr>
      </table>
		</div>
	</form>
    <!-- search End -->
	
    <!-- result Start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey">&nbsp;</th>
            <th class="text-center thGrey">Card Type</th>
            <th class="text-center thGrey">Name</th>
            <th class="text-center thGrey">Photo</th>
            <th class="text-center thGrey">Studies/Teaches at</th>
            <th class="text-center thGrey">Born</th>
            <th class="text-center thGrey">Documents</th>
            <th class="text-center thGrey">Approval</th>
        </tr>
		<?php for ($i = 0; $i < $rowcount1; $i++){ ?>
        <tr>
			<td class="text-center"><?php echo $applist[$i]['appNo']?></td>
			<?php 
				if ($applist[$i]['cardType'] === 'REVA') { 
					switch(substr($applist[$i]['oriSerialNum'],0,1)){ 
						case 'S' :
								$revCardType = 'ISIC';
								break;
						case 'T' :
								$revCardType = 'ITIC';
								break;
						case 'Y' :
								$revCardType = 'IYTC';
								break;
						default:
								$revCardType = 'ERR';
								break;
					}
					$thisCardType = $revCardType.' '.$applist[$i]['cardType'];
				} else {
					$thisCardType = $applist[$i]['cardType'];
				}
			?>
            <td class="text-center"><?php echo $thisCardType; ?></td>
            <td class="text-center"><a href="./main_content.php?menu=searchDetail&no=<?php echo $applist[$i]['appNo']; ?>"><?php echo $applist[$i]['name1']; ?><br /><?php echo $applist[$i]['engFname'].' / '.$applist[$i]['engLname']; ?></a></td>
            <td class="text-center">
            	<p>
					<?php 
						$photoName = ''; 
						if(isset($applist[$i]['photoName'])) 
						{
						?>
					<?php 
						$photoName = pathinfo($applist[$i]['photoName']); 
					?>
					<?php 
						}
						else
						{
							$photoName['filename'] = 'none';
							$photoName['extension'] = 'none';
						}
					?>
					<input type="hidden" id="appNo<?php echo $i+1 ?>" value="<?php echo $applist[$i]['appNo']?>">
					<input type="hidden" id="filenameName<?php echo $i+1 ?>" value="<?php echo $photoName['filename']?>">	
					<input type="hidden" id="filenameExt<?php echo $i+1 ?>" value="<?php echo $photoName['extension']?>">	
					<input type="hidden" id="filename<?php echo $i+1 ?>" value="<?php echo read_img($i+1,$photoName['filename'],$photoName['extension'],$applist[$i]['appNo']); ?>">	
					<div id="divPrev<?php echo $i+1;?>">None</div>
					<canvas id="prev<?php echo $i+1;?>" onclick="point(<?php echo $i+1;?>,event)" style="border:0px solid #ccc; cursor:crosshair;" width="130" height="130">HTML5 Support Required</canvas>
					<!--<img height="130" src="<?php //echo '../'.$storageDir.$applist[$i]['photoName']; ?>" class="approveListPhoto">-->
				</p>
				<?php if($applist[$i]['cardType'] === 'REVA'){ ?>
				<a href="./main_content.php?menu=searchDetail&oriSerialNum=<?php echo $applist[$i]['oriSerialNum']; ?>"><?php echo $applist[$i]['oriSerialNum']; ?></a>
				<?php } else { ?>
							<div>
								<button type="button" class="btn btn-kyp2 btn-xs" id="rotate_left<?php echo $i+1;?>" onclick="rotate1(1,<?php echo $i+1;?>);"><i class="fa fa-undo" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" id="rotate_right<?php echo $i+1;?>" onclick="rotate1(2,<?php echo $i+1;?>);"><i class="fa fa-redo" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" id="crop_button<?php echo $i+1;?>" onclick="crop1(<?php echo $i+1;?>);" disabled><i class="fa fa-crop" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" onclick="unselect1(<?php echo $i+1;?>);"><i class="fa fa-retweet" aria-hidden="true"></i></button>
								<button type="button" class="btn btn-kyp2 btn-xs" onclick="callEditor01(<?php echo $i+1;?>);">Save</button>
							</div>
				<?php } ?>
			</td>
            <td class="text-center"><?php echo $applist[$i]['school'] ?></td>
            <td class="text-center"><?php echo $applist[$i]['birthDay'].'/'. $applist[$i]['birthMonth'].'/'. $applist[$i]['birthYear']; ?></td>
			<td class="text-center">
				<?php if(isset($applist[$i]['proof1Name']) && !empty($applist[$i]['proof1Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof1Name']?>&appNo=<?php echo $applist[$i]['appNo']; ?>" target="_blank">Doc1</a><br>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof2Name']) && !empty($applist[$i]['proof2Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof2Name']?>&appNo=<?php echo $applist[$i]['appNo']; ?>" target="_blank">Doc2</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof3Name']) && !empty($applist[$i]['proof3Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof3Name']?>&appNo=<?php echo $applist[$i]['appNo']; ?>" target="_blank">Doc3</a>
				<?php }else{} ?>
				<?php if(isset($applist[$i]['proof4Name']) && !empty($applist[$i]['proof4Name'])) {?>
				<a href="./work/dwd1.php?fname=<?php echo $applist[$i]['proof4Name']?>&appNo=<?php echo $applist[$i]['appNo']; ?>" target="_blank">Doc4</a>
				<?php }else{} ?>
			</td>
            <td class="text-center">
			<?php 
			if($applist[$i]['cardSerialNum'] == null || empty($applist[$i]['cardSerialNum'])){
				if($applist[$i]['payStatus'] === 'paid'){ 
					echo "<p>Paid</p>";
				}else{
					echo "<p>Not Paid</p>";
				} 
			}else{}
			?>
			<?php if($applist[$i]['status'] != 'approved'){ ?>
				<?php if (isset($resultUserInfo->sCanValidate) && $resultUserInfo->sCanValidate === 'yes'  && isset($resultOfficeInfo->canValidate) && $resultOfficeInfo->canValidate ==='yes' ){ ?>
				<a href="./work/approveApply.php?appNo=<?php echo $applist[$i]['appNo']; ?>&cardType=<?php echo $applist[$i]['cardType']; ?>" role="button" class="btn btn-kyp">Approve</a>
				<?php } else {} ?>
			
			<?php }else if(isset($applist[$i]['cardSerialNum']) && $applist[$i]['cardType'] != 'REVA'){ ?>
				<?php if (isset($resultUserInfo->sCanPrint) && $resultUserInfo->sCanPrint === 'yes'  && isset($resultOfficeInfo->canPrint) && $resultOfficeInfo->canPrint ==='yes' ){ ?>
				
					<?php 
						$cardSerialNum = substr($applist[$i]['cardSerialNum'], 0, 1).' '.substr($applist[$i]['cardSerialNum'], 1, 3).' '.substr($applist[$i]['cardSerialNum'], 4, 3).' '.substr($applist[$i]['cardSerialNum'], 7, 3).' '.
						substr($applist[$i]['cardSerialNum'], 10, 3).' '.substr($applist[$i]['cardSerialNum'], 13, 1);
					?>
					<p class="text-center"><?php echo $cardSerialNum; ?></p>
					<?php 
						switch($applist[$i]['cardType']){
							case 'ISIC':
								$cardTypeCode = '7';
								break;
							case 'ITIC':
								$cardTypeCode = '8';
								break;
							case 'IYTC':
								$cardTypeCode = '9';
								break;
							default :
								$cardTypeCode = '7';
								break;
						}
					?>
					<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
					<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="printApply(<?php echo $applist[$i]['appNo']; ?>,1);">Card Print</button></p>
					<!--<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="printApply(<?php //echo $applist[$i]['appNo']; ?>,<?php //echo $cardTypeCode ?>,2);">Label Print</button></p>-->
				<?php } else {} ?>
			<!-- auto serial issuing start -->
				<?php }else if($applist[$i]['cardType'] != 'REVA' && $applist[$i]['issueType']==='auto' && $applist[$i]['payStatus'] === 'paid'){ ?>
					<?php if (isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' ){ ?>
						<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
						<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
						<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
						<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $_SESSION['valid_user'] ?>">
						<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
						<!--<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,222)">Auto Issue<i class="fas fa-caret-right"></i></button></p>-->
						<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,222)">Auto Issue <i class="fas fa-caret-right"></i></button></p>
					<?php } else {} ?>
			<!-- auto serial issuing end -->
			<!-- manual serial issuing start -->
			<?php }else if($applist[$i]['cardType'] != 'REVA' && $applist[$i]['issueType']==='manual' && $applist[$i]['payStatus'] === 'paid'){ ?>
				<?php if (isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' ){ ?>
				<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
				<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
				<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
				<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $_SESSION['valid_user'] ?>">
				<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
	  
					<p class="m0 pt10 text-center"><input type="text" class="form-control" maxlength="14" id="cardSerialNum<?php echo $applist[$i]['appNo']; ?>" name="cardSerialNum<?php echo $applist[$i]['appNo']; ?>"></div></p>
					<!--<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,333)">Manual Issue <i class="fas fa-caret-right"></i></button></p>-->
					<p class="m0 pt10 text-center"><button type="button" class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,333)">Manual Issue<i class="fas fa-caret-right"></i></button></p>
				<?php } else {} ?>
			<!-- manual serial issuing end -->
			<!-- manual issue reva start -->
			<?php }else if($applist[$i]['cardType'] === 'REVA' && $applist[$i]['payStatus'] === 'paid'){ ?>
				<?php if (isset($resultUserInfo->sCanIssue) && $resultUserInfo->sCanIssue === 'yes'  && isset($resultOfficeInfo->canIssue) && $resultOfficeInfo->canIssue ==='yes' ){ ?>
				<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
				<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
				<div class="row">
					<div class="col-xs-7 pr3"><input type="text" class="form-control" maxlength="7" id="revaNum<?php echo $applist[$i]['appNo']; ?>" name="revaNum<?php echo $applist[$i]['appNo']; ?>"></div>
					<!--<div class="col-xs-5 pl3"><button type="button" class="btn btn-kyp btn-block" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,111)">Register <i class="fas fa-caret-right"></i></button></div>-->
					<div class="col-xs-5 pl3"><button type="button" class="btn btn-kyp btn-block" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,111)">Register<i class="fas fa-caret-right"></i></button></div>
				</div>
				<?php } else {} ?>
			<!-- manual issue reva end -->
			<?php }else if (0 > 9){ ?>
				<input type="hidden" id="id<?php echo $applist[$i]['appNo']; ?>" name="id<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['appNo']; ?>">
				<input type="hidden" id="type<?php echo $applist[$i]['appNo']; ?>" name="type<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['cardType']; ?>">
				<input type="hidden" id="cardStatus<?php echo $applist[$i]['appNo']; ?>" name="cardStatus<?php echo $applist[$i]['appNo']; ?>" value="1">
				<input type="hidden" id="issuedBy<?php echo $applist[$i]['appNo']; ?>" name="issuedBy<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $_SESSION['valid_user'] ?>">
				<input type="hidden" id="issueType<?php echo $applist[$i]['appNo']; ?>" name="issueType<?php echo $applist[$i]['appNo']; ?>" value="<?php echo $applist[$i]['issueType2']?>">
				<!--<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply3(<?php //echo $applist[$i]['appNo']; ?>,111)">No Issue<i class="fas fa-caret-right"></i></button></p>-->
				<p class="m0 pt10 text-center"><button class="btn btn-kyp" onclick="issueApply4(<?php echo $applist[$i]['appNo']; ?>,111)">No Issue<i class="fas fa-caret-right"></i></button></p>
			
			<?php } else{}?>
			</td>
        </tr>
		<?php } ?>
    </table>
    <!-- result End -->
 	<!-- Paging Start -->
	<?php 
		if($rowcount1 === 0)
		{}
		else if ($searchByBorn === 'yes' || $searchByFname === 'yes'|| $searchByLname === 'yes')
		{}
		else
		{
	?>	
    <div class="pagination">
        <ul class="pagination-pages">
            <li><a href="./main_content.php?menu=bulkList&pagenum=1&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-left"></i></a></li>	
			<?php 
				$previouspage = $pagenum -1;
				if($previouspage < 1){
					$previouspage = 1;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=bulkList&pagenum=<?php echo $previouspage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-left"></i></a></li>
			<?php 
				for ($location = 1; $location < $howmanypage +1; $location++){
					if ($currentgrouplocation === $location){
						$activepage = 'class="activePaging"';
					}
					else{
						$activepage = '';
					}
					$pagingno = $currentgroup*$howmanypage-$howmanypage+$location; //Outputs the pages belonging to the current group in position(location).
					if ($pagingno >$maxpage){
					}
					else{
						echo "<li $activepage><a href=\"./main_content.php?menu=bulkList&pagenum=$pagingno&searchType=$searchTypeSel\">$pagingno</a></li>";
					}
				}
			?>
			<?php
				$nextpage = $pagenum +1;
				if($nextpage > $maxpage){
					$nextpage = $maxpage;
				}
				else{}
			?>
            <li><a href="./main_content.php?menu=bulkList&pagenum=<?php echo $nextpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="./main_content.php?menu=bulkList&pagenum=<?php echo $maxpage; ?>&searchType=<?php echo $searchTypeSel?>"><i class="fa fa-angle-double-right"></i></a></li>
         </ul>
    
    </div>        
	<?php } ?>
    <!-- pagingEnd -->


</div>
</form>	   
<!-- Conent End -->