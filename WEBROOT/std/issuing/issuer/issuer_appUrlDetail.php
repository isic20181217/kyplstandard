<?php
//201114 check
?>
<?php 
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($_GET['appSettingId']) && !empty($_GET['appSettingId'])){
		$appSettingId = preg_replace("/<|\/|_|>/","", $_GET['appSettingId']);
	}else{
		echo 'Access Denied1';
		exit;
	}
	require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
	$query = "SELECT officeId,name FROM $tablename12 WHERE activeOffice= :activeOffice AND canUrl = :canUrl";
	$valueYes1 = 'yes';
	$stmt = $db->prepare($query);
	$stmt->bindParam(':activeOffice', $valueYes1);
	$stmt->bindParam(':canUrl', $valueYes1);
	$stmt->execute();
	if($stmt->rowCount() >= 1){
		$officeCount = $stmt->rowCount();
		$i =0;
		while($result0 = $stmt->fetch(PDO::FETCH_OBJ)){
			$officeList[$i] = $result0;
			$i++;
		}
	}else{
		echo 'Access Denied2';
		$db= NULL;
		exit;
	}
	/*
	echo '<pre>';
	print_r($officeList);
	echo '</pre>';
	*/
	$query = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId";
	$stmt = $db->prepare($query);
	$stmt->bindParam(':appSettingId', $appSettingId);
	$stmt->execute();
	if($stmt->rowCount() == 1){
		$result = $stmt->fetch(PDO::FETCH_OBJ);
	}else{
		echo 'Access Denied2';
		$db= NULL;
		exit;
	}
	//$query = "SELECT * FROM $tablename25 WHERE extraDesignName = :searchValue1";
	$query = "SELECT * FROM $tablename25 WHERE extraAppSettingId = :extraAppSettingId";
	$stmt = $db->prepare($query);
	//$stmt->bindParam('extraDesignId', $result->url);
	$stmt->bindParam('extraAppSettingId', $appSettingId);
	$stmt->execute();
	//echo $query;
	//echo $stmt->rowCount();
	if($stmt->rowCount() >= 1 ){
		$stmt->execute();
		$extraCount = $stmt->rowCount();
		$i = 0;
		while($result2 = $stmt->fetch(PDO::FETCH_OBJ)){
			$extraList[$i] = $result2;
			$i++;
		}
	}else{
		//echo 'noextra';
		$extraCount =0;
	}
	$query = "SELECT * FROM $tablename29 WHERE appSettingId = :appSettingId";
	$stmt = $db->prepare($query);
	$stmt->bindParam(':appSettingId', $appSettingId);
	$stmt->execute();
	//echo $query;
	//echo $stmt->rowCount();
	if($stmt->rowCount() >= 1 ){
		$stmt->execute();
		$appOfficeCount = $stmt->rowCount();
		$i = 0;
		while($result3 = $stmt->fetch(PDO::FETCH_OBJ)){
			$appOfficeList[$i] = $result3;
			$i++;
		}
	}else{
		//echo 'noextra';
		$appOfficeCount =0;
	}
	$query = "SELECT * FROM $tablename30 WHERE activeDesign = :activeDesign";
	$stmt = $db->prepare($query);
	$paramYes = 'yes';
	$stmt->bindParam(':activeDesign', $paramYes);
	$stmt->execute();
	//echo $query;
	//echo $stmt->rowCount();
	if($stmt->rowCount() >= 1 ){
		$stmt->execute();
		$designCount = $stmt->rowCount();
		$i = 0;
		while($result4 = $stmt->fetch(PDO::FETCH_OBJ)){
			$designList[$i] = $result4;
			$i++;
		}
	}else{
		//echo 'nodesign';
		$designCount =0;
	}
	$db= NULL;
	/*
	echo '<pre>';
	print_r($result);
	echo '</pre>';
	*/
	/*
	echo '<pre>';
	print_r($extraList);
	echo '</pre>';
	*/
?>
<script src="./issuer/issuer_appUrlDetail.js?ver=1"></script>
<!-- content start -->
<form name="modifyUrl" id="modifyUrl" method="POST" enctype="multipart/form-data">
<input type="hidden" name="appSettingId" id="appSettingId" value="<?php echo $result->appSettingId ?>">
<div id="contents">
	<h1>Manage applications  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Modify Application URL  &amp; additional fields settings</span></h1>						  
	<h2>Modify applicatino URL settings</h2>
	<table class="table table-bordered">
		<tr>
			<th class="text-left thGrey w20p">Type of ID card to be used	</th>
			<td class="w30p">
				<select class="form-control" name="serialType" id="serialType">
					<?php 
						$thisColumn1 = 'serialType';
						if(isset($result->$thisColumn1)){ 
							$selectISIC =' ';
							$selectITIC =' ';
							$selectIYTC =' ';
							switch($result->$thisColumn1){
								case 'ISIC':
									$selectISIC ='selected';
									break;
								case 'ITIC':
									$selectITIC ='selected';
									break;
								case 'IYTC':
									$selectIYTC ='selected';
									break;
								default:
									break;
							}		
					?>
					<option <?php echo $selectISIC; ?> value="ISIC">ISIC</option>
					<option <?php echo $selectITIC; ?> value="ITIC">ITIC</option>
					<option <?php echo $selectIYTC; ?> value="IYTC">IYTC</option>
					<?php }else{ ?>
					<option value="ISIC">ISIC</option>
					<option value="ITIC">ITIC</option>
					<option value="IYTC">IYTC</option>
					<?php } ?>
				</select>
			</td>        
			<th class="text-left thGrey w20p">Serial Name</th>
			<td>
				<input type="text" class="form-control"  name="serialName" id="serialName" maxlength="25" value="<?php echo $result->serialName?>">
				<small style="color: #999;">* Select the serial name that you registered on the ’Serial upload’ page.</small></td>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Printing the serial number	</th>
			<td>
				<select class="form-control" name="needSerial" id="needSerial">
					<?php 
						$thisColumn1 = 'needSerial';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Real time updating to CCDB	</th>
			<td>
				<select class="form-control" name="ccdbSend" id="ccdbSend">
					<?php 
						$thisColumn1 = 'ccdbSend';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Printer type</th>
			<td>
				<select class="form-control" name="issueType" id="issueType">
					<?php 
						$thisColumn1 = 'issueType';
						if(isset($result->$thisColumn1)){ 
							$selectManual =' ';
							$selectAuto =' ';
							switch($result->$thisColumn1){
								case 'manual':
									$selectManual  ='selected';
									break;
								case 'auto':
									$selectAuto ='selected';
									break;
								default:
									break;
							}		
					?>
					<option <?php echo $selectManual; ?> value="manual">Manual</option>
					<option <?php echo $selectAuto; ?> value="auto">Auto</option>
					<?php }else{ ?>
					<option value="manual">Manual</option>
					<option value="auto">Auto</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Id type	</th>
			<td>
				<select class="form-control" name="issueType2" id="issueType2">
					<?php 
						$thisColumn1 = 'issueType2';
						if(isset($result->$thisColumn1)){ 
							$selectPlastic=' ';
							$selectVirtual =' ';
							switch($result->$thisColumn1){
								case 'P':
									$selectPlastic  ='selected';
									break;
								case 'V':
									$selectVirtual ='selected';
									break;
								default:
									break;
							}		
					?>
					<option <?php echo $selectPlastic; ?> value="P">Plastic</option>
					<option <?php echo $selectVirtual; ?> value="V">Virtual</option>
					<?php }else{ ?>
					<option value="P">Plastic</option>
					<option value="V">Virtual</option>
					<?php } ?>
				</select>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Using online payment	</th>
			<td>
				<select class="form-control" name="payment" id="payment">
					<?php 
						$thisColumn1 = 'payment';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">ID price</th>
			<td><?php echo $localCurrency?> <input type="text" class="form-control formYoonNm" style="width: 90px;" maxlength="5" id="price" name="price" value="<?php echo $result->price?>"> <small style="color: #999;">* e.g - 9.00</small></td>
	    </tr>
		<tr>
			<th class="text-left thGrey">URL address</th>
			<td colspan="3"><?php echo $thisSiteDomain?>/app/app01.php?appSet=
				<input type="text" class="form-control formYoon" placeholder="keyword" / name="url" id="url" value="<?php echo $result->url?>"> <small style="color: #999;">* Alphabets and numeric characters only.</small></td>
		</tr>
		<tr>
			<th class="text-left thGrey">Application Name</th>
			<td colspan="3">
				<input type="text" class="form-control"  name="appName" id="appName" maxlength="20"  value="<?php echo $result->appName?>"></td>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey">Office ID</th>
			<td>
				<?php 
					//$currentOfficeId = 0;
					for($i=0;$i<$appOfficeCount;$i++) { 
				?>
				<?php 
					echo $appOfficeList[$i]->officeId; 
					//$currentOfficeId = $appOfficeList[$i]->officeId; 
					for($j=0;$j<$officeCount;$j++){
						if($officeList[$j]->officeId === $appOfficeList[$i]->officeId){
							echo ' '.$officeList[$j]->name;
						}else{}
					}
				?>
				<br/>
				<?php } ?>
			</td>
			<th class="text-left thGrey">Add Office ID</th>
			<td>
				<div class="row">
					<div class="col-xs-6 pr-3">
						<select class="form-control" name="appOfficeId" id="appOfficeId">
								<option value="none">none</option>
							<?php 
								for($i=0;$i<$officeCount;$i++)
								{
									$selected = '';
									if(isset($appOfficeList[0]) && $appOfficeList[0]->officeId === $officeList[$i]->officeId)
									{
										$selected ='selected';
									}
									else
									{}
							?>
								<option <?php echo $selected?> value="<?php echo $officeList[$i]->officeId?>"><?php echo $officeList[$i]->officeId?> <?php echo $officeList[$i]->name?></option>
							<?php 
								}
							?>
						</select>
					</div>
					<div class="col-xs-3 pr-3"><button type="button" class="btn btn-kyp btn-block" onclick="appOfficeChange(1)">Set</button></div>
					<!--<div class="col-xs-3 pr-3"><button type="button" class="btn btn-kyp btn-block" onclick="appOfficeAdd(1)">Add</button></div>-->
					<!--<div class="col-xs-3 pl-3"><button type="button" class="btn btn-kyp btn-block" onclick="appOfficeDelete(1)">Delete</button></div>-->
				</div>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Available in application</th>
			<td>
				<select class="form-control" name="activeApp" id="activeApp">
					<?php 
						$thisColumn1 = 'activeApp';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Available in inOffice</th>
			<td>
				<select class="form-control" name="activeInOffice" id="activeInOffice">
					<?php 
						$thisColumn1 = 'activeInOffice';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
	    </tr>
		<tr>
			<th class="text-left thGrey w20p">Card design number</th>
			<td class="w30p">
				<select class="form-control" name="designId" id="designId">
					<?php 
						for($i=0;$i<$designCount;$i++){
							$selected='';
							if(isset($designList[$i]->designId) && isset($result->designId) && $designList[$i]->designId === $result->designId){
								$selected='selected';
							} else {
								$selected='';
							}
					?>
						<option <?php echo $selected ?> value="<?php echo $designList[$i]->designId?>"><?php echo $designList[$i]->designNo?></option>
					<?php 
						}
					?>
				</select>
			</td>        
			<th class="text-left thGrey w20p">Modified date</th>
			<td>
				<?php
				if(isset($result->modifiedDate)){
					echo $result->modifiedDate;
				}else{}
				?>
			</td>
		</tr>
    </table>


	<h2 class="pt-20">Application details settings</h2>
	<table class="table table-bordered">   
		<tr>
		  <th class="text-left w20p thGrey">Use name in local language field</th>
		  <td class="w30p">
				<select class="form-control" name="localName" id="localName">
					<?php 
						$thisColumn1 = 'localName';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		  <th class="text-left w20p thGrey">Use gender field</th>
			<td>
				<select class="form-control" name="gender" id="gender">
					<?php 
						$thisColumn1 = 'gender';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Use school name field</th>
			<td>
				<select class="form-control" name="school" id="school">
					<?php 
						$thisColumn1 = 'school';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
						<option value="custom">Custom</option>
					<?php }else if (isset($result->$thisColumn1) && $result->$thisColumn1 === 'no'){ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
						<option value="custom">Custom</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option value="no">No</option>
						<option selected value="custom">Custom</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">School Name Customising</th>
			<td>
				<input type="text" class="form-control" name="schoolCustom" id="schoolCustom" value="<?php echo $result->schoolCustom?>" maxlength="35">
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Use certification(proof documents) attachment</th>
			<td>
				<select class="form-control" name="proofStatus" id="proofStatus">
					<?php 
						$thisColumn1 = 'proofStatus';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Use identification attachment</th>
			<td>
				<select class="form-control" name="proofBirth" id="proofBirth">
					<?php 
						$thisColumn1 = 'proofBirth';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Accept online applications of children under 16</th>
			<td>
				<select class="form-control" name="underAge" id="underAge">
					<?php 
						$thisColumn1 = 'underAge';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey"></th>
			<td>
			</td>
		</tr>
	</table>

		<h2 class="pt-20">Information settings for postal delivery</h2>
	<table class="table table-bordered">   	
		<tr>
		  <th class="text-left w20p thGrey">Use the 1st address field	</th>
		  <td class="w30p">
				<select class="form-control" name="address" id="address">
					<?php 
						$thisColumn1 = 'address';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
		  </td>
		  <th class="text-left w20p thGrey">Use the 2nd address field</th>
			<td>
				<select class="form-control" name="address2" id="address2">
					<?php 
						$thisColumn1 = 'address2';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use city field</th>
			<td>
				<select class="form-control" name="city" id="city">
					<?php 
						$thisColumn1 = 'city';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Use country field</th>
			<td>
				<select class="form-control" name="Country" id="Country">
					<?php 
						$thisColumn1 = 'country';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use postal code field	</th>
			<td>
				<select class="form-control" name="postal" id="postal">
					<?php 
						$thisColumn1 = 'postal';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Use nationality field	</th>
			<td>
				<select class="form-control" name="nationality" id="nationality">
					<?php 
						$thisColumn1 = 'nationality';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Use phone number field	</th>
			<td>
				<select class="form-control" name="phone" id="phone">
					<?php 
						$thisColumn1 = 'phone';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Use email field	</th>
			<td>
				<select class="form-control" name="email" id="email">
					<?php 
						$thisColumn1 = 'email';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
	</table>


	<table class="table table-bordered">
		<tr>
			<th colspan="2" class="text-left w20p thGrey">Total post delivery type	</th>
			<td>
				<input type="text" class="form-control formYoon" name="postType" id="postType" value="<?php echo $result->postType?>" maxlength="20"style="width: 100px;">
				<small style="color: #999">* 1-4까지 숫자로 입력할 수 있습니다.  ( 아래에 4가지 종류로 우편배송 종류를 구분하고, 고객에 지급할 우편배송료를 지정할 수 있습니다. )</small>
			</td>
		</tr>
		<?php for($i=1;$i<5;$i++) {?>
		<?php 
			$thisPostName = 'post'.$i.'Name';
			if(isset($result->$thisPostName)){
			}else{
				$result->$thisPostName = '';
			}
			$thisPostDesc = 'post'.$i.'Desc';
			if(isset($result->$thisPostDesc)){
			}else{
				$result->$thisPostDesc = '';
			}
			$thisPostPrice = 'post'.$i.'Price';
			if(isset($result->$thisPostPrice)){
			}else{
				$result->$thisPostPrice = '';
			}
			/*
			if(isset($result->$thisPostPrice) && !empty($result->$thisPostPrice)){
				echo $i.'issetEmpty!';
			}else{}
			*/
			/*
			$zeroString = '0';
			if(isset($zeroString)){
				echo ' isset 0 ';
			}else{}
			if(empty($zeroString)){
				echo ' empty 0 ';
			}else{}
			if(isset($zeroString) && !empty ($zeroString)){
				echo ' isset 0 !empty 0';
			}else{
				echo ' value 0 ';
			}
			if(isset($zeroString) && empty ($zeroString)){
				echo ' isset 0 empty 0';
			}else{
				echo ' value 0 ';
			}*/
			/*
			if(empty($result->$thisPostPrice)){
				echo $i.'empty!';
			}else{}
			*/
		?>
		<tr>
			<th rowspan="3" class="text-left thGrey">Post type<?php echo $i?></th>
			<th class="text-left thGrey">Name</th>
			<td>
				<input type="text" class="form-control formYoon" name="<?php echo $thisPostName?>" id="<?php echo $thisPostName?>" value="<?php echo $result->$thisPostName?>" maxlength="20">
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Description</th>
			<td>
				<input type="text" class="form-control" name="<?php echo $thisPostDesc?>" id="<?php echo $thisPostDesc?>" value="<?php echo $result->$thisPostDesc?>" maxlength="200">
			</td>
		</tr>
		<tr>
		  <th class="text-left thGrey">Price</th>
			<td>
				<input type="text" class="form-control formYoon" name="<?php echo $thisPostPrice?>" id="<?php echo $thisPostPrice?>" value="<?php echo $result->$thisPostPrice?>" maxlength="20">
			</td>
		</tr>
		<?php } ?>	
		</table>

		<h2 class="pt-20">Regulations and issuance settings</h2>
		<table class="table table-bordered">   
		<tr>
		  <th class="text-left w20p thGrey">Terms</th>
		  <td class="w30p">
				<select class="form-control" name="terms" id="terms">
					<?php 
						$thisColumn1 = 'terms';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		  <th class="text-left w20p thGrey">Privacy</th>
			<td>
				<select class="form-control" name="privacy" id="privacy">
					<?php 
						$thisColumn1 = 'privacy';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		<tr>
			<th style="height class="text-left w20p thGrey">Terms &amp; conditions</th>
				<td colspan="3">
					<div style="height: 500px;">
						<iframe src="/std/app/App_terms.html" scrolling="yes" style="height: 500px;" class="form-control agreeIframe"></iframe>
					</div>
			<!--<textarea class="form-control" rows="5"><?php //echo $result->termsText?></textarea>-->
			</td>
		</tr>
		<tr>
			<th class="text-left w20p thGrey">Privacy policy</th>
				<td colspan="3">
				<div style="height: 500px;">
					<iframe src="/std/app/App_privacy.html" scrolling="yes" style="height: 500px;" class="form-control agreeIframe"></iframe>
				</div>
			<!--<textarea class="form-control" rows="5"><?php //echo $result->privacyText?></textarea>-->
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Receive E-mail News</th>
			<td>
				<select class="form-control" name="news" id="news">
					<?php 
						$thisColumn1 = 'news';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Validity Type</th>
			<td>
				<select class="form-control" name="validityStart" id="validityStart">
					<?php 
						$thisColumn1 = 'validityStart';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'validityStartFix'){
					?>
						<option selected value="validityStartFix"><?php echo $validityStartFix.' ~ '.$validityEndFix ?></option>
						<option value="validityStart1">1Year</option>
						<option value="validityStartC">Custom Value</option>
					<?php }else if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'validityStart1'){ ?>
						<option value="validityStartFix"><?php echo $validityStartFix.' ~ '.$validityEndFix ?></option>
						<option selected value="validityStart1">1Year</option>
						<option value="validityStartC">Custom Value</option>
					<?php }else{ ?>
						<option value="validityStartFix"><?php echo $validityStartFix.' ~ '.$validityEndFix ?></option>
						<option value="validityStart1">1Year</option>
						<option selected value="validityStartC">Custom Value</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		</table>

		<h2 class="pt-20">Custom validity settings</h2>
		<table class="table table-bordered">   
		<tr>
		  <th class="text-left w20p thGrey">Valid from</th>
		  <td class="w30p">
				<input type="text" class="form-control" name="validityStart11" id="validityStart11" value="<?php echo $result->validityStart11?>" maxlength="7">
			</td>
		  <th class="text-left w20p thGrey">Valid until</th>
			<td>
				<input type="text" class="form-control" name="validityEnd11" id="validityEnd11" value="<?php echo $result->validityEnd11?>" maxlength="7">
			</td>
		</tr>	
		</table>

		<h2 class="pt-20">Vat related information settings</h2>
		<table class="table table-bordered">   
		<tr>
		  <th class="text-left w20p thGrey">Vat Number</th>
		  <td class="w30p">
				<select class="form-control" name="vatNumber" id="vatNumber">
					<?php 
						$thisColumn1 = 'vatNumber';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
		  </td>
		  <th class="text-left w20p thGrey">Associate Number</th>
			<td>
				<select class="form-control" name="associateNumber" id="associateNumber">
					<?php 
						$thisColumn1 = 'associateNumber';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Associate Name</th>
			<td>
				<select class="form-control" name="associateName" id="associateName">
					<?php 
						$thisColumn1 = 'associateName';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
			<th class="text-left thGrey">Associate Relation</th>
			<td>
			  <select class="form-control" name="associateRelation" id="associateRelation">
					<?php 
						$thisColumn1 = 'associateRelation';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
			</td>
		</tr>	
		</table>

	<h2 class="pt20">Promotion settings</h2>
	<table class="table table-bordered">   
		<tr>
			<th class="text-left thGrey w20p">Using promotion code</th>
		  <td class="w30p">
				<select class="form-control" name="promo" id="promo">
					<?php 
						$thisColumn1 = 'promo';
						if(isset($result->$thisColumn1) && $result->$thisColumn1 === 'yes'){
					?>
						<option selected value="yes">Yes</option>
						<option value="no">No</option>
					<?php }else{ ?>
						<option value="yes">Yes</option>
						<option selected value="no">No</option>
					<?php } ?>
				</select>
		  </td>
		  <th class="text-left w20p thGrey">Promotion Group Id</th>
			<td><input type="text" class="form-control" id="promoGroupId" name="promoGroupId" maxlength="20" value="<?php echo $result->promoGroupId?>"></td>
		</tr>
    </table>
	
    <div class="btnDiv">
      <button type="button" class="btn btn-kyp" onclick="submit1(1)">Save</button>
	  <button type="button" class="btn btn-kyp" onclick="delete1(1)">Delete</button>
      <button type="button" class="btn btn-kyp" onclick="goBack(1)">Back to list</button>
    </div>

<hr>

	<h2 class="pt-20 mb-10">Additional fields settings</h2>
	<table class="table table-bordered no-border"> 
		<?php for($i=0;$i<6;$i++){?>
		<?php $currentExtra = $i + 1;?>
		<?php 
			if(isset($extraList[$i]) && !empty($extraList[$i])){
				$thisExtraId = $extraList[$i]->extraFieldName;
				$thisExtraMaxLength = $extraList[$i]->extraMaxLength;
				$thisExtraDescription1 = $extraList[$i]->extraDescription1;
				$thisExtraDescription2 = $extraList[$i]->extraDescription2;
				$thisExtraDescription3 = $extraList[$i]->extraDescription3;
				$thisExtraDescription4 = $extraList[$i]->extraDescription4;
			}else{
				$thisExtraId = '';
				$thisExtraMaxLength ='';
				$thisExtraDescription1 = '';
				$thisExtraDescription2 = '';
				$thisExtraDescription3 = '';
				$thisExtraDescription4 ='';
			}
		?>
		<tr>
			<th colspan="4" class="no-border pl-0 pt-20">Additional field <?php echo $currentExtra ?> </th>
		</tr>
		<tr>
			<th class="text-left w20p thGrey"> Id</th>
			<td class="w30p">
				<input type="text" class="form-control formYoon" name="extra<?php echo $currentExtra ?>" id="extra<?php echo $currentExtra ?>" value="<?php echo $thisExtraId ?>" maxlength="20"><br/><small style="color: #999;">* Alphabets and numeric characters only.</small>
			</td>
			<th class="text-left w20p thGrey">MaxLength</th>
			<td>
				<input type="text" class="form-control" name="extra<?php echo $currentExtra ?>MaxLength" id="extra<?php echo $currentExtra ?>MaxLength" value="<?php echo $thisExtraMaxLength ?>" maxlength="20">
			</td>
		</tr>	
		<tr>
			<th class="text-left thGrey">Name1</th>
			<td>
				<input type="text" class="form-control" name="extra<?php echo $currentExtra ?>Des1" id="extra<?php echo $currentExtra ?>Des1" value="<?php echo $thisExtraDescription1 ?>" maxlength="100">
			</td>
			<th class="text-left thGrey">Name1 Description</th>
			<td>
				<input type="text" class="form-control" name="extra<?php echo $currentExtra ?>Des2" id="extra<?php echo $currentExtra ?>Des2"  value="<?php echo $thisExtraDescription2 ?>" maxlength="100">
			</td>
		</tr>
		<tr>
			<th class="text-left thGrey">Name2</th>
			<td>
				<input type="text" class="form-control" name="extra<?php echo $currentExtra ?>Des3" id="extra<?php echo $currentExtra ?>Des3"  value="<?php echo $thisExtraDescription3 ?>" maxlength="100">
			</td>
			<th class="text-left thGrey">Name2 Description</th>
			<td>
				<input type="text" class="form-control" name="extra<?php echo $currentExtra ?>Des4" id="extra<?php echo $currentExtra ?>Des4"  value="<?php echo $thisExtraDescription4 ?>" maxlength="100">
			</td>
		</tr>			
		<?php } ?>
	</table>	
	<textarea class="form-control" rows="5"></textarea>
	
    <div class="btnDiv">
      <button type="button" class="btn btn-kyp" onclick="submit1(1)">Save</button>
	  <button type="button" class="btn btn-kyp" onclick="delete1(1)">Delete</button>
      <button type="button" class="btn btn-kyp" onclick="goBack(1)">Back to list</button>
    </div>
</div>
</form>
<!-- content End -->

