<?php 
// 201114  check
	if (isset($_SESSION['valid_user']) && isset($_SESSION['user_type'])){
		$user = $_SESSION['valid_user'];
		switch($user_type = $_SESSION['user_type']){
			case 'Manager':
			case 'Issuer':
				break;
			default:
				echo '<script>alert(\'You are not manager.\');</script>';
				echo '<script>location.replace("/std/index.php");</script>';
				exit;
		}
	}
	else{
		echo '<script>alert(\'Login please.\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}
	if(isset($resultUserInfo->sMenuApplication) && $resultUserInfo->sMenuApplication === 'yes'  && isset($resultOfficeInfo->menuApplication) && $resultOfficeInfo->menuApplication ==='yes' ){
	} else {
		echo '<script>alert(\'Access Denied1\');</script>';
		echo '<script>location.replace("/std/index.php");</script>';
		exit;
	}																																														   
		require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';	
		$query = "SELECT activeApp,appSettingId, appName, serialType,serialName, url, modifiedDate,agency FROM $tablename23";
		$stmt = $db->prepare($query);
		$stmt->execute();
		$appSettingCount =0;
		if($stmt->rowCount() >= 1){
			while($result = $stmt->fetch(PDO::FETCH_OBJ)){
				$appSettingList[$appSettingCount]= $result;
				$appSettingCount++;
			}
		}else{
			$db= NULL;
		}
		$db= NULL;
?>
<!-- content Start-->
<div id="contents">
    <div class="row">
      <div class="col-sm-6">
        <h1>Manage applications  <i class="fas fa-angle-double-right"></i> <span class="h1Sub">Application list</span><a href="http://std.aretede.com/mockup/issuing/appMange.html" target="_blank"><i class="far fa-question-circle qButton"></i></a></h1>
      </div>
      <div class="col-sm-6 text-right">
        <a href="./main_content.php?menu=appUrlNew" role="button" class="btn btn-kyp fr">Add New application <i class="fa fa-caret-right"></i></a>
      </div>			
		</div>
  
    <!-- result Start -->
    <table class="table table-bordered">
        <tr>
			<th class="text-center thGrey"></th>
            <th class="text-center thGrey">App Name</th>
			<th class="text-center thGrey">Serial Name</th>
			<th class="text-center thGrey">Serial Type</th>
            <th class="text-center thGrey">Date</th>
			<th class="text-center thGrey">Manage</th>
        </tr>
		<?php for($i=0;$i<$appSettingCount;$i++){?>
        <tr>
			<td class="text-center"><?php echo $i+1?></td>
			<td class="text-center">
				<?php echo $appSettingList[$i]->appName?>
			</td>
			<td class="text-center"><?php echo $appSettingList[$i]->serialName?></td>
			<td class="text-center"><?php echo $appSettingList[$i]->serialType?></td>
			<td class="text-center"><?php echo $appSettingList[$i]->modifiedDate?></td>
			<td class="text-center">
				( <a href="./main_content.php?menu=appUrlDetail&appSettingId=<?php echo $appSettingList[$i]->appSettingId?>">Detail</a>
				<?php if($appSettingList[$i]->activeApp === 'yes'){ ?>
				<a target="_blank" href="/std/app/app01.php?appSet=<?php echo $appSettingList[$i]->url?>">| View App</a>
				<?php }else{} ?>
				)
			</td>
        </tr>
		<?php } ?>
    </table>
    <!-- result End -->
	
    <!-- paging Start -->              
    <!-- paging end -->
	
</div>
<!-- content End -->

