<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ISIC</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="./css/bt.min.css" rel="stylesheet" type="text/css" />
<link href="./css/yoonCustom.css" rel="stylesheet" type="text/css" />
<link href="../css/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/app03.js?time=1"></script>
</head>

<body>
<?php 
// 201113 check
	session_start();
	if(isset($_SESSION['currentAppStatus']) && $_SESSION['currentAppStatus'] === 'complete' ){
	}else{
		echo 'Access Denied1';
		exit;
	}
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent.'/req.php';
		require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename07 LEFT JOIN $tablename24 ON $tablename07.appNo = $tablename24.extraAppNo WHERE appNo =:appNo";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appNo', $_SESSION['currentApp']);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo 'Access Denied4';
			print_r($stmt->errorInfo());
			$db= NULL;
			exit;
		}
		$query = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingIdApp AND activeApp = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingIdApp', $result->appSettingIdApp);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);
			$result->appSettingInfo = $appSettingInfo;
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		$queryCardDesign = "SELECT * FROM $tablename30 WHERE designId = :designId";
		$stmtCardDesign = $db->prepare($queryCardDesign);
		$stmtCardDesign->bindParam(':designId', $result->appSettingInfo->designId);
		$stmtCardDesign->execute();
		//echo $queryCardDesign;
		//echo $appList[$i]->cardType;
		//echo $stmtCardDesign->rowCount();
		if($stmtCardDesign->rowCount() === 1){
			$resultCardDesign = $stmtCardDesign->fetch(PDO::FETCH_OBJ);
			$result->designInfo = $resultCardDesign;
		}else{
			echo 'Access Denied45';
			//print_r($stmtCardDesign->errorInfo());
			$db= NULL;
			//echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;
		}	
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}
	/*
	echo '<pre>';
	print_r($result);
	echo '</pre>';
	*/
	require __DIR__.$goParent.$reqDir1.'/_require1/encDec.php';
	$school = decrypt1($result->school, $result->encId);
	$birthDay = decrypt1($result->birthDay, $result->encId);
	$birthMonth = decrypt1($result->birthMonth, $result->encId);
	$birthYear = decrypt1($result->birthYear, $result->encId);
	$engFname = decrypt1($result->engFname, $result->encId);
	$engLname = decrypt1($result->engLname, $result->encId);
	if ($result->appSettingInfo->validityStart === 'validityStartFix'){
		$result->validityStart  = $validityStartFix;
	}else{
		$result->validityStart  = $validityStart1;
	}
	if ($result->appSettingInfo->validityEnd === 'validityEndFix'){
		$result->validityEnd = $validityEndFix;
	}else{
		$result->validityEnd = $validityEnd1;
	}
	if (isset($result->photoName)){
			$photoName = pathinfo($result->photoName);
	}else{
		$photoName = 'nophoto';
	}
	$cardDesignDetail = '';
	$appDetail = '';
	for($element=0;$element<$result->designInfo->total;$element++){
		$info = 'info'.(string)($element+1);
		$content = 'content'.(string)($element+1);
		$cardDesignDetail .= $result->designInfo->$info;
		$cardDesignDetail .= '/iEnd/';
		$cardDesignDetail .= $result->designInfo->$content;
		$cardDesignDetail .= '/cEnd/';
		if(substr($result->designInfo->$content,0,6) ==='birth_'){
			$birthDateType = substr($result->designInfo->$content,6);
			$birthDateType = explode('_',$birthDateType);
			for($birthCount=0;$birthCount<count($birthDateType);$birthCount++){
				if($birthCount != 0){
					$appDetail .= '/';
				}else{}
				switch($birthDateType[$birthCount]){
					case 'DD':
						$day = decrypt1($result->birthDay, $result->encId);
						$appDetail .= $day;
						break;
					case 'MM':
						$month = decrypt1($result->birthMonth, $result->encId);
						if(strlen($month) < 2){
							$month = '0'.$month;
						}else{}
						$appDetail .= $month;
						break;
					case 'YY':
						$year = substr(decrypt1($result->birthYear, $result->encId),2);
						$appDetail .= $year;
						break;
					case 'YYYY':
						$year = decrypt1($result->birthYear, $result->encId);
						$appDetail .= $year;
						break;
					default:
						$appDetail .= 'ERROR';
						break;
				}
				
			}
		}else if(substr($result->designInfo->$content,0,7) ==='engName'){
			if($nameStyle1 === 'FL'){
				$appDetail .= decrypt1($result->engFname, $result->encId).' '.decrypt1($result->engLname, $result->encId);			
			}else if($nameStyle1 === 'LF'){
				$appDetail .= decrypt1($result->engLname, $result->encId).' '.decrypt1($result->engFname, $result->encId);			
			}else{
				echo 'ERROR';
			}						
		}else if(substr($result->designInfo->$content,0,9) ==='serialNum'){
			$cardSerialNum =' ';
			$appDetail .= $cardSerialNum;
		}else if(substr($result->designInfo->$content,0,9) ==='validity_'){
			$validityType = substr($result->designInfo->$content,9);
			switch($validityType){
				case 'END_MMYY':
					$appDetail .= substr($result->validityEnd,0,2).'/'.substr($result->validityEnd,5,2);
					break;
				case 'STARTEND_MMYYYY':
					$appDetail .= $result->validityStart.' ~ '.$result->validityEnd;
					break;
				default:
					$appDetail .= 'ERROR';
					break;
			}
		}else if(substr($result->designInfo->$content,0,5) === 'logo_'){
			//$appDetail .= $cardDesignDir.substr($result->designInfo->$content,5);
			$appDetail .= $cardDesignDir.$result->designInfo->designId.'/'.substr($result->designInfo->$content,5);
		}else if(substr($result->designInfo->$content,0,5) === 'fill_'){
				$appDetail .= $result->designInfo->$content;
		}else{
			switch($result->designInfo->$content){
				case 'engFname':
				case 'engLname':
				case 'name1':
				case 'birthYear':
				case 'birthMonth':
				case 'birthDay':
				case 'school':
				case 'extra1Value':
				case 'extra2Value':
				case 'extra3Value':
				case 'extra4Value':
				case 'extra5Value':
				case 'extra6Value':
				case 'extra7Value':
				case 'extra8Value':
				case 'extra9Value':
				case 'extra10Value':
					$contentOne = $result->designInfo->$content;
					$contentOne = decrypt1($result->$contentOne, $result->encId);	
					break;
				case 'photoName':
					$contentOne = $result->designInfo->$content;
					$contentOne = $result->$contentOne;
					$contentOne = read_img('1',$photoName['filename'],$photoName['extension']);
					break;
				default:
					$contentOne = $result->designInfo->$content;
					$contentOne = $result->$contentOne;
			}
			$appDetail .= $contentOne;
		}
		$appDetail .= '/aEnd/';
	}
	function read_img($index,$name,$ext){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}
	if(isset($result->appSettingInfo->payment) && $result->appSettingInfo->payment === 'yes'){
		//require __DIR__.$goParent.$reqDir1.'/_require1/appMailSend.php';
	}else{
	}

?>
<?php
	// local language;
	$useLocalLan= 'yes';
	$localLan ='loc';
	$localLanT ='locT';
	$section['sec1_1']['eng'] = 'Personal details';
	$section['sec1_1'][$localLan] = '';
	$section['sec1_1'][$localLanT] = '';
	$section['sec1_2']['eng'] = 'Payment';
	$section['sec1_2'][$localLan] = '';
	$section['sec1_2'][$localLanT] = '';
	$section['sec1_3']['eng'] = 'Finished';
	$section['sec1_3'][$localLan] = '';
	$section['sec1_3'][$localLanT] = '';
	$section['sec2']['eng'] = 'Your online application has been successfully registered!';
	$section['sec2'][$localLan] ='';
	$section['sec2'][$localLanT] ='';
	$section['sec3']['eng'] = 'You have entered the below information and your card will be issued like the image below, pending approval. <br/>Please make sure your name, date of birth, photo and school/ university/ college and photo are correct. <br/>If you need to amend something, please visit [<a href="/std/app/myid_main.php">My ID</a>] corner.';
	$section['sec3'][$localLan] ='';
	$section['sec3'][$localLanT] ='';
	$section['sec4']['eng'] = 'Do you see your photo image properly? (See card preview above.)';
	$section['sec4'][$localLan] ='';
	$section['sec4'][$localLanT] ='';
	$section['sec5']['eng'] = 'Do you think your photo is not appropriate for ID card? You can edit/change your photo in [<a href="/std/app/myid_main.php">My ID</a>] after login.';
	$section['sec5'][$localLan] ='';
	$section['sec5'][$localLanT] ='';
	$section['sec6']['eng'] = 'Please check the documents that you uploaded.';
	$section['sec6'][$localLan] ='';
	$section['sec6'][$localLanT] ='';
	$section['sec7']['eng'] = 'Personal Identification Document (National ID or Passport)';
	$section['sec7'][$localLan] ='';
	$section['sec8']['eng'] = 'Click';
	$section['sec8'][$localLan] ='';
	$section['sec9']['eng'] = 'Proof of student/teacher';
	$section['sec9'][$localLan] ='';
	$section['sec10']['eng'] = 'Please confirm your mailing data.';
	$section['sec10'][$localLan] ='';
	$section['sec10'][$localLanT] ='';
	$section['sec11']['eng'] = 'If you have any additional question about the issuance and shipment of your card please send us an e-mail to ';
	$section['sec11'][$localLan] ='';
	$section['sec11'][$localLanT] ='';
	$section['sec12']['eng'] = 'Finish';
	$section['sec12'][$localLan] ='';
	$section['sec12'][$localLanT] ='';
?>
<div id="wrap">
<div id="wrapIn">
  
  
	<?php 
		if($result->appSettingInfo->serialType === 'ISIC'){ 
	?>
	<!-- ISIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="images/ISIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">International Student <br class="mo">Identity Card<br>Online application</td>
			<td class="text-right"><img src="images/ISIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ISIC top -->
	<?php 
		} else if ($result->appSettingInfo->serialType === 'ITIC') { 
	?>
	<!-- ITIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="images/ITIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">International Teacher <br class="mo">Identity Card<br>Online application</td>
			<td class="text-right"><img src="images/ITIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ITIC top -->
	<?php
		} else { 
	?>
	<!-- /IYTC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="images/IYTC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">International Youth <br class="mo">Travel Card<br>Online application</td>
			<td class="text-right"><img src="images/IYTC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- IYTC top -->
	<?php
		}
	?>
	<?php if($appSettingInfo->payment === 'yes'){ ?>
	<h2 id="topTitleM">Step 3/3</h2>
	<div id="topTitleDiv">
		<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
			<tr>
				<td class="topTitleX"  style="width:33%;">
					<?php 
							$thisSection ='sec1_1';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</td>
				<td class="topTitleX">
					<?php 
							$thisSection ='sec1_2';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</td>
				<td class="topTitleO"  style="width:33%;"><!--Finished<br><span class="subEn"></span>-->
					<?php 
							$thisSection ='sec1_3';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</td>
			</tr>
		</table>
	</div>
	<?php } else { ?>
	<h2 id="topTitleM">Step 2/2</h2>
	<div id="topTitleDiv">
		<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
			<tr>
				<td class="topTitleX"  style="width:33%;">
					<?php 
							$thisSection ='sec1_1';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>					
				</td>
				<td class="topTitleO"  style="width:33%;"><!--Finished<br><span class="subEn"></span>-->
					<?php 
							$thisSection ='sec1_3';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<img src="<?php //echo read_img('1',$photoName['filename'],$photoName['extension']);?>">
	<div id="appTableDiv">
		<h3 style="text-align:center;">
			<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
			<!--Your online application has been successfully registered!-->
				<?php 
					$thisSection ='sec2';
					if($useLocalLan === 'yes') {
						
			?>
			<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
			<?php }else{?>
			<!--Your online application has been successfully registered!-->
				<?php 
						$thisSection ='sec2';
						if($useLocalLan === 'yes') {
							
				?>
				<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php } ?>
										   
		</h3>
		<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
		<p class="center mb5" align="center"><!--You have entered the below information and your card will be issued like the image below, pending approval. <br class="mx">Please make sure your name, date of birth, photo and school/ university/ college and photo are correct. <br class="mx">If you need to amend something, please visit “My ID” corner.-->
			<?php 
					$thisSection ='sec3';
					if($useLocalLan === 'yes') {
						
			?>
			<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
		</p>
		<?php }else{?>
		<p class="center mb5" align="center"><!--You have entered the below information and your card will be issued like the image below, pending approval. <br class="mx">Please make sure your name, date of birth, photo and school/ university/ college and photo are correct. <br class="mx">If you need to amend something, please visit “My ID” corner.-->
			<?php 
					$thisSection ='sec3';
					if($useLocalLan === 'yes') {
						
			?>
			<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
		</p>
		<?php } ?>
		<p class="center" style="line-height: 110%;"><span class="subEn subColor"></span></p>
		<input type="hidden" id="maxnum" value="1">
		<input type="hidden" id="prevUrl1" value="<?php echo $cardDesignDir.$result->designInfo->designId.'/'.$result->designInfo->bgImgName?>.jpg" size="100">
		<input type="hidden" id="prevTotal1" value="<?php echo $result->designInfo->total?>" size="100">
		<input type="hidden" id="prevDetail1" value="<?php echo $cardDesignDetail?>" size="100">
		<input type="hidden" id="prevAppDetail1" value="<?php echo $appDetail?>" size="100">
		<p class="center">
			<div class="preview_p mx">
				<div class="preview_pIn">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><!-- 505 317 -->
						<tr>
							<td style="text-align:center">
								<canvas id="prev1" style="border:0px solid #ccc;" width="540" height="331">HTML5 Support Required</canvas>
								
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="preview_m mo">
				<div class="preview_mIn">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- 260 163 -->
						<tr>
							<td style="text-align:center">
								<canvas id="prev1m" style="border:0px solid #ccc;" width="260" height="163">HTML5 Support Required</canvas>
							</td>
						</tr>
					</table>
			  </div>        
			</div>
		</p>
		<div style="width:95%; max-width:620px; padding:30px 0; margin:0 auto;">
			<p class="mb-5">
				<strong>
				<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
					<!--Do you see your photo image properly? (See card preview above.)-->
					<?php 
							$thisSection ='sec4';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php }else{?>
					<!--Do you see your photo image properly? (See card preview above.)-->
					<?php 
							$thisSection ='sec4';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php } ?>
				</strong>
				
				<br><span class="subColor subEn"></span>
			</p>
			<ul>
				<li class="link">			
				<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
					<!--Do you think your photo is not appropriate for ID card? You can update your photo in -->
					<?php 
							$thisSection ='sec5';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php }else{?>
					<!--Do you think your photo is not appropriate for ID card? You can update your photo in -->
					<?php 
							$thisSection ='sec5';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php } ?>	
				<a href="./myid_main.php">[My Card<i class="fa fa-caret-right" aria-hidden="true"></i>]</a> <span class="subEn">after login.</span>
				</li>
			</ul> 
			<p><span class="subColor subEn"></span></p>
			<p class="mt-20 mb-5">
				<strong>
				<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
					<!--Please check the documents that you uploaded.-->
					<?php 
							$thisSection ='sec6';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php }else{?>
					<!--Please check the documents that you uploaded.-->
					<?php 
							$thisSection ='sec6';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php } ?>
				</strong>
				<span class="subColor subEn"> </span>
			</p>      
			<ul>
				<?php if($result->appSettingInfo->underAge === 'yes'){?>
				<li class="link">Authorisation Declaration for children under 16 : <a href="./app03_dwd1.php" target="_blank">[Click]</a></li>
				<?php }else{} ?>
				<?php if($result->appSettingInfo->proofStatus === 'yes'){?>
				<li class="link"><!-- Proof of status document : -->
				<?php 
							$thisSection ='sec9';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<a href="./app03_dwd2.php" target="_blank">[<!--[Click]-->
					<?php 
								$thisSection ='sec8';
								if($useLocalLan === 'yes') {
									
						?>
						<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
				]</a></li>
				<?php }else{} ?>
				<?php if($result->appSettingInfo->proofBirth === 'yes'){?>
				<li class="link"><!--Proof of date of birth : -->
				<?php 
							$thisSection ='sec7';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
					<a href="./app03_dwd3.php" target="_blank">[<!--[Click]-->
						<?php 
								$thisSection ='sec8';
								if($useLocalLan === 'yes') {
									
						?>
						<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					]</a></li>
				<?php }else{} ?>
			</ul>           
			<p class="mt-20 mb-5">
				<strong>
				<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
				<!--Please confirm your mailing data.-->
					<?php 
							$thisSection ='sec10';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLanT]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php }else{?>
				<!--Please confirm your mailing data.-->
					<?php 
							$thisSection ='sec10';
							if($useLocalLan === 'yes') {
								
					?>
					<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				<?php } ?>
				</strong>
				<span class="subColor subEn"> </span>
			</p>
			<ul>
				<li>Address : <?php echo decrypt1($result->address, $result->encId)?> <?php echo decrypt1($result->address2, $result->encId) ?> <?php echo decrypt1($result->city, $result->encId) ?> <?php echo decrypt1($result->country, $result->encId) ?> (<?php echo decrypt1($result->postal, $result->encId) ?>)</li>
				<li>E-mail : <?php echo decrypt1($result->email, $result->encId)?></li>
			</ul>  
		</div>
		<div id="apBtBox">
			<p class="comTitle3 text-center">
			<?php if($result->appSettingInfo->serialType === 'ITIC'){ ?>
				<!--If you have any additional question about the issuance and shipment of your card please send us an e-mail to-->
				<?php 
						$thisSection ='sec11';
						if($useLocalLan === 'yes') {
							
				?>
				<?php echo $section[$thisSection][$localLanT]?> <br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php }else{?>
				<!--If you have any additional question about the issuance and shipment of your card please send us an e-mail to -->
				<?php 
						$thisSection ='sec11';
						if($useLocalLan === 'yes') {
							
				?>
				<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php } ?>	
				<a href="mailto:<?php echo $officeEmail?>"><?php echo $officeEmail?></a><br><span class="subEn subColor"></span>
																																																								 
	</div>
  
		<p class="buttonDiv">
			<a role="button" class="btn btn-isic btn-lg" href="<?php echo $thisSiteDomain?>" style="width:250px;" target="_parent">
				<?php 
						$thisSection ='sec12';
						if($useLocalLan === 'yes') {
							
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			</a>
		</p>
    
  
  </div>
  </div>
</body>
</html>
