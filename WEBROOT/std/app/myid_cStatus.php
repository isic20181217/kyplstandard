<?php 
// 200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;				
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent.$reqDir1.'/_require1/function.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/encDec.php';
	try {
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		for($i=1; $i < $validCount+1; $i++){
			$query = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE appNo=:searchValue1;";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchValue1', $_SESSION['validApp'][$i]);
			if($stmt->execute()){
				$appList[$i] = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else{
				echo '<script>alert(\'Forbidden.\');</script>';
				session_destroy();
				$db= NULL;
				echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
			$queryAppSetting = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			$stmtAppSetting->bindParam(':appSettingId', $appList[$i]->appSettingIdApp);
			$stmtAppSetting->execute();
			//echo $queryAppSetting;
			//echo $appList[$i]->cardType;
			//echo $stmtAppSetting->rowCount();
			if($stmtAppSetting->rowCount() === 1){
				$resultAppSetting = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				$appList[$i]->appSetting = $resultAppSetting;
			}else{
				echo 'Access Denied4';
				//print_r($stmtAppSetting->errorInfo());
				$db= NULL;
				//echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
			// Pay info area
			/*
			$queryMultibanco = "SELECT * FROM $tablename28 WHERE payAppNo = :searchValue1 AND payPayType = :searchValue2 order by payId DESC";
			$stmtMultibanco = $db->prepare($queryMultibanco);
			$stmtMultibanco->bindParam(':searchValue1', $_SESSION['validApp'][$i]);
			$payType = 'Multibanco';
			$stmtMultibanco->bindParam(':searchValue2', $payType);
			$stmtMultibanco->execute();
			if($stmtMultibanco->rowCount() > 0 ){
				$resultMultibanco = $stmtMultibanco->fetch(PDO::FETCH_OBJ);
				$appList[$i]->Multibanco = $resultMultibanco;
			}else{
			}
			$queryCreditcard = "SELECT * FROM $tablename28 WHERE payAppNo = :searchValue1 AND payPayType = :searchValue2 AND payLastFour IS NOT NULL order by payId DESC";
			$stmtCreditcard = $db->prepare($queryCreditcard);
			$stmtCreditcard->bindParam(':searchValue1', $_SESSION['validApp'][$i]);
			$payType = 'CreditCard';
			$stmtCreditcard->bindParam(':searchValue2', $payType);
			$stmtCreditcard->execute();
			if($stmtCreditcard->rowCount() > 0 ){
				$resultCreditcard = $stmtCreditcard->fetch(PDO::FETCH_OBJ);
				$appList[$i]->Creditcard = $resultCreditcard;
			}else{
			}			
			*/
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;
	}
	/*
	echo '<pre>';
	print_r($appList);
	echo '</pre>';
	*/
?>
<div id="appTableDiv">

	<div class="myidWrapDiv">
		<h3>Online application  <br class="mo"><span class="subEnH3"></span></h3>	
		<p class="myidSub">
			If you have registered your online application for ID issuance, you can check the registered application information, the proof of student or teacher approval status, the payment amount, and the payment status.
			<span class="subEn cGrey">()</span>
		</p>

		<?php for($i=1; $i < $validCount+1; $i++){ ?>
		<div class="appStatusDiv_outline">
			<?php if($appList[$i]->appSetting->serialType === 'ISIC'){ ?>
			<p class="appStatusTP"><img src="./images/ISIC_icon.png" class="idType_icon"> <?php echo  $appList[$i]->cardType ?><!--<button type="button" class="btn btn-default btn-xs dBtn">Remover</button>--></p>
			<?php }else if($appList[$i]->appSetting->serialType === 'ITIC'){ ?>
			<p class="appStatusTP"><img src="./images/ITIC_icon.png" class="idType_icon"> <?php echo  $appList[$i]->cardType ?><!--<button type="button" class="btn btn-default btn-xs dBtn">Remover</button>--></p>
			<?php }else if($appList[$i]->appSetting->serialType === 'IYTC'){ ?>
			<p class="appStatusTP"><img src="./images/IYTC_icon.png" class="idType_icon"><?php echo  $appList[$i]->cardType ?><!--<button type="button" class="btn btn-default btn-xs dBtn">Remover</button>--></p>
			<?php }else{} ?>
			<div class="appStatusDiv">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Application date <span class="mx"><br></span><small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8"> <?php echo  $appList[$i]->regDate ?></div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">ID validity<span class="mx"><br></span><small class="cGrey normal"></small> </label>
							<div class="col-xs-12 col-sm-8"> <?php echo  $appList[$i]->validityStart?> ~  <?php echo  $appList[$i]->validityEnd ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Delivery option<span class="mx"><br></span><small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8">
								<?php 
									if($appList[$i]->receiveType === $appList[$i]->appSetting->post1Name){ 
										echo $appList[$i]->appSetting->post1Desc.' '.$appList[$i]->appSetting->post1Price;
									}else if ($appList[$i]->receiveType === $appList[$i]->appSetting->post2Name){
										echo $appList[$i]->appSetting->post2Desc.' '.$appList[$i]->appSetting->post2Price;
									}else if ($appList[$i]->receiveType === $appList[$i]->appSetting->post3Name){
										echo $appList[$i]->appSetting->post3Desc.' '.$appList[$i]->appSetting->post3Price;
									}else if ($appList[$i]->receiveType === $appList[$i]->appSetting->post4Name){
										echo $appList[$i]->appSetting->post4Desc.' '.$appList[$i]->appSetting->post4Price;
									}else{}
								?>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Amount <span class="mx"><br></span><small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8"><?php echo $localCurrency?> <?php printf("%.2f",(float)$appList[$i]->cardPrice + (float)$appList[$i]->shippingPrice) ?><br><small>(Card price <?php echo $localCurrency?> <?php echo $appList[$i]->cardPrice ?> + Shipping price <?php echo $localCurrency?> <?php echo $appList[$i]->shippingPrice ?>)</small></div>
						</div>
					</div>
				</div>								
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Payment <span class="mx"><br></span><small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8">
								<?php if(isset($appList[$i]->payStatus) && isset($appList[$i]->proof4Name) && $appList[$i]->payStatus != 'paid'){?>
								<p>We are on checking your payment proof. <small></small></p>
								<?php }else if(isset($appList[$i]->payStatus) && $appList[$i]->payStatus === 'paid'){?>
								<p>Paid in full <small></small></p>
								<?php }else{ ?>
								<p>Not paid <small></small></p>					
								<hr>
								<p class="notPaid_comment">Please choose one of the payment methods under or, alternatively, make a bank transfer to  <?php echo $accountInfo1?>, and upload the proof. <br><small>()</small></p>
									<?php if (isset($appList[$i]->Multibanco->payEntity) && !empty($appList[$i]->Multibanco->payEntity) &&
												isset($appList[$i]->Multibanco->payReference) && !empty($appList[$i]->Multibanco->payReference) &&
												isset($appList[$i]->Multibanco->payValue) && !empty($appList[$i]->Multibanco->payValue)){
									?>
									<hr>
									<table class="table table-bordered">
										<tr>
											<th class="th_ho" colspan="2">Multibanco Payment Info</th>
										</tr>
										<tr>
											<th class="th_ho">Entity</th>
											<td><?php echo $appList[$i]->Multibanco->payEntity; ?></a>
											</td>
										</tr>
										<tr>
											<th class="th_ho">Reference</th>
											<td><?php echo $appList[$i]->Multibanco->payReference; ?></a>
											</td>
										</tr>
										<tr>
											<th class="th_ho">Value</th>
											<td><?php echo $localCurrency.' '; ?><?php echo $appList[$i]->Multibanco->payValue; ?></a>
											</td>
										</tr>
									</table>
									<?php }else { ?>
										<?php if ($appList[$i]->cardType === 'ISICPAYTEST1'){ ?>
											<a href="./paymentT1My_M.php?no=<?php echo $i?>&backMenu=status&startFrom=myId" class="btn btn-block btn-isic"><small>Multibanco<small class="mx"></small></small></a>
										<?php }else{?>
											<a href="./paymentT1My_M.php?no=<?php echo $i?>&backMenu=status&startFrom=myId" class="btn btn-block btn-isic"><small>Multibanco<small class="mx"></small></small></a>
										<?php } ?>
									<?php } ?>
									<hr>
									<?php if (isset($appList[$i]->Creditcard->payEntity) && !empty($appList[$i]->Creditcard->payEntity) &&
												isset($appList[$i]->Creditcard->payReference) && !empty($appList[$i]->Creditcard->payReference) &&
												isset($appList[$i]->Creditcard->payValue) && !empty($appList[$i]->Creditcard->payValue) &&
												isset($appList[$i]->Creditcard->payLastFour) && !empty($appList[$i]->Creditcard->payLastFour)
												){
									?>
									<p>We are on checking your payment proof. <small></small></p>									
									<?php }else { ?>
										<?php if ($appList[$i]->cardType === 'ISICPAYTEST1'){ ?>									
											<a href="./paymentT1My_C.php?no=<?php echo $i?>&backMenu=status&startFrom=myId" class="btn btn-block btn-isic"><small>Credit Card<small class="mx"></small></small></a>
										<?php }else{?>
											<a href="./paymentT1My_C.php?no=<?php echo $i?>&backMenu=status&startFrom=myId" class="btn btn-block btn-isic"><small>Credit Card<small class="mx"></small></small></a>
										<?php } ?>
									<?php }?>								
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Upload payment proof and new proof files<span class="mx"><br></span><small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8">
								<div class="row pb10">
									<?php if(isset($appList[$i]->proof4Name)){?>
										<a href="./myIdDocDownload.php?appNum=<?php echo $i?>&proof=4" target="_blank"><?php echo $proof4Setting ?></a>
									<?php }else{} ?>
									<div class="col-xs-5 pl3">
									<?php if($appList[$i]->status != 'approved'){ ?>
										<a href="./myid_main.php?menu=docUpload&no=<?php echo $i?>" class="btn btn-block btn-isic"><small>Proof Upload <small class="mx"></small></small></a>
									<?php }else{}?>
									</div>
								</div>
							</div>
						</div>
					</div>									
				</div>				
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4"><span class="mx"></span>Uploaded files <small class="cGrey normal"></small></label>
							<div class="col-xs-12 col-sm-8">
								<p>
									<?php if(isset($appList[$i]->proof1Name)){?>
										<a href="./myIdDocDownload.php?appNum=<?php echo $i?>&proof=1" target="_blank"><?php echo $proof1Setting ?></a>
									<?php }else{} ?>
									</br/>
									<?php if(isset($appList[$i]->proof2Name)){?>
										<a href="./myIdDocDownload.php?appNum=<?php echo $i?>&proof=2" target="_blank"><?php echo $proof2Setting ?></a>
									<?php }else{} ?>
									</br/>
									<?php if(isset($appList[$i]->proof3Name)){?>
										<a href="./myIdDocDownload.php?appNum=<?php echo $i?>&proof=3" target="_blank"><?php echo $proof3Setting ?></a>
									<?php }else{} ?>
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<label class="col-xs-12 col-sm-4">Card issuance <span class="mx"><br></span><small class="cGrey normal"></small></label>
							<?php if(isset($appList[$i]->cardSerialNum)){?>
							<div class="col-xs-12 col-sm-8">Completed <small></small></div>
							<?php }else if($appList[$i]->status === 'approved'){?>
							<div class="col-xs-12 col-sm-8">Approved<small></small></div>
							<?php }else{ ?>
							<div class="col-xs-12 col-sm-8">Not issued yet <small></small></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<?php } ?>	
		<ul class="myidSub_ul mb20">
		<li>If you registered online application but do not paid the fee, your application will be automatically deleted after 1 month from the date of registration.  <span class="subEn cGrey"></span></li>
		<li>The registered data will be kept during 1 month after the card expiration date unless you request data deletion. <span class="subEn cGrey"></span></li>
		</ul>
	</div>
</div>