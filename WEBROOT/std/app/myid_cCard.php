<script src="./js/myid_cCard.js"></script>
<?php 
//200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;				
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent.$reqDir1.'/_require1/function.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/encDec.php';
	try {
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		for($i=1; $i < $validCount+1; $i++){
			$query = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo LEFT JOIN $tablename20 ON $tablename07.appNo = $tablename20.revaAppNo LEFT JOIN $tablename24 ON $tablename07.appNo = $tablename24.extraAppNo WHERE appNo=:searchValue1";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchValue1', $_SESSION['validApp'][$i]);
			if($stmt->execute()){
				$appList[$i] = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else{
				echo '<script>alert(\'Forbidden.\');</script>';
				session_destroy();
				$db= NULL;
				echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
			$queryAppSetting = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			$stmtAppSetting->bindParam(':appSettingId', $appList[$i]->appSettingIdApp);
			$stmtAppSetting->execute();
			//echo $queryAppSetting;
			//echo $appList[$i]->cardType;
			//echo $stmtAppSetting->rowCount();
			if($stmtAppSetting->rowCount() === 1){
				$resultAppSetting = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				$appList[$i]->appSetting = $resultAppSetting;
			}else{
				echo 'Access Denied4';
				//print_r($stmtAppSetting->errorInfo());
				$db= NULL;
				//echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
			$queryCardDesign = "SELECT * FROM $tablename30 WHERE designId = :designId";
			$stmtCardDesign = $db->prepare($queryCardDesign);
			$stmtCardDesign->bindParam(':designId', $appList[$i]->appSetting->designId);
			$stmtCardDesign->execute();
			//echo $queryCardDesign;
			//echo $appList[$i]->cardType;
			//echo $stmtCardDesign->rowCount();
			if($stmtCardDesign->rowCount() === 1){
				$resultCardDesign = $stmtCardDesign->fetch(PDO::FETCH_OBJ);
				$appList[$i]->cardDesign = $resultCardDesign;
			}else{
				echo 'Access Denied4';
				//print_r($stmtCardDesign->errorInfo());
				$db= NULL;
				//echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}			
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;
	}
	function read_img($index,$name,$ext){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}
?>
<input type="hidden" id="maxnum" value="<?php echo $validCount; ?>">
<div id="appTableDiv">
				
	<div class="myidWrapDiv">
		<h3>MY Card <span class="subEnH3"></span></h3>						
		<p class="myidSub mb5">
			To protect your privacy, we do not provide the full ISIC·ITIC·IYTC number. The full number can be found on the ISIC App or on the physical card. 
			<span class="subEn cGrey"></span>
		</p>
		<ul class="myidSub_ul">
			<li>After you confirm your card issuance, download the Mobile ISIC App and activate the virtual ID to be prepared just in case your card is lost while traveling.<br class="mx"><span class="subEn"></span></li>
			<li><a href="https://www.isic.org/the-isic-global-app/" target="_blank">See more about ISIC App and Download <i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
		</ul>
		<div class="pt30 pb30">
		<?php for($i=1;$i<$_SESSION['validCount']+1;$i++){ ?>
		<?php if (isset($appList[$i]->cardSerialNum) || isset($appList[$i]->revaNum)){ ?>
		<?php
			$school = decrypt1($appList[$i]->school, $appList[$i]->encId);
			$birthDay = decrypt1($appList[$i]->birthDay, $appList[$i]->encId);
			$birthMonth = decrypt1($appList[$i]->birthMonth, $appList[$i]->encId);
			$birthYear = decrypt1($appList[$i]->birthYear, $appList[$i]->encId);
			$engFname = decrypt1($appList[$i]->engFname, $appList[$i]->encId);
			$engLname = decrypt1($appList[$i]->engLname, $appList[$i]->encId);
			if (isset($appList[$i]->photoName)){
					$photoName = pathinfo($appList[$i]->photoName);
			}else{
				$photoName = 'nophoto';
			}
			if(substr($appList[$i]->cardType,0,4) === 'REVA'){
				$cardSerialNum = substr($appList[$i]->revaNum, 0, 1).' '.substr($appList[$i]->revaNum, 1, 3).' '.substr($appList[$i]->revaNum, 4, 3).' '.substr($appList[$i]->revalNum, 7, 3).' '.substr($appList[$i]->revaNum, 10, 3).' '.substr($appList[$i]->revaNum, 13, 1);
			}else{
				$cardSerialNum = substr($appList[$i]->cardSerialNum, 0, 1).' '.substr($appList[$i]->cardSerialNum, 1, 3).' '.substr($appList[$i]->cardSerialNum, 4, 3).' '.substr($appList[$i]->cardSerialNum, 7, 3).' '.substr($appList[$i]->cardSerialNum, 10, 3).' '.substr($appList[$i]->cardSerialNum, 13, 1);
			}
			
			$cardDesignDetail = '';
			$appDetail = '';
			for($element=0;$element<$appList[$i]->cardDesign->total;$element++){
				$info = 'info'.(string)($element+1);
				$content = 'content'.(string)($element+1);
				$cardDesignDetail .= $appList[$i]->cardDesign->$info;
				$cardDesignDetail .= '/iEnd/';
				$cardDesignDetail .= $appList[$i]->cardDesign->$content;
				$cardDesignDetail .= '/cEnd/';
				if(substr($appList[$i]->cardDesign->$content,0,6) ==='birth_'){
					$birthDateType = substr($appList[$i]->cardDesign->$content,6);
					$birthDateType = explode('_',$birthDateType);
					for($birthCount=0;$birthCount<count($birthDateType);$birthCount++){
						if($birthCount != 0){
							$appDetail .= '/';
						}else{}
						switch($birthDateType[$birthCount]){
							case 'DD':
								$day = decrypt1($appList[$i]->birthDay, $appList[$i]->encId);
								$appDetail .= $day;
								break;
							case 'MM':
								$month = decrypt1($appList[$i]->birthMonth, $appList[$i]->encId);
								if(strlen($month) < 2){
									$month = '0'.$month;
								}else{}
								$appDetail .= $month;
								break;
							case 'YY':
								$year = substr(decrypt1($appList[$i]->birthYear, $appList[$i]->encId),2);
								$appDetail .= $year;
								break;
							case 'YYYY':
								$year = decrypt1($appList[$i]->birthYear, $appList[$i]->encId);
								$appDetail .= $year;
								break;
							default:
								$appDetail .= 'ERROR';
								break;
						}
						
					}
				}else if(substr($appList[$i]->cardDesign->$content,0,7) ==='engName'){
					if($nameStyle1 === 'FL'){
						$appDetail .= decrypt1($appList[$i]->engFname, $appList[$i]->encId).' '.decrypt1($appList[$i]->engLname, $appList[$i]->encId);			
					}else if($nameStyle1 === 'LF'){
						$appDetail .= decrypt1($appList[$i]->engLname, $appList[$i]->encId).' '.decrypt1($appList[$i]->engFname, $appList[$i]->encId);			
					}else{
						echo 'ERROR';
					}						
				}else if(substr($appList[$i]->cardDesign->$content,0,9) ==='serialNum'){
					$appDetail .= $cardSerialNum;
				}else if(substr($appList[$i]->cardDesign->$content,0,9) ==='validity_'){
					$validityType = substr($appList[$i]->cardDesign->$content,9);
					switch($validityType){
						case 'END_MMYY':
							$appDetail .= substr($appList[$i]->validityEnd,0,2).'/'.substr($appList[$i]->validityEnd,5,2);
							break;
						case 'STARTEND_MMYYYY':
							$appDetail .= $appList[$i]->validityStart.' ~ '.$appList[$i]->validityEnd;
							break;
						default:
							$appDetail .= 'ERROR';
							break;
					}
				}else if(substr($appList[$i]->cardDesign->$content,0,5) === 'logo_'){
					$appDetail .= $cardDesignDir.$designInfo->designId.'/'.substr($result->cardDesign->$content,5);
				}else{
					switch($appList[$i]->cardDesign->$content){
						case 'engFname':
						case 'engLname':
						case 'name1':
						case 'birthYear':
						case 'birthMonth':
						case 'birthDay':
						case 'school':
						case 'extra1Value':
						case 'extra2Value':
						case 'extra3Value':
						case 'extra4Value':
						case 'extra5Value':
						case 'extra6Value':
						case 'extra7Value':
						case 'extra8Value':
						case 'extra9Value':
						case 'extra10Value':
							$contentOne = $appList[$i]->cardDesign->$content;
							$contentOne = decrypt1($appList[$i]->$contentOne, $appList[$i]->encId);	
							break;
						case 'photoName':
							$contentOne = $appList[$i]->cardDesign->$content;
							$contentOne = $appList[$i]->$contentOne;
							$contentOne = read_img('1',$photoName['filename'],$photoName['extension']);
							break;
						default:
							$contentOne = $appList[$i]->cardDesign->$content;
							$contentOne = $appList[$i]->$contentOne;
					}
					$appDetail .= $contentOne;
				}
				$appDetail .= '/aEnd/';
			}
		?>		
            <div>
				<input type="hidden" id="prevUrl<?php echo $i?>" value="<?php echo $cardDesignDir.$appList[$i]->cardDesign->bgImgName?>.jpg" size="100">
				<input type="hidden" id="prevTotal<?php echo $i?>" value="<?php echo $appList[$i]->cardDesign->total?>" size="100">
				<input type="hidden" id="prevDetail<?php echo $i?>" value="<?php echo $cardDesignDetail?>" size="100">
				<input type="hidden" id="prevAppDetail<?php echo $i?>" value="<?php echo $appDetail?>" size="100">            		
				<!-- big table start -->
				<div class="preview_p mx">
					<div class="preview_pIn">

						<table width="100%" border="0" cellspacing="0" cellpadding="0"><!-- 505 317 -->
							<tr>
								<td>
									<canvas id="prev<?php echo $i?>" style="border:0px solid #ccc;" width="540" height="331">HTML5 Support Required</canvas>
									
								</td>
							</tr>
						</table>
				  </div>
				</div>
				<!--<canvas id="prev<?php //echo $i?>m" style="border:0px solid #ccc;" width="260" height="163">HTML5 Support Required</canvas>-->
				<!-- big table end --> 
				<!-- small table start -->
				<div class="preview_m mo">
					<div class="preview_mIn">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- 260 163 -->
							<tr>
								<td>
									<canvas id="prev<?php echo $i?>m" style="border:0px solid #ccc;" width="260" height="163">HTML5 Support Required</canvas>
								</td>
							</tr>
						</table>
				  </div>        
				</div>
				<!-- small table end -->   			
			</div>
			<?php } else {} // serial of revanum check end?>						
			<?php } // for loop ?>							
		</div>					
	</div>		
</div>

