<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Application 2</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="./css/bt.min.css" rel="stylesheet" type="text/css" />
<link href="./css/yoonCustom.css" rel="stylesheet" type="text/css" />
<link href="../css/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrap">
<div id="wrapIn">
<?php 
// 210526 add change PRice
///200615 check		
	session_start();
	if(isset($_SESSION['appInfo'])){
		unset($_SESSION['appInfo']);
	} else{}
	/*
	echo '<pre>';
	print_r($_SESSION);
	echo '</pre>';
	*/
	if(isset($_SESSION['currentAppStatus']) && $_SESSION['currentAppStatus'] === 'complete' ){
	}else{
		echo 'Access Denied1';
		exit;
	}
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent.'/req.php';
		require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename07 WHERE appNo =:appNo";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appNo', $_SESSION['currentApp']);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);

		}else{
			echo 'Access Denied4';
			$db= NULL;
			exit;
		}
		$query = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingIdApp AND activeApp = 'yes' AND payment ='yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appSettingIdApp', $result->appSettingIdApp);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$appSettingInfo = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}

		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}
	$now = time();
	$today1 = date("Y-m-d H:i:s",$now);
	// payment info packaging start
	$paymentKey = hash('sha256',$result->appNo.$today1);
	$_SESSION[$paymentKey] = new ArrayObject();
	$_SESSION[$paymentKey]->p_method = 'undefined';
	$_SESSION[$paymentKey]->no = $result->appNo;
	$_SESSION[$paymentKey]->p_goods = $appSettingInfo->appName;
	$p_price = (float)$result->cardPrice+(float)$result->shippingPrice-(float)$result->changePrice;
	$_SESSION[$paymentKey]->p_price= (string)$p_price;
	$_SESSION[$paymentKey]->p_rurl= $thisSiteDomain.'/std/app/app03.php';
	// payment info packaging end
	
	//echo '<pre>';
	//print_r($result);
	//echo '</pre>';

?>
<?php
	// local language;
	$useLocalLan= 'no';
	$localLan ='loc';
	$localLanT ='locT';
	$section['sec1_1']['eng'] = 'Personal details';
	$section['sec1_1'][$localLan] = '';
	$section['sec1_1'][$localLanT] = '';
	$section['sec1_2']['eng'] = 'Payment';
	$section['sec1_2'][$localLan] = '';
	$section['sec1_2'][$localLanT] = '';
	$section['sec1_3']['eng'] = 'Finished';
	$section['sec1_3'][$localLan] = '';
	$section['sec1_3'][$localLanT] = '';
	$section['sec1']['eng'] = 'Payment';
	$section['sec1'][$localLan] = '';
	$section['sec2']['eng'] = 'Card type';
	$section['sec2'][$localLan] ='';
	$section['sec2_1']['eng'] = 'Amount';
	$section['sec2_1'][$localLan] ='';
	if(isset($result->changePrice) && !empty($result->changePrice))
	{
		$section['sec2_2']['eng'] = "{$p_price} [Card price {$result->cardPrice} - Promotion {$result->changePrice} + Shipping price ({$localCurrency} {$result->shippingPrice} Shipping & processing fee)]";
	}
	else
	{
		$section['sec2_2']['eng'] = "{$p_price} [Card price {$result->cardPrice} + Shipping price ({$localCurrency} {$result->shippingPrice} Shipping & processing fee)]";
	}
	$section['sec2_2'][$localLan] ="";
	$section['sec3_1']['eng'] = 'Payment';
	$section['sec3_1'][$localLan] ='';
	$section['sec3_1'][$localLanT] ='';
	$section['sec3_2']['eng'] = 'Pay Now - Credit/Debit Card';
	$section['sec3_2'][$localLan] ='';
	$section['sec3_3']['eng'] = 'Pay Now - Virtual Account';
	$section['sec3_3'][$localLan] ='';
	$section['sec3_4']['eng'] = 'Pay later - Pay after student proof approval';
	$section['sec3_4'][$localLan] ='';
	$section['sec4_1']['eng'] = 'Credit/Debit Card';
	$section['sec4_1'][$localLan] ='';
	$section['sec4_2']['eng'] = 'Virtual Account';
	$section['sec4_2'][$localLan] ='';
	$section['sec4_3']['eng'] = 'Pay later';
	$section['sec4_3'][$localLan] ='';
	$section['sec5']['eng'] = '';
	$section['sec5'][$localLan] ='';
?>
<?php 
	switch($result->serialTypeApp)
	{
		case 'ISIC':
?>
	<!-- ISIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/ISIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">ISIC Online application</td>
			<td class="text-right"><img src="/std/app/images/ISIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ISIC top -->
<?php 
			break;
?>
<?php 
		case 'ITIC':
?>
	<!-- ITIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/ITIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">ITIC Online application</td>
			<td class="text-right"><img src="/std/app/images/ITIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ITIC top -->
<?php 
			break;
?>
<?php 
		case 'IYTC':
?>
	<!-- IYTC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/IYTC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">IYTC Online application</td>
			<td class="text-right"><img src="/std/app/images/IYTC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /IYTC top -->
<?php 
			break;
?>
<?php
		default:
			echo 'Access Denied30';
			exit;	
	}
?>
	<h2 id="topTitleM">Step 2/3</h2>
	<div id="topTitleDiv">
		<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
			<tr>
				<td class="topTitleX"  style="width:33%;"><!--Personal details-->
					<?php 
						$thisSection ='sec1_1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>					
				</td>
				<td class="topTitleO">
					<?php 
						$thisSection ='sec1_2';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
				<td class="topTitleX"  style="width:33%;">
					<?php 
						$thisSection ='sec1_3';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
			</tr>
		</table>
	</div>
        
	<div id="appTableDiv">
		<h3 style="margin-bottom:20px;"><!--Payment <span class="subEn subColor"></span>-->
			<?php 
				$thisSection ='sec1';
				if($useLocalLan === 'yes') {
					
			?>
			<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
		</h3>
		<table class="table table-bordered">
			<tr>
				<th width="30%" class="th_ho"><!--Card type <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec2';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td><?php echo $result->appNameApp?></td>
			</tr>
			<tr>
				<th class="th_ho">
					<?php 
						$thisSection ='sec2_1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</span>
				</th>
				<td>
					<?php 
						$thisSection ='sec2_2';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor" subColor><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
					<!--<?php //echo $localCurrency?> <?php //echo (float)$result->cardPrice+(float)$result->shippingPrice ?> (card price <?php //echo $localCurrency?><?php //echo $result->cardPrice ?> + shipping price <?php //echo $localCurrency?><?php //echo $result->shippingPrice ?>)-->
				</td>
			</tr>
			<tr>
				<th class="th_ho"><!--Payment<br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec3_1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<?php if ($result->cardType === 'ISICPAYTEST1'){ ?>
					<p>
						<ul class="m0">
							<?php 
								$thisSection ='sec3_2';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
							<?php 
								$thisSection ='sec3_3';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
							<?php 
								$thisSection ='sec3_4';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
						</ul>
					</p>
					<?php }else{ ?>
					<p>
				
												 
	 
												   
	
			  
				
							
	 
							
	
   
  
						<ul class="m0">
							<?php 
								$thisSection ='sec3_2';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
							<?php 
								$thisSection ='sec3_3';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
							<?php 
								$thisSection ='sec3_4';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
						</ul>
					</p>
					<?php } ?>
				</td>
			</tr>
		</table>
		<?php 
		if ($result->cardType === 'ISICPAYTEST1'){ 
		?> 
		<p class="buttonDiv">
			<a role="button" class="btn btn-isic btn-lg" href="./paymentApp_E.php?no=currentApp&backMenu=app03&startFrom=app&pKey=<?php echo $paymentKey?>&pT=1" style="width:300px;">
				<?php 
					$thisSection ='sec4_1';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span></a>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?></a>
				<?php 
					}
				?>
			</a>
		</p>
		<p class="buttonDiv">
			<a role="button" class="btn btn-isic btn-lg" href="./paymentApp_E.php?no=currentApp&backMenu=app03&startFrom=app&pKey=<?php echo $paymentKey?>&pT=2" style="width:300px;">
				<?php 
					$thisSection ='sec4_2';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span></a>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?></a>
				<?php 
					}
				?>
			</a>
		</p>
		<?php 
		}else{
		?>
		<p class="buttonDiv">
			<a role="button" class="btn btn-isic btn-lg" href="./paymentApp_E.php?no=currentApp&backMenu=app03&startFrom=app&pKey=<?php echo $paymentKey?>&pT=1" style="width:300px;">
				<?php 
					$thisSection ='sec4_1';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span></a>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?></a>
				<?php 
					}
				?>
			</a>
		</p>
		<p class="buttonDiv">
			<a role="button" class="btn btn-isic btn-lg" href="./paymentApp_E.php?no=currentApp&backMenu=app03&startFrom=app&pKey=<?php echo $paymentKey?>&pT=2" style="width:300px;">
				<?php 
					$thisSection ='sec4_2';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span></a>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?></a>
				<?php 
					}
				?>
			</a>
		</p>
		<?php 
		} 
		?>
		<p class="buttonDiv"><a role="button" class="btn btn-isic btn-lg" href="./app03.php" style="width:300px;"><!--Next <small></small>-->
			<?php 
				$thisSection ='sec4_3';
				if($useLocalLan === 'yes') {
					
			?>
			<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span></a>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?></a>
			<?php 
				}
			?>
			</p>
	</div>  
  
  </div>
  </div>
</body>
</html>
