<?php
// 210629 change xlsx to .xlsx
// 210626 add get appSettingIdApp from table process
// 200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;				
	}
	if(isset($_GET['no'])){
		$appNum = $_GET['no'];
		if (isset($_SESSION['validApp'][$appNum])){
			$appNo = $_SESSION['validApp'][$appNum];
		}
		else{		
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;		
	}
	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent.'/req.php';
		require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT appNo,cardType,appSettingIdApp FROM $tablename07 WHERE appNo=:searchValue1 AND status!='approved';";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if($stmt->execute() && $stmt->rowCount() != 0){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$queryAppSetting = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			$stmtAppSetting->bindParam(':appSettingId', $result->appSettingIdApp);
			$stmtAppSetting->execute();
			if($stmtAppSetting->rowCount() === 1){
				$resultAppSetting = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				$result->appSetting = $resultAppSetting;
			}else{
				echo 'Access Denied4';
				//print_r($stmtAppSetting->errorInfo());
				$db= NULL;
				//echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
		}
		else{
			echo "<script> window.history.go(-1); </script>";
			echo '<script>alert(\'no\');</script>';
			$db= NULL;
			exit;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}	
?>     
<script type="text/javascript" src="./js/myid_cDocUpload.js"></script>
<!-- Start contents -->
<form id="proofChangeForm" name="proofChangeForm" action="" method="POST"  enctype="multipart/form-data">
	<input type="hidden" name="appNum" id="appNum" value="<?php echo $appNum; ?>">
	<input type="hidden" name="pageStatus" id="pageStatus" value="proofChangeForm">
	<input type="hidden" id="serialType" name="serialType" value="<?php echo $result->appSetting->serialType ?>"> 
	<div id="appTableDiv"> 					
		<div class="myidWrapDiv">
			<h3>File upload</h3>
			<table class="table table-bordered mt-20">
				<?php if($result->appSetting->underAge === 'yes'){?>
				<tr>
					<th class="th_ho" width="30%"><br><span class="enSub"><?php echo $proof1Setting?></span></th>
					<td>
					<p><input type="file" name="proof1Name" id="proof1Name"accept=".jpeg, .jpg, .png, .pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx" class="form-control"></p>											
					</td>
				</tr>
				<?php }else{}?>
				<?php if($result->appSetting->proofStatus === 'yes'){?>
				<tr>
					<th class="th_ho"><br><span class="enSub"><?php echo $proof2Setting?></span></th>
					<td>
					<p><input type="file" name="proof2Name" id="proof2Name" accept=".jpeg, .jpg, .png, .pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx" class="form-control"></p>
					</td>
				</tr>
				<?php }else{}?>
				<?php if($result->appSetting->proofBirth === 'yes'){?>
				<tr>
					<th class="th_ho"><br><span class="enSub"><?php echo $proof3Setting?></span></th>
					<td>
					<p><input type="file" name="proof3Name" id="proof3Name"accept=".jpeg, .jpg, .png, .pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx" class="form-control"></p>
					</td>
				</tr>
				<?php }else{}?>
				<?php if($result->appSetting->payment === 'yes'){?>
				<tr>
					<th class="th_ho"><br><span class="enSub"><?php echo $proof4Setting?></span></th>
					<td>
					<p><input type="file" name="proof4Name" id="proof4Name"accept=".jpeg, .jpg, .png, .pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx" class="form-control"></p>
					</td>
				</tr>
				<?php }else{}?>
			</table>
							
			<p class="pt-30 text-center">
				<button type="button" class="btn btn-lg btn-isic" onclick="submit01(1)">
					<span class="enSub">Upload </span> 
					<i class="fa fa-caret-right"></i>
				</button>
			</p>

		</div>
	</div>
</form>
        <!-- End contents -->

