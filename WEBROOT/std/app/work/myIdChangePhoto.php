<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>changeimg</title>
<?php 
// 210630 change file_exists to is_file
// 210629 add remove old file process
// 210625 add docuDir process
// 200602 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		
		//if($validCount >= 1 && $_SESSION['myIdUserIP'] === $userIP){
		if($validCount >0){
		}
		else{
			echo '<script>alert(\'Forbidden1.\');</script>';
			session_destroy();
			echo '<script>location.replace("/twn/twapp/myid_login.php");</script>';
			exit;				
		}
	}
	else{
		echo '<script>alert(\'Forbidden2.\');</script>';
		session_destroy();
		echo '<script>location.replace("/twn/twapp/myid_login.php");</script>';
		exit;				
	}
	if(isset($_POST['appNum'])){
		$appNum = $_POST['appNum'];
		if (isset($_SESSION['validApp'][$appNum])){
			$appNo = $_SESSION['validApp'][$appNum];
		}
		else{		
			echo '<script>alert(\'Forbidden3.\');</script>';
			session_destroy();
			echo '<script>location.replace("/twn/twapp/myid_login.php");</script>';
			exit;
		}
	}
	else{
		echo '<script>alert(\'Forbidden4.\');</script>';
		session_destroy();
		echo '<script>location.replace("/twn/twapp/myid_login.php");</script>';
		exit;		
	}
	if (isset($_POST['pageStatus'])){
		$pageStatus = $_POST['pageStatus'];
	}
	else{
		echo '<script>alert(\'Forbidden5.\');</script>';
		session_destroy();
		echo '<script>location.replace("/twn/twapp/myid_login.php");</script>';
		exit;
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	require __DIR__.$goParent2.'/req.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/setting.php';
	require __DIR__.$goParent2.$reqDir1.'/_require1/function.php';	
	require __DIR__.$goParent2.$reqDir1.'/_require1/encDec.php';	
	$uploaddir = __DIR__.$goParent2.$reqDir1.$storageDir;
	$photoName = 'photoName';
	$proof1Name = 'proof1Name';
	$proof2Name = 'proof2Name';
	$proof3Name = 'proof3Name';
	$proof4Name = 'proof4Name';
	if ($pageStatus === 'photoUploadForm'){
		try {
			require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
			$query = "SELECT countryId,officeId,appNo,docuDir FROM $tablename07 WHERE appNo = :searchValue1 AND status != 'approved';";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchValue1', $appNo);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			if(!$result){
				echo "<script> window.history.go(-1); </script>";
				echo '<script>alert(\'Error1\');</script>';
				$db= NULL;
				exit;
			}
			else {
				if(isset($result->docuDir) && !empty($result->docuDir))
				{
					$uploaddir = $uploaddir.$result->docuDir;
					if(!is_dir($uploaddir))
					{
						mkdir($uploaddir,0777,true);
					}
					else
					{
						echo 'exist';
					}
				}
				else
				{}
				$newfilename = $result->countryId.'_'.$result->officeId.'_'.$result->appNo;
				upload2($photoName,$newfilename, $uploaddir);
				$newfilename0 = $newfilename.'.jpg';
				$query = "UPDATE $tablename07 SET photoName = '$newfilename0', modifiedDate =now(), modifiedBy=:inputValue1";
				$query .=" WHERE appNo=:inputValue100 AND status != 'approved';";
				$stmt = $db->prepare($query);
				$stmt->bindParam(':inputValue1', $userIP);
				$stmt->bindParam(':inputValue100', $appNo);
				$stmt->execute();
				$db= NULL;	
				//$target ="../myid_pInfo.php"
				echo '<script>location.replace("/std/app/myid_main.php?menu=pInfo");</script>';;
			}
		}
		catch (PDOExeception $e){
			//echo "Error: ".$e->getMessage();
			echo "<script> window.history.go(-1); </script>";
			echo '<script>alert(\'INSERT error2\');</script>';
			$db= NULL;
			exit;
		}
	}
	else if($pageStatus === 'proofChangeForm'){
		try {
			require __DIR__.$goParent2.$reqDir1.'/_require1/db_co.php';
			$query = "SELECT countryId,officeId,appNo,docuDir, proof1Name, proof2Name,proof3Name,proof4Name FROM $tablename07 WHERE appNo = :searchterm1 AND status != 'approved';";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchterm1', $appNo);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			if(!$result){
				echo "<script> window.history.go(-1); </script>";
				echo '<script>alert(\'Error3\');</script>';
				$db= NULL;
				exit;
			}
			else {
				$newfilename = $result->countryId.'_'.$result->officeId.'_'.$result->appNo;
			}
			if(isset($result->docuDir) && !empty($result->docuDir))
			{
				$uploaddir = $uploaddir.$result->docuDir;
			}
			else
			{}
			if(isset($result->proof1Name) && !empty($result->proof1Name))
			{
				$proof1NameOld = pathinfo($result->proof1Name);		
				$proof1NameOldExt = $proof1NameOld['extension'];
				$proof1NameOldExt = strtolower($proof1NameOldExt);
			}
			else
			{}
			if(isset($result->proof2Name) && !empty($result->proof2Name))
			{
				$proof2NameOld = pathinfo($result->proof2Name);
				$proof2NameOldExt = $proof2NameOld['extension'];
				$proof2NameOldExt = strtolower($proof2NameOldExt);
			}
			else
			{}
			if(isset($result->proof3Name) && !empty($result->proof3Name))
			{
				$proof3NameOld = pathinfo($result->proof3Name);
				$proof3NameOldExt = $proof3NameOld['extension'];
				$proof3NameOldExt = strtolower($proof3NameOldExt);
			}
			else
			{}
			if(isset($result->proof4Name) && !empty($result->proof4Name))
			{
				$proof4NameOld = pathinfo($result->proof4Name);
				$proof4NameOldExt = $proof4NameOld['extension'];
				$proof4NameOldExt = strtolower($proof4NameOldExt);
			}
			else
			{}
			$query = "UPDATE $tablename07 SET modifiedDate =now(), modifiedBy=:inputValue1";
			if(isset($_FILES[$proof1Name]) && $_FILES[$proof1Name]['tmp_name']){
				uploadFile2($proof1Name,$newfilename.'_1.',$uploaddir);
				echo 'proof1';
				$name = $_FILES[$proof1Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof1Name='{$newfilename}_1.{$ext}'";
				echo $proof1NameOldExt.' '.$ext;
				if(isset($proof1NameOldExt) && $proof1NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $uploaddir.$result->proof1Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($uploaddir.$result->proof1Name);	
						echo ' delete old proof1 ';
					}else{}
				}
			}else{}
			if(isset($_FILES[$proof2Name]) && $_FILES[$proof2Name]['tmp_name']){
				uploadFile2($proof2Name,$newfilename.'_2.',$uploaddir);
				echo 'proof2';
				$name = $_FILES[$proof2Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof2Name='{$newfilename}_2.{$ext}'";
				echo $proof2NameOldExt.' '.$ext;
				if(isset($proof2NameOldExt) && $proof2NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $uploaddir.$result->proof2Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($uploaddir.$result->proof2Name);	
						echo ' delete old proof1 ';
					}else{}
				}
			}else{}
			if(isset($_FILES[$proof3Name]) && $_FILES[$proof3Name]['tmp_name']){
				uploadFile2($proof3Name,$newfilename.'_3.',$uploaddir);
				echo 'proof3';
				$name = $_FILES[$proof3Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof3Name='{$newfilename}_3.{$ext}'";
				echo $proof3NameOldExt.' '.$ext;
				if(isset($proof3NameOldExt) && $proof3NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $uploaddir.$result->proof3Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($uploaddir.$result->proof3Name);	
						echo ' delete old proof3 ';
					}else{}
				}
			}else{}
			if(isset($_FILES[$proof4Name]) &&  $_FILES[$proof4Name]['tmp_name']){
				uploadFile2($proof4Name,$newfilename.'_4.',$uploaddir);
				echo 'proof4';
				$name = $_FILES[$proof4Name]['name'];	
				$orifilename = pathinfo($name);
				$ext = $orifilename['extension'];
				$ext = strtolower($ext);
				$query .= ", proof4Name='{$newfilename}_4.{$ext}'";
				echo $proof4NameOldExt.' '.$ext;
				if(isset($proof4NameOldExt) && $proof4NameOldExt === $ext)
				{}
				else
				{
					$path = '';	
					$path = $uploaddir.$result->proof4Name;
					$isFileExists = is_file($path);
					if($isFileExists){
						unlink($uploaddir.$result->proof4Name);	
						echo ' delete old proof4 ';
					}else{}
				}
			}else{}
			$query .=" WHERE appNo=:inputValue100 AND status != 'approved';";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':inputValue1', $userIP);
			$stmt->bindParam(':inputValue100', $appNo);		
			if($stmt->execute()){
				//$target ="../myid_appStatus.php";
				$db= NULL;		
				echo '<script>location.replace("/std/app/myid_main.php?menu=status");</script>';
			}
			else {
				echo '<script>location.replace("/std/app/myid_main.php?menu=status");</script>';
				echo '<script>alert(\'Error4\');</script>';
				$db= NULL;
				exit;
			}
		}
		catch (PDOExeception $e){
			//echo "Error: ".$e->getMessage();
			echo '<script>location.replace("/std/app/myid_main.php?menu=status");</script>';
			echo '<script>alert(\'INSERT error5\');</script>';
			$db= NULL;
			exit;
		}
	}
	else{
		echo "<script> window.history.go(-1); </script>";
		echo '<script>alert(\'error6\');</script>';
		$db= NULL;
		exit;
	}
?>
<script>
	window.onload = function(){
		//alert('hello');
		//document.forms['reload'].submit();
	}
</script>
</head>
<body>
	<form name="reload" action="<?php //echo $target ?>" method="GET"> 
		<input type="hidden" name="no" value="<?php //echo $appNum; ?>">
		<input type="submit">
	</form> 
</body>
</html>

