<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Application 1</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/std/app/css/bt.min.css" rel="stylesheet" type="text/css" />
<link href="/std/app/css/yoonCustom.css" rel="stylesheet" type="text/css" />
<!--<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet" type="text/css" />-->
<link href="/std/css/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="#" />
<script type="text/javascript" src="/std/app/js/app01.js?time=16"></script>
</head>

<body>
	<div id="wrap">
	<div id="wrapIn">
<?php
// 201113 check
	session_start();
	$_SESSION['currentApp'] = NULL;
	$_SESSION['currentAppSetting'] = NULL;
	$_SESSION['currentAppStatus'] = NULL;
	$_SESSION['currentAppMail'] = NULL;
	if(isset($_GET['appSet']) && !empty($_GET['appSet'])){
		$url = preg_replace("/<|\/|_|>/","", $_GET['appSet']);
	}else{
		echo 'Access Denied1';
		exit;
	}

	try {
		$goParent ='/..';
		$goParent2 ='/../..';
		$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
		require __DIR__.$goParent.'/req.php';
		require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT * FROM $tablename23 WHERE url = :url AND activeApp = 'yes'";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':url', $url);
		$stmt->execute();
		if($stmt->rowCount() === 1){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
		}else{
			echo 'Access Denied2';
			$db= NULL;
			exit;
		}
		if(isset($_GET['set1']) && !empty($_GET['set1']))
		{
			$officeId = preg_replace("/<|\/|_|>/","", $_GET['set1']);
			
			$query = "SELECT * FROM $tablename29 WHERE officeId =:officeId AND appSettingId =:appSettingId";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':officeId', $officeId);
			$stmt->bindParam(':appSettingId', $result->appSettingId);
			$stmt->execute();
			if($stmt->rowCount() === 1)
			{}
			else
			{
				//echo $officeId;
				//echo $result->appSettingId;
				//echo $query;
				//print_r($stmt->errorInfo());
				echo 'Access Denied5';
				$db= NULL;
				exit;
			}
			$query = "SELECT officeId FROM $tablename12 WHERE officeId =:officeId AND activeOffice =:activeOffice AND canUrl =:canUrl";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':officeId', $officeId);
			$activeOffice ='yes';
			$canUrl ='yes';
			$stmt->bindParam(':activeOffice', $activeOffice);
			$stmt->bindParam(':canUrl', $canUrl);
			$stmt->execute();
			if($stmt->rowCount() === 1)
			{}
			else
			{
				echo 'Access Denied6';
				$db= NULL;
				exit;
			}
		}
		else
		{
			///$officeId = preg_replace("/<|\/|_|>/","", $_GET['set1']);
			
			$query = "SELECT * FROM $tablename29 WHERE appSettingId =:appSettingId";
			$stmt = $db->prepare($query);
			//$stmt->bindParam(':officeId', $officeId);
			$stmt->bindParam(':appSettingId', $result->appSettingId);
			$stmt->execute();
			if($stmt->rowCount() === 1)
			{
				$resultOfficeId = $stmt->fetch(PDO::FETCH_OBJ);
				$officeId = $resultOfficeId->officeId;
				$query = "SELECT officeId FROM $tablename12 WHERE officeId =:officeId AND activeOffice =:activeOffice AND canUrl =:canUrl";
				$stmt = $db->prepare($query);
				$stmt->bindParam(':officeId', $officeId);
				$activeOffice ='yes';
				$canUrl ='yes';
				$stmt->bindParam(':activeOffice', $activeOffice);
				$stmt->bindParam(':canUrl', $canUrl);
				$stmt->execute();
				if($stmt->rowCount() === 1)
				{}
				else
				{
					echo 'Access Denied6';
					$db= NULL;
					exit;
				}
			}
			else
			{
				$officeId = 'main';
			}

			
		}		

		$query = "SELECT * FROM $tablename25 WHERE extraAppSettingId = :extraAppSettingId";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':extraAppSettingId', $result->appSettingId);
		$stmt->execute();
		//echo $query;
		if($stmt->rowCount() >0 ){
			$extraCount = $stmt->rowCount();
			$i = 0;
			while($result2 = $stmt->fetch(PDO::FETCH_OBJ)){
				$extraList[$i] = $result2;
				$i++;
			}
		}else{
			//echo 'noextra';
			$extraCount =0;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		exit;
	}

	//echo '<pre>';
	//print_r($result);
	//echo '</pre>';
	/*
	echo '<pre>';
	print_r($extraList);
	echo '</pre>';
	*/
?>
<?php
	// local language;
	$useLocalLan= 'no';
	$localLan ='loc';
	$localLanT ='locT'; // language for ITIC
	$section['sec1']['eng'] = 'Personal details';
	$section['sec1'][$localLan] = '';
	$section['sec1'][$localLanT] = '';
	$section['sec1_1']['eng'] = 'Payment';
	$section['sec1_1'][$localLan] = '';
	$section['sec1_1'][$localLanT] = '';
	$section['sec1_2']['eng'] = 'Finished';
	$section['sec1_2'][$localLan] = '';
	$section['sec1_2'][$localLanT] = '';
	$section['sec2']['eng'] = 'Please enter your information for ID issuance.';
	$section['sec2'][$localLan] ='';
	$section['sec2'][$localLanT] ='';
	$section['sec3']['eng'] = 'First name in English';
	$section['sec3'][$localLan] ='';
	$section['sec4']['eng'] = 'Last name English';
	$section['sec4'][$localLan] ='';
	$section['sec5']['eng'] = 'Password for MY ID login';
	$section['sec5'][$localLan] ='';
	$section['sec6']['eng'] = 'Please enter a 6-digit number.';
	$section['sec6'][$localLan] ='';
	$section['sec6_1']['eng'] = 'You need to remember this password in order to login to MY ID for check your application and ID issuing status.';
	$section['sec6_1'][$localLan] ='';
	$section['sec7']['eng'] = 'Gender';
	$section['sec7'][$localLan] ='';
	$section['sec8']['eng'] = 'Date of birth';
	$section['sec8'][$localLan] ='';
	$section['sec9']['eng'] = 'Day';
	$section['sec9'][$localLan] ='';
	$section['sec10']['eng'] = 'Month';
	$section['sec10'][$localLan] ='';
	$section['sec11']['eng'] = 'Year';
	$section['sec11'][$localLan] ='';
	$section['sec12']['eng'] = 'Authorisation Declaration for minors of 16 years';
	$section['sec12'][$localLan] ='';
	$section['sec13']['eng'] = 'According to the new General Data Protection Regulation, the processing of personal data of all the under 16 years old minors is only possible with the consent of the legal holder of parental responsibility. In this regard, to continue with your ISIC application, we ask you to please upload the Authorisation Declaration for minors of 16 years.';
	$section['sec13'][$localLan] ='';
	$section['sec14']['eng'] = 'ID card Photo';
	$section['sec14'][$localLan] ='照片';
	$section['sec15']['eng'] = 'Please upload your headshot photo. Passport photo is recommended.';
	$section['sec15'][$localLan] ='';
	$section['sec15'][$localLanT] =''; 
	$section['sec16']['eng'] = 'File name should be number or english. And the photo file shoul be JPG or PNG image file';
	$section['sec16'][$localLan] ='';
	$section['sec17']['eng'] = 'When issuing the card, if the photo does not show your face well, it can be edited according to the regulations';
	$section['sec17'][$localLan] ='';
	$section['sec17_1']['eng'] = 'Please check below to upload your photo and documents successfully.';
	$section['sec17_1'][$localLan] ='';
	$section['sec17_2']['eng'] = 'For security reason, uploading files with the name of 2 alphabets + 5-digit number is recommended.';
	$section['sec17_2'][$localLan] ='';
	$section['sec17_3']['eng'] = 'Please upload image files in JPG, and the files will be uploaded securely when each file is smaller than 500KB.';
	$section['sec17_3'][$localLan] ='';
	$section['sec18']['eng'] = 'Preview';
	$section['sec18'][$localLan] ='';
	$section['sec19']['eng'] = 'You can edit the uploaded photo';
	$section['sec19'][$localLan] ='';
	$section['sec20']['eng'] = 'Click the left top point that you want to start crop.';
	$section['sec20'][$localLan] ='';
	$section['sec20'][$localLanT] ='';
	$section['sec21']['eng'] = 'Click the right bottom point that you want to end crop.';
	$section['sec21'][$localLan] ='';
	$section['sec21'][$localLanT] ='';
	$section['sec22']['eng'] = 'Click the [Crop] button.';
	$section['sec22'][$localLan] ='';
	$section['sec22'][$localLanT] =' ';
	$section['sec22_1']['eng'] = 'Rotate Left';
	$section['sec22_1'][$localLan] ='';
	$section['sec22_2']['eng'] = 'Rotate Right';
	$section['sec22_2'][$localLan] ='';
	$section['sec22_3']['eng'] = 'Crop';
	$section['sec22_3'][$localLan] ='';
	$section['sec22_4']['eng'] = 'Reset';
	$section['sec22_4'][$localLan] ='';
	$section['sec23']['eng'] = 'Studies at';
	$section['sec23'][$localLan] ='';
	$section['sec24']['eng'] = 'Teaches at';
	$section['sec24'][$localLanT] ='';
	$section['sec25_1']['eng'] = 'Proof of student/teacher';
	$section['sec25_1'][$localLan] ='';
	$section['sec25_2']['eng'] = 'For more information regarding what kind of documents is accepted , Please refer to <a href="/home/en/faq.html" target=_blank>FAQ</a>.';
	$section['sec25_2'][$localLan] ='';
	$section['sec25']['eng'] = 'File name should be in number or English. File should be JPG or PNG image file.';
	$section['sec25'][$localLan] ='';
	$section['sec26']['eng'] = 'Personal Identification Document (National ID or Passport)';
	$section['sec26'][$localLan] ='';
	$section['sec27']['eng'] = 'File name should be in number or English. File should be JPG or PNG image file.';
	$section['sec27'][$localLan] ='';
	$section['sec28']['eng'] = 'Address';
	$section['sec28'][$localLan] ='';
	$section['sec29']['eng'] = 'Address2';
	$section['sec29'][$localLan] ='';
	$section['sec30']['eng'] = 'City';
	$section['sec30'][$localLan] ='';
	$section['sec31']['eng'] = 'Country';
	$section['sec31'][$localLan] ='';
	$section['sec32']['eng'] = 'Postal code';
	$section['sec32'][$localLan] ='';
	$section['sec33']['eng'] = 'Nationality';
	$section['sec33'][$localLan] ='';
	$section['sec34']['eng'] = 'Phone number';
	$section['sec34'][$localLan] ='';
	$section['sec35']['eng'] = '';
	$section['sec35'][$localLan] ='Email';
	$section['sec36']['eng'] = 'Price per card & shipping';
	$section['sec36'][$localLan] ='';
	$section['sec37']['eng'] = 'Card type';
	$section['sec37'][$localLan] ='';
	$section['sec38']['eng'] = 'Price per card';
	$section['sec38'][$localLan] ='';
	$section['sec39']['eng'] = 'How to receive card';
	$section['sec39'][$localLan] ='';
	$section['sec40']['eng'] = 'Select';
	$section['sec40'][$localLan] ='';
	$section['sec41']['eng'] = 'Promotional code';
	$section['sec41'][$localLan] ='';
	$section['sec42']['eng'] = 'If you have a promotional code, please enter it.';
	$section['sec42'][$localLan] ='';
	$section['sec42'][$localLanT] ='';
	$section['sec43']['eng'] = 'Please check the user consents.';
	$section['sec43'][$localLan] ='';
	$section['sec43'][$localLanT] ='';
	$section['sec44']['eng'] = 'Terms & Conditions';
	$section['sec44'][$localLan] ='';
	$section['sec45']['eng'] = 'Please read our \'Terms and conditioins\' below and choose whether you agree with it or not.';
	$section['sec45'][$localLan] ='';
	$section['sec45'][$localLanT] ='';
	$section['sec45_1']['eng'] = 'Without issuing a card, your application and the corresponding personal information will be stored for a maximum of 62 days';
	$section['sec45_1'][$localLan] ='';
	$section['sec45_1'][$localLanT] ='';
	$section['sec45_2']['eng'] = 'The personal information of an issued card is stored for 36 months from the issued date.';
	$section['sec45_2'][$localLan] ='';
	$section['sec45_2'][$localLanT] ='';
	$section['sec46']['eng'] = 'I agree';
	$section['sec46'][$localLan] ='';
	$section['sec47']['eng'] = 'I do not agree';
	$section['sec47'][$localLan] ='';
	$section['sec48']['eng'] = 'Privacy policy';
	$section['sec48'][$localLan] ='';
	$section['sec49']['eng'] = 'Please read our \'Privacy policy\' below and choose whether you agree with it or not.';
	$section['sec49'][$localLan] ='';
	$section['sec49'][$localLanT] ='';
	$section['sec50']['eng'] = 'As described above, you have the right not to agree to the collection and processing of your personal data. However, if you do not agree, you will not be able to proceed with the application for the card and the ISIC '.$countryName[$countryId].' will not be able to send it to you.';
	$section['sec50'][$localLan] ='';
	$section['sec50'][$localLanT] ='';
	$section['sec51']['eng'] = 'I give my consent, free and clear, to ISIC '.$countryName[$countryId].' treat my personal data in order to issue and send my ISIC/ITIC/IYTC card.';
	$section['sec51'][$localLan] ='';
	$section['sec52']['eng'] = 'Receive newsletter';
	$section['sec52'][$localLan] ='';
	$section['sec53']['eng'] = 'Subscribe the ISIC '.$countryName[$countryId].' newsletter and be the first to hear the news about your card. Join the ISIC community.';
	$section['sec53'][$localLan] ='';
	$section['sec53'][$localLanT] ='';
	$section['sec54']['eng'] = 'I agree to receive newsletters and other promotional communications from ISIC '.$countryName[$countryId].' . My email will not be given to third parties. I can cancel my subscription at any time by clicking "unsubscribe" at the end of each newsletter.';
	$section['sec54'][$localLan] ='';
	$section['sec54'][$localLanT] ='';
	$section['sec55']['eng'] = 'Next';
	$section['sec55'][$localLan] ='';
	$section['sec56']['eng'] = '';
	$section['sec56'][$localLan] ='';
	$section['sec57']['eng'] = '';
	$section['sec57'][$localLan] ='';
	$section['sec58']['eng'] = '';
	$section['sec58'][$localLan] ='';
?>
<?php 
	switch($result->serialType)
	{
		case 'ISIC':
?>
	<!-- ISIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/ISIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">ISIC Online application</td>
			<td class="text-right"><img src="/std/app/images/ISIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ISIC top -->
<?php 
			break;
?>
<?php 
		case 'ITIC':
?>
	<!-- ITIC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/ITIC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">ITIC Online application</td>
			<td class="text-right"><img src="/std/app/images/ITIC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /ITIC top -->
<?php 
			break;
?>
<?php 
		case 'IYTC':
?>
	<!-- IYTC top -->
	<table id="appTop">
		<tr>
			<td width="60"><img src="/std/app/images/IYTC_icon.png" class="w100"></td>
			<td class="pl-15 pb-5">IYTC Online application</td>
			<td class="text-right"><img src="/std/app/images/IYTC_standard_personalised_F19.jpg" id="topCard"></td>
		</tr>
	</table>
	<!-- /IYTC top -->
<?php 
			break;
?>
<?php
		default:
			echo 'Access Denied30';
			exit;	
	}
?>
	<form id="appForm1" name="appForm1" action="" method="POST"  enctype="multipart/form-data">
	<input type="hidden" name="pageStatus" id="pageStatus" value="app01" >
	<input type="hidden" id="set1" name="set1" value="<?php echo $officeId ?>"> 
	<input type="hidden" id="url" name="url" value="<?php echo $url ?>"> 
	<input type="hidden" id="serialType" name="serialType" value="<?php echo $result->serialType ?>"> 
	<?php if($result->payment === 'yes'){ ?>
	<h2 id="topTitleM">Step 1/3</h2>
	<div id="topTitleDiv">
		<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
			<tr>
				<td class="topTitleO"  style="width:33%;"><!--Personal details-->
					<?php 
						$thisSection ='sec1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>					
				</td>
				<td class="topTitleX">
					<?php 
						$thisSection ='sec1_1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
				<td class="topTitleX"  style="width:33%;">
					<?php 
						$thisSection ='sec1_2';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
			</tr>
		</table>
	</div>
	<?php } else { ?>
	<h2 id="topTitleM">Step 1/2</h2>
	<div id="topTitleDiv">
		<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
			<tr>
				<td class="topTitleO"  style="width:33%;"><!--Personal details-->
					<?php 
						$thisSection ='sec1';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
				<td class="topTitleX"  style="width:33%;">
					<?php 
						$thisSection ='sec1_2';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<div id="appTableDiv">
		<h3 style="margin-bottom:20px;">
			<!-- Personal details -->
			<?php 
				$thisSection ='sec1';
				if($useLocalLan === 'yes') {
					
			?>
			<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
			<br>
			<small>
			<!--Please enter your name as it appears on your ID Card/ Passport-->
			<?php if($result->serialType === 'ITIC'){ ?>
				<?php 
					$thisSection ='sec2';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLanT]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php }else{?>
				<?php 
					$thisSection ='sec2';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php } ?>
			</small>
		</h3>
		<table class="table table-bordered">
			<tr>
				<th width="30%" class="th_ho">
					<!-- First Name -->
					<?php 
						$thisSection ='sec3';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>				
				</th>
				<?php if(isset($_SESSION['appInfo']->engFname)){ ?>
				<td><input type="text" class="form-control" name="engFname" id="engFname" maxlength="20" value="<?php echo $_SESSION['appInfo']->engFname?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" name="engFname" id="engFname" maxlength="20"></td>
				<?php } ?>
			</tr>
			<tr>
				<th class="th_ho">
					<!--Last name-->
					<?php 
						$thisSection ='sec4';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>					
				</th>
				<?php if(isset($_SESSION['appInfo']->engLname)){ ?>
				<td><input type="text" class="form-control" name="engLname" id="engLname" maxlength="20" value="<?php echo $_SESSION['appInfo']->engLname?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" name="engLname" id="engLname" maxlength="20"></td>
				<?php } ?>
			</tr>
			<tr>
				<th class="th_ho">
					<!--Password-->
					<?php 
						$thisSection ='sec5';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				
				<td>
					<p>
						<input type="password" class="form-control" maxlength="20" name="password" id="password">
					</p>
					<small>
						<ul class="m0">
							<!--(MyId Password must be longer than 6 character)-->
							<?php 
								$thisSection ='sec6';
								if($useLocalLan === 'yes') {
									
							?>
							<li><?php echo $section[$thisSection][$localLan]?> <br><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li><?php echo $section[$thisSection]['eng']?><li>
							<?php 
								}
							?>
							<?php 
								$thisSection ='sec6_1';
								if($useLocalLan === 'yes') {
									
							?>
							<li class="pt5"><?php echo $section[$thisSection][$localLan]?><br><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span></li>
							<?php 
								} else {
							?>
							<li class="pt5"><?php echo $section[$thisSection]['eng']?></li>
							<?php 
								}
							?>
						</ul>
					</small>
				</td>
			</tr>
			<?php if($result->gender === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Gender-->
					<?php 
						$thisSection ='sec7';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->gender) && $_SESSION['appInfo']->gender ==='m'){ ?>
				<td>
					<input type="hidden" name="genderCheck" id="genderCheck" value="yes"><input type="radio" checked name="gender" id="genderM" value="m"> 
					Male <span class="subEn subColor"></span> &nbsp;&nbsp;&nbsp; <input type="radio"  name="gender" id="genderF" value="f"> 
					Female<span class="subEn subColor"></span>
				</td>
				<?php }else if(isset($_SESSION['appInfo']->gender) && $_SESSION['appInfo']->gender ==='f'){ ?>
				<td>
					<input type="hidden" name="genderCheck" id="genderCheck" value="yes"><input type="radio" name="gender" id="genderM" value="m"> 
					Male <span class="subEn subColor"></span> &nbsp;&nbsp;&nbsp; <input type="radio"  checked name="gender" id="genderF" value="f"> 
					Female <span class="subEn subColor"></span>
				</td>
				<?php }else{ ?>
				<td>
					<input type="hidden" name="genderCheck" id="genderCheck" value="yes"><input type="radio" name="gender" id="genderM" value="m"> 
					Male <span class="subEn subColor"></span> &nbsp;&nbsp;&nbsp; 
					<input type="radio"  name="gender" id="genderF" value="f"> 
					Female <span class="subEn subColor"></span>
				</td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<tr>
				<th class="th_ho"><!--Date of birth-->
					<?php 
						$thisSection ='sec8';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<div class="row">
						<div class="col-xs-4 mppr3">
							<select class="form-control" name="birthDay" id="birthDay">
								<?php 
									if(isset($_SESSION['appInfo']->birthDay)){ 
										for($dayLoop=1;$dayLoop<32;$dayLoop++){ 
											$selected = ' ';
											if(sprintf('%02d', $dayLoop) === $_SESSION['appInfo']->birthDay){
												$selected = 'selected';
											}else{ 
												$selected = ' ';
											} ?>
											<option <?php echo $selected ?> value="<?php printf('%02d', $dayLoop) ?>"><?php printf('%02d', $dayLoop) ?></option>
								<?php } ?>
								<?php
									}else{ ?>
								<option value="" disabled selected="selected"><!--Day-->
									<?php 
										$thisSection ='sec9';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
								<?php 
									} ?>

							</select>
						</div>
						<div class="col-xs-4 mppr3 mppl3">
							<select class="form-control" name="birthMonth" id="birthMonth">
								<?php 
									if(isset($_SESSION['appInfo']->birthMonth)){ 
										for($monthLoop=1;$monthLoop<13;$monthLoop++){ 
											$selected = ' ';
											if(sprintf('%02d', $monthLoop) === $_SESSION['appInfo']->birthMonth){
												$selected = 'selected';
											}else{ 
												$selected = ' ';
											} ?>
											<option <?php echo $selected ?> value="<?php printf('%02d', $monthLoop) ?>"><?php printf('%02d', $monthLoop) ?></option>
								<?php } ?>
								<?php
									}else{ ?>
								<option value="" disabled selected="selected"><!--Month-->
									<?php 
										$thisSection ='sec10';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
								<?php 
									} ?>
							</select>
						</div>
						<div class="col-xs-4 mppl3">
							<select class="form-control" name="birthYear" id="birthYear">
								<?php 
									$selected = ' ';
									if(isset($_SESSION['appInfo']->birthYear)){ 
										$selected = ' ';
									} else { 
										$selected = 'selected';
									}
								?>
								<option <?php echo $selected ?> value="" disabled><!--Year-->
									<?php 
										$thisSection ='sec11';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</option>
										<?php 
											$yearStart=(int)date("Y")-12;
											for($i=$yearStart;$i>1900;$i--){
												$selected = ' ';
												if(isset($_SESSION['appInfo']->birthYear) && $_SESSION['appInfo']->birthYear === (string)$i){
													$selected = 'selected';
												}else{
													$selected = ' ';
												}
										?>
										  <option <?php echo $selected ?> value="<?php echo $i;?>"><?php echo $i;?></option>
										<?php
											}
										?>
							</select>
						</div>
					</div>
				</td>
			</tr>
			<?php if($result->underAge === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Authorization declaration for children under 16-->
					<?php 
						$thisSection ='sec12';
						if($useLocalLan === 'no') {
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<p><!--According to the new General Data Protection Regulation, the processing of personal data of all the under 16 years old minors is only possible with the consent of the legal holder of parental responsibility. In this regard, to continue with your ISIC application, we ask you to please upload the <a href="./pdf/declaration_ISIC_EN.pdf" target="_blank">Authorisation Declaration</a> for children under 16.-->
						<?php 
							$thisSection ='sec13';
							if($useLocalLan === 'yes') {	
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					</p>
					<p class="m0"><input type="file" accept=".jpeg, .jpg, .png, .pdf, .doc, .docx, .ppt" name="underAge" id="underAge"  class="form-control"></p>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<tr>
				<th class="th_ho"><!--ID card Photo-->
					<?php
						$thisSection ='sec14';
						if($useLocalLan === 'yes') {
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<p>
						<?php if($result->serialType === 'ITIC'){ ?>
							<!--Please upload your headshot photo. Passport photo is recommended.-->
							<?php 
								$thisSection ='sec15';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php }else{?>
							<!--Please upload your headshot photo. Passport photo is recommended.-->
							<?php 
								$thisSection ='sec15';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php } ?>	
					</p> 
					<p class="mb5"><input type="file" class="form-control"  accept=".jpeg, .jpg, .png" name="photoName" id="photoName" onchange="read_2(1,'prev1');"></p>
					<small>
					<ul>
						<li><!--File name should be number or english. And the photo file shoul be JPG or PNG image file-->
							<?php 
								$thisSection ='sec16';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</li>
						<li><!--When issuing the card, if the photo does not show your face well, it can be edited according to the regulations.-->
							<?php 
								$thisSection ='sec17';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>	
						<br>
							<?php if($result->serialType === 'ITIC'){ ?>
							<img src="/std/app/images/photosample_itic.jpg" style="width: 100%; max-width: 430px;">
							<?php } else if ($result->serialType === 'IYTC') {?>
							<img src="/std/app/images/photosample_iytc.jpg" style="width: 100%; max-width: 430px;">
							<?php } else {?>
							<img src="/std/app/images/photosample.jpg" style="width: 100%; max-width: 430px;">
							<?php } ?>
						</li>
						<li class="mt5">
							<?php 
								$thisSection ='sec17_1';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
							<ol>
								<li>
									<?php 
										$thisSection ='sec17_2';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</li>
								<li>
									<?php 
										$thisSection ='sec17_3';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?> <br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</li>
							</ol>
						</li>
					</ul>
					</small>
					<div id="p_new">
						<div>
							<div class="row" style="padding-top: 15px;">
								<div class="col-sm-4 ppr0">
									<table class="table table-bordered w100">
										<tr>
											<td class="text-center" style="padding:5px; background:#efefef;"><!--Preview-->
												<?php 
													$thisSection ='sec18';
													if($useLocalLan === 'yes') {
														
												?>
												<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
												<?php 
													} else {
												?>
												<?php echo $section[$thisSection]['eng']?>
												<?php 
													}
												?>
											</td>
										</tr>
										<tr>
											<td class="text-center" height="170" valign="middle">
												<div id="divPrev1">
													<img src="/std/app/images/photoSample2018.jpg" style="max-height:150px;">
												</div>
												<canvas id="prev1" onclick="point(1,event)" style="border:0px solid #ccc; cursor:crosshair;display:none" width="110" height="110">HTML5 Support Required</canvas>
											</td>
										</tr>
									</table>
								</div>
								<div class="col-sm-8">														
									<div>
										<p style="margin-bottom: 5px;"><!--You can edit the uploaded photo.-->
											<?php 
												$thisSection ='sec19';
												if($useLocalLan === 'yes') {
													
											?>
											<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
											<?php 
												} else {
											?>
											<?php echo $section[$thisSection]['eng']?>
											<?php 
												}
											?>			
										</p>
										<ol class="mb10">
											<li>
												<?php if($result->serialType === 'ITIC'){ ?>
												<!--Click the left top point that you want to start crop.-->
													<?php 
														$thisSection ='sec20';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php }else{?>
												<!--Click the left top point that you want to start crop.-->
													<?php 
														$thisSection ='sec20';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php } ?>
											</li>
											<li>
												<?php if($result->serialType === 'ITIC'){ ?>
												<!--Click the right bottom point that you want to end crop.-->
													<?php 
														$thisSection ='sec21';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php }else{?>
												<!--Click the right bottom point that you want to end crop.-->
													<?php 
														$thisSection ='sec21';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php } ?>
												<br><span class="subEn subColor"></span>
											</li>
											<li>
												<?php if($result->serialType === 'ITIC'){ ?>
												<!--Click the 'Crop' button.-->
													<?php 
														$thisSection ='sec22';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLanT]?><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php }else{?>
												<!--Click the 'Crop' button.-->
													<?php 
														$thisSection ='sec22';
														if($useLocalLan === 'yes') {
															
													?>
													<?php echo $section[$thisSection][$localLanT]?><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
													<?php 
														} else {
													?>
													<?php echo $section[$thisSection]['eng']?>
													<?php 
														}
													?>	
												<?php } ?>
												<br class="mo"><span class="subEn subColor"></span>
											</li>
										</ol>
									</div>
									<div class="row">
										<input type="hidden" name="x1" id="x1" value="">
										<input type="hidden" name="y1" id="y1" value="">
										<input type="hidden" name="x2" id="x2" value="">
										<input type="hidden" name="y2" id="y2" value="">
										<input type="hidden" name="scale" id="scale" value="">
										<input type="hidden" name="rotate" id="rotate" value="">	
										<p class="col-xs-6 ppr3">
											<button type="button" class="btn btn-isic2 btn-block" id="rotate_left1" onclick="rotate1(1,1);"><i class="fa fa-undo" aria-hidden="true"></i> 
												<?php 
													$thisSection ='sec22_1';
													if($useLocalLan === 'yes') {
														
												?>
												<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
												<?php 
													} else {
												?>
												<?php echo $section[$thisSection]['eng']?>
												<?php 
													}
												?>
											</button>
										</p>
										<p class="col-xs-6 ppl3">
											<button type="button" class="btn btn-isic2 btn-block" id="rotate_right1" onclick="rotate1(2,1);""><i class="fa fa-redo" aria-hidden="true"></i> 
												<?php 
													$thisSection ='sec22_2';
													if($useLocalLan === 'yes') {
														
												?>
												<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
												<?php 
													} else {
												?>
												<?php echo $section[$thisSection]['eng']?>
												<?php 
													}
												?>
											</button>
										</p>
										<p class="col-xs-6 ppr3">
											<button type="button" class="btn btn-isic2 btn-block"disabled id="crop_button1" onclick="crop1(1);""><i class="fa fa-crop" aria-hidden="true"></i>
												<?php 
													$thisSection ='sec22_3';
													if($useLocalLan === 'yes') {
														
												?>
												<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
												<?php 
													} else {
												?>
												<?php echo $section[$thisSection]['eng']?>
												<?php 
													}
												?>
											</button>
										</p>
										<p class="col-xs-6 ppl3">
											<button type="button" class="btn btn-isic2 btn-block" onclick="unselect1(1);"><i class="fa fa-retweet" aria-hidden="true"></i> 
												<?php 
													$thisSection ='sec22_4';
													if($useLocalLan === 'yes') {
														
												?>
												<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
												<?php 
													} else {
												?>
												<?php echo $section[$thisSection]['eng']?>
												<?php 
													}
												?>
											</button>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div>
						</div>                
					</div>
				</td>
			</tr>
			<?php if($result->school === 'yes'){ ?>
			<tr>
				<th class="th_ho">
				<?php if($result->serialType ==='ITIC'){ ?>
					<!--Teaches at-->
					<?php 
						$thisSection ='sec24';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				<?php } else { ?>
					<!--Studies at-->
					<?php 
						$thisSection ='sec23';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				<?php } ?>
				</th>
				<?php if(isset($_SESSION['appInfo']->school)){ ?>
					<td><input type="text" class="form-control"  maxlength="32" name="school" id="school" value ="<?php echo $_SESSION['appInfo']->school ?>"></td>
				<?php }else{ ?>
					<td><input type="text" class="form-control"  maxlength="32" name="school" id="school"></td>
				<?php } ?>
			</tr>
			<?php } else if ($result->school === 'no'){ ?>
			<?php } else if ($result->school === 'custom' && isset($result->schoolCustom) ) { //preload school name?> 
			<tr>
				<th class="th_ho">
				<?php if($result->serialType ==='ITIC'){ ?>
					<!--Teaches at-->
					<?php 
						$thisSection ='sec24';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				<?php } else { ?>
					<!--Studies at-->
					<?php 
						$thisSection ='sec23';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				<?php } ?>
				</th>
				<td><input type="text" class="form-control"  readonly maxlength="32" name="school" id="school" value="<?php echo $result->schoolCustom ?>"></td>
			</tr>
			<?php } else{}?>
			<?php if($result->proofStatus === 'yes'){ ?>
				<?php //if($result->altMsg ==='alt1'){ ?>
				<?php if($result->serialType ==='ITIC'){ ?>
				<tr>
					<th class="th_ho">
						<?php 
							$thisSection ='sec25_1';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					</th>
					<td>
						<p>
							<?php 
								$thisSection ='sec25_2';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</p>
						<p>
							<input type="file" accept=".jpeg, .jpg, .png, .pdf, .doc, .docx, .ppt" name="proofStatus" id="proofStatus" class="form-control">
						</p>
						<p style="margin:10px 0 0 0;"><!--To get your Card you must be a full time teacher and your proof of status must clearly show the name of the school/ university/ college and the school year. (The size of the file should be smaller than 512Kb)<br><span class="subEn subColor"></span>-->
							<ul class="m0">
								<li>
									<?php 
										$thisSection ='sec25';
										if($useLocalLan === 'yes') {
										
									?>
									<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</li>
							</ul>
						</p>
					</td> 
				</tr>
				<?php }else { ?>
				<tr>
					<th class="th_ho">
						<?php 
							$thisSection ='sec25_1';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>	
					</th>
					<td>
						<p>
							<?php 
								$thisSection ='sec25_2';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</p>
						<p>
							<input type="file" accept=".jpeg, .jpg, .png, .pdf, .doc, .docx, .ppt" name="proofStatus" id="proofStatus" class="form-control">
						</p>
						<p style="margin:10px 0 0 0;"><!--To get your Card you must be a full time student and your proof of status must clearly show the name of the school/ university/ college and the school year. (The size of the file should be smaller than 512Kb)<br>
						<span class="subEn subColor"></span>-->
							<ul class="m0">
								<li>
									<?php 
										$thisSection ='sec25';
										if($useLocalLan === 'yes') {
											
									?>
									<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
									<?php 
										} else {
									?>
									<?php echo $section[$thisSection]['eng']?>
									<?php 
										}
									?>
								</li>
							</ul>
						</p>
					</td> 
				</tr>
				<?php } ?>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->proofBirth === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Proof of Date of Birth <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec26';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</th>
				<td>
					<p><input type="file" class="form-control" accept=".jpeg, .jpg, .png, .pdf, .doc, .docx, .ppt" name="proofBirth" id="proofBirth"></p>
			
					<ul class="m0">
						<li><!--The size of the file should be smaller than 512Kb <span class="subEn subColor"></span>-->
						<?php 
							$thisSection ='sec27';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>	
						</li>
					</ul>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->address === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Address <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec28';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</th>
				<?php if(isset($_SESSION['appInfo']->address)){ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="address" id="address"  value ="<?php echo $_SESSION['appInfo']->address ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="address" id="address"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->address2 === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Address2<br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec29';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</th>
				<?php if(isset($_SESSION['appInfo']->address2)){ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="address2" id="address2"  value ="<?php echo $_SESSION['appInfo']->address2 ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="address2" id="address2"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->city === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--City <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec30';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->city)){ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="city" id="city"  value ="<?php echo $_SESSION['appInfo']->city ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="city" id="city"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->country === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Country <br><span class="subEn subColor"></span>-->
				<?php 
						$thisSection ='sec31';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->country)){ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="country" id="country"  value ="<?php echo $_SESSION['appInfo']->country ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="country" id="country"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->postal === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Postal code <br>
				<span class="subEn subColor"></span>-->
				<?php 
						$thisSection ='sec32';
						if($useLocalLan === 'yes') {

					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->postal)){ ?>
				<td><input type="text" class="form-control"  maxlength="20" name="postal" id="postal" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"  value ="<?php echo $_SESSION['appInfo']->postal ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="20" name="postal" id="postal" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->nationality === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Nationality <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec33';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
				<select class="form-control" name="nationality" id="nationality">
					<?php
						if(isset($_SESSION['appInfo']->nationality)) {
							for($i=1;$i<241;$i++){
								if($_SESSION['appInfo']->nationality === (string)$i){
									echo "<option selected value =\"{$i}\">{$countryName[$i]}</option>";
								}else {
									echo "<option value =\"{$i}\">{$countryName[$i]}</option>";
								}
							}
						} else{
							for($i=1;$i<241;$i++){
								if ($i === $countryId){
									echo "<option selected value =\"{$i}\">{$countryName[$i]}</option>";
								}
								else{
									echo "<option value =\"{$i}\">{$countryName[$i]}</option>";
								}
							}
						}
					?>
				</select>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->phone === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Phone number <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec34';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->phone)){ ?>
				<td><input type="text" class="form-control"  maxlength="20" name="phone" id="phone" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"  value ="<?php echo $_SESSION['appInfo']->phone ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="20" name="phone" id="phone" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->email === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Email-->
					<?php 
						$thisSection ='sec35';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<?php if(isset($_SESSION['appInfo']->email)){ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="email" id="email"  value ="<?php echo $_SESSION['appInfo']->email ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control"  maxlength="100" name="email" id="email"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->vatNumber === 'yes'){ ?>
			<tr>
				<th class="th_ho">VAT number <br><span class="subEn subColor"></span></th>
				<?php if(isset($_SESSION['appInfo']->vatNumber)){ ?>
				<td><input type="text" class="form-control" maxlength="9" name="vatNumber" id="vatNumber"  onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"  value ="<?php echo $_SESSION['appInfo']->vatNumber ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" maxlength="9" name="vatNumber" id="vatNumber"  onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' onfocusout="removeChar(event)"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->associateName === 'yes'){ ?>
			<tr>
				<th class="th_ho">Associate name<br><span class="subEn subColor"></span></th>
				<?php if(isset($_SESSION['appInfo']->associateName)){ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateName" id="associateName"  value ="<?php echo $_SESSION['appInfo']->associateName ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateName" id="associateName"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->associateNumber=== 'yes'){ ?>
			<tr>
				<th class="th_ho">Associate number <br><span class="subEn subColor"></span></th>
				<?php if(isset($_SESSION['appInfo']->associateNumber)){ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateNumber" id="associateNumber"  value ="<?php echo $_SESSION['appInfo']->associateNumber ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateNumber" id="associateNumber"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->associateRelation=== 'yes'){ ?>
			<tr>
				<th class="th_ho">Relationship with the associate<br><span class="subEn subColor"></span></th>
				<?php if(isset($_SESSION['appInfo']->associateRelation)){ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateRelation" id="associateRelation"  value ="<?php echo $_SESSION['appInfo']->associateRelation ?>"></td>
				<?php }else{ ?>
				<td><input type="text" class="form-control" maxlength="60" name="associateRelation" id="associateRelation"></td>
				<?php } ?>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<!-- start extra fields -->
			<?php if($extraCount >0){ ?>
				<?php for($i=0;$i<$extraCount;$i++){ ?>
				<tr>
					<input type="hidden" name="extraId<?php echo $i?>" id="extraId<?php echo $i?>" value="<?php echo  $extraList[$i]->extraFieldName?>">
					<th class="th_ho"><?php echo $extraList[$i]->extraDescription1 ?><br><span class="subEn subColor"><?php echo $extraList[$i]->extraDescription2 ?></span></th>
					<td>
						<input type="text" class="form-control" maxlength="<?php echo  $extraList[$i]->extraMaxLength?>" name="extra<?php echo $i?>" id="extra<?php echo $i?>">
						<small>
							<ul class="m0">
								<li><?php echo $extraList[$i]->extraDescription3 ?><span class="subEn subColor"><?php echo $extraList[$i]->extraDescription4 ?></span></li>
							</ul>
						</small>
					</td>
				</tr>
				<?php } ?>
			<?php } else { ?>
			<?php } ?>
			<!-- end extra fields -->
		</table>      
		<?php if($result->payment === 'yes'){ ?>
		<h3 style="margin:40px 0 20px 0;"><!--Price per card &amp; shipping <span class="subEn subColor"></span>-->
			<?php 
				$thisSection ='sec36';
				if($useLocalLan === 'yes') {
					
			?>
			<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
		</h3>
		<table class="table table-bordered">
			<tr>
				<th width="30%" class="th_ho"><!--Card type <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec37';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td><?php echo $result->appName ?></td>
			</tr>
			<tr>
				<th class="th_ho"><!--Price per card <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec38';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td><?php echo $localCurrency?> <?php echo $result->price ?></td>
			</tr>
			<?php if($result->postType>0){ ?>
			<tr>
				<th class="th_ho"><!--Delivery <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec39';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<div class="mb5">
						<select class="form-control" name="receiveType" id="receiveType">
							<?php
								for($i=1;$i<(int)$result->postType+1;$i++){
									$postNameNow = 'post'.$i.'Name';
									$postDescNow = 'post'.$i.'Desc';
									$postPriceNow = 'post'.$i.'Price';
									$postNameNow = $result->$postNameNow;
									$postDescNow = $result->$postDescNow;
									$postPriceNow = $result->$postPriceNow;
									if($postPriceNow === '0'){
										$postPriceNow = '0';
									}else{
										//$postPriceNow = ' '.$postPriceNow.'.00';
									}
									?>
							<option value="<?php echo $postNameNow ?>_A_<?php echo $postPriceNow ?>"><?php echo $postNameNow ?></option>
							<?php } ?>
						</select>
					</div>

					<ul class="m0">
					<?php
						for($i=1;$i<(int)$result->postType+1;$i++)
						{
							$postNameNow = 'post'.$i.'Name';
							$postDescNow = 'post'.$i.'Desc';
							$postPriceNow = 'post'.$i.'Price';
							$postNameNow = $result->$postNameNow;
							$postDescNow = $result->$postDescNow;
							$postPriceNow = $result->$postPriceNow;
							if($postPriceNow === '0')
							{
								$postPriceNow = '0';
							}else{
								//$postPriceNow = ' '.$postPriceNow.'.00';
							}
					?>
					<li><?php echo $postNameNow ?><br/><span class="subEn subColor"><?php echo $postDescNow ?><br/><?php echo $localCurrency?> <?php echo $postPriceNow ?></span></li>
					<?php 
						} 
					?>
					</ul>

				</td>
			</tr>
			<?php }else{}?>
			<?php if($result->promo === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Promotional code <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec41';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
				<?php if(isset($_SESSION['appInfo']->promo)){ ?>
				<p><input type="text" class="form-control" maxlength="20" name="promo" id="promo"  value ="<?php echo $_SESSION['appInfo']->promo ?>"></p>
				<?php }else{ ?>
				<p><input type="text" class="form-control" maxlength="20" name="promo" id="promo"></p>
				<?php } ?>
				<p class="m0">
					<?php if($result->serialType === 'ITIC'){ ?>
					<!--If you have a promotional code, please enter it.-->
						<?php 
							$thisSection ='sec42';
							if($useLocalLan === 'yes') {

						?>
						<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					<?php }else{?>
					<!--If you have a promotional code, please enter it.-->
						<?php 
							$thisSection ='sec42';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					<?php } ?>
				<br><span class="subEn subColor"></span></p></td>
			</tr>
			<?php } else { ?>
			<?php } ?>
		</table>
		<?php } else { ?>
		<?php } ?>
		<?php if($result->terms === 'yes' || $result->privacy === 'yes' || $result->news === 'yes'){ ?>
		<h3 style="margin:40px 0 20px 0;">
			<?php if($result->serialType === 'ITIC'){ ?>
			<!--Please check the user consents.-->
				<?php 
					$thisSection ='sec43';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLanT]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php }else{?>
			<!--Please check the user consents.-->
				<?php 
					$thisSection ='sec43';
					if($useLocalLan === 'yes') {
						
				?>
				<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
				<?php 
					} else {
				?>
				<?php echo $section[$thisSection]['eng']?>
				<?php 
					}
				?>
			<?php } ?>
			<br class="mo"><span class="subEn subColor"></span>
		</h3>
		<?php }else{} ?>
		<table class="table table-bordered">
			<?php if($result->terms === 'yes'){ ?>
			<tr>
				<th width="30%" class="th_ho"><!--terms &amp; conditions <br>
					<span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec44';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>	
				</th>
				<td>
					<input type="hidden" name="termCheck" id="termCheck" value="yes">
					<p>
						<?php if($result->serialType === 'ITIC'){ ?>
						<!--Please read our General Conditions of Use and Purchase ISIC, ITIC and IYTC cards and give ISIC <?php //echo $countryName[$countryId]?> your consent.-->
							<?php 
								$thisSection ='sec45';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php }else{?>
						<!--Please read our General Conditions of Use and Purchase ISIC, ITIC and IYTC cards and give ISIC <?php //echo $countryName[$countryId]?> your consent.-->
							<?php 
								$thisSection ='sec45';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php } ?>
					</p>
					<p><iframe src="/std/app/App_terms.html" scrolling="yes" class="form-control agreeIframe"></iframe></p>
					<ul class="subEn">
						<li>如果卡證未能核發，您的申請資料與個人資訊將儲存在申辦系統最多達62天。<br><span class="subColor">(Without issuing a card, your application and the corresponding personal information will be stored for a maximum of 62 days.)</span></li>
						<li>卡證申請獲得核發的卡的相關個人資訊則從申辦日起儲存36個月。<br><span class="subColor">(The personal information of an issued card is stored for 36 months from the issued date.)</span></li>
                    </ul>
					<div class="row">
						<div class="col-sm-6"><input type="radio" name="termAgree" id="termYes" value="yes"><!-- I agree <span class="subEn subColor"></span>-->
							<?php 
								$thisSection ='sec46';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</div>
						<div class="col-sm-6"><input type="radio" name="termAgree" id="termNo" value="no"><!--I do not agree <span class="subEn subColor"></span>-->
							<?php 
								$thisSection ='sec47';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</div>
					</div>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->privacy === 'yes'){ ?>
			<tr>
				<th class="th_ho"><!--Privacy policy <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec48';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
					<input type="hidden" name="privacyCheck" id="privacyCheck" value="yes">
					<p>
						<?php if($result->serialType === 'ITIC'){ ?>
						<!--We need your consent to collect and process your personal data. Please read the following content and, if you agree, give your consent. For more information, check our -->
							<?php 
								$thisSection ='sec49';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php }else{?>
						<!--We need your consent to collect and process your personal data. Please read the following content and, if you agree, give your consent. For more information, check our -->
							<?php 
								$thisSection ='sec49';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						<?php } ?>
						<!--<a href="" target="_blank">Privacy Policy</a>. <br><span class="subEn subColor"></span>--></p>
					<p><iframe src="/std/app/App_privacy.html" scrolling="yes" class="form-control agreeIframe"></iframe></p>
					<p>
						<?php if($result->serialType === 'ITIC'){ ?>
						<!-- As described above, you have the right not to agree to the collection and processing of your personal data. However, if you do not agree, you will not be able to proceed with the application for the card and the ISIC <?php //echo $countryName[$countryId]?> will not be able to send it to you. -->
							<!--<?php 
								$thisSection ='sec50';
								if($useLocalLan === 'yes') {
									
							?>
							<?php //echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php //echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php //echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>-->
						<?php }else{?>
						<!-- As described above, you have the right not to agree to the collection and processing of your personal data. However, if you do not agree, you will not be able to proceed with the application for the card and the ISIC <?php //echo $countryName[$countryId]?> will not be able to send it to you. -->
							<!--<?php 
								$thisSection ='sec50';
								if($useLocalLan === 'yes') {
									
							?>
							<?php //echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php //echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php //echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>-->
						<?php } ?>
					</p>
					<!--<p class="m0"><input type="checkbox" name="privacy" id="privacy" value="yes">-->
					<div class="row">
						<div class="col-sm-6"><input type="radio" name="privacyAgree" id="privacyYes" value="yes"><!-- I agree <span class="subEn subColor"></span>-->
							<?php 
								$thisSection ='sec46';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</div>
						<div class="col-sm-6"><input type="radio" name="privacyAgree" id="privacyNo" value="no"><!--I do not agree <span class="subEn subColor"></span>-->
							<?php 
								$thisSection ='sec47';
								if($useLocalLan === 'yes') {
									
							?>
							<?php echo $section[$thisSection][$localLan]?> <span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
							<?php 
								} else {
							?>
							<?php echo $section[$thisSection]['eng']?>
							<?php 
								}
							?>
						</div>
					</div>
					<!--I give my consent, free and clear, to ISIC <?php //echo $countryName[$countryId]?> treat my personal data in order to issue and send my ISIC/ITIC/IYTC card.<br><span class="subEn subColor"></span>-->
						<!--<?php 
							$thisSection ='sec51';
							if($useLocalLan === 'yes') {
								
						?>
						<?php //echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php //echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php //echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>-->
					</p>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
			<?php if($result->news === 'yes'){ ?>
			<tr>
				<th class="th_ho">Newsletter ISIC <?php echo $countryName[$countryId]?><br><!--<span class="subEn subColor">(Receive newsletter)</span>-->
					<?php 
						$thisSection ='sec52';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</th>
				<td>
				<p>
					<?php if($result->serialType === 'ITIC'){ ?>
					<!--
					Subscribe the ISIC <?php //echo $countryName[$countryId]?> newsletter and be the first to hear the news about your card. Join the ISIC community.
					<br><span class="subEn subColor"></span>-->
						<?php 
							$thisSection ='sec53';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLanT]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					<?php }else{?>
					<!--
					Subscribe the ISIC <?php echo $countryName[$countryId]?> newsletter and be the first to hear the news about your card. Join the ISIC community.
					<br><span class="subEn subColor"></span>-->
						<?php 
							$thisSection ='sec53';
							if($useLocalLan === 'yes') {
								
						?>
						<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
						<?php 
							} else {
						?>
						<?php echo $section[$thisSection]['eng']?>
						<?php 
							}
						?>
					<?php } ?>
				</p>
				<p class="m0"><input type="checkbox" name="news" id="news" value="yes"><!--I agree to receive newsletters and other promotional communications from ISIC <?php echo $countryName[$countryId]?>. My email will not be given to third parties. I can cancel my subscription at any time by clicking "unsubscribe" at the end of each newsletter. <br><span class="subEn subColor"></span>-->
					<?php 
						$thisSection ='sec54';
						if($useLocalLan === 'yes') {
							
					?>
					<?php echo $section[$thisSection][$localLan]?><br/><span class="subEn subColor"><?php echo $section[$thisSection]['eng']?></span>
					<?php 
						} else {
					?>
					<?php echo $section[$thisSection]['eng']?>
					<?php 
						}
					?>
				</p>
				</td>
			</tr>
			<?php } else { ?>
			<?php } ?>
		</table>
		<span class="subEn subColor">For security reasons, please close the browser after finishing application registration.</span><br/>
		<p class="buttonDiv"><button type="button" class="btn btn-isic btn-lg" style="width:300px;"  onclick="submit01(1)"><!--Next<i class="fa fa-caret-right" aria-hidden="true"></i><small></small>-->
			<?php 
				$thisSection ='sec55';
				if($useLocalLan === 'yes') {
					
			?>
			<?php echo $section[$thisSection][$localLan]?> <span class="subEn"><?php echo $section[$thisSection]['eng']?></span>
			<?php 
				} else {
			?>
			<?php echo $section[$thisSection]['eng']?>
			<?php 
				}
			?>
		</button></p>
		<?php 
			if(isset($_SESSION['appInfo'])){
				echo '<script>document.getElementById(\'password\').focus();</script>';
				unset($_SESSION['appInfo']);
			} else{}
		?>
	</div>
	</form>
	
	</div>
	</div>
</body>
</html>
