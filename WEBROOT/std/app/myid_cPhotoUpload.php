<script type="text/javascript" src="./js/myid_cPhotoUpload.js"></script>
<?php
// 210626 add get appSettingIdApp from table process
// 200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;				
	}
	if(isset($_GET['no'])){
		$appNum = $_GET['no'];
		if (isset($_SESSION['validApp'][$appNum])){
			$appNo = $_SESSION['validApp'][$appNum];
		}
		else{		
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;		
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	try {
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT appNo,cardType,appSettingIdApp FROM $tablename07 WHERE appNo=:searchValue1 AND status!='approved';";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':searchValue1', $appNo);
		if($stmt->execute() && $stmt->rowCount() != 0){
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$queryAppSetting = "SELECT * FROM $tablename23 WHERE appSettingId = :appSettingId";
			$stmtAppSetting = $db->prepare($queryAppSetting);
			$stmtAppSetting->bindParam(':appSettingId', $result->appSettingIdApp);
			$stmtAppSetting->execute();
			if($stmtAppSetting->rowCount() === 1){
				$resultAppSetting = $stmtAppSetting->fetch(PDO::FETCH_OBJ);
				$result->appSetting = $resultAppSetting;
			}else{
				echo 'Access Denied4';
				//print_r($stmtAppSetting->errorInfo());
				$db= NULL;
				//echo '<script>location.replace("/std/app/myid_main.php");</script>';
				exit;
			}
		}
		else{
			echo "<script> window.history.go(-1); </script>";
			echo '<script>alert(\'no\');</script>';
			$db= NULL;
			exit;
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}	
?>
        <!-- Start contents -->
		<form id="photoUploadForm" name="photoUploadForm" action="" method="POST"  enctype="multipart/form-data">
			<input type="hidden" id="serialType" name="serialType" value="<?php echo $result->appSetting->serialType ?>"> 
			<input type="hidden" name="appNum" id="appNum" value="<?php echo $appNum; ?>">
			<input type="hidden" name="pageStatus" id="pageStatus" value="photoUploadForm">
			<input type="hidden" name="x1" id="x1" value="">
			<input type="hidden" name="y1" id="y1" value="">
			<input type="hidden" name="x2" id="x2" value="">
			<input type="hidden" name="y2" id="y2" value="">
			<input type="hidden" name="scale" id="scale" value="">
			<input type="hidden" name="rotate" id="rotate" value="">		
			<div id="appTableDiv">					
				<div class="myidWrapDiv">		
					<h3>Photo upload</h3>	
					<table class="table table-bordered">
						<tr>
							<th class="th_ho" width="20%">ID card Photo</th>
							<td>
								<p><?php if($result->appSetting->serialType === 'ITIC'){ ?>
								Please upload your headshot photo. Passport photo is recommended.
								<?php }else{?>
								Please upload your headshot photo. Passport photo is recommended.
								<?php } ?>
								<br><span class="enSub"></span></p> 
								<p style="margin-bottom: 5px;"><input type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photoName" id="photoName" onchange="read_2(1,'prev1');"></p>
								<small>
								<ul>
									<li>
										<?php if($result->appSetting->serialType === 'ITIC'){ ?>
										File name should be number or english. And the photo file shoul be JPG or PNG image file.
										<?php }else{?>
										File name should be number or english. And the photo file shoul be JPG or PNG image file.
										<?php } ?>
										<br><span class="enSub"></span>
									</li>
									<li class="mt5">
										<?php if($result->appSetting->serialType === 'ITIC'){ ?>
										When issuing the card, if the photo does not show your face well, it can be edited according to the regulations.
										<?php }else{?>
										When issuing the card, if the photo does not show your face well, it can be edited according to the regulations.
										<?php } ?>
										<br><span class="enSub"></span>
									</li>
								</ul>
								</small>
								<div id="p_new">
									<div>
										<div class="row" style="padding-top: 15px;">
											<div class="col-sm-4 ppr0">
												<table class="table table-bordered w100">
													<tr>
														<td class="text-center" style="padding:5px; background:#efefef;">Preview <span class="enSub"></span></td>
													</tr>
													<tr>
														<td class="text-center" height="185">
															<div id="divPrev1">
																<img src="images/photoSample2018.jpg" style="with:100%; max-height:140px;">
															</div>
															<canvas id="prev1" onclick="point(1,event)" style="border:0px solid #ccc; cursor:crosshair;display:none" width="139" height="139">HTML5 Support Required</canvas>
														</td>
													</tr>
												</table>
											</div>
											<div class="col-sm-8">																	
												<div>
													<p>You can edit the uploaded photo.<span class="enSub"></span></p>
													<ol>
														<li> 
															<?php if($result->appSetting->serialType === 'ITIC'){ ?>
															Click the left top point that you want to start crop.
															<?php }else{?>
															Click the left top point that you want to start crop.
															<?php } ?>
															<br><span class="enSub"></span>
														</li>
														<li> 
															<?php if($result->appSetting->serialType === 'ITIC'){ ?>
															Click the right bottom point that you wnat to end crop.
															<?php }else{?>
															Click the right bottom point that you wnat to end crop.
															<?php } ?>
															<br><span class="enSub"></span>
														</li>
														<li> 
															<?php if($result->serialType === 'ITIC'){ ?>
															Click the 'Crop' button.
															<?php }else{?>
															Click the 'Crop' button.
															<?php } ?>
															<br class="mo">
															<span class="enSub"></span>
														</li>
													</ol>
												</div>		
												<div class="row">
													<p class="col-xs-6"><button type="button" class="btn btn-isic2 btn-block" id="rotate_left1" onclick="rotate1(1,1);"><i class="fa fa-undo" aria-hidden="true"></i>Rotate Left <br><span class="subEn subColor"></button></p>
													<p class="col-xs-6"><button type="button" class="btn btn-isic2 btn-block" id="rotate_right1" onclick="rotate1(2,1);"><i class="fa fa-redo" aria-hidden="true"></i>Rotate Right <br><span class="subEn subColor"></span></button></p>
													<p class="col-xs-6"><button type="button" class="btn btn-isic2 btn-block" id="crop_button1" onclick="crop1(1);"><i class="fa fa-crop" aria-hidden="true"></i>Crop <br><span class="subEn subColor"></span></button></p>
													<p class="col-xs-6"><button type="button" class="btn btn-isic2 btn-block" onclick="unselect1(1);"><i class="fa fa-retweet" aria-hidden="true"></i>Reset <br><span class="subEn subColor"></span></button></p>
												</div>
											</div>
										</div>
									</div>
									<div>
									</div>                
								</div>
							</td>
						</tr>
					</table>
					<div class="buttonDiv">
						<button type="button" class="btn btn-lg btn-isic" onclick="submit01(1)">Upload<span class="enSub"></span><i class="fa fa-caret-right"></i></button>
					</div>	
				</div>			
			</div>
		</form>
        <!-- End contents -->