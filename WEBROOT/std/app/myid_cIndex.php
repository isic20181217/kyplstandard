<?php 
// 200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;				
	}
?>
<div id="appTableDiv">
	<div class="myidWrapDiv">
		<h3 class="pcml">Please select a MY ID menu.</h3>	
		<p class="myidSub pb20 pcml">On MY ID, customers who have completed the online application for issuing ISIC, IYTC, and ITIC can view the application details.</p>
		<div id="myid_index_btn" class="row">
			<div class="col-xs-5 col-sm-3 myid_index_btn">
				<a href="./myid_main.php?menu=status" class="btn btn-lg btn-block myid_icon" style="background: #9fc554; color: #fff;">
					<p><i class="fa fa-list fa-2x"></i></p>
					<p class="m0">Online <br class="mo">application <i class="fa fa-caret-right" aria-hidden="true"></i></p>
				</a>
			</div>
			<div class="col-xs-7 col-sm-9">
				<h4 style="color: #9fc554;"><span class="small">Online application <span class="subEn"></span></span></h4>
				<p class="mb5">You can view and delete your application you registered to issue ISIC, IYTC, and ITIC card, and delete or register your photo on the online application. (On 「Online application」, you can see only the application registration status, and you can see the personal information registered on the application on 「Personal information」.)</p>
				<p class="subEn cGrey"></p>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-5 col-sm-3 myid_index_btn">
				<a href="./myid_main.php?menu=card" class="btn btn-lg btn-block myid_icon" style="background: #65b6b7; color: #fff;">
					<p><i class="fas fa-address-card fa-2x"></i></p>
					<p class="m0">My card <i class="fa fa-caret-right" aria-hidden="true"></i><br><small></small></p>
				</a>
			</div>
			<div class="col-xs-7 col-sm-9">
				<h4 style="color: #65b6b7;"><span class="small">My card <span class="subEn"></span></span></h4>
				<p class="mb5">You can see the image and information of the card issued.<br class="mx"> Download the mobile ISIC App and activate your mobile ID after confirming card issuance <br class="mx"> to be prepared just in case your card is lost while traveling.</p>
				<p class="subEn cGrey"></p>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-5 col-sm-3 myid_index_btn">
				<a href="./myid_main.php?menu=pInfo" class="btn btn-lg btn-block myid_icon" style="background: #51519c; color: #fff;">
					<p><i class="fas fa-address-book fa-2x"></i></p>
					<p class="m0">Personal <br class="mo">information <i class="fa fa-caret-right" aria-hidden="true"></i></p>
				</a>
			</div>
			<div class="col-xs-7 col-sm-9">
				<h4 style="color: #51519c;"><span class="small">Personal information <span class="subEn"></span></span></h4>
				<p class="mb5">You can view your personal information registered on the online application form for the issuance of your card. We inform you your personal information divided into two types, the personal information stored to keep the information recorded on the card during the period you use the card, and the one that we keep for the purpose of providing information to you. You can view your personal information and delete it if you want to.</p>
				<p class="subEn cGrey"></p>
			</div>
		</div>
	</div>
</div>