<?php 
// 200601 check
	session_start();
	if(isset($_SESSION['validCount']) && isset($_SESSION['myIdUserIP'])){
		$validCount = (int)$_SESSION['validCount'];
		$userIP = $_SERVER['REMOTE_ADDR'];
		if($validCount > 0){
		//if($validCount > 0 && $_SESSION['myIdUserIP'] === $userIP){
		}
		else{
			/*
			echo '<script>alert(\'Forbidden.\');</script>';
			session_destroy();
			echo '<script>location.replace("/std/app/myid_main.php");</script>';
			exit;	
			*/
		}
	}
	else{
		echo '<script>alert(\'Forbidden.\');</script>';
		session_destroy();
		echo '<script>location.replace("../myid_main.php");</script>';
		exit;				
	}
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
	require __DIR__.$goParent.$reqDir1.'/_require1/function.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/encDec.php';
	try {
		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		for($i=1; $i < $validCount+1; $i++){
			$query = "SELECT * FROM $tablename07 LEFT JOIN  $tablename18 ON $tablename07.appNo = $tablename18.cusAppNo WHERE appNo=:searchValue1;";
			$stmt = $db->prepare($query);
			$stmt->bindParam(':searchValue1', $_SESSION['validApp'][$i]);
			if($stmt->execute()){
				$appList[$i] = $stmt->fetch(PDO::FETCH_OBJ);
			}
			else{
				echo '<script>alert(\'Forbidden.\');</script>';
				session_destroy();
				$db= NULL;
				echo '<script>location.replace("../myid_main.php");</script>';
				exit;
			}
		}
		$db= NULL;
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		exit;
	}
	function read_img($index,$name,$ext){
		$index =(int)$index;
		$_SESSION[md5($name)][$index]='imgLoad';
		$now =time();
		$src="./work/imgReader.php?time=".$now."&name=".$name."&ext=".$ext."&index=".$index;
		return $src;
	}
	/*
	echo '<pre>';
	print_r($appList);
	echo '</pre>';
	*/
?>
<script type="text/javascript" src="./js/myid_cPInfo.js"></script>
<div id="appTableDiv">
	<div class="myidWrapDiv">
	<h3>Personal information</h3>	

	<div class="col-xs-12">
		<?php for($i=1; $i < $validCount+1; $i++){ ?>
		<div class="row">
			<div class="col-sm-6 p0">
				<table class="table table-bordered pInfoTable">
					<tr>
						<th><span style="color: #f00;">*</span> Photo</th>
						<td class="text-center">
							<?php if(isset($appList[$i]->photoName)) {?>
							<?php $photoName = pathinfo($appList[$i]->photoName); ?>
							<p id="myInfo_photo" style="height: 130px;"><img src="<?php echo read_img('1',$photoName['filename'],$photoName['extension']);?>"></p>
							<?php }else{} ?>
							<?php if($appList[$i]->status === 'approved'){ ?>
							<?php }else{ ?>
							<div class="mt20 text-center">
								<a href="./myid_main.php?menu=photoUpload&no=<?php echo $i?>" class="btn btn-isic">Change photo</a>
							</div>
							<?php } ?>
						</td>
					</tr>
					<tr>
						<th><span style="color: #f00;">*</span> Name</th>
						<td>
							<?php if($appList[$i]->status === 'approved'){ ?>
							<div class="row">
								<div class="col-sm-4 ppr3"><?php echo decrypt1($appList[$i]->engFname,$appList[$i]->encId)?></div>
								<div class="col-sm-4 ppl3 ppr3"><?php echo decrypt1($appList[$i]->engLname,$appList[$i]->encId)?></div>
								<div class="col-sm-4 ppl3"></div>
							</div>
							<?php }else{ ?>
							<div class="row">
								<div class="col-sm-4 ppr3"><input type="text" class="form-control input-sm" value="<?php echo decrypt1($appList[$i]->engFname,$appList[$i]->encId)?>"  name="engFname<?php echo $i?>" id="engFname<?php echo $i?>" maxlength="20"></div>
								<div class="col-sm-4 ppl3 ppr3"><input type="text" class="form-control input-sm" value="<?php echo decrypt1($appList[$i]->engLname,$appList[$i]->encId)?>" name="engLname<?php echo $i?>" id="engLname<?php echo $i?>" maxlength="20"></div>
								<div class="col-sm-4 ppl3"><button type="button" class="btn btn-block btn-isic btn-sm" onclick="submit01(<?php echo $i?>,1)">Save</button></div>
							</div>
							<?php } ?>
						</td>
					</tr>
					<tr>
						<th><span style="color: #f00;">*</span> Date of birth</th>
						<td><?php echo decrypt1($appList[$i]->birthDay,$appList[$i]->encId)?>/<?php echo decrypt1($appList[$i]->birthMonth,$appList[$i]->encId)?>/<?php echo decrypt1($appList[$i]->birthYear,$appList[$i]->encId)?></td>
					</tr>
					<tr>
						<th><span style="color: #f00;"></span>New Password</th>
						<td>
							<div class="col-sm-8 ppr3"><input type="password" class="form-control input-sm" value="" placeholder="(More than 6 character)" name="password<?php echo $i?>" id="password<?php echo $i?>" maxlength="20"></div>
							<div class="col-sm-4 ppl3"><button type="button" class="btn btn-block btn-isic btn-sm" onclick="submit01(<?php echo $i?>,2)">Save</button></div>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-6 p0">
				<table class="table table-bordered pInfoTable">
					<tr>
						<th><span style="color: #f00;">*</span> Studies/Teaches at</th>
						<td> <?php echo decrypt1($appList[$i]->school,$appList[$i]->encId)?></td>
					</tr>
					<tr>
						<th><span style="color: #f00;">* </span>Nationality</th>
						<td><?php echo $countryName[$appList[$i]->nationality]?></td>
					</tr>
					<tr>
						<th>Gender</th>
						<?php if(isset($appList[$i]->gender) && $appList[$i]->gender === 'm'){?>
						<td>Male<button class="btn btn-xs btn-default dBtn" onclick='deleteValue(<?php echo $i?>,2)'>Remove</button></td>
						<?php }else if (isset($appList[$i]->gender) &&  $appList[$i]->gender === 'f'){ ?>
						<td>Female<button class="btn btn-xs btn-default dBtn" onclick='deleteValue(<?php echo $i?>,2)'>Remove</button></td>
						<?php }else { ?>
						<td></td>
						<?php } ?>
					</tr>
					<tr>
						<th>Phone no.</th>
						<td><?php echo decrypt1($appList[$i]->phone,$appList[$i]->encId)?><button class="btn btn-xs btn-default dBtn" onclick='deleteValue(<?php echo $i?>,4)'>Remove</button></td>
					</tr>
					<tr>
						<th>Email</th>
						<td><?php echo decrypt1($appList[$i]->email,$appList[$i]->encId)?><!--<button class="btn btn-xs btn-default dBtn" onclick='deleteValue(<?php //echo $i?>,3'>Remover</button>-->
								<?php 
									if ($appList[$i]->emailNews === 'yes'){
										$emailNewsYes ='checked="checked"';
										$emailNewsNo =' ';
									}
									else{
										$emailNewsYes =' ';
										$emailNewsNo = 'checked="checked"';
									}
								?>		
								<div class="pt5 small">
									<p class="mb5"><input type="radio" name="emailNews<?php echo $i ?>" id="emailYes<?php echo $i ?>" value="yes" <?php echo $emailNewsYes?>>Permit to receive newsletter<br/><span class="subEn cGrey"></span></div>
									<p class="m0"><input type="radio" name="emailNews<?php echo $i ?>" id="emailNo<?php echo $i ?>" value="no" <?php echo $emailNewsNo?>>Not permit to receive newsletter<br/><span class="subEn cGrey"></span></div>
								</div>
								<button type="button" class="btn btn-block btn-isic btn-sm" onclick="submit01(<?php echo $i?>,3)">Save</button>
						</td>
					</tr>
					<tr>
						<th>Address</th>
						<td><button class="btn btn-xs btn-default dBtn" onclick='deleteValue(<?php echo $i?>,6)'>Remove</button>
						<?php echo decrypt1($appList[$i]->address,$appList[$i]->encId)?> 
						<?php echo decrypt1($appList[$i]->address2,$appList[$i]->encId)?> 
						<?php echo decrypt1($appList[$i]->city,$appList[$i]->encId)?> 
						<?php echo decrypt1($appList[$i]->country,$appList[$i]->encId)?> 
						<?php echo decrypt1($appList[$i]->postal,$appList[$i]->encId)?></td>
					</tr>
				</table>
			</div>
		</div>
		<?php } ?>
		<ul style="padding:15px 0 0 0; font-size:90%;">
			<li>The personal information written above is used for issuing the ID applied. <span class="subEn cGrey"></span></li>
			<li class="mt5">The information with <strong style="color: #f00;">*</strong> mark must be kept during the ID validity. Other personal information can be deleted during the ID validity. Please request to register the customer information again after deletion if the customer information is required to use online affiliate benefits or ID card benefits.
			<span class="subEn cGrey"></span></li>
			<li class="mt5">If you delete the Email address, you need to register another password as MY ID login information to log in MY ID.<br class="mx"><span class="subEn cGrey"></span></li>
			<li class="mt5">The address can be used as the card delivery address, and phone number and Email can be used as an emergency contact information if the card is lost. <span class="subEn cGrey"></span></li>
			<li class="mt5">The information with a <strong style="color: #f00;">*</strong>  mark is reflected in the issuance of the card. Please contact <a href="mailto:<?php echo $officeEmail?>"><?php echo $officeEmail?></a> if the information with a <strong style="color: #f00;">*</strong> mark needs to be corrected before issuing the card.<span class="subEn cGrey"></span></li>
			<li class="mt5">Registering the online application, you have confirmed and agreed to the 「Privacy policy」 and the 「Terms &amp; conditions」 below.<span class="subEn cGrey"></span>
				<div class="row mt5">
					<div class="col-sm-6 pr3">
						<p class="termsP">Privacy policy</p>
						<div class="termDiv" style="height: 150px;"><iframe scrolling="auto" src="./App_privacy.html" marginheight="0" frameborder="0" width="100%" height="150"></iframe></div>
					</div>
					<div class="col-sm-6 pl3">
						<p class="termsP">Terms &amp; conditions</p>
						<div class="termDiv" style="height: 150px;"><iframe scrolling="auto" src="./App_terms.html" marginheight="0" frameborder="0" width="100%" height="150"></iframe></div>
					</div>
				</div>
			</li>
		</ul>
		</div>
	</div>
</div>