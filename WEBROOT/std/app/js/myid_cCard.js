///200602 check		
var widthLimit = 540;
var heightLimit = 331;
var widthLimitM = 260;
var heightLimitM = 163;
var photocanvas = new Array();
var itemOneArray = new Array();
var scaleFactor = widthLimitM / widthLimit;
function classItem(){
	this.c;
	this.ctx;
	this.img;
	this.w;
	this.h;
	this.x1;
	this.y1;
	this.x2;
	this.y2;
	this.newx1;
	this.newy1;
	this.newx2;
	this.newy2;
	this.wcalc;
	this.hcalc;
	this.scale;
	this.rotateAng;
	this.phase;
	this.filename;
	this.croped;
	this.detail;
	this.appDetail;
	this.total;
	this.tmpImg;
}

window.onload =function(){
	var maxnum = Number(document.getElementById('maxnum').value);
	//console.log(maxnum);
	for(i=1;i<maxnum+1;i++){
		if(document.getElementById('prevUrl'+String(i))){
			var filename = document.getElementById('prevUrl'+String(i)).value;
			imgload(i,'prev'+String(i),filename);
		}else{}
	}
}
function imgload(canvasNum,canvasName,filename){
	var timestamp1 = new Date().getTime();
	photocanvas[canvasNum]= new classItem();
	photocanvas[canvasNum].phase = '0';
	photocanvas[canvasNum].detail = document.getElementById('prevDetail'+String(i)).value;
	photocanvas[canvasNum].appDetail = document.getElementById('prevAppDetail'+String(i)).value;
	var detailOne = photocanvas[canvasNum].detail.split('/cEnd/');
	var appDetailOne = photocanvas[canvasNum].appDetail.split('/aEnd/');
	//console.log(detailOne);
	//console.log(appDetailOne);
	photocanvas[canvasNum].total = document.getElementById('prevTotal'+String(i)).value;
	//console.log(photocanvas[canvasNum].phase);
	photocanvas[canvasNum].c=document.getElementById(canvasName);
	photocanvas[canvasNum].ctx=photocanvas[canvasNum].c.getContext('2d');
	photocanvas[canvasNum].c2=document.getElementById(canvasName+'m');
	photocanvas[canvasNum].ctx2=photocanvas[canvasNum].c2.getContext('2d');	
	photocanvas[canvasNum].img = new Image();
	photocanvas[canvasNum].img.onload = function(){
		photocanvas[canvasNum].w = photocanvas[canvasNum].img.width;
		photocanvas[canvasNum].h = photocanvas[canvasNum].img.height;
		
		// when the image is too small to resize.
		if (photocanvas[canvasNum].w < widthLimit || photocanvas[canvasNum].h < heightLimit){
			photocanvas[canvasNum].scale = 1;
			photocanvas[canvasNum].wcalc = photocanvas[canvasNum].w;
			photocanvas[canvasNum].hcalc = photocanvas[canvasNum].h;
		}
		else{
			if (photocanvas[canvasNum].w < photocanvas[canvasNum].h){
				photocanvas[canvasNum].scale = heightLimit / photocanvas[canvasNum].h;
				photocanvas[canvasNum].wcalc = photocanvas[canvasNum].scale * photocanvas[canvasNum].w;
				photocanvas[canvasNum].wcalc = Math.round(photocanvas[canvasNum].wcalc);
				photocanvas[canvasNum].hcalc = heightLimit;
			}
			//
			else if(photocanvas[canvasNum].w > photocanvas[canvasNum].h){
				photocanvas[canvasNum].scale = widthLimit / photocanvas[canvasNum].w;
				photocanvas[canvasNum].hcalc = photocanvas[canvasNum].scale * photocanvas[canvasNum].h;
				photocanvas[canvasNum].hcalc = Math.round(photocanvas[canvasNum].hcalc);
				photocanvas[canvasNum].wcalc = widthLimit;
			}
			//when width = height
			else{
				photocanvas[canvasNum].scale = heightLimit / photocanvas[canvasNum].h;
				photocanvas[canvasNum].wcalc = widthLimit;
				photocanvas[canvasNum].hcalc = widthLimit;
			}
		}
		//photocanvas[canvasNum].ctx.clearRect(0,0,9999,9999);
		photocanvas[canvasNum].ctx.drawImage(photocanvas[canvasNum].img,0,0, widthLimit,heightLimit);
		photocanvas[canvasNum].wcalc= widthLimit * scaleFactor;
		photocanvas[canvasNum].hcalc= heightLimit * scaleFactor;
		//photocanvas[canvasNum].ctx2.clearRect(0,0,9999,9999);
		photocanvas[canvasNum].ctx2.drawImage(photocanvas[canvasNum].img,0,0, photocanvas[canvasNum].wcalc,photocanvas[canvasNum].hcalc);		
		photocanvas[canvasNum].filename = filename;
		//console.log(photocanvas);
		//console.log('onload');
		photocanvas[canvasNum].tmpImg = new Array();

		for(i=0;i<photocanvas[canvasNum].total;i++){
			var itemOne = detailOne[i].split('/iEnd/');
			itemOneArray[i] = itemOne[0].split('_');
			
			if(itemOneArray[i][0] === 'i'){
				var imgFilename = appDetailOne[i];
				photocanvas[canvasNum].tmpImg[i] = new Image();
				photocanvas[canvasNum].tmpImg[i].imgX = Number(itemOneArray[i][1]);
				photocanvas[canvasNum].tmpImg[i].imgY= Number(itemOneArray[i][2]);
				photocanvas[canvasNum].tmpImg[i].imgW = Number(itemOneArray[i][3]);
				photocanvas[canvasNum].tmpImg[i].imgH = Number(itemOneArray[i][4]);			
				photocanvas[canvasNum].tmpImg[i].onload = function(){
					if(this.width>this.height){
						this.croppedW = this.imgH/this.imgW*this.width;
						photocanvas[canvasNum].ctx.drawImage(this,0,0,this.croppedW,this.height,this.imgX,this.imgY,this.imgW,this.imgH);
					}else if (this.width<this.height){
						this.croppedH = this.imgH/this.imgW*this.width;
						photocanvas[canvasNum].ctx.drawImage(this,0,0,this.width,this.croppedH,this.imgX,this.imgY,this.imgW,this.imgH);	
					}else{
						photocanvas[canvasNum].ctx.drawImage(this,0,0,this.width,this.height,this.imgX,this.imgY,this.imgW,this.imgH);
					}	

					this.imgXm = this.imgX * scaleFactor;
					this.imgYm = this.imgY * scaleFactor;
					this.newimgWm = this.newimgW * scaleFactor;
					this.newimgHm = this.newimgH * scaleFactor;
					photocanvas[canvasNum].ctx2.drawImage(this,this.imgXm,this.imgYm,this.newimgWm,this.newimgHm);
				}
				photocanvas[canvasNum].tmpImg[i].src = appDetailOne[i]+'?'+timestamp1;

			}else if(itemOneArray[i][0] ==='t'){
				
				var defFont = window.getComputedStyle(document.body,null).getPropertyValue("font-family");
				var fontSize = itemOneArray[i][3];
				var xPos = itemOneArray[i][1];
				var yPos = itemOneArray[i][2];
				photocanvas[canvasNum].ctx.font = (String(fontSize)+'px'+' '+defFont);
				photocanvas[canvasNum].ctx.fillText(appDetailOne[i],xPos,yPos);
				fontSizem = fontSize * scaleFactor; 
				xPosm = xPos * scaleFactor;
				yPosm = yPos * scaleFactor;
				photocanvas[canvasNum].ctx2.font = (String(fontSizem)+'px'+' '+defFont);
				photocanvas[canvasNum].ctx2.fillText(appDetailOne[i],xPosm,yPosm);
				
			}else{
				consolo.log('error');
			}
			
		}
	}
	photocanvas[canvasNum].img.onerror = function(){
		document.getElementById('prev'+String(canvasNum)).style.display = "none";
		//console.log('onerror');
		
	}
	photocanvas[canvasNum].img.src = filename+'?'+timestamp1;
	
}