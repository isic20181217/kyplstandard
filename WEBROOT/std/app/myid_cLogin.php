<?php 
// 200601 check
?>
<script type="text/javascript" src="./js/myid_cLogin.js?ver=190306"></script>
<div id="appTableDiv">
	<div class="myidWrapDiv">
		<form name="myIdLoginForm" id="myIdLoginForm" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
		<input type="hidden" id="pageStatus" name="pageStatus" value="login">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<h3 class="pb30">MY ID Login</h3>	
			<div>
				<div class="form-group" style="padding-bottom:0; border:none;">
				<label class="col-sm-4 control-label" style="text-align:left;">First name : </label>
				<div class="col-sm-8"><input type="text" name="engFname" id="engFname" class="form-control"  maxlength="20"></div>
				</div>
				<div class="form-group" style="padding-bottom:0; border:none;">
					<label class="col-sm-4 control-label" style="text-align:left;">Last name : </label>
					<div class="col-sm-8"><input type="text" name="engLname" id="engLname" class="form-control"  maxlength="20"></div>
				</div>
				<div class="form-group" style="padding-bottom:0; border:none;">
					<label class="col-sm-4 control-label" style="text-align:left;">Email : </label>
					<div class="col-sm-8"><input type="email" name="email" id="email" class="form-control"  maxlength="50"></div>
				</div>
				<div class="form-group" style="padding-bottom:0; border:none;">
					<label class="col-sm-4 control-label" style="text-align:left;">Password : </label>
					<div class="col-sm-8"><input type="password" name="password" id="password" class="form-control" maxlength="20"></div>
				</div>
				<p class="buttonDiv"><button type="button" onclick="submit01(1)" class="btn btn-lg btn-isic btn-block">Login</button>
				<a href="./myid_cLogout.php" class="btn btn-lg btn-isic btn-block">Logout<a></p> 
			</div>       
		</div>
		<div class="col-sm-2"></div>
		<div style="clear:both;"></div>
		</form>
	</div>
</div>