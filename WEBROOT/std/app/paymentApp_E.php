  <!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Payment</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="./css/bt.min.css" rel="stylesheet" type="text/css" />
<link href="./css/yoonCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php 
// 201113 check
?>
<?php
	session_start();
	$now = time();
	$today1 = date("Y-m-d H:i:s",$now);
	$goParent ='/..';
	$goParent2 ='/../..';
	$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
	require __DIR__.$goParent.'/req.php';
	require __DIR__.$goParent.$reqDir1.'/_require1/setting.php';	
// test log start
	function log2File1($logPath,$logTime1,$logName1,$logMsg) {
		$file = fopen($logPath,"a+");
		fwrite($file,$logTime1." ");
		fwrite($file,$logName1." ");
		fwrite($file,print_r($logMsg,true));
		fwrite($file,"\n");
		fclose($file);
	}
	if(isset($_GET) ) {
		$logPath = __DIR__.$goParent.$reqDir1.'/logs/paymentLog.txt';
		log2File1($logPath, $today1, 'paymentApp_E_GET', $_GET);
	} else {
		echo 'Access Denied44';
		log2File1($logPath, $today1, 'paymentApp_E', 'error44');
		exit;
	}
// test log end
?>
<?php
	// check value start
	if(isset($_GET['no']) && isset($_GET['backMenu']) && isset($_GET['startFrom']) &&isset($_GET['pKey'])){
		$paymentKey = preg_replace("/<|\/|_|>/","", $_GET['pKey']);
	}else {
		echo 'Access Denied444';
		$db= NULL;
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		//log2File1($logPath, $today1, 'paymentApp_E', 'error444');
		exit;
	}
	switch($_GET['backMenu']){
		case 'app02':
			$backMenu = '/std/app/app02.php';
			break;
		default :
			$backMenu = '/std/app/myid_main.php';
			break;
	}
	if(isset($_SESSION[$paymentKey]->p_status) && $_SESSION[$paymentKey]->p_status == 'start')
	{
		echo '<script>alert(\'forbidden111\');</script>';
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
	}
	else if (isset($_SESSION[$paymentKey]->p_status) && $_SESSION[$paymentKey]->p_status == 'end')
	{
		echo '<script>alert(\'forbidden222\');</script>';
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
	}
	else
	{}
	// check value end
	
	// 'pseudo POST' start
	$_POST['p_method']= $_SESSION[$paymentKey]->p_method;
	$_POST['no'] = $_SESSION[$paymentKey]->no;
	$_POST['p_goods'] =$_SESSION[$paymentKey]->p_goods;
	$_POST['p_price'] = $_SESSION[$paymentKey]->p_price;
	$_POST['p_rurl'] =$_SESSION[$paymentKey]->p_rurl;
	if(isset($_GET['pT']))
	{
		switch($_GET['pT'])
		{
			case '1':
				$_POST['p_method'] = 'card';
				$_SESSION[$paymentKey]->p_method ='card';
				break;
			case '2':
				$_POST['p_method'] = 'atm';
				$_SESSION[$paymentKey]->p_method ='atm';
				break;
			default :
				echo '<script>location.replace("/std/app/myid_main.php");</script>';
				//log2File1($logPath, $today1, 'paymentApp_E', 'error444');
				exit;
		}
	}
	else
	{
		echo '<script>location.replace("/std/app/myid_main.php");</script>';
		//log2File1($logPath, $today1, 'paymentApp_E', 'error444');
		exit;
	}
	// 'pseudo POST' end
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	*/
	// test log start
	if(isset($_POST) ) {
		$logPath = __DIR__.$goParent.$reqDir1.'/logs/paymentLog.txt';
		log2File1($logPath, $today1, 'paymentApp_E_POST', $_POST);
	} else {
		echo 'Access Denied45';
		//log2File1($logPath, $today1, 'paymentApp_E', 'error45');
		//echo '<script>location.replace("/std/app/myid_main.php");</script>';
		exit;
	}
	// test log end
	
	// payStatus check start
	try {

		require __DIR__.$goParent.$reqDir1.'/_require1/db_co.php';
		$query = "SELECT appNo FROM $tablename07 WHERE appNo =:appNo AND payStatus ='no' AND paidDate IS NULL";
		$stmt = $db->prepare($query);
		$stmt->bindParam(':appNo', $_POST['no']);
		$stmt->execute();
		if($stmt->rowCount() === 1){
		}else{
			echo 'Access Denied4';
			$db= NULL;
			log2File1($logPath, $today1, 'paymentApp_E', 'error4');
			echo '<script>location.replace("'.$backMenu.'");</script>';
			exit;
		}
		$db= NULL;
		$_SESSION[$paymentKey]->p_status = 'start';
		echo '<script>location.replace("/std/app/work/payment2.php?pKey='.$paymentKey.'");</script>';
	}
	catch (PDOExeception $e){
		//echo "Error: ".$e->getMessage();
		$db= NULL;
		echo 'Access Denied3';
		log2File1($logPath, $today1, 'paymentApp_E', 'error3');
		echo '<script>location.replace("'.$backMenu.'");</script>';
		exit;
	}
	// payStatus check end
	
	// payment process start
	
	// payment process end
?>
</body>
</html>
