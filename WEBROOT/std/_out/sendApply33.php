<?php
// 201112 check
$domainBase ='YOURDOMAINNAME';
if(isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] === $domainBase)
{
	//echo '<pre>';
	//print_r($_POST);
	//echo '</pre>';
	//echo $_SERVER['SERVER_NAME'];
}
else
{
	exit;
}

if(isset($_POST['id'])&& !empty($_POST['id']))
{
	$id1 = $_POST['id'];
}
else
{
	exit;
}
if(isset($_POST['pwd'])&& !empty($_POST['pwd']))
{
	$pwd1 = $_POST['pwd'];
}
else
{
	exit;
}
if(isset($_POST['cardNumber'])&& !empty($_POST['cardNumber']))
{
	$cardNumber = $_POST['cardNumber'];
}
else
{
	exit;
}
if(isset($_POST['cardType'])&& !empty($_POST['cardType']))
{
	switch($_POST['cardType'])
	{
		case 'S':
			$cardType = 'ISIC';
			break;
		case 'T':
			$cardType = 'ITIC';
			break;
		case 'Y':
			$cardType = 'IYTC';
			break;
		default:
			exit;
	}
}
else
{
	exit;
}

if(isset($_POST['cardStatus'])&& !empty($_POST['cardStatus']))
{
	switch($_POST['cardStatus'])
	{
		case '1':
			$cardStatus = 'VALID';
			break;
		case '2':
			$cardStatus = 'VOIDED';
			break;
		default:
			exit;
	}
}
else
{
	exit;
}
if(isset($_POST['printedName'])&& !empty($_POST['printedName']))
{
	$printedName = $_POST['printedName'];
}
else
{
	exit;
}
if(isset($_POST['firstName'])&& !empty($_POST['firstName']))
{
	$firstName = $_POST['firstName'];
}
else
{
	exit;
}
if(isset($_POST['lastName'])&& !empty($_POST['lastName'])){
	$lastName = $_POST['lastName'];
}else{
	
	exit;
}

if(isset($_POST['dateOfBirth'])&& !empty($_POST['dateOfBirth']))
{
	$dateOfBirthArray = explode("/",$_POST['dateOfBirth']);
	if(isset($dateOfBirthArray[0]) && !empty($dateOfBirthArray[0]) && isset($dateOfBirthArray[1]) && !empty($dateOfBirthArray[1]) && isset($dateOfBirthArray[2]) && !empty($dateOfBirthArray[2]) )
	{
		$dateOfBirth = $dateOfBirthArray[2].'-'.$dateOfBirthArray[1].'-'.$dateOfBirthArray[0];
	}
	else
	{
		exit;
	}
}
else
{
	exit;
}

if(isset($_POST['validFrom'])&& !empty($_POST['validFrom']))
{
	$validFromArray = explode("/",$_POST['validFrom']);
	if(isset($validFromArray[0]) && !empty($validFromArray[0]) && isset($validFromArray[1]) && !empty($validFromArray[1]) )
	{
		$validFrom = $validFromArray[1].'-'.$validFromArray[0].'-01';
	}
	else
	{
		exit;
	}
}
else
{
	exit;
}
if(isset($_POST['validTo'])&& !empty($_POST['validTo']))
{
	$validToArray = explode("/",$_POST['validTo']);
	if(isset($validToArray[0]) && !empty($validToArray[0]) && isset($validToArray[1]) && !empty($validToArray[1]) )
	{
		$validTo = date("Y-m-t",mktime(0,0,3,$validToArray[0],1,$validToArray[1]));
	}
	else
	{
		exit;
	}
}
else
{
	exit;
}
if(isset($_POST['institutionName'])&& !empty($_POST['institutionName']))
{
	$institutionName = $_POST['institutionName'];
}
else
{
	$institutionName='None';
}
if(isset($_POST['issuedBy'])&& !empty($_POST['issuedBy']))
{
	$issuedBy = $_POST['issuedBy'];
}
else
{
	exit;
}
if(isset($_POST['email'])&& !empty($_POST['email']))
{
	$email = $_POST['email'];
}
else
{
	exit;
}
if(isset($_POST['issuedOn'])&& !empty($_POST['issuedOn'])){
	$issuedOnArray = explode("/",$_POST['issuedOn']);
	if(isset($issuedOnArray[0]) && !empty($issuedOnArray[0]) && isset($issuedOnArray[1]) && !empty($issuedOnArray[1]) && isset($issuedOnArray[2]) && !empty($issuedOnArray[2]) ){
		$issuedOn = date("c", mktime(0, 0, 0, $issuedOnArray[1], $issuedOnArray[0], $issuedOnArray[2]) );
	}else{
		exit;
	}
}else{
	
	exit;
}
if(isset($_POST['issueType'])&& !empty($_POST['issueType']))
{
	switch($_POST['issueType'])
	{
		case 'P':
		case 'p':
			$issueType = 'PLASTIC_CARD';
			break;
		case 'V':
		case 'v':
			$issueType = 'VIRTUAL_CARD';
			break;
		default:
			exit;
	}
}
else
{
	
	exit;
}

$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><card></card>');
$xml->addChild('cardNumber',$cardNumber);
$xml->addChild('cardType',$cardType);
$xml->addChild('cardStatus',$cardStatus);
$xml->addChild('printedName',$printedName);
$xml->addChild('firstName',$firstName);
$xml->addChild('lastName',$lastName);
$xml->addChild('dateOfBirth',$dateOfBirth);
$xml->addChild('validFrom',$validFrom);
$xml->addChild('validTo',$validTo);
$xml->addChild('institutionName',$institutionName);
$xml->addChild('issuedBy',$issuedBy);
$xml->addChild('email',$email);
$xml->addChild('issuedOn',$issuedOn);
$xml->addChild('issuedBy',$issuedBy);
$xmlNew = $xml->asXML();

$goParent ='/..';
$goParent2 ='/../..';
$reqKey = hash('sha256', $_SERVER['SERVER_ADDR']); 
require __DIR__.$goParent.'/req.php';
require __DIR__.$goParent.$reqDir1.'/_require1/outValidKey2.php';	
$requestUrl = $urlBase.$urlCreateUpdate;
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
curl_setopt($ch, CURLOPT_USERPWD, $id1 . ":" . $pwd1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlNew);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_URL, $requestUrl);
$response10 = curl_exec($ch);
echo $response10;
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
if(isset($httpcode) && !empty($httpcode))
{
	echo $httpcode;
}
else
{
	echo 'noResponse';
}
?>