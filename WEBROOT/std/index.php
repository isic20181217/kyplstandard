<html>
<head>
<meta charset="UTF-8">
<title>ISIC</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="issuing/css/bt.css" rel="stylesheet" type="text/css" />
<link href="issuing/css/yoonCustom.css" rel="stylesheet" type="text/css" />
<link href="css/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		body {background:#777 !important; padding:120px 10px; color:#666; font-size:13px; max-width: 1280px; margin: 0 auto;}
		#wrap {padding:0; margin: 0;}
		#contents {padding: 80px 40px 50px 40px; margin: 0; min-height: 750px;}
		h1 {font-size: 28px; text-align: center; margin: 0; padding-bottom: 20px;}
    h2 {font-size: 17px; color: #3F9393; font-weight: bold; text-align: center; margin-bottom: 20px;}
		
    
    .unit {margin-bottom: 40px; height: 330px; background: #F9F7ED;}
    .unit th {vertical-align: top !important; padding: 10px 10px 0 10px; text-align: center;}
    .unit td {vertical-align: bottom !important; padding: 20px 5px; font-size:13px; line-height: 140%;}
    .unit ul {list-style: none; padding: 0 20px;}
    .unit img {width: 95%;}
    
    .text-isic {color: #3F9393;}
    .inherit {color: inherit!important; text-decoration: none!important;}
    
    @media (max-width: 750px) {
		#contents {padding: 30px;}
      .unit {width: 100%;}
      body {padding:10px;}
    }
	</style>
</head>
<?php
	// 210625
?>
<body>
<div id="wrap">
<div id="contents">
    
  <div class="row pb-20">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <div style="border-bottom: 3px solid #44bebf;">
        <h1>KYPL solutions</h1>
      </div>
    </div>
    <div class="col-sm-4"></div>
  </div>
    
    
    <div class="row pt-15 mt-30">
      
    	<div class="col-sm-3">
        <h2><br>About KYPL solutions</h2>
        <a href="aboutKYPL.html" class="inherit">
          <table class="unit">
            <tr>
              <th><img src="issuing/images/index_solution.png"></th>
            </tr>
            <tr>
              <td>
                <ul>
                  <li><i class="fa fa-check text-isic mr-5"></i> KYPL solutions introduction</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> System summary</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Setup guide</li>
                </ul>
              </td>
            </tr>
          </table>
        </a>
      </div>
      
    	<div class="col-sm-3">
        <h2><br>ID issuing system</h2>
        <a href="./issuing/login.php" class="inherit" target="_blank">
          <table class="unit">
            <tr>
              <th><img src="issuing/images/index_issuing.png"></th>
            </tr>
            <tr>
              <td>
                <ul>
                  <li><i class="fa fa-check text-isic mr-5"></i> Auto print </li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Virtual issuing</li>
                </ul>
              </td>
            </tr>
          </table>
        </a>
      </div>
      
    	<div class="col-sm-3">
        <h2>Online application<br>URL creation system</h2>
        <a href="./app/app01.php?appSet=tstd" class="inherit" target="_blank">
          <table class="unit">
            <tr>
              <th><img src="issuing/images/index_application.png"></th>
            </tr>
            <tr>
              <td>
                <ul>
                  <li><i class="fa fa-check text-isic mr-5"></i> Create an online application URL <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for IO or partners.</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Customize subsections of <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;an online application.</li>
                </ul>
              </td>
            </tr>
          </table>
        </a>
      </div>
      
    	<div class="col-sm-3">
        <h2><br>My ID</h2>
        <a href="./app/myid_main.php" class="inherit" target="_blank">
          <table class="unit">
            <tr>
              <th><img src="issuing/images/index_myid.png"></th>
            </tr>
            <tr>
              <td>
                <ul>
                  <li><i class="fa fa-check text-isic mr-5"></i> Application status</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Payment status</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> ID issuing status</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Card delivery status</li>
                  <li><i class="fa fa-check text-isic mr-5"></i> Manage personal information</li>
                </ul>
              </td>
            </tr>
          </table>
        </a>
      </div>
    </div>
  
  
</div>
</div>
  

<div class="p-20" style="background: #D5EFEE; color: #3F9393;">
	<div class="row">
		<div class="col-xs-6 text-left">
      <p class="m-0">KYPL platform © Arete Consulting GmbH</p>
    </div>
		<div class="col-xs-6 text-right">
			<p class="m-0"><a href="./issuing/images/KyplSoftwareConditions-eng.pdf" target="_blank" style="color: inherit;"><i class="far fa-file-alt"></i> KYPL software terms &amp; conditions</a></p>
		</div>
	</div>
</div>
</body>
</html>