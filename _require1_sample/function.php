<?php
// 210702
	$curKey = hash('sha256', $_SERVER['SERVER_ADDR']);
	if(isset($reqKey) && $reqKey === $curKey){
	}else{
		echo 'forbidden';
		exit;
	}
	function forbid(){
		echo 'forbidden';
		//echo '<script>location.replace("../app01.php");</script>';
		exit;
	}
	//variable for Image resize
	$recalSize;
	$recalWidth = 0;
	$recalHeight = 0;
	//$recalScale = 0;
	//load image 
	function createImage($ext,$tmpName){ 
		$createdImage =NULL;
		switch($ext) {
			case 'jpg':
				ini_set('memory_limit', '256M');
				@ini_set('gd.jpeg_ignore_warning', 1);
				$createdImage = @imagecreatefromjpeg($tmpName); //hide wrong extension message for now
				echo '...';
				if ($createdImage === false){
					echo '...';
					return 1;
				}
				else{
					echo '...';
					break;
				}
			case 'png':
				ini_set('memory_limit', '256M');
				$createdImage = imagecreatefrompng($tmpName);
				echo '...';
				if ($createdImage === false){
					echo '...';
					return 1;
				}
				else{
					echo '...';
					break;
				}
			default:
				return 2;
		}
		return $createdImage;
	}
	//calculate new image size (max height or max width is length)
	function calculateImage($fileName,$length,$isFile){
		global $recalSize;
		if($isFile ==='YES'){
			$recalSize = getimagesize($fileName);
		}
		else{
			$recalSize[0] =imagesx($fileName);
			$recalSize[1] =imagesy($fileName);
		}
		global $recalWidth;
		$recalWidth = $recalSize[0];
		global $recalHeight;
		$recalHeight = $recalSize[1];
		$recalScale;
		if ($recalWidth < $recalHeight){
			$recalScale = $length / $recalHeight;
			$recalWidth = $recalScale * $recalWidth;
			$recalWidth = round($recalWidth);
			$recalHeight = $length;
		}
		else if($recalWidth > $recalHeight){
			$recalScale = $length / $recalWidth;
			$recalHeight = $recalScale * $recalHeight;
			$recalHeight = round($recalHeight);
			$recalWidth = $length;
		}
		else{
			$recalWidth = $length;
			$recalHeight = $length;
		}
		//if the image smaller than length return 2(no need to resize image)
		if ($recalSize[0] < $length || $recalSize[1] <$length){
			return 2;
		}
		else{}
	}
	//Crop,Rotate,Resize and Upload Image
	function upload2($inputName,$saveName,$dstDir){
		global $recalSize;
		global $recalWidth;
		global $recalHeight;
		//global $recalScale;
		if (isset($_FILES[$inputName]['tmp_name']) && is_uploaded_file($_FILES[$inputName]['tmp_name'])){
			echo '...';
			$tmpName = $_FILES[$inputName]['tmp_name'];
			$name = $_FILES[$inputName]['name'];	
			$imgInfo1 = @getimagesize($tmpName);
			switch($imgInfo1['mime']){
				case 'image/jpeg';
					$ext = 'jpg';
					break;
				case 'image/png';
					$ext = 'png';
					break;
				default :			
					echo '<script>alert(\'Wrong image file extension.\');</script>';
					echo "<script> window.history.go(-1); </script>";
					exit;
			}
			$img = createImage($ext,$tmpName);
			switch($img){// image verify
				case 1:
					echo '<script>alert(\'Wrong image file extension.\');</script>';
					echo "<script> window.history.go(-1); </script>";
					exit;
				case 2:
					echo '<script>alert(\'JPG/PNG Only\');</script>';				
					echo "<script> window.history.go(-1); </script>";					
					exit;		
				default:
					echo '...';
					if(isset($_POST['y2']) && !empty($_POST['y2'])){ ///Crop
						$croped_img = crop1($img, $_POST['x1'], $_POST['y1'], $_POST['x2'], $_POST['y2'], $_POST['scale']);
						if($croped_img){
							$img = $croped_img; 
						}
						else{
							echo '<script>alert(\'Can not crop this image.\');</script>';
							echo "<script> window.history.go(-1); </script>";
							exit;
						}
					}else{}
					if(isset($_POST['rotate']) && !empty($_POST['rotate'])){ 
						if($_POST['rotate'] === '0'){ ///Rotate
						}
						else{
							$rotated_img = rotate1($img, $_POST['rotate']);
							if($rotated_img){
								$img = $rotated_img; 
							}
							else{
								echo '<script>alert(\'Can not Rotate this image.\');</script>';
								echo "<script> window.history.go(-1); </script>";
								exit;
							}
						}
					}else{}
					$bigImage = calculateImage($img,625,'NO'); //Check if the image is larger then 625 pixels
					if ($bigImage === 2){
						imagejpeg($img,$dstDir.$saveName.'.jpg', 100);
						imagejpeg($img,$dstDir.$saveName.'.old', 100);
					}
					else{
						//echo $recalWidth.' '.$recalHeight;
						$newImg = imagecreatetruecolor($recalWidth,$recalHeight);
						imagecopyresampled($newImg,$img,0,0,0,0,$recalWidth,$recalHeight,$recalSize[0],$recalSize[1]);
						imagejpeg($newImg,$dstDir.$saveName.'.jpg', 100);
						imagejpeg($newImg,$dstDir.$saveName.'.old', 100);
						imagedestroy($newImg);
					}
					imagedestroy($img);
			}
		}
		else{
			forbid();
		}
	}
	//rotate image
	function rotate1($img, $degrees){
		$rotated_img = imagerotate($img, -$degrees, 0);
		return $rotated_img;
	}
	//crop image
	function crop1($img, $x1, $y1, $x2, $y2, $scale){
		$x1 = $x1 / $scale;
		$y1 = $y1 / $scale;
		$x2 = $x2 / $scale;
		$y2 = $y2 / $scale;
		floor($x1);//beware of image length overflow after scaling
		floor($y1);
		floor($x2);
		floor($y2);
		$w1 = abs($x2 - $x1);
		$h1 = abs($y2 - $y1);
		$croped_img = imagecreatetruecolor($w1, $h1);
		if($x1 < $x2 && $y1 < $y2){
			imagecopy($croped_img, $img, 0,0, $x1, $y1, $w1, $h1);
		}
		else if($x1 > $x2 && $y1 < $y2){
			imagecopy($croped_img, $img, 0,0, $x2, $y1, $w1, $h1);
		}
		else if($x1 < $x2 && $y1 > $y2){
			imagecopy($croped_img, $img, 0,0, $x1, $y2, $w1, $h1);
		}
		else if($x1 > $x2 && $y1 > $y2){
			imagecopy($croped_img, $img, 0,0, $x2, $y2, $w1, $h1);
		}
		else{
			return false;
		}
		return $croped_img;
	}
	//upload file
	function uploadFile2($inputName,$saveName,$dstDir){
		if(is_uploaded_file($_FILES[$inputName]['tmp_name'])){
			$tmp_name = $_FILES[$inputName]['tmp_name'];	
			$name = $_FILES[$inputName]['name'];	
			$orifilename = pathinfo($name);
			if(isset($orifilename['extension'])){
				$ext = $orifilename['extension'];
			}else{
				echo '<script>alert(\'File Upload Error11\');</script>';
				echo "<script> window.history.go(-1); </script>";
				exit;
			}
			$ext = strtolower($ext);
			switch($ext){
				case 'jpg':
				case 'jpeg':
				case 'png':
				case 'pdf':
				case 'doc':
				case 'docx':
				case 'xls':
				case 'xlsx':
				case 'ppt':
				case 'pptx':
					$saveName = $saveName.$ext;
					if(!move_uploaded_file($_FILES[$inputName]['tmp_name'], $dstDir.$saveName)){
						echo '<script>alert(\'File Upload Error1\');</script>';
						echo "<script> window.history.go(-1); </script>";
						exit;
					}
					else{}				
					break;
				default:
					echo "<script>alert('Can not upload {$ext}');</script>";
					echo "<script> window.history.go(-1); </script>";
					exit;
			}
		}
		else{
			echo '<script>alert(\'File Upload Error2\');</script>';
			echo "<script> window.history.go(-1); </script>";
			exit;
		}
	}
?>