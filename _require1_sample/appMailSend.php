<?php
// 210702
$curKey = hash('sha256', $_SERVER['SERVER_ADDR']);
if(isset($reqKey) && $reqKey === $curKey){
}else{
	echo 'forbidden';
	exit;
}
$yourDomainName = 'YOURDOMAINNAME include https or http'; // ex) https://www.domain.com
	
if(isset($_SESSION['currentAppStatus']) && $_SESSION['currentAppStatus'] === 'complete' ){
}else{
	echo 'Access Denied1';
	exit;
}
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
//echo 'mailstart';
	require __DIR__.'/src/Exception.php';
	require __DIR__.'/src/PHPMailer.php';
	require __DIR__.'/src/SMTP.php';
if(isset($_SESSION['currentAppMail']) && $_SESSION['currentAppMail'] ==='yes'){
	//echo 'nomail';
}else{
	//echo 'yesmail';

	$myIdAddress = $yourDomainName.'/std/app/myid_main.php';
	$email = decrypt1($result->email, $result->encId);
	switch($result->cardDesign->serialType){
		case 'ISIC':
			$charImage = $yourDomainName.'/std/img/mail/ISIC_characters_cards_isic.jpg';
			break;
		case 'ITIC':
			$charImage = $yourDomainName.'/std/img/mail/ISIC_characters_cards_itic.jpg';
			break;
		case 'IYTC':
			$charImage = $yourDomainName.'/std/img/mail/ISIC_characters_cards_iytc.jpg';
			break;
		default:
			echo 'Serial Type Error';
			exit;
	}
	$logoImage = $yourDomainName.'/std/img/mail/logoIcon.jpg';
	
	$bankAccount = $accountInfo1;
	$name1 = $engFname.' '.$engLname;
	$mailForISICTitle = 'Your online appplication has been successfully registered!';
	$mailForISIC = "
	<!doctype html>
	<html>
	<head>
	<meta charset=\"UTF-8\">
	<title>ISIC</title>
	</head>

	<body>
	<div style=\"width:750px; margin:0 auto; padding:0; border:2px solid #12b3ae; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:13px; color:#444;\">
	<p style=\"margin:0; padding:20px 5px; text-align:center;\"><img src=\"$logoImage\"></p>
	<div style=\"background:#12b3ae; padding:20px 30px; margin:0;\">
		<h3 style=\"font-size:120%; color:#fff; margin:0; padding:0; text-align:center;\">Your online appplication has been successfully registered!</h3>
	</div>

	<div style=\"margin:0; padding:20px 35px 80px 35px;\">     
			<img src=\"$charImage\" width=\"20%\" style=\"float: right; margin: 0 0 20px 20px;\">
		
			<h3 style=\"font-size:120%; margin:0; padding:0;\">Dear $name1,</h3>
			
			<p style=\"color:#999; font-size: 11px;\">
			Thank you for registering your online application to issue ISIC. <br>
			The information you have registered online is used to issue your card. <br>
			Benefits for card holders may not be available if the information on the issued card is incorrect. <br>
			Please visit MY ID and make sure that you have entered the correct information.<br>
			You can modify the registered information before the card is issued. <br>
			Please make the payment after confirming your information at MY ID. <br>
			After validating the payment in our account we will issue your card! 
			</p>
			<p style=\"color:#999; font-size: 11px;\">In order to issue your card, we ask you to choose one of the payment methods available (Multibanco and Credit Card) or, alternatively, to make a bank transfer to $bankAccount and send us the proof of it to pagamentos@isic.pt.</p>
			<p style=\"color:#999; font-size: 11px;\">After payment validation we will issue the card and send it according to delivery method chosen!</p>
		
		<p style=\"margin:0 auto; margin-top:40px; padding:10px; border-radius:5px; text-align:center; max-width:300px; background:#f0ad4e; font-size:120%;\"><a href=\"$myIdAddress\" target=\"_blank\" style=\"color:#fff; text-decoration:none;\">Ir para “O meu cartão” ▶<br><small style=\"color: #e8e8e8;\">(Go to &quot;My Card&quot; corner)</small></a></p>

		

	</div>

	<div style=\"background:#12b3ae; color:#fff; text-align:center; padding:25px; margin:0;\">
		<p style=\"margin:0; padding:0;\">(C) ISIC-$countryName[$countryId]</p>
	</div>

	</div>
	</body>
	</html>
	";
	$mailForITICTitle = 'Your online appplication has been successfully registered!';
	$mailForITIC ="
	<!doctype html>
	<html>
	<head>
	<meta charset=\"UTF-8\">
	<title>ISIC</title>
	</head>

	<body>
	<div style=\"width:750px; margin:0 auto; padding:0; border:2px solid #12b3ae; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:13px; color:#444;\">
	<p style=\"margin:0; padding:20px 5px; text-align:center;\"><img src=\"$logoImage\"></p>
	<div style=\"background:#12b3ae; padding:20px 30px; margin:0;\">
		<h3 style=\"font-size:120%; color:#fff; margin:0; padding:0; text-align:center;\">Your online appplication has been successfully registered!</h3>
	</div>

	<div style=\"margin:0; padding:20px 35px 80px 35px;\">     
			<img src=\"$charImage\" width=\"20%\" style=\"float: right; margin: 20px 0 20px 20px;\">   
			<h3 style=\"font-size:120%; margin:0; padding:0;\">Dear $name1,</h3>

			<p style=\"color:#999; font-size: 11px;\">
			Thank you for registering your online application to issue ITIC. <br>
			The information you have registered online is used to issue your ID card. <br>
			Benefits for ID holders may be unavailable if the information on the issued card is wrong. <br>
			Please visit MY ID and make sure that you have entered the correct information.<br>
			You can modify the registered information before the card is issued. <br>
			Please transfer the issuing fee after confirming your information at MY ID, and your card will be issued. 
			</p>
			<p style=\"color:#999; font-size: 11px;\">In order to issue your card, we ask you to choose one of the payment methods available (Multibanco and Credit Card) or, alternatively, to make a bank transfer to $bankAccount and send us the proof of it to pagamentos@isic.pt.</p>
			<p style=\"color:#999; font-size: 11px;\">After payment validation we will issue the card and send it according to delivery method chosen!</p>
			  
		<p style=\"margin:0 auto; margin-top:40px; padding:10px; border-radius:5px; text-align:center; max-width:300px; background:#f0ad4e; font-size:120%;\"><a href=\"$myIdAddress\" target=\"_blank\" style=\"color:#fff; text-decoration:none;\">Ir para “O meu cartão” ▶<br><small style=\"color: #e8e8e8;\">(Go to &quot;My Card&quot; corner)</small></a></p>

		

	</div>

	<div style=\"background:#12b3ae; color:#fff; text-align:center; padding:25px; margin:0;\">
		<p style=\"margin:0; padding:0;\">(C) ISIC-$countryName[$countryId]</p>
	</div>

	</div>
	</body>
	</html>
	";
	$mailForIYTCTitle = 'Your online appplication has been successfully registered!';
	$mailForIYTC ="
	<!doctype html>
	<html>
	<head>
	<meta charset=\"UTF-8\">
	<title>ISIC</title>
	</head>

	<body>
	<div style=\"width:750px; margin:0 auto; padding:0; border:2px solid #12b3ae; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:13px; color:#444;\">
	<p style=\"margin:0; padding:20px 5px; text-align:center;\"><img src=\"$logoImage\"></p>
	<div style=\"background:#12b3ae; padding:20px 30px; margin:0;\">
		<h3 style=\"font-size:120%; color:#fff; margin:0; padding:0; text-align:center;\">Your online appplication has been successfully registered!</h3>
	</div>

	<div style=\"margin:0; padding:20px 35px 80px 35px;\">        
			<img src=\"$charImage\" width=\"20%\" style=\"float: right; margin: 0 0 20px 20px;\">
		
			<h3 style=\"font-size:120%; margin:0; padding:0;\">Dear $name1,</h3>
			<p style=\"color:#999; font-size: 11px;\">
			Thank you for registering your online application to issue IYTC. <br>
			The information you have registered online is used to issue your card. <br>
			BBenefits for card holders may not be available if the information on the issued card is incorrect. <br>
			Please visit MY ID and make sure that you have entered the correct information.<br>
			You can modify the registered information before the card is issued. <br>
			Please make the payment after confirming your information at MY ID. <br>
			</p>
			<p style=\"color:#999; font-size: 11px;\">In order to issue your card, we ask you to choose one of the payment methods available (Multibanco and Credit Card) or, alternatively, to make a bank transfer to $bankAccount and send us the proof of it to pagamentos@isic.pt.</p>
			<p style=\"color:#999; font-size: 11px;\">After payment validation we will issue the card and send it according to delivery method chosen</p>
		
		<p style=\"margin:0 auto; margin-top:40px; padding:10px; border-radius:5px; text-align:center; max-width:300px; background:#f0ad4e; font-size:120%;\"><a href=\"$myIdAddress\" target=\"_blank\" style=\"color:#fff; text-decoration:none;\">Ir para “O meu cartão” ▶<br><small style=\"color: #e8e8e8;\">(Go to &quot;My Card&quot; corner)</small></a></p>

		

	</div>

	<div style=\"background:#12b3ae; color:#fff; text-align:center; padding:25px; margin:0;\">
		<p style=\"margin:0; padding:0;\">(C) ISIC-$countryName[$countryId]</p>
	</div>

	</div>
	</body>
	</html>
	";
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions


	try {
		//Server settings
		//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = '';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '';                 // SMTP username
		$mail->Password = '';                           // SMTP password
		$mail->CharSet= 'UTF-8';
	   // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 25;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('', '');
		//$mail->addAddress('.', 'Joe User');     // Add a recipient
		$mail->addAddress($email);               // Name is optional
		$mail->addReplyTo('', '');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');

		//Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		//Content
		$today = date('Y-m-d H:i:s');
		$mail->isHTML(true);                                  // Set email format to HTML
		switch($result->cardDesign->serialType){
			case 'ISIC':
				$mail->Subject =  $mailForISICTitle.$today;
				$mail->Body = $mailForISIC;
				break;
			case 'ITIC':
				$mail->Subject =  $mailForITICTitle.$today;
				$mail->Body = $mailForITIC;
				break;
			case 'IYTC':
				$mail->Subject = $mailForIYTCTitle.$today;
				$mail->Body = $mailForIYTC;
				break;
			default : 
				echo 'Serial Type Error';
				exit;
		}
		$mail->AltBody = 'Your online application has been successfully registered!';

		$mail->send();
		//echo 'Message has been sent';
		$_SESSION['currentAppMail'] = 'yes';
	} catch (Exception $e) {
		//echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
}
?>